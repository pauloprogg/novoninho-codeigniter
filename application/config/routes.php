<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	http://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There area two reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router what URI segments to use if those provided

| in the URL cannot be matched to a valid route.

|

*/



$route['default_controller'] = "home";

$route['404_override'] = '';



/* Paineis */



$route['painel-proprietario'] = 'painel_proprietario';

$route['painel-proprietario/imoveis'] = 'painel_proprietario';

$route['painel-proprietario/editarperfil'] = 'painel_proprietario/editarperfil';

$route['painel-proprietario/cadastrar'] = 'painel_proprietario/cadastrar';

$route['painel-proprietario/editar'] = 'painel_proprietario/editar';

$route['painel-proprietario/editar/(:any)'] = 'painel_proprietario/editar/$1';

$route['painel-proprietario/deletar'] = 'painel_proprietario/deletar';

$route['painel-proprietario/deletar/(:any)'] = 'painel_proprietario/deletar/$1';

$route['painel-proprietario/publicar'] = 'painel_proprietario/publicar';

$route['painel-proprietario/sair'] = 'painel_proprietario/sair';

$route['painel-proprietario/minhalista'] = 'painel_proprietario/minhalista';



// $route['painel-proprietario/salvarDadosPublicar'] = 'painel_proprietario/salvarDadosPublicar';

// $route['painel-proprietario/cep'] = 'painel_proprietario/cep';

// $route['painel-proprietario/editImovel'] = 'painel_proprietario/editImovel';

// $route['painel-proprietario/editImovelBloqueado'] = 'painel_proprietario/editImovelBloqueado';



$route['painel-corretor'] = 'painel_corretor';

$route['painel-corretor/imoveis'] = 'painel_corretor';

$route['painel-corretor/editarperfil'] = 'painel_corretor/editarperfil';

$route['painel-corretor/cadastrar'] = 'painel_corretor/cadastrar';

$route['painel-corretor/editar'] = 'painel_corretor/editar';

$route['painel-corretor/editar/(:any)'] = 'painel_corretor/editar/$1';

$route['painel-corretor/publicar'] = 'painel_corretor/publicar';

$route['painel-corretor/sair'] = 'painel_corretor/sair';

$route['painel-corretor/minhalista'] = 'painel_corretor/minhalista';



$route['painel-imobiliaria'] = 'painel_imobiliaria';

$route['painel-imobiliaria/imoveis'] = 'painel_imobiliaria';

$route['painel-imobiliaria/imoveis/(:any)'] = 'painel_imobiliaria/index/$1';

$route['painel-imobiliaria/editarperfil'] = 'painel_imobiliaria/editarperfil';

$route['painel-imobiliaria/cadastrar'] = 'painel_imobiliaria/cadastrar';

$route['painel-imobiliaria/editar'] = 'painel_imobiliaria/editar';

$route['painel-imobiliaria/editar/(:any)'] = 'painel_imobiliaria/editar/$1';

$route['painel-imobiliaria/sair'] = 'painel_imobiliaria/sair';

$route['painel-imobiliaria/minhalista'] = 'painel_imobiliaria/minhalista';



$route['painel-imobiliaria/xml'] = 'painel_imobiliaria/xml';

$route['painel-imobiliaria/doOcultar'] = 'painel_imobiliaria/doOcultar';

$route['painel-imobiliaria/doDestaque'] = 'painel_imobiliaria/doDestaque';

$route['painel-imobiliaria/doSuperDestaque'] = 'painel_imobiliaria/doSuperDestaque';

$route['painel-imobiliaria/doFeirao'] = 'painel_imobiliaria/doFeirao';

$route['painel-imobiliaria/doDestaqueFeirao'] = 'painel_imobiliaria/doDestaqueFeirao';

$route['painel-imobiliaria/doSuperDestaqueFeirao'] = 'painel_imobiliaria/doSuperDestaqueFeirao';

$route['painel-imobiliaria/importacaoImoveisCron'] = 'painel_imobiliaria/importacaoImoveisCron';



/* ------- */



/* Noticias */

$route['noticia/(:any)'] = 'noticias/show/$1';



/* Imovel */ 

$route['imovel/(:any)'] = 'imovel/show/$1';

$route['imovel/adicionarFavorito'] = 'imovel/adicionarFavorito';

$route['imovel/removerFavorito'] = 'imovel/removerFavorito';



# descomente as linhas abaixo quando o site for multilingue

// $route['(\w{2})/(.*)'] = '$2';

// $route['(\w{2})'] = $route['default_controller'];





/* End of file routes.php */

/* Location: ./application/config/routes.php */