<!-- Conteudos especificos separados por sessoes -->

<div class="row empreendimentos">

	<div class="columns small-12 medium-9 large-9">
	
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<div class="titulo_azul barra-azul">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">Empreendimentos</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				
				<label class="texto_padrao">
				
				{texto}
				
				</label>
				
			</div>
			
			{itens}
			
				<div class="columns small-12 medium-12 large-12" title="{titulo}">
					<a href="{link}"><img src="{base_url}assets/uploads/banner_empreendimento/{imagem}"></a>
				</div>
			
			{/itens}
			
		</div>
	
	</div>
	
	<div class="columns small-12 medium-3 large-3">
		
		{anuncios}
		
	</div>
	
</div>

<!-- Fim dos conteudos especificos -->