<!-- Conteudos especificos separados por sessoes -->

<div class="row contato">

	<div class="columns small-12 medium-9 large-9">
	
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<div class="titulo_azul barra-azul">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">Contato</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				
				<label class="texto_padrao">
				
				{texto1}
				
				</label>
				
				<div class="row">
				
					<div class="columns small-12 medium-7 large-7">
						<div class="formulario_contato">
							
							<form id="formulario_contato" action="{base_url}contato/send" method="post">
								
								<div class="linha">
									<label class="campo"> Nome: </label> 
									<input type="text" name="nome" id="nome">
								</div>
								
								<div class="linha">
									<label class="campo"> E-mail: </label>
									<input type="text" name="email" id="email">
								</div>
								
								<div class="linha">
									<label class="campo"> Assunto: </label>
									<input type="text" name="assunto" id="assunto">
								</div>
								<div class="linha">
									<label class="campo"> N° Imóvel: </label>
									<input type="text" name="nImovel" id="nImovel" style="width: 90px;float: left;">
								</div>
								
								<div class="linha">
									<label class="campo"> Mensagem: </label>
									<textarea name="mensagem" id="mensagem"></textarea>
								</div>
								
								<input type="image" class="enviar" src="{base_url}assets/images/btn_enviar_azul.png">
								
							</form>
							
						</div>
					</div>
					
					<div class="columns small-12 medium-5 large-5">
						<div class="texto">
							
							<label class="texto_padrao">
							
								{texto2}
								
							</label>
							
						</div>
					</div>
					
				</div>
				
				</label>
				
			</div>
		</div>
	
	</div>
	
	<div class="columns small-12 medium-3 large-3">
		
		{anuncios}
		
	</div>
	
</div>

{noticias}

<!-- Fim dos conteudos especificos -->