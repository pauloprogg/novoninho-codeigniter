<!-- Conteudos especificos separados por sessoes -->

<div class="imovel">

	<div class="row">

		<div class="columns small-12 medium-9 large-9">
		
			<div class="row">
				<div class="columns small-12 medium-12 large-12">
				
					<div class="titulo_vermelho">
					
						<div class="titulo_conteudo">
							<h2 class="titulo_box">Residencial // Venda</h2>
						</div>
						
						<div class="botao_voltar">
							<a href=""><img src="{base_url}assets/images/btn_voltar.png"></a>
						</div>
						
					</div>
					
				</div>
			</div>
			
			<div class="row">
			
				<div class="columns small-12 medium-12 large-12">
					
					<h2 class="preto">APARTAMENTO PADRÃO - JD PRESTES DE BARROS, SOROCABA - SP </h2>
					
					<label class="texto_padrao">
					
					<p>Nam pharetra feugiat nunc, faucibus semper augue porta vitae. Nulla vitae dapibus velit. Proin rhoncus dui et nisi tincidunt, ut hendrerit mi cursus. Etiam auctor elementum elementum. Nulla aliquet felis vel euismod congue. Curabitur a dolor vel purus convallis viverra. Vivamus gravida purus ut libero ornare, in scelerisque eros rutrum.</p>
					
					</label>
					
					<div class="row">
					
						<div class="columns small-8 medium-8 large-8">
						
							<div class="galeria">
							
								<div class="imagem_principal">
									<img src="{base_url}assets/uploads/imovel/foto.jpg">
								</div>
								
								<div class="fotos">
									<a class="galeria_foto">
										<img src="{base_url}assets/uploads/imovel/foto.jpg">
									</a>
									<a class="galeria_foto">
										<img src="{base_url}assets/uploads/imovel/foto2.jpg">
									</a>
									<a class="galeria_foto">
										<img src="{base_url}assets/uploads/imovel/foto3.jpg">
									</a>	
									<a class="galeria_foto">
										<img src="{base_url}assets/uploads/imovel/foto.jpg">
									</a>	
									<a class="galeria_foto">
										<img src="{base_url}assets/uploads/imovel/foto3.jpg">
									</a>
								</div>
							
							</div>
						
						</div>
						
						<div class="columns small-4 medium-4 large-4">
						
							<div class="dados">
							
								<div class="preco">
									<span>Valor:</span>
									<h2>R$ 208.480,00</h2>
								</div>
								
								<div class="informacoes">
									<p><b>Área útil:</b> 65 m2</p>
									<p><b>Valor m2:</b> R$ 3.207,00</p>
									<p><b>Dormitórios:</b> 2 quartos/dts</p>
									<p><b>Suítes:</b> 1 suite</p>
									<p><b>Andares:</b> 8 andares</p>
								</div>
								
								<div class="redes">
									<img src="{base_url}assets/images/imovel_facebook.jpg">
									<img src="{base_url}assets/images/imovel_twitter.jpg">
									<img src="{base_url}assets/images/imovel_google.jpg">
								</div>
								
								<div class="texto">
					
									<p>Nam pharetra feugiat nunc, faucibus semper augue porta vitae. Nulla vitae dapibus velit. Proin rhoncus dui et nisi tincidunt, ut hendrerit mi cursus. Etiam auctor elementum elementum. Nulla aliquet felis vel euismod congue. Curabitur a dolor vel purus.</p>
					
								</div>
							
							</div>
						
						</div>
					
					</div>
					
					<h2 class="preto2"> CONTATO </h2>
					
					<div class="row">
					
						<div class="columns small-12 medium-12 large-12">
							
							<div class="contato">
								
								<div class="row">
									
									<div class="columns small-6 medium-6 large-6">
										
										<p><b>Nome: </b> Marcus Felet </p>
										<p><b>E-mail: </b> marcus@sorocabacom.com.br </p>
										<p><b>Fone: </b> 15 3013 5056 / 99151 4356 </p>
										<p><b>Melhor Horario para Ligar: </b> 19h </p>
										
										<p class="texto">Nam pharetra feugiat nunc, faucibus semper augue porta vitae. Nulla vitae dapibus velit. Proin rhoncus dui et nisi tincidunt, ut hendrerit mi cursus. Etiam auctor elementum elementum. Nulla aliquet felis vel euismod congue. Curabitur a dolor vel purus.</p>
									
									</div>
									
									<div class="columns small-6 medium-6 large-6">
									
										<div class="formulario">
											<form id="contato_imovel">
											
												<label>Nome:</label>
												<input class="campo" type="text" name="nome" id="nome">
												
												<label>Email:</label>
												<input class="campo" type="text" name="email" id="email">
												
												<label>Mensagem:</label>
												<textarea class="campo" name="mensagem" id="mensagem"></textarea>
												
												<input class="botao" type="image" src="{base_url}assets/images/btn_enviar_azul.png">
												
											</form>
										</div>
									
									</div>
									
								</div>
								
							</div>
							
						</div>
					
					</div>
					
				</div>
				
			</div>
			
		</div>
		
		<div class="columns small-12 medium-3 large-3">
			
			{anuncios}
			
		</div>
		
	</div>
	
	<div class="row">
		
		<h2 class="preto2" style="padding-left: 20px;"> LOCALIZAÇÃO </h2>
		
		<div class="columns small-12 medium-12 large-12">
			
			<div class="mapa">
			
				<iframe width="970" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=18095-550&key=AIzaSyBEKFcYT22HGZOWyb2eNpykzJVCIgkf2uE"></iframe>
			
			</div>
			
		</div>
		
	</div>
	
</div>

<!-- Fim dos conteudos especificos -->