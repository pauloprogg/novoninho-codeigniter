<!-- Conteudos especificos separados por sessoes -->

<div class="row painel imobiliaria_editardados">

	<form id="imobiliaria_editarperfil" Action="{base_url}painel_imobiliaria/editPerfil" method="post">

	<div class="columns small-12 medium-12 large-12">

		<?php if($dados_usuario[0]->tipo_xml == 1) { ?>

			<h2 class="titulo_azul">Importacação automática</h2>
			<p>Abaixo você pode configurar se pretende gerar a importação de imóveis automaticamente. <br>
				Se optar por ativar, uma vez por dia é gerado o codigo automatico da importação, baseado no link colocado abaixo: </p>

			<label>Ativar importação automática</label>
			<label><input class="campo" type="radio" name="importacao_automatica" id="importacao_automatica" value="0" <?php if($dados_usuario[0]->importacao_automatica == 0) { echo 'checked'; } ?>> Não</label>
			<label><input class="campo" type="radio" name="importacao_automatica" id="importacao_automatica" value="1" <?php if($dados_usuario[0]->importacao_automatica == 1) { echo 'checked'; } ?>> Sim</label>
			<br>

			<label>Link da Importação:</label>
			<input class="campo" type="text" name="link_ivalue" id="link_ivalue" value="<?php echo $dados_usuario[0]->link_ivalue; ?>">

		<?php } ?>
	
		<h2 class="titulo_azul">Alterar meus Dados</h2>
		
		<div class="row">
		
			<div class="columns small-12 medium-12 large-12">
			{dados}
					<input type="hidden" name="id" id="id" value="{id}">
					
					<div class="row linha">
				
						<div class="columns small-12 medium-8 large-8">
							
							<label>Imobiliária:</label>
							<input class="campo" type="text" name="imobiliaria" id="imobiliaria" value="{nome}">
							
						</div>
						
						<div class="columns small-12 medium-4 large-4">
							
							<label>CRECI:</label>
							<input class="campo" type="text" name="creci" id="creci" value="{creci}">
							
						</div>
						
					</div>
					
					<div class="row linha">
					
						<div class="columns small-12 medium-12 large-12">
							
							<label>Responsável:</label>
							<input class="campo" type="text" name="responsavel" id="responsavel" value="{responsavel}">
							
						</div>
						
					</div>
					
					<div class="row linha">
				
						<div class="columns small-12 medium-10 large-10">
							
							<label>Endereco:</label>
							<input class="campo" type="text" name="endereco" id="endereco" value="{endereco}">
							
						</div>
						
						<div class="columns small-12 medium-2 large-2">
							
							<label>Nº:</label>
							<input class="campo" type="text" name="numero" id="numero" value="{numero}">
							
						</div>
						
					</div>
					
					<div class="row linha">
				
						<div class="columns small-12 medium-5 large-5">
							
							<label>Complemento:</label>
							<input class="campo" type="text" name="complemento" id="complemento" value="{complemento}">
							
						</div>
						
						<div class="columns small-12 medium-5 large-5">
							
							<label>Cidade:</label>
							<input class="campo" type="text" name="cidade" id="cidade" value="{cidade}">
							
						</div>
						
						<div class="columns small-12 medium-2 large-2">
							
							<label>Estado:</label>
							<input class="campo" type="text" name="estado" id="estado" value="{estado}">
							
						</div>
						
					</div>
					
					<div class="row linha">
				
						<div class="columns small-12 medium-6 large-6">
							
							<label>Telefone:</label>
							<input class="campo telefone" type="text" name="telefone" id="telefone" value="{telefone}">
							
						</div>
						
						<div class="columns small-12 medium-6 large-6">
							
							<label>Celular:</label>
							<input class="campo celular" type="text" name="celular" id="celular" value="{celular}">
							
						</div>
						
					</div>
					
					<div class="row linha">
				
						<div class="columns small-12 medium-6 large-6">
							
							<label>E-mail:(Este será seu Usuário / Login) </label>
							<input class="campo" type="text" name="email" id="email" value="{email}">
							
						</div>
					</div>
					<div class="row linha">
				
						<div class="columns small-12 medium-6 large-6">
							{selectResp}
						</div>

						<div class="columns small-12 medium-6 large-6">
							{selectVendedor}
						</div>
						
					</div>
					
					<div class="row linha">
				
						<div class="columns small-6 medium-6 large-6">
							
							<label>CPF / CNPJ:(Apenas para emissão de nota fiscal)</label>
							<input class="campo {classe_mascara}" type="text" name="cpf_cnpj" id="cpf_cnpj" value="{cpf_cnpj}">
							
						</div>
						
						<div class="columns small-6 medium-6 large-6">
						
							<div class="row">
								<div class="columns small-6 medium-6 large-6">
									<br/>
									<label><input type="radio" name="check_cpf_cnpj" value="1" class="check_mascara" {check_cpf}> CPF</label>
								</div>
								<div class="columns small-6 medium-6 large-6">
									<br/>
									<label><input type="radio" name="check_cpf_cnpj" value="2" class="check_mascara" {check_cnpj}> CNPJ</label>
								</div>
							</div>
							
						</div>
						
					</div>
					
					<div class="row linha">
				
						<div class="columns small-12 medium-12 large-12">
							
							<label>Melhor Horário para Ligar:</label>
							<input class="campo" type="text" name="melhor_horario" id="melhor_horario" value="{melhor_horario}">
							
						</div>
						
					</div>
					
					<div class="row linha">
				
						<div class="columns small-12 medium-6 large-6">
							
							<label>Senha:</label>
							<input class="campo" type="password" name="senha" id="senha">
							
						</div>
						
						<div class="columns small-12 medium-6 large-6">
							
							<label>Confirmar Senha:</label>
							<input class="campo" type="password" name="confirmar_senha" id="confirmar_senha">
							
						</div>
						
					</div>
					<div class="row linha contrato">
				
						<div class="columns small-12 medium-12 large-12">
							<label>
								<input type="checkbox" name="contrato" id="contrato" value="1" required>
								<span>Li e concordo com os <a href="#" data-reveal-id="modalTermos" style="font-weight: bold">Termos do Contrato</a></span>
							</label>
						</div>
						
					</div>

					<div class="row linha contrato">
				
						<div class="columns small-12 medium-12 large-12">
							{newsletter}
						</div>
						
					</div>
					
					<div class="row linha">
					
						<div class="small-12 large-12 medium-12 columns">
						
							<input type="submit" class="btn_azul" value="Salvar">
						
						</div>
						
					</div>
					
				</form>
				
			{/dados}
			</div>
			
		</div>
		
	</div>
	
</div>

<!-- Fim dos conteudos especificos -->