<!-- Conteudos especificos separados por sessoes -->

{mensagem_erro}

{estrutura}

<div id="modalEscolherXML" class="reveal-modal" data-reveal>
	<h2>Importação de Imóveis por XML.</h2>


	<label>Obs: Se no xml o imóvel tiver mais de 10 imagens cadastradas, serão apenas importadas as 10 primeiras.</label>
	<label>Obs2: O tamanho máximo do arquivo tem que ser de 5MB.</label>
	<br/>
	
	{estrutura_xml}
	
	<div class="row">
    	<div class="small-12 form_result" style="text-align: center; display: none"></div>
     </div>
	
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>

</div>

<!-- Fim dos conteudos especificos -->