	
<label>O xml deve conter imóveis de apenas uma cidade, selecione o estado e a cidade dos imóveis e faça upload do xml.</label>
<br/>

<form id="form_xml" action="{base_url}xml/xmlVivareal" method="POST" enctype="multipart/form-data">

	<div class="row">

		<div class="columns small-12 medium-6 large-1 left">
			<span class="direita">Estado:</span> 
		</div>
		
		<div class="columns small-12 medium-6 large-3 left">
			{select_estado}
		</div>
		
		<div class="columns small-12 medium-6 large-1 left">
			<span class="direita">Cidade:</span> 
		</div>
		
		<div class="columns small-12 medium-6 large-3 left">
			{select_cidade}
		</div>
		
	</div>
	
	<div class="row">

		<div class="columns small-12">
		
			<a class="botao_azul" id="carrega_xml">Selecionar Arquivo</a>	

			<input type="file" class="file_xml" name="xml" id="xml" accept=".xml" style="display: none"> <span class="nome_arquivo"></span>
			
			<!-- onchange="this.form.submit()" -->

		</div>

	</div>
	
	<div class="row submit_xml" style="display: none">

		<div class="columns small-12">
			
			<label>Quanto mais imóveis, mais o sistema ira demorar para importar o arquivo.</label>
		
			<input type="submit" class="btn_azul" value="Importar XML" style="float: left">

		</div>
	
	</div>
	
</form>