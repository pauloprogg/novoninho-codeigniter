<div class="locacao">
	<div class="row">
		<div class="columns small-12 medium-12 large-12">
			<div class="titulo_azul barra-azul">
				<div class="titulo_conteudo">
					<h2 class="titulo_box">Locação</h2>
					<div class="botoes">
						<a class="prev_azul" id="prev_locacao"></a>
						<a class="pause_azul" id="pause_locacao"></a>
						<a class="next_azul" id="next_locacao"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	<div class="row">
		<div class="columns large-12 lista_venda">
			<ul id="carrousel_locacao">
				
				<!--
				<li>
					<div class="locacao_item">
						
						<div class="imagem">
							<a href="{base_url}imovel">
								<img class="imovel" src="{base_url}assets/images/imovel.png">
								<img class="status" src="{base_url}assets/images/status_locacao.png">
							</a>
						</div>
						
						<div class="dados">
							<a href="{base_url}imovel">
								<h2>CADA JD. RODRIGO</h2>
							</a>
							<p><b>Tipo:</b> Case</p>
							<p><b>Local:</b> Jd. Rodrigo</p>
							<p><b>Valor:</b> R$ 269.000,00 </p>
						</div>
					
					</div>
				</li>
				-->
				
				{lista_locacao}
				
			</ul>
		</div>
	</div>
</div>