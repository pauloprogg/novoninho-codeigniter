	{termos}
	{footer}

		<!-- Footer -->
		<footer>
			<div class="row">
			
				<div class="columns small-12 medium-2 large-2">
					<ul class="lista_footer">
						<li>
							<a href="{base_url}home/">Home</a>
						</li>
						<li>
							<a href="{base_url}classificados/">Classificados</a>
						</li>
						<li>
							<a href="{base_url}noticias/">Noticias</a>
						</li>
						<li>
							<a href="{base_url}anuncieaqui/">Como Anunciar?</a>
						</li>
						<li>
							<a href="#" data-reveal-id="modalTermos">Termos / Políticas</a>
						</li>
						<li>
							<a href="{base_url}contato/">Contato</a>
						</li>
					</ul>
				</div>
				<div class="columns small-12 large-offset-2 medium-offset-2 medium-3 large-3">
					<div class="footer_facebook">
						<iframe src="//www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/Portal-Imobili%C3%A1rio-NovoNinho-921222324623435?fref=ts&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=504745702895923" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:100%;" allowTransparency="true"></iframe>
					</div>
				</div>
				<div class="columns small-12 large-offset-1 medium-offset-1 medium-4 large-4">
					<a href="{base_url}home" class="logo-footer"><img src="{base_url}assets/images/logo.jpg" alt="Novo Ninho"></a>
					<img src="{base_url}assets/images/bandeiras.jpg" class="small-12 medium-12 large-10 bandeiras-footer" alt="Formas de Pagamento, Bandeiras Cielo e PagSeguro">
				</div>
			</div>
		</footer>

		<?php
		// <div class="direitos-footer">
		// 	<div class="row">
		// 		<div class="columns small-6 medium-4 large-3">
		// 			<p>&copy; 2014 | Todos os direitos reservados.</p>
		// 		</div>
		// 		<div class="columns small-6 medium-1 large-1">
		// 			<a href="http://fazul.com.br" target="_blank" class="right">
		// 				<img src="{base_url}assets/images/fazul.png" alt="Fazul Agência digital"/>
		// 			</a>
		// 		</div>
		// 	</div>
		// </div>
		?>
		<!-- Fim do Footer -->

		</div> <!-- .wrap -->
		<!-- Fim do Conteiner Geral -->

		<!-- Scripts -->
		<!--[if lt IE 9]>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
			<script type="text/javascript" src="{base_url}assets/js/lib/rem.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
			<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
			<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
		<!--<![endif]-->
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
		
		<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/foundation/foundation.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/foundation/foundation.reveal.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/foundation/foundation.tooltip.js"></script>
        
        <script type="text/javascript" src="{base_url}assets/js/lib/foundation/foundation.equalizer.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/uploadify/jquery.uploadify.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/jquery.uploadifive.min.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/jquery.validate.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/jquery.maskedinput.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/jquery.maskMoney.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/gmaps.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/jquery.carouFredSel.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/tabcontent.js"></script>

		<script type="text/javascript" src="{base_url}assets/js/html2canvas.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/jquery.html2canvas.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/download.js"></script>

		<script type="text/javascript" src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
		
		<script type="text/javascript" src="{base_url}assets/js/lib/jquery.form.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/general.js"></script>

		{js}
		<script type="text/javascript" src="{src}"></script>
		{/js}

		{js_inline}
		<script type="text/javascript">{script}</script>
		{/js_inline}
		<!-- Fim dos Scripts -->

	{/footer}

	<!-- Google Analytics -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-81354437-1', 'auto');
	  ga('send', 'pageview');

	</script>

	</body>
</html>