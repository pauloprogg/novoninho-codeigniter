{estilo}
<form class="busca_avancada" action="{base_url}classificados" method="get">
	<input type="hidden" name="usar_cep" id="usar_cep" value="{usar_cep}" />
	<div class="row linha">
		<div class="columns small-12 medium-4 large-4">
			<div class="row">
				{radio_tipo}
			</div>
		</div>
		<div class="columns small-12 medium-4 large-4">
			<div class="row">
				<div class="columns small-12 medium-4 large-4">
					<span class="direita">Tipo:</span>
				</div>
				<div class="columns small-12 medium-8 large-8">
					{select_tipo_imovel}
				</div>	
			</div>	
		</div>			
		<div class="columns small-12 medium-4 large-4">
			<div class="row">
				<div class="columns small-12 medium-4 large-4">
					<span class="direita">Finalidade:</span>
				</div>
				<div class="columns small-12 medium-8 large-8">
					{select_finalidade}
				</div>	
			</div>		
		</div>	
	</div>	
	<div class="row separador">	
		<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
			<img src="{base_url}assets/images/separador.jpg">
		</div>
	</div>	
	<div class="row linha">	
		<div class="columns small-12 medium-4 large-4">	
			<div class="row">
				<div class="columns small-12 medium-4 large-4">	
					<span class="direita">Dormitórios:</span> 
				</div>			
				<div class="columns small-12 medium-4 large-4">			
					<select name="dorm_min" id="dorm_min" class="t2">
						<option value="">Mínimo</option>
							{select_dormitorios_min}
					</select>			
				</div>				
				<div class="columns small-12 medium-4 large-4">
					<select name="dorm_max" id="dorm_max" class="t2">
					<option value="">Máximo</option>
						{select_dormitorios_max}
					</select>
				</div>
			</div>	
		</div>			
		<div class="columns small-12 medium-4 large-4">				
			<div class="row">						
				<div class="columns small-12 medium-4 large-4">		
					<span class="direita">Suítes:</span> 	
				</div>				
				<div class="columns small-12 medium-4 large-4">
					<select name="suite_min" id="suite_min" class="t2">	
						<option value="">Mínimo</option>
						{select_suites_min}					
					</select>	
				</div>						
				<div class="columns small-12 medium-4 large-4">
					<select name="suite_max" id="suite_max" class="t2">
						<option value="">Máximo</option>
						{select_suites_max}
					</select>
				</div>	
			</div>
		</div>	
		<div class="columns small-12 medium-4 large-4">	
			<div class="row">								
				<div class="columns small-12 medium-4 large-4">		
					<span class="direita">Garagem:</span> 		
				</div>						
				<div class="columns small-12 medium-4 large-4">		
					<select name="gar_min" id="gar_min" class="t2">					
						<option value="">Mínimo</option>	
						{select_garagem_min}					
					</select>		
				</div>	
				<div class="columns small-12 medium-4 large-4">
					<select name="gar_max" id="gar_max" class="t2">
						<option value="">Máximo</option>	
						{select_garagem_max}	
					</select>
				</div>		
			</div>		
		</div>	
	</div>	
	<div class="row separador">	
		<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">	
			<img src="{base_url}assets/images/separador.jpg">		
		</div>
	</div>		
			<div class="row linha">		
				<div class="columns small-12 medium-4 large-4">		
					<div class="row">
						<div class="columns small-12 medium-4 large-4">	
							<span class="direita">Valor:</span>	
						</div>							
						<div class="columns small-12 medium-4 large-4">	
							<input type="text" name="val_min" id="val_min" class="t2 moeda" placeholder="Mínimo" value="{valor_minimo}">		
						</div>	
						<div class="columns small-12 medium-4 large-4">
							<input type="text" name="val_max" id="val_max" class="t2 moeda" placeholder="Máximo" value="{valor_maximo}">		
						</div>		
					</div>		
				</div>	
				<div class="columns small-12 medium-4 large-4">	
					<div class="row">					
						<div class="columns small-12 medium-4 large-4">		
							<span class="direita">Falar com:</span> 
						</div>				
							{check_falarcom}
					</div>	
				</div>				
				<div class="columns small-12 medium-4 large-4">		
					<div class="row">				
						<div class="columns small-12 medium-4 large-4">	
							<span class="direita">Imobiliária:</span> 	
						</div>						
						<div class="columns small-12 medium-8 large-8">	
							<select name="id_imobiliaria" id="id_imobiliaria" class="t1">	
								<option value="">Todas</option>
								{select_imobiliaria}
							</select>				
						</div>	
					</div>		
				</div>	
			</div>
			<div class="row separador">	
				<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">	
					<img src="{base_url}assets/images/separador.jpg">		
				</div>
			</div>
			<div class="row linha">
				<div class="columns small-12 medium-4 large-4">			
			<div class="row">
				<div class="columns small-12 medium-4 large-4">			
					<span class="direita">Feirão?</span>
				</div>
				<div class="columns small-12 medium-8 large-8">
					{select_feirao}						
				</div>
			</div>	
		</div>  
			</div>	
			<div class="row separador">	
				<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">		
					<img src="{base_url}assets/images/separador.jpg">	
				</div>
			</div>	
			<div class="row linha">		
				<div class="columns small-12 medium-4 large-4">	
					<div class="row">	
						<div class="columns small-12 medium-4 large-4">	
							<span class="direita">Condomínio:</span>
						</div>
							{check_condominio}
					</div>		
				</div>		 
				<!-- Parte da cidade e estado para uso sem cep -->	
			<div class="columns small-12 medium-4 large-4 semcep">			
				<div class="row">				
					<div class="columns small-12 medium-4 large-4">		
						<span class="direita">Estado:</span> 			
					</div>					
					<div class="columns small-12 medium-8 large-8">
						{select_estado}		
					</div>	
				</div>	
			</div>	
			<div class="columns small-12 medium-4 large-4 semcep">			
				<div class="row">
					<div class="columns small-12 medium-4 large-4">			
						<span class="direita">Cidade:</span>
					</div>
					<div class="columns small-12 medium-8 large-8">
						{select_cidade}					
						<label>
							<a class="limpar_select">Limpar Todos</a>
						</label>	
					</div>
				</div>	
			</div>            
			<!-- Parte da cidade e estado para uso com cep -->       
			<div class="columns small-12 medium-4 large-4 comcep">			
				<div class="row">		
					<div class="columns small-12 medium-4 large-4">				
						<span class="direita">Estado:</span> 
					</div>								
					<div class="columns small-12 medium-8 large-8">                
						<input type="hidden" name="id_estado" id="id_estado" value="{id_estado}">
						<input type="text" name="cep_estado" id="cep_estado" class="t2 estado" value="{cep_estado}" readonly>
					</div>			
				</div>	
			</div>		
			<div class="columns small-12 medium-4 large-4 comcep">			
				<div class="row">		 			
					<div class="columns small-12 medium-4 large-4">		
						<span class="direita">Cidade:</span> 	
					</div>	
					<div class="columns small-12 medium-8 large-8">           
						<input type="hidden" name="id_cidade" id="id_cidade" value="{id_cidade}">				
						<input type="text" name="cep_cidade" id="cep_cidade" class="t2 cidade" value="{cep_cidade}" readonly>	
					</div>	
				</div>
			</div>

		</div>
		<div class="row separador">	
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">		
				<img src="{base_url}assets/images/separador.jpg">		
			</div>	
		</div>       
		<div class="row">
			<div class="columns small-12 medium-12 large-12 text-center">     
			   	<a class="ativar_cep botao_azul"><span class="cep_texto">{texto_cep}</span></a>     
			</div>      
		</div>    
		<div class="row separador">	
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">	
				<img src="{base_url}assets/images/separador.jpg">		
			</div>	
		</div>	
		<!-- Parte da pesquisa de bairros, sem usar o CEP -->	
		<div class="row semcep lista_bairros">	
			<div class="columns small-12 medium-12 large-12">		
				<span> Bairro </span>					
				<div class="bairros">	
					<ul class="small-block-grid-4 medium-block-grid-4 large-block-grid-4">				
						<li>
							<input type="checkbox" class="check" id="bairro" name="bairro[]" value="">Todos os Bairros
						</li>	
							{estrutura_bairros}						
					</ul>
				</div>		
			</div>	
		</div>
		<!-- =========================================== -->
		<!-- Parte da pesquisa por cep, sem usar o bairro, cidade e estado -->    
		<div class="row linha comcep">	
			<div class="columns small-12 medium-5">		
				<div class="row">
					<div class="columns small-12 medium-3 large-3">	
						<span class="direita">CEP:</span>
					</div>
					<div class="columns small-12 medium-6 large-6">
						<div class="row collapse">			
							<div class="small-10 columns">	
								<input type="text" name="cep" id="cep" class="t2 cep" value="{cep}">	
							</div>
							<div class="small-2 columns">		
								<button type="button" class="button postfix go-cep">Ok</button>				
							</div>		
						</div>			
					</div>				
					<div class="columns small-12 medium-3 large-3 small-only-text-center">
						<a href="http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCep.cfm" target="_blank" title="Não sabe seu CEP? Procure no site do Correios!"><img src="{base_url}assets/images/correios.png" style="width: 80px;">
						</a>
					</div>   
				</div>					
				<div class="row">					
					<div class="columns small-12" style="margin-left: 60px">
						<label class="text-right">Consulte CEP pelo endereço</label> 		
					</div>   
				</div>	
			</div>			
			<div class="columns small-12 medium-7">			
				<div class="row">	
					<div class="columns small-12 medium-2 large-2">
						<span class="direita">Bairro:</span> 	
					</div>	
					<div class="columns small-12 medium-10 large-10">              
				  		<input type="hidden" name="id_bairro" id="id_bairro" value="{id_bairro}">				
						<input type="text" name="cep_bairro" id="cep_bairro" class="t2 bairro" value="{cep_bairro}" readonly>		
					</div>
				</div>	
			</div>
		</div>  
		<div class="row separador comcep">		
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">	
				<img src="{base_url}assets/images/separador.jpg">		
			</div>
		</div>  
		<div class="row linha comcep">   
		   	<div class="columns small-12 medium-12 large-12">					
				<div class="row">		
					<div class="columns small-12 medium-2 large-2">	
						<span class="direita">Endereço:</span> 	
					</div>
					<div class="columns small-12 medium-10 large-10">  
				        <input type="hidden" name="id_rua" id="id_rua" value="{id_rua}">				
						<input type="text" name="cep_rua" id="cep_rua" class="t2 endereco" value="{cep_rua}" readonly>				
					</div>					
				</div>		
			</div>     
		</div> 
		<!-- =========================================== -->        
		<div class="row separador">		
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">				
			</div>
		</div>		
		<div class="row">      
			<div class="columns small-12 medium-6 large-6">				
				<div class="row">                       
					<div class="columns small-12 medium-6 large-6">			
						<span class="direita">Pesquisar pelo código do imóvel:</span>			
					</div>							
					<div class="columns small-12 medium-6 large-6">				
						<input type="text" name="numero" id="numero" value="{numero}">		
					</div>						
				</div>			
			</div>       
		</div>	
		<div class="row botao">		
			<div class="columns small-12 medium-12 large-12">	
				<input type="image" src="{base_url}assets/images/btn_buscar_azul.png">			
			</div>	
		</div>
</form>