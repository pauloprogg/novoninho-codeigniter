<div class="columns small-12 medium-9 large-9">
		
	<div class="{class_tipo}">
	
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<div class="{cor_tipo} {barra_tipo}">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">{texto_tipo}</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<ul class="small-block-grid-3 medium-block-grid-3 large-block-grid-3">
					{lista_pesquisa}
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="columns small-12 medium-12 large-12 text-center">
				{paginacao}
			</div>
		</div>
		
	</div>
		
</div>
