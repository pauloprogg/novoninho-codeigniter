

<nav class="menu_principal">

	<div class="row">
		<div class="large-12 columns">
			<ul class="menu_header hide-for-small-only">
			{menu}
				<li>
					<a class="{class_active}" href="{href}">{label}</a><span class="{last_span}"> / </span>
					{submenu}
				</li>
				<li class="divider"></li>
			{/menu}
			</ul>
            
            <ul class="menu_mobile show-for-small-only">
            	<label class="open_menu">MENU</label>
			{menu_mobile}
				<li>
					<a class="" href="{href}">{label}</a>
				</li>
			{/menu_mobile}
			</ul>
            
		</div>
	</div>
	
</nav>