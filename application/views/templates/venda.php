<div class="venda">
	<div class="row">
		<div class="columns small-12 medium-12 large-12">
			<div class="titulo_vermelho barra-vermelho">
				<div class="titulo_conteudo">
					<h2 class="titulo_box">Venda</h2>
					<div class="botoes">
						<a class="prev_vermelho" id="prev_venda"></a>
						<a class="pause_vermelho" id="pause_venda"></a>
						<a class="next_vermelho" id="next_venda"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	<div class="row">
		<div class="columns large-12 lista_venda">
			<ul id="carrousel_venda">
				
				<!--
				<li>
					<div class="venda_item">
						
						<div class="imagem">
							<a href="{base_url}imovel">
								<img class="imovel" src="{base_url}assets/images/imovel.png">
								<img class="status" src="{base_url}assets/images/status_venda.png">
							</a>
						</div>
						
						<div class="dados">
							<a href="{base_url}imovel">
								<h2>CADA JD. RODRIGO</h2>
							</a>
							<p><b>Tipo:</b> Case</p>
							<p><b>Local:</b> Jd. Rodrigo</p>
							<p><b>Valor:</b> R$ 269.000,00 </p>
						</div>
					
					</div>
				</li>
				-->
				
				{lista_vendas}
				
			</ul>
		</div>
	</div>
</div>