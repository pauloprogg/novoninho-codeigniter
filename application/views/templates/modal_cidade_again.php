<a href="#" id="abrirModalCidadesAgain" data-reveal-id="modalCidades" style="display: none"></a>
			
			<div id="modalCidades" class="reveal-modal" data-options="close_on_background_click:false" data-reveal>
				<form action="{base_url}login/trocarCidade" id="trocarCidade" method="post">
					<div class="row">
					
						<div class="columns small-12 medium-12 large-12 text-center">
						
							<h2>Selecione sua localização abaixo!</h2>
						
						</div>
						
						<div class="columns small-12 medium-6 large-6 text-center">
						
							<img src="{base_url}assets/images/logo_modal_cidades.png">
						
						</div>
						
						<div class="columns small-12 medium-6 large-6 text-left">
							
							<div class="row">
							
								<div class="columns small-12 medium-12 large-6">
									<label>Escolha um Estado:</label>
								</div>
								
								<div class="columns small-12 medium-12 large-6">
									{select_estado}
								</div>
							
							</div>
							
							<div class="row modal_cidade">
							
								<div class="columns small-12 medium-12 large-6">
									<label>Escolha uma Cidade:</label>
								</div>
								
								<div class="columns small-12 medium-12 large-6">
									{select_cidade}
								</div>
							
							</div>

							

							<?php 
								if($select_feirao){
							?>
							<div class="row modal_cidade">
								
								<div class="columns small-12 medium-12 large-6">
									<label style="margin-left: -50px;"><img src="{base_url}assets/images/expo_casa.png" style="max-width:50px;">Quer visitar integra expo?</label>
								</div>
								
								<div class="columns small-12 medium-12 large-6">
									{select_feirao}
								</div>
							
							</div>
						<?php
							}
						?>
							
							<div class="row modal_cidade">
							
								<div class="columns small-12 medium-12 large-12">
									<input type="submit" class="btn_azul" value="Entrar">
								</div>
							
							</div>
						
						</div>
					
					<a class="close-reveal-modal">&#215;</a>
				</form>
			</div>