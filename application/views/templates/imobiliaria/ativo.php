<!-- Conteudos especificos separados por sessoes -->



<div class="row painel proprietario_imoveis imobiliaria">



	<div class="columns small-12 medium-12 large-12">

		

		<h2 class="titulo_azul">Imóveis</h2>

		

		<div class="row">

		

			<div class="columns small-12 medium-4 large-4">

			

				<a href="{base_url}painel-imobiliaria/cadastrar" class="botao_azul">Cadastrar Imóvel</a>

				{botao_importar}

				

			</div>

			

			<div class="columns small-12 medium-2 large-2">

			

				<label class="legenda_texto">Legenda:</label>

				

				<div class="legenda_publicado">

					<div></div>

					<label>Publicado</label>

				</div> 

				

				<div class="legenda_oculto">

					<div></div>

					<label>Oculto</label>

				</div> 

				

				

			

			</div>

		

		</div>

		<div class="row">
		<div class="columns small-12 medium-12 large-12">

			<form action="{base_url}{pagina}" method="get">
				

				<div class="columns medium-3 large-3">
					<select name="feirao" id="slcOrdenacao" />
						<option value="">Feirão</option>
						<option value="1">Sim</option>
						<option value="0">Não</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="destaque_feirao" id="slcOrdenacao" />
						<option value="">Destaque Feirão</option>
						<option value="1">Sim</option>
						<option value="0">Não</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="super_destaque_feirao" id="slcOrdenacao" />
						<option value="">Super Destaque Feirão</option>
						<option value="1">Sim</option>
						<option value="0">Não</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="finalidade" />
						<option value="">Finalidade</option>
						<option value="1">Comercial</option>
						<option value="2">Residencial</option>
						<option value="3">Industrial</option>
						<option value="4">Rural</option>
						<option value="5">Lazer</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="tipo_imovel" id="slcOrdenacao" />
						<option value="">Tipo Imovel</option>
						<option value="1">Apartamento</option>
						<option value="2">Apartamento Duplex</option>
						<option value="3">Área</option>
						<option value="4">Barracão</option>
						<option value="5">Casa</option>
						<option value="6">Chácara</option>
						<option value="7">Cobertura</option>
						<option value="8">Fazenda</option>
						<option value="9">Galpão</option>
						<option value="10">Kitnet</option>
						<option value="11">Pousada</option>
						<option value="12">Prédio</option>
						<option value="13">Rancho</option>
						<option value="14">Sala</option>
						<option value="15">Salão</option>
						<option value="16">Sítio</option>
						<option value="17">Sobrado</option>
						<option value="18">Terreno</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="destaque" id="slcOrdenacao" />
						<option value="">Destaque</option>
						<option value="1">Sim</option>
						<option value="0">Não</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="super_destaque" id="slcOrdenacao" />
						<option value="">Super Destaque</option>
						<option value="1">Sim</option>
						<option value="0">Não</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="tipo" id="slcOrdenacao" />
						<option value="">Procura</option>
						<option value="1">Venda</option>
						<option value="2">Locação</option>
						<option value="3">Temporada</option>
					</select>
				</div>
				
				<div class="columns medium-3 large-3">
					<select name="dormitorios" id="slcOrdenacao" />
						<option value="">Dormitorios</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
				</div>
				
				<div class="columns medium-3 large-3">
					<select name="valor" />
						<option value="">Valor</option>
						<option value="100000.00">Mais de R$ 100 mil</option>
						<option value="200000.00">Mais de R$ 200 mil</option>
						<option value="300000.00">Mais de R$ 300 mil</option>
						<option value="400000.00">Mais de R$ 400 mil</option>
						<option value="500000.00">Mais de R$ 500 mil</option>
					</select>
				</div>

				<div class="columns medium-3 large-3">
					<input type="text" name="id" id="id" placeholder="Número de Referência Novoninho">
				</div>

				<div class="columns medium-3 large-3">
					<input type="text" name="referencia_xml" id="referencia_xml" placeholder="Número de Referência Proprietario">
				</div>

				
				
				
				<div class="columns medium-12 large-12 text-right">
					<div class="row">
						<input type="submit" value="buscar" class="button tiny" />
					</div>
				</div>
				
			</form>
		</div>
		</div>

		

	</div>

	

	<div class="columns small-12 medium-12 large-12">

	

		<div class="row">

		

			<div class="row header_lista">



				<h6><b>{diasFaltam}</b></h6>

		

				<div class="columns small-7 medium-7 large-7">

					<label>Imóvel </label>

				</div>

				

				<div class="columns small-5 medium-5 large-5">

					

					<label style="text-align: center">Ações</label>

					

				</div>

				

			</div>

			

			{lista_imoveis}

			

		</div>

		

		<div class="row">

		

			<div class="columns small-12 medium-12 large-12 paginacao">

				

				<div class="paginacao">



					<?php for($x = 1; $x <= $pages; $x++) { 

						$active = '';

						if($x == $this->uri->segment(3)) {

							$active = 'active';

						}

						?>

						<div class="pag-item">

							<a href="{base_url}painel-imobiliaria/imoveis/<?php echo $x; ?>" class="<?php echo $active; ?>"><?php echo $x; ?></a>

						</div>

					<?php } ?>

				</div>

				

			</div>

			

		</div>

	</div>

	

</div>



<!-- Fim dos conteudos especificos -->