<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6 no-js" lang="pt-br"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7 no-js" lang="pt-br"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8 no-js" lang="pt-br"><![endif]-->
<!--[if IE 9 ]><html class="ie ie9 no-js" lang="pt-br"><![endif]-->
<!--[if gt IE 9 ]>
<!-->
<html class="no-js" lang="pt-br">
<!-->
<![endif]--><head>
	
	{head}
	
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compative" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="robots" content="index, follow">

		<title>{title} | {site_name}</title>

		<meta name="description" content="{description}">
		<meta name="keywords" content="{keywords}">
		<meta name="author" content="{author}">

		<!-- Open Graph Meta Tags INÍCIO -->
		<meta property="og:locale" content="pt_BR">
		<meta property="og:url" content="{base_url}">
		<meta property="og:title" content="{title}">
		<meta property="og:site_name" content="{site_name}">
		<meta property="og:description" content="{description}">
		<meta property="og:image" content="{base_url}{image}">
		<meta property="og:type" content="website">
		<!-- Open Graph Meta Tags FIM -->

		<link rel="stylesheet" type="text/css" href="{base_url}assets/css/lib/normalize.css">
		<link rel="stylesheet" type="text/css" href="{base_url}assets/css/lib/foundation.css">
		<link rel="stylesheet" type="text/css" href="{base_url}assets/css/lib/tabcontent.css">
		<link rel="stylesheet" type="text/css" href="{base_url}assets/css/style.css">
	
		


		<link rel="stylesheet" type="text/css" href="{base_url}assets/js/lib/uploadify/uploadify.css">
		<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

		{css}
		<link rel="stylesheet" type="text/css" href="{href}">
		{/css}

		<script type="text/javascript">window.PATH = "{base_url}";</script>
		
		<script type="text/javascript" src="{base_url}assets/js/lib/vendor/modernizr.js"></script>

		<!--[if lt IE 10]>
			<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.js"></script>
		<![endif] -->

		<!-- Favicons -->
		<link rel="shortcut icon" href="{base_url}assets/images/favicon.png">
		<link rel="apple-touch-icon" href="{base_url}assets/images/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="{base_url}assets/images/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="{base_url}assets/images/apple-touch-icon-114x114.png">

	{/head}
	</head>