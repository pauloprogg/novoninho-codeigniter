<div class="temporada">
	<div class="row">
		<div class="columns small-12 medium-12 large-12">
			<div class="titulo_cinza barra-cinza">
				<div class="titulo_conteudo">
					<h2 class="titulo_box">Temporada</h2>
					<div class="botoes">
						<a class="prev_cinza" id="prev_temporada"></a>
						<a class="pause_cinza" id="pause_temporada"></a>
						<a class="next_cinza" id="next_temporada"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	<div class="row">
		<div class="columns large-12 lista_venda">
			<ul id="carrousel_temporada">
			
				<!--
				<li>
					<div class="temporada_item">
						
						<div class="imagem">
							<a href="{base_url}imovel">
								<img class="imovel" src="{base_url}assets/images/imovel.png">
								<img class="status" src="{base_url}assets/images/status_temporada.png">
							</a>
						</div>
						
						<div class="dados">
							<a href="{base_url}imovel">
								<h2>CADA JD. RODRIGO</h2>
							</a>
							<p><b>Tipo:</b> Case</p>
							<p><b>Local:</b> Jd. Rodrigo</p>
							<p><b>Valor:</b> R$ 269.000,00 </p>
						</div>
					
					</div>
				</li>
				-->
				
				{lista_temporada}
				
			</ul>
		</div>
	</div>
</div>