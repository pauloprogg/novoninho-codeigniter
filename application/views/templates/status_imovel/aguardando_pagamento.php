<div class="linha">

	<div class="columns small-1 medium-1 large-1 centro">
		
		<input type="checkbox" class="selecao" value="{id}">
		
	</div>
	
	<div class="columns small-2 medium-2 large-2">
		
		<span>{titulo}</span>
		
	</div>
	
	<div class="columns small-2 medium-2 large-2">
		
		<span class="aguardandopagamento">Aguardando Pagamento</span>
		
	</div>
	
	<div class="columns small-1 medium-1 large-1 acoes">
		
		<a href="{base_url}{caminho}/editar/{id}" alt="Editar Imóvel" title="Editar Imóvel"><img src="{base_url}assets/images/editar.png"></a>
		<a id="{id}" class="deletar_imovel" caminho="{caminho}" alt="Deletar Imóvel" title="Deletar Imóvel"><img src="{base_url}assets/images/deletar.png"></a>
		
	</div>
	<div class="columns small-6 medium-6 large-6">
		<fieldset class="normal">
			<legend>Feirão</legend>
		
		<div class="row">
			<div class="columns small-2 medium-3 large-3 ">
				<input type="checkbox" id = "feirao" class="feirao" value="1">
				<span>Feirão</span>
			</div>
			<div class="columns small-2 medium-3 large-3 ">
				<input type="checkbox"  id="destaque_feirao" class="destaque_feirao" value="1" disabled>
				<span>Destaque </span>
			</div>
			<div class="columns small-2 medium-3 large-5 ">
				<input type="checkbox" id="super_destaque_feirao" class="super_destaque_feirao" value="1" disabled>
				<span>Super Destaque </span>
			</div>
		</div>
		</fieldset>
		<fieldset class="normal">
			<legend>Normal</legend>
		<div class="row">
			<div class="columns small-3 medium-3 large-3">	
				<input type="checkbox" class="feiraox" value="0" disabled="" checked="">
				<span>Normal</span>
			</div>
			<div class="columns small-3 medium-3 large-3 destaque">
				<input type="checkbox" class="destaque" value="1">
				<span>Destaque</span>
			</div>
			<div class="columns small-3 medium-3 large-5 super_destaque">
				<input type="checkbox" class="super_destaque" value="1">
				<span>Super Destaque</span>
			</div>
		</div>
	</fieldset>
	</div>
		
</div>