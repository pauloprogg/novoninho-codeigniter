<div class="linha">
	<div class="columns small-1 centro">
		&nbsp;
	</div>
	
	<div class="columns small-5 medium-5">
		
		<span>{titulo}</span>
		
	</div>
	
	<div class="columns small-5 medium-3">
		
		<span class="aguardandoaprovacao" style="color: green">Pago / Recusado</span>
	</div>
	
	<div class="columns small-12 medium-3 acoes">
		<span data-tooltip aria-haspopup="true" class="has-tip" title="{msg}">?</span>
		
		<a href="{base_url}{caminho}/editar/{id}" alt="Editar Imóvel" title="Editar Imóvel"><img src="{base_url}assets/images/editar.png"></a>

	</div>

</div>