﻿<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" style="border-bottom: solid 13px #0092A1;padding-bottom: 60px;">
	<tr>
		<td style="text-align:center;">
			<a href="#"><img href="#" src="http://novoninho.com.br/assets/images/logoMail.png" style="border-botton:solid 1px #cccccc;"></a>
		</td>
	</tr>
	<tr>
		<td>
			<p style="color:#0092A1;font-size: 18px;"><strong>Novo Contato NovoNinho.com.br</strong></p>
			<p style="font-family:Arial, Helvetica, sans-serif; font-size: 15px; color:#000;">Mensagem: Um usu&aacute;rio est&aacute; interessado no seu im&oacute;vel: <strong>{numero_referencia}</strong></p>
		</td>
	</tr>
	<tr>
		<td style="padding: 10px; background-color: #F1F1F1;">
			<p style="font-family:Arial, Helvetica, sans-serif; font-size: 15px; color:#000; padding:5px; margin:0;"> 
				<strong>Nome: </strong>{nome}
			</p>
			<p style="font-family:Arial, Helvetica, sans-serif; font-size: 15px; color:#000; padding:5px; margin:0;">
				<strong>Email: </strong>{email}
			</p>
			<p style="font-family:Arial, Helvetica, sans-serif; font-size: 15px; color:#000; padding:5px; margin:0;">
				<strong>Mensagem: </strong>{mensagem}
			</p>
		</td>
	</tr>
	<tr>
		<td>
			<p style="color:#0092A1;font-size: 18px;"><strong>Informa&ccedil;&otilde;es do im&oacute;vel</strong></p>
		</td>
	</tr>
	<tr >
		<td>
			<table>
				<tr>
					<td>
						<a href="#"><img src="{imagem_principal}" width="300"></a>
					</td>
					<td style="padding-left: 20px;">
					{dados}	
						<p style="margin: 2px;font-size: 18px;"><strong>{titulo}</strong></p>	
						<p style="margin: 2px;"><b>Valor: </b>R$ {valor_br}</p>
                    	<p style="margin: 2px;"><b>Nº: </b>#{id}</p>
						<p style="margin: 2px;"><b>Finalidade: </b> {finalidade_texto}</p>
						<p style="margin: 2px;"><b>Nº Referência: </b> {numero_referencia}</p>
						<p style="margin: 2px;"><b>Área útil (m2): </b> {area}</p>
						<p style="margin: 2px;"><b>Valor m2: </b> R$ {valor_m2}</p>
						<p style="margin: 2px;"><b>Dormitórios: </b> {dormitorios} quarto(s)</p>
						<p style="margin: 2px;"><b>Suítes: </b> {suites} suite(s)</p>
						<p style="margin: 2px;"><b>Garagem: </b> {garagem} lugare(s)</p>
                        <p style="margin: 2px;"><b>Banheiros: </b> {banheiros}</p>
						<p style="margin: 2px;"><b>Condomínio: </b> {condominio}</p>
					{/dados}	
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
