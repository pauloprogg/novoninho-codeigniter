<body>

	<!-- Conteiner Geral -->
	<div class="wrap">
	{header}

		<!-- Header -->
		<header>
		
			<div id="overlay">
				<div id="overlay_load">Carregando</div>
			</div>
			
			<div class="login">
				<div class="row" data-equalizer>
                	
                    <div class="small-12 medium-6 large-6 columns selecionarCidade" data-equalizer-watch>
						<div class="trocar_cidade" style="margin-top: -25px; top: 100%; position: relative">
                        	<span class="alterarLocalizacao"><a href="#" id="abrirModalCidadesAgain" data-reveal-id="modalCidades">Alterar sua Localização</a></span>
                        </div>
					</div>
                    
                    <div class="small-12 medium-6 large-6 columns" data-equalizer-watch>
						{texto_header}
					</div>
                   
				</div>
			</div>

			<div class="head_banner">
				<div class="row header_2">

					<div class="columns small-12 medium-4 large-4 small-text-center">
						<a href="{base_url}home"><img src="{base_url}assets/images/logo.jpg"></a>
					</div>

					<div class="columns small-12 medium-8 large-8 small-text-center">
						<div class="banner_header">

							<ul id="carrousel_banner_logo">

								{banners}
									<li>
										<a href="{link}" target="_blank"><img src="{base_url}assets/uploads/banner_topo/{imagem}"></a>
									</li>
								{/banners}

							</ul>

						</div>
					</div>

				</div>
			</div>

			<!-- Nav -->
				{menu}
			<!-- Fim do Nav -->

			{bannerheader}
			
			<!-- Login e Formulário de Cadastro -->

			<div id="modalCadastro" class="reveal-modal" data-reveal>
				
				<h2>ESQUECI MINHA SENHA</h2>
				
				<form id="esqueci_senha" action="{base_url}login/esqueciSenha" method="post" >
				
					<label>Preencha o email cadastrado abaixo</label>
					<label>Email:</label>
					<input type="text" name="email" id="email" class="campo">
					
					<input type="submit" class="btn" value="Enviar">
					
				</form>
					
				<a class="close-reveal-modal">&#215;</a>
			</div>
			
			<!-- Formulario e Login de usuários comuns -->
			<div id="modalCadastroUsuario" class="reveal-modal" data-reveal>
				
				<div class="row">
				
					<div class="small-12 columns">
						<h2>Login</h2>
					</div>
				
					<div class="small-12 columns">
						<label class="titulo">Ja é cadastrado?</label>
					</div>
				
					<div class="small-12 columns">
						<form id="formulario_login_favorito" action="{base_url}login/logar" method="post" >
						
							<label>Email:</label>
							<input type="text" name="login_email" class="campo" placeholder="Usuário/Login">
							
							<label>Senha:</label>
							<input type="password" name="login_senha" class="campo" placeholder="senha">
							
							<input type="submit" class="btn" value="Entrar">
							
						</form>
					</div>
				
					<div class="small-12 columns">
						<label class="titulo">Ainda não é cadastrado? <a class="viewFormUsuario"><u>Clique Aqui</u></a></label>
					</div>
				
				</div>
				
				<div class="row cadastroUsuario">
				
					<div class="small-12 columns">
						<h2>Cadastro</h2>
					</div>
				
					<div class="small-12 columns">
						<form id="cadastro_usuario_comum" action="{base_url}cadastrar/cadastrar_proprietario" method="post" >
							<input type="hidden" name="anunciante" value="0">
						
							<label>Nome:</label>
							<input type="text" name="nome" class="campo">
							
							<label>Email: (Este será seu Usuário/Login)</label>
							<input type="text" name="email" class="campo">
							
							<label>Telefone:</label>
							<input type="text" name="telefone" class="campo telefone">
							
							<label>Senha:</label>
							<input type="password" name="senha" id="senha_usuario" class="campo">
							
							<label>Confirmar Senha:</label>
							<input type="password" name="confirmar_senha" class="campo">
							
							<input type="submit" class="btn" value="Cadastrar">
							
						</form>
					</div>
				
				</div>
				
				<a class="close-reveal-modal">&#215;</a>
			</div>
			
			{modalCidade}
            
            {modalCidadeAgain}
			<!-- <a href="#" id="abrirModalCidades" data-reveal-id="modalCidades" style="display: none"></a> -->
			
		</header>
		<!-- Fim do Header -->
	{/header}