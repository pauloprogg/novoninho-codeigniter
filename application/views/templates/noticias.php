<div class="noticias">

	<div class="row">
		<div class="columns small-12 medium-12 large-12">
			<div class="titulo_azul barra-azul">
				<div class="titulo_conteudo">
					<h2 class="titulo_box">Notícias</h2>
				</div>
			</div>
		</div>
	</div>
		
	<div class="row">
		<div class="columns large-12 small-12 medium-12">
			<ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-2 lista_noticias">
				
				{dados}
					<li>
						<div class="noticia_item">
							<div class="row">
								<div class="columns large-5 small-12 medium-5">
									<div class="imagem">
										<img src="{base_url}assets/uploads/noticias/thumb/{imagem}">
									</div>
								</div>
								<div class="columns large-7 small-12 medium-7">
									<div class="dados">
										<h2>{titulo}</h2>
										<p>{resumo}</p>
										<a href="{base_url}noticia/{slug}">Saiba mais</a>
									</div>
								</div>
						</div>
					</li>
				{/dados}
				
			</ul>
		</div>
	</div>
</div>