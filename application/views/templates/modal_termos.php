<!-- Modal de Termos -->

	<div id="modalTermos" class="reveal-modal" data-reveal>

		<div class="row">

			<div class="columns small-12 medium-12 large-12">

				<h2>{titulo}</h2>

				<label class="texto_padrao">

					{texto}

				</label>

			</div>

		</div>
        
		<a class="close-reveal-modal">&#215;</a>

	</div>
