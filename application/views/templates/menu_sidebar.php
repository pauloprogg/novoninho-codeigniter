{menu}
	<li class="item-menu-sidebar {class_active}">
		<a href="{href}">{label}</a>
		{submenu}
	</li>
{/menu}