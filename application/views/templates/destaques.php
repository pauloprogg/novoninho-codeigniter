<div class="destaques">

	<div class="row">
		<div class="columns small-12 medium-12 large-12">
			<div class="titulo_azul barra-azul">
				<div class="titulo_conteudo">
					<h2 class="titulo_box">Super Destaques</h2>
					<div class="botoes">
						<a class="prev_azul" id="prev_destaque"></a>
						<a class="pause_azul" id="pause_destaque"></a>
						<a class="next_azul" id="next_destaque"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="columns large-12 lista_detalhes">
			<ul id="carrousel_destaque">
						
				{lista_destaques}
				
			</ul>
		</div>
	</div>

</div>