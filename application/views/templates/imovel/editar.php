{msg}
{dados}
	<form id="{form}" class="formulario_imovel" action="{base_url}painel/{envio}" method="post" caminho="{caminho}">
		
		<input type="hidden" id="id_imovel" name="id_imovel" value="{id}">
		<input type="hidden" id="id_usuario" name="id_usuario" value="{id_usuario}">
		
		<div class="row linha">
		
			<div class="columns small-1 medium-1 large-1">
				<span>Titulo:</span>
			</div>
			
			<div class="columns small-11 medium-11 large-11">
				<input type="text" name="titulo" id="titulo" class="campo" {bloqueado} value="{titulo}">
			</div>
		
		</div>
		
		<div class="row separador">
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
				<img src="{base_url}assets/images/separador.jpg">
			</div>
		</div>
		
		<div class="row linha">
		
			<div class="columns small-12 medium-12 large-12">
				<span>Descrição do imóvel:</span>
				<span style="float: right">800 caracteres</span>
				<textarea id="descricao1" name="descricao1" maxlength="800" {bloqueadoarea}>{descricao1}</textarea>
			</div>
		
		</div>
		
		<div class="row separador">

			<div class="small-12 large-10 medium-10 large-centered small-centered medium-centered columns">
	
				<img src="{base_url}assets/images/separador.jpg">
	
			</div>
	
		</div>
		
		<div class="row linha">
	
			<div class="columns small-12 medium-2 large-2">
	
				<span>Número de Referência:</span>
	
			</div>
	
			<div class="columns small-12 medium-4 large-4 left">
	
				<input type="text" name="numero_referencia" id="numero_referencia" class="campo" value="{referencia_xml}">
	
			</div>
	
		</div>

		<div class="row separador">
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
				<img src="{base_url}assets/images/separador.jpg">
			</div>
		</div>
				
		<div class="row linha">
		
			<div class="columns small-12 medium-4 large-4 coluna">
			
				{radio_tipo}
			
			</div>
			
			<div class="columns small-4 medium-4 large-4 coluna">
			
				<div class="row">
			
					<div class="columns small-3 medium-3 large-3">
						<span class="direita">Tipo:</span> 
					</div>
					
					<div class="columns small-9 medium-9 large-9">
						{select_tipo_imovel}
					</div>
					
				</div>
			
			</div>
			
			<div class="columns small-4 medium-4 large-4">
			
				<div class="row">
			
					<div class="columns small-3 medium-3 large-3">
						<span class="direita">Finalidade:</span> 
					</div>
					
					<div class="columns small-9 medium-9 large-9">
						{select_finalidade}
					</div>
					
				</div>
			
			</div>

		</div>
		
		<div class="row separador">
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
				<img src="{base_url}assets/images/separador.jpg">
			</div>
		</div>
		
		<div class="row linha">
		
			<div class="columns small-3 medium-3 large-3">
			
				<div class="row">
				
					<div class="columns small-6 medium-6 large-6">
						<span class="direita">Dormitórios:</span> 
					</div>
				
					<div class="columns small-6 medium-6 large-6">
						{select_dormitorios}
					</div>
					
				</div>
			
			</div>
			
			<div class="columns small-3 medium-3 large-3">
			
				<div class="row">
				
					<div class="columns small-6 medium-6 large-6">
						<span class="direita">Suites:</span> 
					</div>
				
					<div class="columns small-6 medium-6 large-6">
						{select_suites}
					</div>
					
				</div>
			
			</div>
			
			<div class="columns small-3 medium-3 large-3">
			
				<div class="row">
				
					<div class="columns small-6 medium-6 large-6">
						<span class="direita">Garagem:</span> 
					</div>
				
					<div class="columns small-6 medium-6 large-6">
						{select_garagem}
					</div>
					
				</div>
			
			</div>
			
			<div class="columns small-3 medium-3 large-3">
			
				<div class="row">
				
					<div class="columns small-6 medium-6 large-6">
						<span class="direita">Banheiros:</span> 
					</div>
				
					<div class="columns small-6 medium-6 large-6">
						{select_banheiros}
					</div>
					
				</div>
			
			</div>

		</div>
        
        <div class="row separador">
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
				<img src="{base_url}assets/images/separador.jpg">
			</div>
		</div>
        
        <div class="row linha">
    
            <div class="columns small-12 medium-4 large-4">
            
                <div class="row">
                
                    <div class="columns small-6 medium-6 large-6">
						<span class="direita">Área útil (m2):</span> 
					</div>
				
					<div class="columns small-6 medium-6 large-6">
						<input type="text" name="area_m2" id="area_m2" class="campo moeda" {bloqueado} value="{area_m2}">
					</div>
                    
                </div>
            
            </div>
            
            <div class="columns small-12 medium-4 large-4">
            
                <div class="row mini_titulo">
                
                    <div class="columns small-12 medium-12 large-12">
                        <span class="direita">Aparecer endereço na página do Anúncio?</span> 
                    </div>
                
                </div>
            
                <div class="row">
                    
                   {radio_mapa}
                    
                </div>
            
            </div>
        
        </div>
		
		<div class="row separador">
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
				<img src="{base_url}assets/images/separador.jpg">
			</div>
		</div>
		
		<div class="row linha">
		
			<div class="columns small-12 medium-3 large-3">
			
				<div class="row">
				
					<div class="columns small-3 medium-3 large-3">
						<span class="direita">CEP:</span> 
					</div>
				
					<div class="columns small-9 medium-9large-9">
						<input type="text" name="cep" id="cep" class="campo cep" {bloqueado} value="{cep}">
					</div>
					
				</div>
			
			</div>
			
			<div class="columns small-12 medium-4 large-4">
			
				<div class="row">
				
					<div class="columns small-2 medium-2 large-2">
						<span class="direita">Rua:</span> 
					</div>
				
					<div class="columns small-10 medium-10 large-10">
						<input type="hidden" name="id_rua" id="id_rua" value="{rua}">
						<input type="text" name="rua" id="rua" class="campo endereco" value="{texto_rua}" {bloqueado}>
					</div>
					
				</div>
			
			</div>
			
			<div class="columns small-12 medium-2 large-2">
			
				<div class="row">
				
					<div class="columns small-2 medium-2 large-2">
						<span class="direita">Nº:</span> 
					</div>
				
					<div class="columns small-10 medium-10 large-10">
						<input type="text" name="numero" id="numero" class="campo" {bloqueado} value="{numero}">
					</div>
					
				</div>
			
			</div>
			
			<div class="columns small-12 medium-3 large-3">
			
				<div class="row">
				
					<div class="columns small-4 medium-4 large-4">
						<span class="direita">Complem.</span> 
					</div>
				
					<div class="columns small-8 medium-8 large-8">
						<input type="text" name="complemento" id="complemento" class="campo" {bloqueado} value="{complemento}">
					</div>
					
				</div>
			
			</div>

		</div>
		
		<div class="row separador">
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
				<img src="{base_url}assets/images/separador.jpg">
			</div>
		</div>
		
		<div class="row linha">
		
			<div class="columns small-12 medium-4 large-4">
			
				<div class="row">
				
					<div class="columns small-3 medium-3 large-3">
						<span class="direita">Bairro:</span> 
					</div>
				
					<div class="columns small-9 medium-9 large-9">	
						<input type="hidden" name="id_bairro" id="id_bairro" value="{bairro}">
						<input type="text" name="bairro" id="bairro" class="campo bairro" value="{texto_bairro}" {bloqueado}>
					</div>
					
				</div>
			
			</div>
			
			<div class="columns small-12 medium-4 large-4">
			
				<div class="row">
				
					<div class="columns small-3 medium-3 large-3">
						<span class="direita">Cidade:</span> 
					</div>
				
					<div class="columns small-9 medium-9 large-9">
						<input type="hidden" name="id_cidade" id="id_cidade" value="{cidade}">
						<input type="text" name="cidade" id="cidade" class="campo cidade" value="{texto_cidade}" {bloqueado}>
					</div>
					
				</div>
			
			</div>
			
			<div class="columns small-12 medium-2 large-2">
			
				<div class="row">
				
					<div class="columns small-6 medium-6 large-6">
						<span class="direita">Estado:</span> 
					</div>
				
					<div class="columns small-6 medium-6 large-6">
						<input type="hidden" name="id_estado" id="id_estado" value="{estado}">
						<input type="text" name="estado" id="estado" class="campo estado" value="{texto_estado}" {bloqueado}>
					</div>
					
				</div>
			
			</div>
			
			<div class="columns small-12 medium-2 large-2">
			
				<div class="row mini_titulo">
				
					<div class="columns small-12 medium-12 large-12">
						<span class="direita">Condomínio:</span> 
					</div>
				
				</div>
			
				<div class="row">
					
					{radio_condominio}
					
				</div>
			
			</div>

		</div>
		
		<div class="row separador">
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
				<img src="{base_url}assets/images/separador.jpg">
			</div>
		</div>
		
		<div class="row linha">
		
			<div class="columns small-12 medium-3 large-3">
			
				<div class="row">
				
					<div class="columns small-3 medium-3 large-3">
						<span class="direita"><strong>Valor:</strong></span> 
					</div>
				
					<div class="columns small-9 medium-9 large-9">
						<input type="text" name="valor" id="valor" class="campo moeda" {bloqueado} value="{valor_imovel}">
					</div>
					
				</div>
			
			</div>

			<div class="columns small-12 medium-3 large-3">
				<div class="row mini_titulo">
					<div class="columns small-12 medium-12 large-12">
						<span class="direita ">Usar dados de contato do Cadastro?</span> 
					</div>
				</div>

				<div class="row">
					{dadosCadastrosOption}
				</div>
			</div>
			<div class='dados_contato' style="display:block;">
				<div class="columns small-12 medium-3 large-3">
				
					<div class="row">
					
						<div class="columns small-4 medium-4 large-4">
							<span class="direita">Fone:</span> 
						</div>
					
						<div class="columns small-8 medium-8 large-8">
							<input type="text" name="telefone" id="telefone" class="campo telefone" {bloqueado} value="{fone}">
						</div>
						
					</div>
				
				</div>
				
				<div class="columns small-12 medium-3 large-3">
				
					<div class="row">
					
						<div class="columns small-3 medium-3 large-3">
							<span class="direita">Email:</span> 
						</div>
					
						<div class="columns small-9 medium-9 large-9">
							<input type="text" name="email" id="email" class="campo" {bloqueado} value="{email}">
						</div>
						
					</div>
				
				</div>
			</div>
		</div>
		
		<div class="row separador">
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
				<img src="{base_url}assets/images/separador.jpg">
			</div>
		</div>
		
		<div class="row linha">
		
			<div class="columns small-12 medium-12 large-12">
				<span>Descrição da área comum: </span> <img class="interrogacao" src="{base_url}assets/images/interrogacao.png" title="Descreva as areas comuns do seu Empreendimento ou Condomínio">
				<span style="float: right">400 caracteres</span>
				<textarea id="descricao2" name="descricao2" maxlength="400" {bloqueadoarea} >{descricao2}</textarea>
			</div>
		
		</div>
		
		<div class="row separador">
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
				<img src="{base_url}assets/images/separador.jpg">
			</div>
		</div>
		<div class="row linha">
		
			<div class="columns small-12 medium-12 large-12">
				<span>Informações para contato:</span>
				<span style="float: right">400 caracteres</span>
				<textarea id="descricao3" name="descricao3" maxlength="400" {bloqueadoarea} >{descricao3}</textarea>
			</div>
		
		</div>
		
		
		<div class="row separador">
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
				<img src="{base_url}assets/images/separador.jpg">
			</div>
		</div>
		
		<div class="row linha imagens">
		
			<div class="columns small-12 medium-12 large-12">
			
				<label style="color: #0a71b3; font-weight: bold; margin-bottom: 10px">Envie {bloqueadoarea} as fotos do imóvel e selecione qual deles vai ser a de Destaque (Máximo de 10 imagens por imóvel)<br>Tamanho máximo da imagem 2mb, tipo do arquivo (png, jpg).</label>
				
				<input type="file" name="file_upload" id="file_upload{bloqueadoarea}" class="btn_azul" value="Enviar Imagem" />
				
				<div id='resultado_f' class='sortableGaleria' style='position:relative'>
					
					{imagens}
						<div class="links_fotos" id="l{id_imagem}">
							<a href="javascript:void(0)" rel="foto_galeria" style="cursor:move">
								<img id="{id_imagem}" data-id="{id_imagem}" src="{base_url}assets/uploads/imovel/{id_usuario}/{arquivo}" alt="{arquivo}">
							</a>
							<a href="javascript:void(0)" onclick="excluir('{id_imagem}')" class="excluir"></a>
							<input type='radio' class='btn_foto_destaque' id='btn_foto_destaque' name='btn_foto_destaque' onclick='fotoDestaque()' value='{id_imagem}' title='Imagem Destaque' {destaque_texto} />
						</div>
					{/imagens}
				
				</div>
				
			</div>
		
		</div>
		
		<div class="row separador">
				<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
					<img src="{base_url}assets/images/separador.jpg">
				</div>
			</div>
			
			<div class="visualizarimovel" style="display: none">
		
			<div class="small-12 large-12 medium-12 columns">
				<h2 class="titulo_azul" style="margin: 20px 0px;">Visualização de Imóvel</h2></h2>
				<label style="color: #0a71b3; font-weight: bold; margin-bottom: 10px">Abaixo está um preview de como seu anúncio ficará no site.</label>
			</div>
			
			<div class="small-12 large-12 medium-12 columns dados_imovel">
				<div id="dados_imovel">
				
				</div>
			</div>
			
			<div class="row separador">
				<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
					<img src="{base_url}assets/images/separador.jpg">
				</div>
			</div>
		
		</div>
		
		<div class="row linha botao">
		
			<div class="columns small-12 medium-10 large-10">
			
				<input type="button" id="visualizar_anuncio" class="btn_azul" value="Visualizar">
				
			</div>
		
			<div class="columns small-12 medium-2 large-2">
				
				<input type="submit" class="btn_azul" value="Editar">
				
			</div>
		
		</div>
		
	</form>
{/dados}