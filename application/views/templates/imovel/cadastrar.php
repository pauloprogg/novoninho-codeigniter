<!-- Conteudos especificos separados por sessoes -->



<form id="cadastrar_imovel" class="formulario_imovel" action="{base_url}painel/cadastrarImovel" method="post" caminho="{caminho}">

	<input type="hidden" id="id_usuario" name="id_usuario" value="{id_usuario}">

	<div class="row linha">

	

		<div class="columns small-12 medium-1 large-1">

			<span>Titulo:</span>

		</div>

		

		<div class="columns small-12 medium-11 large-11">

			<input type="text" name="titulo" id="titulo" class="campo">

		</div>

	

	</div>

	

	<div class="row separador">

		<div class="small-12 large-10 medium-10 large-centered small-centered medium-centered columns">

			<img src="{base_url}assets/images/separador.jpg">

		</div>

	</div>

	

	<div class="row linha">

	

		<div class="columns small-12 medium-12 large-12">

			<span>Descrição do imóvel:</span>

			<span style="float: right">800 caracteres</span>

			<textarea id="descricao1" name="descricao1" maxlength="800"></textarea>

		</div>

	

	</div>
	
	<div class="row separador">

		<div class="small-12 large-10 medium-10 large-centered small-centered medium-centered columns">

			<img src="{base_url}assets/images/separador.jpg">

		</div>

	</div>
	
	<div class="row linha">

		<div class="columns small-12 medium-2 large-2">

			<span>Número de Referência:</span>

		</div>

		<div class="columns small-12 medium-4 large-4 left">

			<input type="text" name="numero_referencia" id="numero_referencia" class="campo">

		</div>

	</div>

	<div class="row separador">

		<div class="small-12 large-10 medium-10 large-centered small-centered medium-centered columns">

			<img src="{base_url}assets/images/separador.jpg">

		</div>

	</div>

			

	<div class="row linha">

	

		<div class="columns small-12 medium-4 large-4 coluna">

		

			<div class="row">

			

				<div class="columns small-12 medium-4 large-4">

					<input type="radio" name="tipo_imovel" id="tipo_imovel" value="1" class="radio_tipo" checked="checked"><span>Venda</span>

				</div>

				

				<div class="columns small-12 medium-4 large-4">

					<input type="radio" name="tipo_imovel" id="tipo_imovel" value="2" class="radio_tipo"><span>Locação</span>

				</div>

				

				<div class="columns small-12 medium-4 large-4">

					<input type="radio" name="tipo_imovel" id="tipo_imovel" value="3" class="radio_tipo"><span>Temporada</span>

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-4 large-4 coluna">

		

			<div class="row">

		

				<div class="columns small-12 medium-3 large-3">

					<span class="direita">Tipo:</span> 

				</div>

				

				<div class="columns small-12 medium-9 large-9">

					<select name="tipo" id="tipo">

						{tipo_imovel}

							<option value="{id}">{tipo}</option>

						{/tipo_imovel}

					</select>

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-4 large-4">

		

			<div class="row">

		

				<div class="columns small-12 medium-3 large-3">

					<span class="direita">Finalidade:</span> 

				</div>

				

				<div class="columns small-12 medium-9 large-9">

					<select name="finalidade" id="finalidade">

						{finalidade}

							<option value="{id}">{tipo}</option>

						{/finalidade}

					</select>

				</div>

				

			</div>

		

		</div>



	</div>

	

	<div class="row separador">

		<div class="small-12 large-10 medium-10 large-centered small-centered medium-centered columns">

			<img src="{base_url}assets/images/separador.jpg">

		</div>

	</div>

	

	<div class="row linha">

	

		<div class="columns small-12 medium-3 large-3">

		

			<div class="row">

			

				<div class="columns small-12 medium-6 large-6">

					<span class="direita">Dormitórios:</span> 

				</div>

			

				<div class="columns small-12 medium-6 large-6">

					<select name="dormitorios" id="dormitorios">

						<option value="0">0</option>

						<option value="1">1</option>

						<option value="2">2</option>

						<option value="3">3</option>

						<option value="4">4</option>

						<option value="5">5</option>

						<option value="6">6</option>

						<option value="7">7</option>

						<option value="8">8</option>

						<option value="9">9</option>

						<option value="10">10</option>

					</select>

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-3 large-3">

		

			<div class="row">

			

				<div class="columns small-12 medium-6 large-6">

					<span class="direita">Suites:</span> 

				</div>

			

				<div class="columns small-12 medium-6 large-6">

					<select name="suites" id="suites">

						<option value="0">0</option>

						<option value="1">1</option>

						<option value="2">2</option>

						<option value="3">3</option>

						<option value="4">4</option>

						<option value="5">5</option>

						<option value="6">6</option>

						<option value="7">7</option>

						<option value="8">8</option>

						<option value="9">9</option>

						<option value="10">10</option>

					</select>

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-3 large-3">

		

			<div class="row">

			

				<div class="columns small-12 medium-6 large-6">

					<span class="direita">Garagem:</span> 

				</div>

			

				<div class="columns small-12 medium-6 large-6">

					<select name="garagem" id="garagem">

						<option value="0">0</option>

						<option value="1">1</option>

						<option value="2">2</option>

						<option value="3">3</option>

						<option value="4">4</option>

						<option value="5">5</option>

						<option value="6">6</option>

						<option value="7">7</option>

						<option value="8">8</option>

						<option value="9">9</option>

						<option value="10">10</option>

					</select>

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-3 large-3">

		

			<div class="row">

			

				<div class="columns small-12 medium-6 large-6">

					<span class="direita">Banheiros:</span> 

				</div>

			

				<div class="columns small-12 medium-6 large-6">

					<select name="banheiros" id="banheiros">

						<option value="0">0</option>

						<option value="1">1</option>

						<option value="2">2</option>

						<option value="3">3</option>

						<option value="4">4</option>

						<option value="5">5</option>

						<option value="6">6</option>

						<option value="7">7</option>

						<option value="8">8</option>

						<option value="9">9</option>

						<option value="10">10</option>

					</select>

				</div>



			</div>



		</div>



	</div>



	<div class="row separador">

		<div class="small-12 large-10 medium-10 large-centered small-centered medium-centered columns">

			<img src="{base_url}assets/images/separador.jpg">

		</div>

	</div>



	<div class="row linha">



		<div class="columns small-12 medium-4 large-4">

		

			<div class="row">

			

				<div class="columns small-12 medium-6 large-6">

					<span class="direita">Área útil (m2):</span> 

				</div>

			

				<div class="columns small-12 medium-6 large-6">

					<input type="text" name="area_m2" id="area_m2" class="campo moeda">

				</div>

				

			</div>

		

		</div>



		<div class="columns small-12 medium-4 large-4">



			<div class="row mini_titulo">

			

				<div class="columns small-12 medium-12 large-12">

					<span class="direita">Aparecer endereço na página do Anúncio?</span> 

				</div>

			

			</div>

		

			<div class="row">

				

				<div class="columns small-12 medium-6 large-6">

					<input type="radio" name="mapa" id="mapa" value="1" class="radio_tipo" checked="checked"><span>Sim</span>

				</div>

			

				<div class="columns small-12 medium-6 large-6">

					<input type="radio" name="mapa" id="mapa" value="0" class="radio_tipo" checked="checked"><span>Não</span>

				</div>

				

			</div>

		

		</div>



	</div>

	

	<div class="row separador">

		<div class="small-12 large-10 medium-10 large-centered small-centered medium-centered columns">

			<img src="{base_url}assets/images/separador.jpg">

		</div>

	</div>

	

	<div class="row linha">

	

		<div class="columns small-12 medium-3 large-3">

		

			<div class="row">

			

				<div class="columns small-12 medium-3 large-3">

					<span class="direita">CEP:</span> 

				</div>

			

				<div class="columns small-12 medium-9 large-9">

					<input type="text" name="cep" id="cep" class="campo cep">

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-4 large-4">

		

			<div class="row">

			

				<div class="columns small-12 medium-2 large-2">

					<span class="direita">Rua:</span> 

				</div>

			

				<div class="columns small-12 medium-10 large-10">

					<input type="hidden" name="id_rua" id="id_rua">

					<input type="text" name="rua" id="rua" class="campo endereco" readonly>

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-2 large-2">

		

			<div class="row">

			

				<div class="columns small-12 medium-2 large-2">

					<span class="direita">Nº:</span> 

				</div>

			

				<div class="columns small-12 medium-10 large-10">

					<input type="text" name="numero" id="numero" class="campo">

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-3 large-3">

		

			<div class="row">

			

				<div class="columns small-12 medium-4 large-4">

					<span class="direita">Complem.</span> 

				</div>

			

				<div class="columns small-12 medium-8 large-8">

					<input type="text" name="complemento" id="complemento" class="campo">

				</div>

				

			</div>

		

		</div>



	</div>

	

	<div class="row separador">

		<div class="small-12 large-10 medium-10 large-centered small-centered medium-centered columns">

			<img src="{base_url}assets/images/separador.jpg">

		</div>

	</div>

	

	<div class="row linha">

	

		<div class="columns small-12 medium-4 large-4">

		

			<div class="row">

			

				<div class="columns small-12 medium-3 large-3">

					<span class="direita">Bairro:</span> 

				</div>

			

				<div class="columns small-12 medium-9 large-9">

					<input type="hidden" name="id_bairro" id="id_bairro">

					<input type="text" name="bairro" id="bairro" class="campo bairro" readonly>

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-4 large-4">

		

			<div class="row">

			

				<div class="columns small-12 medium-3 large-3">

					<span class="direita">Cidade:</span> 

				</div>

			

				<div class="columns small-12 medium-9 large-9">

					<input type="hidden" name="id_cidade" id="id_cidade">

					<input type="text" name="cidade" id="cidade" class="campo cidade" readonly>

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-2 large-2">

		

			<div class="row">

			

				<div class="columns small-12 medium-6 large-6">

					<span class="direita">Estado:</span> 

				</div>

			

				<div class="columns small-12 medium-6 large-6">

					<input type="hidden" name="id_estado" id="id_estado">

					<input type="text" name="estado" id="estado" class="campo estado" readonly>

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-2 large-2">

		

			<div class="row mini_titulo">

			

				<div class="columns small-12 medium-12 large-12">

					<span class="direita">Condomínio:</span> 

				</div>

			

			</div>

		

			<div class="row">

				

				<div class="columns small-12 medium-6 large-6">

					<input type="radio" name="condominio" id="condominio" value="0" class="radio_tipo" checked="checked"><span>Sim</span>

				</div>

			

				<div class="columns small-12 medium-6 large-6">

					<input type="radio" name="condominio" id="condominio" value="1" class="radio_tipo" checked="checked"><span>Não</span>

				</div>

				

			</div>

		

		</div>



	</div>

	

	<div class="row separador">

		<div class="small-12 large-10 medium-10 large-centered small-centered medium-centered columns">

			<img src="{base_url}assets/images/separador.jpg">

		</div>

	</div>

	

	<div class="row linha">

	

		<div class="columns small-12 medium-3 large-3">

		

			<div class="row">

			

				<div class="columns small-12 medium-3 large-3">

					<span class="direita"><strong>Valor:</strong></span> 

				</div>

			

				<div class="columns small-12 medium-9 large-9">

					<input type="text" name="valor" id="valor" class="campo moeda">

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-3 large-3">

		

			<div class="row mini_titulo">

			

				<div class="columns small-12 medium-12 large-12">

					<span class="direita">Usar dados de contato do Cadastro?</span> 

				</div>

			

			</div>

		

			<div class="row">

				

				<div class="columns small-12 medium-6 large-6">

					<input type="radio" name="usar_contato" id="usar_contato" value="1" class="radio_tipo usar_contato" checked="checked"><span>Sim</span>

				</div>

			

				<div class="columns small-12 medium-6 large-6">

					<input type="radio" name="usar_contato" id="usar_contato" value="2" class="radio_tipo usar_contato"><span>Não</span>

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-3 large-3">

		

			<div class="row dados_contato">

			

				<div class="columns small-12 medium-4 large-4">

					<span class="direita">Fone:</span> 

				</div>

			

				<div class="columns small-12 medium-8 large-8">

					<input type="text" name="telefone" id="telefone" class="campo telefone">

				</div>

				

			</div>

		

		</div>

		

		<div class="columns small-12 medium-3 large-3">

		

			<div class="row dados_contato">

			

				<div class="columns small-12 medium-3 large-3">

					<span class="direita">Email:</span> 

				</div>

			

				<div class="columns small-12 medium-9 large-9">

					<input type="text" name="email" id="email" class="campo">

				</div>

				

			</div>

		

		</div>



	</div>

	

	<div class="row separador">

		<div class="small-12 large-10 medium-10 large-centered small-centered medium-centered columns">

			<img src="{base_url}assets/images/separador.jpg">

		</div>

	</div>

	

	<div class="row linha">

	

		<div class="columns small-12 medium-12 large-12">

			<span>Descrição da área comum: </span> <img class="interrogacao" src="{base_url}assets/images/interrogacao.png" title="Descreva as areas comuns do seu Empreendimento ou Condomínio">

			<span style="float: right">400 caracteres</span>

			<textarea id="descricao2" name="descricao2" maxlength="400"></textarea>

		</div>

	

	</div>

	

	<div class="row separador">

		<div class="small-12 large-10 medium-10 large-centered small-centered medium-centered columns">

			<img src="{base_url}assets/images/separador.jpg">

		</div>

	</div>

	

	<div class="row linha">

	

		<div class="columns small-12 medium-12 large-12">

			<span>Informações para contato:</span>

			<span style="float: right">400 caracteres</span>

			<textarea id="descricao3" name="descricao3" maxlength="400"></textarea>

		</div>

	

	</div>

	

	<div class="row separador">

		<div class="small-12 large-10 medium-10 large-centered small-centered medium-centered columns">

			<img src="{base_url}assets/images/separador.jpg">

		</div>

	</div>

	

	<div class="row linha imagens">

	

		<div class="columns small-12 medium-12 large-12">

			

			<label style="color: #0a71b3; font-weight: bold; margin-bottom: 10px">Envie as fotos do imóvel e selecione qual deles vai ser a de Destaque (Máximo de 10 imagens por imóvel)<br>Tamanho máximo da imagem 2mb, tipo do arquivo (png, jpg).</label>

			

			<input type="file" name="file_upload" id="file_upload" class="btn_azul" value="Enviar Imagem"/>

			

			<div id='resultado_f' class='sortableGaleria' style='position:relative'></div>

			

		</div>

	

	</div>

	

	<div class="row separador">

		<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">

			<img src="{base_url}assets/images/separador.jpg">

		</div>

	</div>

	

	<div class="visualizarimovel" style="display: none">

	

		<div class="small-12 large-12 medium-12 columns">

			<h2 class="titulo_azul" style="margin: 20px 0px;">Visualização de Imóvel</h2></h2>

			<label style="color: #0a71b3; font-weight: bold; margin-bottom: 10px">Abaixo está um preview de como seu anúncio ficará no site.</label>

		</div>

		

		<div class="small-12 large-12 medium-12 columns dados_imovel">

			<div id="dados_imovel">

			

			</div>

		</div>

		

		<div class="row separador">

			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">

				<img src="{base_url}assets/images/separador.jpg">

			</div>

		</div>

	

	</div>

	

	<div class="row linha botao">

	

		<div class="columns small-12 medium-10 large-10">

			

			<input type="button" id="visualizar_anuncio" class="btn_azul" value="Visualizar">

			

		</div>

	

		<div class="columns small-12 medium-2 large-2">

			

			<input type="submit" class="btn_azul" value="Cadastrar">

			

		</div>

	

	</div>
	
	<div class="small-12 form_result" style="text-align: center;">
		
	</div>

</form>

