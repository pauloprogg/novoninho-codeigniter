
<div class="imovel">
	{dados}
	<div class="row">

		<div class="columns small-12 medium-9 large-9">
		
			<div class="row">
				<div class="columns small-12 medium-12 large-12">
				
					<div class="titulo_vermelho barra-vermelho">
					
						<div class="titulo_conteudo">
							<h2 class="titulo_box">{titulo_pagina}</h2>
						</div>
						
						<div class="botao_voltar">
							<a><img src="{base_url}assets/images/btn_voltar.png"></a>
						</div>
						
					</div>
					
				</div>
			</div>
			
			<div class="row">
			
				<div class="columns small-12 medium-12 large-12">
					
					<h2 class="preto">{titulo}</h2>
					
					<label class="texto_padrao">
					
					<p>{descricao1}</p>
					
					</label>
					
					<div class="row">
					
						<div class="columns small-12 medium-12 large-12">
						
							<div class="galeria">
							
								<div class="imagem_principal">
									<img src="{imagem_principal}">
								</div>
								
								<div class="fotos">
								
									<ul id="carrousel_fotos_imovel">
									
										{galeria}
								
										<li>
											<a class="galeria_foto">
												<img src="{arquivo}">
											</a>
										</li>
										
										{/galeria}
										
									</ul>									
									
								</div>
								
								<div class="control">
								
									<div class="botoes">
										<a class="prev_vermelho" id="prev_galeria"></a>
										<a class="pause_vermelho" id="pause_galeria"></a>
										<a class="next_vermelho" id="next_galeria"></a>
									</div>
								
								</div>
							
							</div>
						
						</div>
					
					</div>

					<div class="row">
					
						<div class="columns small-12 medium-4 large-4">
						
							<div class="dados">
							
								<div class="preco">
									<span>Valor:</span>
									<h2>R$ {valor_br}</h2>
								</div>
								
								<div class="informacoes">
                                	<p><b>Nº:</b> {id_imovel}</p>
									<p><b>Área útil (m2):</b> {area}</p>
									<p><b>Valor m2:</b> R$ {valor_m2}</p>
									<p><b>Dormitórios:</b> {dormitorios} quarto(s)</p>
									<p><b>Suítes:</b> {suites} suite(s)</p>
									<p><b>Garagem:</b> {garagem} lugare(s)</p>
                                    <p><b>Banheiros:</b> {banheiros}</p>
									<p><b>Condomínio:</b> {condominio}</p>
								</div>
							
							</div>
						
						</div>
						
						<div class="columns small-12 medium-8 large-8">
								
							<div class="dados">
							
								<div class="texto">
					
									<p><b>Informações do condomínio: </b>{descricao2}</p>
					
								</div>
								
								<div class="redes">
									<img src="{base_url}assets/images/imovel_facebook.jpg">
									<img src="{base_url}assets/images/imovel_twitter.jpg">
									<img src="{base_url}assets/images/imovel_google.jpg">
								</div>
								
							</div>
								
						</div>
					
					</div>
					
					<h2 class="preto2"> CONTATO </h2>
					
					<div class="row box_contato">
					
						<div class="columns small-12 medium-12 large-12">

							<div class="contato">
								
								<div class="row">
								
									<div class="columns small-12 medium-12 large-12 contato_realizado">
										
										<label>Contato enviado com sucesso! Aguarde uma resposta dentro de pouco tempo!</label>
										
									</div>
									
									<div class="columns small-12 medium-6 large-6">
										{nome_user}
										<p><b>E-mail: </b> {email_user} </p>
										<p><b>Fone: </b> {telefone_user} </p>
										<p><b>Melhor Horario para Ligar: </b> {horario_user} </p>
										
										<p class="texto">{descricao3}</p>
									
									</div>
									
									<div class="columns small-12 medium-6 large-6">
									
										<div class="formulario">
											<form>
											
												
												<label>Nome:</label>
												<input class="campo" type="text" name="" id="">
												
												<label>Email:</label>
												<input class="campo" type="text" name="" id="">
												
												<label>Mensagem/Telefone::</label>
												<textarea class="campo" name="" id=""></textarea>
												
												<input type="button" class="btn_azul" value="Enviar">
												
											</form>
										</div>
									
									</div>
									
								</div>
								
							</div>

						</div>
					
					</div>
					
				</div>
				
			</div>
			
		</div>
		
		<div class="columns small-12 medium-3 large-3">
			
			{anuncios}
			
		</div>
		
	</div>
	
    {mapa_google}
	
	{/dados}
</div>