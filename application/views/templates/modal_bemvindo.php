<!-- Modal de Bem Vindo -->

<a href="#" id="link_modal" data-reveal-id="modalBemvindo" style="display: none"></a>

<div id="modalBemvindo" class="reveal-modal modal-bemvindo" data-reveal>

	<img src="{base_url}assets/images/logo.png">
				
	<h2>Muito obrigado! Cadastro realizado com sucesso.</h2>

	<p>Você agora será redirecionado para o seu painel de controle</p>
	
	<!-- <a class="close-reveal-modal">&#215;</a> -->

</div>

<!-- ----------------- -->