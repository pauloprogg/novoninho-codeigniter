<!-- Conteudos especificos separados por sessoes -->

<div class="row">

	<div class="columns small-12 medium-9 large-9">
	
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<div class="titulo_azul barra-azul">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">Quem Somos</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				
				<h2 class="preto">A EMPRESA</h2>
				<label class="texto_padrao">
				
				{texto1}
				
				</label>
				
				<h2 class="preto">POLÍTICA</h2>
				<label class="texto_padrao">
				
				{texto2}
				
				</label>
				
				<h2 class="preto">OBJETIVO</h2>
				<label class="texto_padrao">
				
				{texto3}
				
				</label>
				
			</div>
		</div>
	
	</div>
	
	<div class="columns small-12 medium-3 large-3">
		
		{anuncios}
		
	</div>
	
</div>

{noticias}

<!-- Fim dos conteudos especificos -->