<!-- Conteudos especificos separados por sessoes -->

<div class="row noticias">

	<div class="columns small-12 medium-9 large-9">
	
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<div class="titulo_azul barra-azul">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">Notícias</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			
			<div class="columns small-12 medium-12 large-12">
			
				{dados}
					<div class="noticia">
						
						<div class="row">
						
							<div class="columns small-12 medium-3 large-3">
								<div class="imagem">
									<img src="{base_url}assets/uploads/noticias/thumb/{imagem}">
								</div>
							</div>
							
							<div class="columns small-12 medium-9 large-9">
								<div class="dados">
									<h2>{titulo}</h2>
									<p>{resumo}</p>
									<a href="{base_url}noticia/{slug}">Saiba mais</a>
								</div>
							</div>
							
						</div>
						
					</div>
				{/dados}
			
			</div>
			
		</div>
	
	</div>
	
	<div class="columns small-12 medium-3 large-3">
		
		{anuncios}
		
	</div>
	
</div>

<div class="row paginacao_noticias">

	<div class="columns small-12 medium-12 large-12">
		<div class="paginacao">
			{paginacao}
		</div>
	</div>

</div>

<!-- Fim dos conteudos especificos -->