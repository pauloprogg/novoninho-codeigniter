<!-- Conteudos especificos separados por sessoes -->

<div class="row noticia_interna">

	<div class="columns small-12 medium-9 large-9">
	
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<div class="titulo_azul barra-azul">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">Notícias</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				
				<h2 class="preto">{titulo}</h2>
				
				<div class="imagem">
					<img src="{base_url}assets/uploads/noticias/{imagem_noticia}">
				</div>
				
				<label class="texto_padrao">
				
				{texto}
				
				</label>
				
			</div>
		</div>
	
	</div>
	
	<div class="columns small-12 medium-3 large-3">
		
		{anuncios}
		
	</div>
	
</div>

{noticias}

<!-- Fim dos conteudos especificos -->