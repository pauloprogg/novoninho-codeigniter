<!-- Conteudos especificos separados por sessoes -->

<div class="row revista">

	<div class="columns small-12 medium-9 large-9">
	
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<div class="titulo_azul barra-azul">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">Multimídia</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
		
			<div class="columns small-12 medium-12 large-12">
				
				<label class="texto_padrao">
				
				{texto}
				
				</label>
				
			</div>
			
			<div class="columns small-12 medium-12 large-12">
			
				<ul class="small-block-grid-3 medium-block-grid-3 large-block-grid-3 lista_logos">
					
					{revistas}
					
						<li>
							<div class="logo_revista">
								<a href="{link}" target="_blank" title="{titulo}"><img src="{base_url}assets/uploads/revistas/{imagem}"></a>
							</div>
						</li>
					
					{/revistas}
					
				</ul>
			
			</div>
			
		</div>
	
	</div>
	
	<div class="columns small-12 medium-3 large-3">
		
		{anuncios}
		
	</div>
	
</div>

{noticias}

<!-- Fim dos conteudos especificos -->