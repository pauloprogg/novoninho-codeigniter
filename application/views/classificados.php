<!-- Conteudos especificos separados por sessoes -->

<div class="classificados">

	{destaques}

	<div class="row">

		<div class="columns small-12 medium-12 large-12">
			
			<h2 class="azul">Busca Avançada</h2>
			
			<div class="busca_classificados">
			
				<span>O que você procura?</span>
			
				{formulario}
				
			</div>
			
		</div>
		
	</div>

	<div class="row">

		{resultado}
		
		<div class="columns small-12 medium-3 large-3">
			
			{anuncios}
			
		</div>
		
	</div>
	
</div>

<!-- Fim dos conteudos especificos -->