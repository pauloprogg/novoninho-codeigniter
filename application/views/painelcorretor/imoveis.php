<!-- Conteudos especificos separados por sessoes --> 

<div class="row painel proprietario_imoveis">

	<div class="columns small-12 medium-12 large-12">
		
		<h2 class="titulo_azul">Imóveis</h2>
		
		<div class="row">
		
			<div class="columns small-12 medium-6 large-6">
			
				<a href="{base_url}painel-corretor/cadastrar " class="botao_azul">Cadastrar Imóvel</a>
				
			</div>
			
			<div class="columns small-12 medium-6 large-6">
			
				<label class="legenda_texto">Legenda:</label>
				
				<div class="legenda_publicado">
					<div></div>
					<label>Publicado</label>
				</div> 
				
				<div class="legenda_destaque">
					<div></div>
					<label>Destaque</label>
				</div>

				<div class="legenda_feirao">

					<div></div>

					<label>Feirão</label>

				</div> 

				<div class="legenda_super_destaque">

					<div></div>

					<label>Super Destaque</label>

				</div>

				<div class="legenda_destaque_feirao">

					<div></div>

					<label>Destaque & Feirão</label>

				</div> 
			
			</div>
		
		</div>

		<div class="row">
		<div class="columns small-12 medium-12 large-12">

			<form action="{base_url}{pagina}" method="get">
				

				<div class="columns medium-3 large-3">
					<select name="feirao" id="slcOrdenacao" />
						<option value="">Feirão</option>
						<option value="1">Sim</option>
						<option value="0">Não</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="destaque_feirao" id="slcOrdenacao" />
						<option value="">Destaque Feirão</option>
						<option value="1">Sim</option>
						<option value="0">Não</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="super_destaque_feirao" id="slcOrdenacao" />
						<option value="">Super Destaque Feirão</option>
						<option value="1">Sim</option>
						<option value="0">Não</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="finalidade" />
						<option value="">Finalidade</option>
						<option value="1">Comercial</option>
						<option value="2">Residencial</option>
						<option value="3">Industrial</option>
						<option value="4">Rural</option>
						<option value="5">Lazer</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="tipo_imovel" id="slcOrdenacao" />
						<option value="">Tipo Imovel</option>
						<option value="1">Apartamento</option>
						<option value="2">Apartamento Duplex</option>
						<option value="3">Área</option>
						<option value="4">Barracão</option>
						<option value="5">Casa</option>
						<option value="6">Chácara</option>
						<option value="7">Cobertura</option>
						<option value="8">Fazenda</option>
						<option value="9">Galpão</option>
						<option value="10">Kitnet</option>
						<option value="11">Pousada</option>
						<option value="12">Prédio</option>
						<option value="13">Rancho</option>
						<option value="14">Sala</option>
						<option value="15">Salão</option>
						<option value="16">Sítio</option>
						<option value="17">Sobrado</option>
						<option value="18">Terreno</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="destaque" id="slcOrdenacao" />
						<option value="">Destaque</option>
						<option value="1">Sim</option>
						<option value="0">Não</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="super_destaque" id="slcOrdenacao" />
						<option value="">Super Destaque</option>
						<option value="1">Sim</option>
						<option value="0">Não</option>
					</select>
				</div>
				<div class="columns medium-3 large-3">
					<select name="tipo" id="slcOrdenacao" />
						<option value="">Procura</option>
						<option value="1">Venda</option>
						<option value="2">Locação</option>
						<option value="3">Temporada</option>
					</select>
				</div>
				
				<div class="columns medium-3 large-3">
					<select name="dormitorios" id="slcOrdenacao" />
						<option value="">Dormitorios</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
				</div>
				
				<div class="columns medium-3 large-3">
					<select name="valor" />
						<option value="">Valor</option>
						<option value="100000.00">Mais de R$ 100 mil</option>
						<option value="200000.00">Mais de R$ 200 mil</option>
						<option value="300000.00">Mais de R$ 300 mil</option>
						<option value="400000.00">Mais de R$ 400 mil</option>
						<option value="500000.00">Mais de R$ 500 mil</option>
					</select>
				</div>

				<div class="columns medium-3 large-3">
					<input type="text" name="id" id="id" placeholder="Número de Referência Novoninho">
				</div>

				<div class="columns medium-3 large-3">
					<input type="text" name="referencia_xml" id="referencia_xml" placeholder="Número de Referência Proprietario">
				</div>
				
				
				
				<div class="columns medium-12 large-12 text-right">
					<div class="row">
						<input type="submit" value="buscar" class="button tiny" />
					</div>
				</div>
				
			</form>
		</div>
		</div>
		
	</div>
	
	<div class="columns small-12 medium-12 large-12">
	
		<div class="row">
		
			<div class="row header_lista">
		
				<div class="columns small-1 medium-1 large-1 centro">
					
					<label>Sel</label>
					
				</div>
				
				<div class="columns small-5 medium-5 large-5">
					
					<label>Imovel</label>
					
				</div>
				
				<div class="columns small-3 medium-3 large-3">
					
					<label>Status</label>
					
				</div>
				
				<div class="columns small-3 medium-3 large-3">
					
					<label>Ações</label>
					
				</div>
				
			</div>
			
			{lista_imoveis}
			
		</div>
		
		{botao_divulgar}
		
	</div>
	
</div>

<!-- Fim dos conteudos especificos -->