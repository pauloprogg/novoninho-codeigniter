<!-- Conteudos especificos separados por sessoes -->

<div class="row painel proprietario_editardados">

	<div class="columns small-12 medium-12 large-12">
	
		<h2 class="titulo_azul">Alterar meus Dados</h2>
		
		<div class="row">
		
			<div class="columns small-12 medium-12 large-12">
			{dados}
			
				<form id="proprietario_editarperfil" action="{base_url}painel_proprietario/editPerfil" method="post">
					<input type="hidden" name="id" id="id" value="{id}">
			
					<div class="row linha">
				
						<div class="columns small-12 medium-6 large-6">
							
							<label>Nome:</label>
							<input class="campo" type="text" name="nome" id="nome" value="{nome}">
							
						</div>
						
						<div class="columns small-12 medium-6 large-6">
							
							<label>E-mail:(Este será seu Usuário / Login) </label>
							<input class="campo" type="text" name="email" id="email" value="{email}">
							
						</div>
						
					</div>
					<div class="row linha">
				
						<div class="columns small-6 medium-6 large-6">
							
							<label>CPF / CNPJ:(Apenas para emissão de nota fiscal)</label>
							<input class="campo {classe_mascara}" type="text" name="cpf_cnpj" id="cpf_cnpj" value="{cpf_cnpj}">
							
						</div>
						
						<div class="columns small-6 medium-6 large-6">
						
							<div class="row">
								<div class="columns small-6 medium-6 large-6">
									<br/>
									<label><input type="radio" name="check_cpf_cnpj" value="1" class="check_mascara" {check_cpf}> CPF</label>
								</div>
								<div class="columns small-6 medium-6 large-6">
									<br/>
									<label><input type="radio" name="check_cpf_cnpj" value="2" class="check_mascara" {check_cnpj}> CNPJ</label>
								</div>
							</div>
							
						</div>
						
					</div>
					
					<div class="row linha">
					
						<div class="columns small-12 medium-6 large-6">
							
							<label>Telefone:</label>
							<input class="campo telefone" type="text" name="telefone" id="telefone" value="{telefone}">
							
						</div>
				
						<div class="columns small-12 medium-6 large-6">
							
							<label>Melhor Horário para Ligar:</label>
							<input class="campo" type="text" name="horario" id="horario" value="{melhor_horario}">
							
						</div>
						
					</div>
					
					<div class="row linha">
				
						<div class="columns small-12 medium-6 large-6">
							{selectResp}
						</div>
						
				
						<div class="columns small-12 medium-6 large-6">
							{selectVendedor}
						</div>
						
					</div>
					<div class="row linha">
				
						<div class="columns small-12 medium-6 large-6">
							
							<label>Senha:</label>
							<input class="campo" type="password" name="senha" id="senha">
							
						</div>
						
						<div class="columns small-12 medium-6 large-6">
							
							<label>Confirmar Senha:</label>
							<input class="campo" type="password" name="confirmar_senha" id="confirmar_senha">
							
						</div>
						
					</div>
					<div class="row linha contrato">
				
						<div class="columns small-12 medium-12 large-12">
							<label>
								<input type="checkbox" name="contrato" id="contrato" value="1" required>
								<span>Li e concordo com os <a href="#" data-reveal-id="modalTermos" style="font-weight: bold">Termos do Contrato</a></span>
							</label>
						</div>
						
					</div>

					<div class="row linha contrato">
				
						<div class="columns small-12 medium-12 large-12">
							{newsletter}
						</div>
						
					</div>
					
					<div class="row linha">
					
						<div class="small-12 large-12 medium-12 columns">
						
							<input type="submit" class="btn_azul" value="Salvar">
						
						</div>
						
					</div>
					
				</form>
			
			{/dados}			
			</div>
			
		</div>
		
	</div>
	
</div>

<!-- Fim dos conteudos especificos -->