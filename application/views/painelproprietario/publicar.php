<!-- Conteudos especificos separados por sessoes -->
<div class="row painel proprietario_publicar">
	<div class="columns small-12 medium-12 large-12" style="margin-bottom: 20px;">
		
		<h2 class="titulo_azul">Publicar</h2>
		
	</div>
	
	<div class="columns small-12 medium-12 large-12">
		
		{lista}
			
		<div class="row aviso">
		
			<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
				
				<div class="texto_aviso">
					<label>ATENÇÃO: Após a compra finalizada você não poderá alterar os dádos dos imóveis</label>
				</div>
				
			</div>
		
		</div>
		
		<div class="row preco">
		
			<div class="small-9 large-9 medium-9 columns">
				
				<span class="texto">Total:</span>
				
			</div>
			
			<div class="small-3 large-3 medium-3 columns">
				
				<span class="preco">R$ {total}</span>
				
			</div>
		
		</div>
		
		<div class="row botao">
		
			<div class="small-12 large-12 medium-12 columns">
			
				<a href="{base_url}painel_proprietario" class="botao_azul">Voltar</a>
				
				<a href="{base_url}pagamento" class="botao_azul" id="publicar-imovel">Confirmar</a>
				
			</div>
		
		</div>
	
	</div>
	
</div>
<!-- Fim dos conteudos especificos -->