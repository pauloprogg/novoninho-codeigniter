<!-- Conteudos especificos separados por sessoes -->

<div class="row anuncie_perfil">

	<div class="columns small-12 medium-9 large-9">
	
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<div class="titulo_azul barra-azul">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">Anuncie Aqui</h2>
					</div>
				</div>
			</div>
		</div>
		
		{dados}
		<div class="row">
		
			<div class="columns small-12 medium-12 large-12">
				
				<h2 class="preto">Corretor Autônomo</h2>
				
				<label class="texto_padrao">
				
				{texto1}
				
				</label>
				
				<div class="columns small-12 medium-12 large-12">
				
					<div class="row vantagens">
					
						<div class="columns small-12 medium-8 large-8">
					
							<label class="preto">VANTAGENS</label>
							
							<div class="row">
						
								<div class="columns small-12 medium-4 large-4">
						
									<ul style="margin-left: 10px">
										<li>{vantagem1}</li>
										<li>{vantagem2}</li>
										<li>{vantagem3}</li>
										<li>{vantagem4}</li>
									</ul>
								
								</div>
							
								<div class="columns small-12 medium-4 large-4">
						
									<ul style="margin-left: 10px">
										<li>{vantagem5}</li>
										<li>{vantagem6}</li>
										<li>{vantagem7}</li>
										<li>{vantagem8}</li>
									</ul>
								
								</div>
							
								<div class="columns small-12 medium-4 large-4">
							
									<ul style="margin-left: 10px">
										<li>{vantagem9}</li>
										<li>{vantagem10}</li>
										<li>{vantagem11}</li>
										<li>{vantagem12}</li>
									</ul>
								
								</div>
								
							</div>
							
						</div>
						
						<div class="columns small-12 medium-4 large-4">
						
							<div class="comprar">
							
								<div class="plano">
									<span>Publicação Comum:</span>
									<h2>R$ {preco_anuncio_comum}</h2>
								</div>
								
								<div class="plano">
									<span>Publicação Destaque:</span>
									<h2>R$ {preco_anuncio_destaque}</h2>
								</div>
								
								<div class="botao2">
									<a href="{base_url}cadastrar/corretor" class="botao">Cadastre-se aqui para anunciar</a>
								</div>
								
							</div>
						
						</div>
						
					</div>
					
				</div>
				
				<label class="texto_padrao">
				
				{texto2}
				
				</label>
				
				<h2 class="preto">Contato</h2>
				
				<div class="row">
				
					<div class="columns small-12 medium-12 large-12">
									
						<form id="formulario_contato" class="contato_corretor" action="{base_url}contato/send" method="post">
							
							<div class="row linha">
								<div class="columns small-12 medium-12 large-12">
									<label> Nome: </label> 
									<input type="text" name="nome" id="nome">
								</div>
							</div>
							
							<div class="row linha">
								<div class="columns small-12 medium-12 large-12">
									<label> E-mail: </label> 
									<input type="text" name="email" id="email">
								</div>
							</div>
							
							<div class="row linha">
								<div class="columns small-12 medium-12 large-12">
									<label> Assunto: </label> 
									<input type="text" name="assunto" id="assunto">
								</div>
							</div>
							
							<div class="row linha">
								<div class="columns small-12 medium-12 large-12">
									<label> Mensagem: </label> 
									<textarea name="mensagem" id="mensagem"></textarea>
								</div>
							</div>
							
							<div class="row linha">
								<div class="columns small-12 medium-12 large-12">
									<input type="submit" class="btn_azul" value="Enviar">
								</div>
							</div>
							
						</form>
						
					</div>
					
				</div>
				
			</div>
			{/dados}
			
		</div>
		
	</div>
	
	<div class="columns small-12 medium-3 large-3">
		
		{anuncios}
		
	</div>
	
</div>

<!-- Fim dos conteudos especificos -->