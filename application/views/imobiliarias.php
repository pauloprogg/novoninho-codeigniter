<!-- Conteudos especificos separados por sessoes -->

<div class="row imobiliarias">

	<div class="columns small-12 medium-9 large-9">
	
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<div class="titulo_azul barra-azul">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">Imobiliárias</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
		
			<div class="columns small-12 medium-12 large-12">
				
				<label class="texto_padrao">
				
				{texto}
				
				</label>
				
			</div>
			
			<div class="columns small-12 medium-12 large-12">
			
				<ul class="small-block-grid-3 medium-block-grid-3 large-block-grid-3 lista_logos">
					
					{imobiliarias}
						<li>
							<div class="logo_imobiliaria">
								<a href="{link}" title="{titulo}" target="_blank"><img src="{base_url}assets/uploads/imobiliarias/{imagem}"></a>
							</div>
						</li>
					{/imobiliarias}
					
				</ul>
			
			</div>
			
		</div>
	
	</div>
	
	<div class="columns small-12 medium-3 large-3">
		
		{anuncios}
		
	</div>
	
</div>

{noticias}

<!-- Fim dos conteudos especificos -->