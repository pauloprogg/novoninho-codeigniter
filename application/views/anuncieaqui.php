<!-- Conteudos especificos separados por sessoes -->

<div class="row anuncie">

	<div class="columns small-12 medium-9 large-9">
	
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<div class="titulo_azul barra-azul">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">Anuncie Aqui</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
		
			<div class="columns small-12 medium-12 large-12">
				
				<label class="texto_padrao">
				
				{texto1}
				
				</label>
				
			</div>
			
		</div>
		
		<div class="row tipo_imagens">
		
			<div class="columns small-12 medium-4 large-4">
				<a href="{base_url}anuncie_proprietario"><img src="{base_url}assets/images/anuncie_proprietario.png"></a>
			</div>
			
			<div class="columns small-12 medium-4 large-4">
				<a href="{base_url}anuncie_corretor"><img src="{base_url}assets/images/anuncie_corretor.png"></a>
			</div>
			
			<div class="columns small-12 medium-4 large-4">
				<a href="{base_url}anuncie_imobiliaria"><img src="{base_url}assets/images/anuncie_imobiliarias.png"></a>
			</div>
	
		</div>
		
		<div class="row" style="margin-top: 30px">
			<div class="columns small-12 medium-12 large-12">
				<div class="titulo_azul barra-azul">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">Visita Acompanhada</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
		
			<div class="columns small-12 medium-12 large-12">
				
				<label class="texto_padrao">
				
				{texto2}
				
				</label>
				
			</div>
			
		</div>
	
	</div>
	
	<div class="columns small-12 medium-3 large-3">
		
		{anuncios}
		
	</div>
	
</div>

<!-- Fim dos conteudos especificos -->