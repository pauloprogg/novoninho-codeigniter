<!-- Conteudos especificos separados por sessoes -->

<div class="imovel">
	{dados}
	<div class="row">

		<div class="columns small-12 medium-9 large-9">
		
			<div class="row">
				<div class="columns small-12 medium-12 large-12">
				
					<div class="{titulo_tipo} {barra_tipo}">
					
						<div class="titulo_conteudo">
							<h2 class="titulo_box">{titulo_pagina}</h2>
						</div>
						
						<div class="botao_voltar">
							<a href="{base_url}{link_busca}"><img src="{base_url}assets/images/btn_voltar.png"></a>							
						</div>
						
					</div>
					
				</div>
			</div>
			
			<div class="row">
			
				<div class="columns small-12 medium-12 large-12">
					
					<div class="row">
					
						<div class="columns small-12 medium-7 large-7">
							<h2 class="preto">{titulo}</h2>
						</div>
						
						<div class="columns small-12 medium-5 large-5 wish_add" style="{wish_add}">
							<a data-id="{id}" data-act="adicionarFavorito" data-logado="{logado}" class="wish_imovel"><img src="{base_url}assets/images/wish-adicionar.png"></a>
						</div>
						
						<div class="columns small-12 medium-5 large-5 wish_remove" style="{wish_remover}">
							<a data-id="{id}" data-act="removerFavorito" data-logado="{logado}" class="wish_imovel"><img src="{base_url}assets/images/wish-remover.png"></a>
						</div>
					
					</div>
					
					<label class="texto_padrao">
					
					<p>{descricao1}</p>
					
					</label>
					
					<div class="row">
					
						<div class="columns small-12 medium-12 large-12">
						
							<div class="galeria">
							
								<div class="imagem_principal">
									<img src="{imagem_principal}">
								</div>
								
								<div class="fotos">
								
									<ul id="carrousel_fotos_imovel">
									
										{galeria}
								
										<li>
											<a class="galeria_foto">
												<img src="{arquivo}">
											</a>
										</li>
										
										{/galeria}
										
									</ul>									
									
								</div>
								
								<div class="control">
								
									<div class="botoes">
										<a class="prev_vermelho" id="prev_galeria"></a>
										<a class="pause_vermelho" id="pause_galeria"></a>
										<a class="next_vermelho" id="next_galeria"></a>
									</div>
								
								</div>
							
							</div>
						
						</div>
					
					</div>

					<div class="row">
					
						<div class="columns small-12 medium-4 large-4">
						
							<div class="dados">
							
								<div class="preco">
									<span>Valor:</span>
									<h2>R$ {valor_br}</h2>
								</div>
								
								<div class="informacoes">
                                	<p><b>Nº:</b>#{id}</p>
									<p><b>Finalidade:</b> {finalidade_texto}</p>
									{numero_referencia}
									<p><b>Área útil (m2):</b> {area}</p>
									<p><b>Valor m2:</b> R$ {valor_m2}</p>
									<p><b>Dormitórios:</b> {dormitorios} quarto(s)</p>
									<p><b>Suítes:</b> {suites} suite(s)</p>
									<p><b>Garagem:</b> {garagem} lugare(s)</p>
                                    <p><b>Banheiros:</b> {banheiros}</p>
									<p><b>Condomínio:</b> {condominio}</p>
								</div>
							
							</div>
						
						</div>
						
						<div class="columns small-12 medium-8 large-8">
								
							<div class="dados">
							
								<div class="texto">
					
									<p><b>Informações do condomínio: </b>{descricao2}</p>
					
								</div>
								
								<div class="redes">
									<img src="{base_url}assets/images/imovel_facebook.jpg">
									<img src="{base_url}assets/images/imovel_twitter.jpg">
									<img src="{base_url}assets/images/imovel_google.jpg">
								</div>
								
							</div>
								
						</div>
					
					</div>
					
					<h2 class="preto2"> CONTATO </h2>
					
					<div class="row box_contato">
					
						<div class="columns small-12 medium-12 large-12">
						{contato}
							<div class="contato">
								
								<div class="row">
								
									<div class="columns small-12 medium-12 large-12 contato_realizado">
										
										<label>Contato enviado com sucesso! Aguarde uma resposta dentro de pouco tempo!</label>
										
									</div>
									
									<div class="columns small-12 medium-6 large-6">
									
										{nome}
										<p><b>E-mail: </b> {email} </p>
										<p><b>Fone: </b> {telefone} </p>
										<p><b>Melhor Horario para Ligar: </b> {melhor_horario} </p>
										
										<p class="texto">{descricao3}</p>
									
									</div>
									
									<div class="columns small-12 medium-6 large-6">
									
										<div class="formulario">
											<form id="contato_imovel" action="{base_url}contato/contatoImovel" method="post">
											
												<input type="hidden" name="email_contato" id="email_contato" value="{email}">
												<input type="hidden" name="imovel_titulo" id="imovel_titulo" value="{titulo}">
												<input type="hidden" name="slug" id="slug" value="{slug}">
											
												<label>Nome:</label>
												<input class="campo" type="text" name="nome" id="nome">
												
												<label>Email:</label>
												<input class="campo" type="text" name="email" id="email">
												
												<label>Mensagem/Telefone::</label>
												<textarea class="campo" name="mensagem" id="mensagem"></textarea>
												
												<input class="botao" type="image" src="{base_url}assets/images/btn_enviar_azul.png">
												
											</form>
										</div>
									
									</div>
									
								</div>
								
							</div>
						{/contato}	
						</div>
					
					</div>
					
				</div>
				
			</div>
			
		</div>
		
		<div class="columns small-12 medium-3 large-3">
			
			{anuncios}
			
		</div>
		
	</div>
	
    {mapa_google}
	
	{/dados}
</div>

<!-- Fim dos conteudos especificos -->