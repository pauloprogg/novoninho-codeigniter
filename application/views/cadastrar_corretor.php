<!-- Conteudos especificos separados por sessoes -->

<div class="row cadastrar">

	<div class="columns small-12 medium-9 large-9">
	
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<div class="titulo_azul barra-azul">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">Passo a Passo</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
		
			<div class="columns small-12 medium-12 large-12">
				
				<div class="row passo">
					<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
					
						<div class="row">
						
							<div class="columns small-12 medium-3 large-3">
								<div class="imagem">
									<img src="{base_url}assets/images/passo1.png">
								</div>
							</div>
							
							<div class="columns small-12 medium-9 large-9">
								<div class="texto">
									<span>Crie uma <br> conta no site</span>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>
				
				<div class="row passo">
					<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
					
						<div class="row">
						
							<div class="columns small-12 medium-3 large-3">
								<div class="imagem">
									<img src="{base_url}assets/images/passo2.png">
								</div>
							</div>
							
							<div class="columns small-12 medium-9 large-9">
								<div class="texto">
									<span>Cadastre <br> seu imóvel</span>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>
				
				<div class="row passo">
					<div class="small-10 large-10 medium-10 large-centered small-centered medium-centered columns">
					
						<div class="row">
						
							<div class="columns small-12 medium-3 large-3">
								<div class="imagem">
									<img src="{base_url}assets/images/passo3.png">
								</div>
							</div>
							
							<div class="columns small-12 medium-9 large-9">
								<div class="texto">
									<span>Escolha forma de <br> pagamento e publique</span>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>
				
			</div>
			
		</div>
		
		<div class="row">
			<div class="columns small-12 medium-12 large-12">
				<div class="titulo_azul barra-azul">
					<div class="titulo_conteudo">
						<h2 class="titulo_box">Entrar</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
		
			<div class="columns small-12 medium-12 large-12">
		
				<h2 class="preto">Formulário de Cadastro de Corretor</h2>
			
				<form id="cadastro_corretor" action="{base_url}cadastrar/cadastrar_corretor" method="post">
				
					<div class="row linha">
				
						<div class="columns small-12 medium-12 large-12">
							
							<label>CRECI:</label>
							<input class="campo" type="text" name="creci" id="creci">
							
						</div>
						
					</div>
			
					<div class="row linha">
				
						<div class="columns small-12 medium-12 large-12">
							
							<label>Nome:</label>
							<input class="campo" type="text" name="nome" id="nome">
							
						</div>
						
					</div>
					
					<div class="row linha">
				
						<div class="columns small-12 medium-12 large-12">
							
							<label>E-mail:(Este será seu Usuário / Login)</label>
							<input class="campo" type="text" name="email" id="email">
							
						</div>
					
					</div>

					<div class="row linha">
				
						<div class="columns small-6 medium-6 large-6">
							
							<label>CPF / CNPJ: (Apenas para emissão de nota fiscal)</label>
							<input class="campo cpf" type="text" name="cpf_cnpj" id="cpf_cnpj">
							
						</div>
						
						<div class="columns small-6 medium-6 large-6">
						
							<div class="row">
								<div class="columns small-6 medium-6 large-6">
									<br/>
									<label><input type="radio" name="check_cpf_cnpj" value="1" class="check_mascara" checked> CPF</label>
								</div>
								<div class="columns small-6 medium-6 large-6">
									<br/>
									<label><input type="radio" name="check_cpf_cnpj" value="2" class="check_mascara"> CNPJ</label>
								</div>
							</div>
							
						</div>
						
					</div>
					
					<div class="row linha">
				
						<div class="columns small-12 medium-12 large-12">
							
							<label>Melhor Horario para Ligar:</label>
							<input class="campo" type="text" name="melhor_horario" id="melhor_horario">
							
						</div>
					
					</div>
					<div class="row linha">
				
						<div class="columns small-12 medium-5 large-5 columns">
							
							<label>Como Soube?</label>
							<select name="como_soube" class="campo">
								<option value="">Escolha</option>
								<?php
									$query = array(
										'campos' => 'texto',
										'tabela' => 'como_soube',
										'where' => array()
									);
									
									$this->select->set($query);
									$respostas = $this->select->resultado();
									foreach ($respostas as $resposta) {
										echo '<option value="'.$resposta->texto.'">'.$resposta->texto.'</value>';
									}
								?>
							</select>
							
						</div>
						<div class="columns small-12 medium-5 large-5 columns">
							
							<label>Vendedor:</label>
							<select name="vendedor" class="campo">
								<option value="">Escolha</option>
								<?php
									$query = array(
										'campos' => 'nome',
										'tabela' => 'vendedores',
										'where' => array('ativo' => 1)
									);
									
									$this->select->set($query);
									$respostas = $this->select->resultado();
									foreach ($respostas as $resposta) {
										echo '<option value="'.$resposta->nome.'">'.$resposta->nome.'</value>';
									}
								?>
							</select>
							
						</div>
					
					</div>

					
					<div class="row linha">
				
						<div class="columns small-12 medium-12 large-12">
							
							<label>Telefone:</label>
							<input class="campo telefone" type="text" name="telefone" id="telefone">
							
						</div>
						
					</div>
					
					<div class="row linha">
				
						<div class="columns small-12 medium-6 large-6">
							
							<label>Senha:</label>
							<input class="campo" type="password" name="senha" id="senha">
							
						</div>
					
						<div class="columns small-12 medium-6 large-6">
						
							<label>Confirmar Senha:</label>
							<input class="campo" type="password" name="confirmar_senha" id="confirmar_senha">
							
						</div>
						
					</div>
					
					<div class="row linha contrato">
				
						<div class="columns small-12 medium-12 large-12">
							
							<label>
								<input type="checkbox" name="contrato" id="contrato" value="1">
								<span>Li e concordo com os <a href="#" data-reveal-id="modalTermos" style="font-weight: bold">Termos do Contrato</a></span>
							</label>
							
						</div>
						
					</div>
					<div class="row linha contrato">
				
						<div class="columns small-12 medium-12 large-12">
							
							<label>
								<input type="checkbox" name="newsletter" id="newsletter" value="1">
								<span><span style="font-weight: bold;color: #008cba;">Aceito e quero receber novidades do Novo Ninho</span></span>
							</label>
							
						</div>
						
					</div>
					
					<div class="row linha">
					
						<div class="small-12 large-12 medium-12 columns">
						
							<input type="submit" class="btn_azul" value="Cadastrar">
						
						</div>
						
					</div>
					
				</form>
				
			</div>
			
		</div>
		
	</div>
	
	<div class="columns small-12 medium-3 large-3">
		
		{anuncios}
		
	</div>
	
</div>



{modal_bemvindo}

<!-- Fim dos conteudos especificos -->