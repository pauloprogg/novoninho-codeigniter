<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Imovel extends Main_Controller {



	public function show( $slug ) {
		//print_r('AKI');
		//exit();



		$origem = $this->input->get( "origem", true );



		$config = array(

			'campos' => 'imo.*, tip.tipo as "texto_tipo"',

			'tabela' => 'imoveis imo',

			'join' => array(

						array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left')

					),

			'where' => array('imo.slug' => $slug),

			'limit' => 1

		);

		

		$this->select->set($config);

		$val = $r = $this->select->total();

		

		if(empty($slug) || $val == 0) {

			redirect( base_url().'home' );	

		}

		

		$r = $this->select->resultado();

		

		$imovel = $r[0];



		//se o imovel não estiver aprovado:

		if($imovel->aprovado == '0') {

			redirect( base_url().'home' );	

		}

		

		if($imovel->condominio == 0) { $condo = 'Sim'; } 

		if($imovel->condominio == 1) { $condo = 'Não'; } 



		if($imovel->tipo == 1) { $t = 'Venda'; $titulo_tipo = 'titulo_vermelho'; $barra_tipo = 'barra-vermelho'; } 

		if($imovel->tipo == 2) { $t = 'Locação'; $titulo_tipo = 'titulo_azul'; $barra_tipo = 'barra-azul'; } 

		if($imovel->tipo == 3) { $t = 'Temporada'; $titulo_tipo = 'titulo_cinza'; $barra_tipo = 'barra-cinza'; } 

		

		//finalidade

		$config = array(

			'campos' => 'tipo',

			'tabela' => 'finalidade',

			'where' => array('id' => $imovel->finalidade)

		);



		$this->select->set($config);

		$finalidade = $this->select->resultado();

		

		$titulo_pagina = $imovel->texto_tipo.' // '.$t;

		

		$valor = $this->util->moeda2br($imovel->valor);

		

		if( $imovel->area_m2 > 0 ) {

		

			$valor_m2 = $imovel->valor / $imovel->area_m2;

			$valor_m2 = $this->util->moeda2br($valor_m2);

			

		} else {

		

			$valor_m2 = 0;

		

		}

		

		$area = $this->util->moeda2br($imovel->area_m2);

		$area = str_replace('.', '', $area);



		/* Galeria */



		$config = array(

			'campos' => 'arquivo',

			'tabela' => 'imagens',

			'where' => array('id_imovel' => $imovel->id),

			'orderBy' => array( 'destaque' => 'DESC', 'id' => 'DESC')

		);



		$this->select->set($config);

		$num_img = $this->select->total();

		$img = $this->select->resultado();



		/* Verifica se a imagem existe */

		foreach($img as $x => $val) {



			$arquivo = trim($val->arquivo);

			$path = '/assets/uploads/imovel/'.$imovel->id_usuario.'/';



			if(file_exists(getcwd().$path.$arquivo)) {

				$img[$x]->arquivo = base_url().$path.$arquivo;

			} else {

				$img[$x]->arquivo = $arquivo;

			}



		}

		

		$config = array(

			'campos' => 'arquivo',

			'tabela' => 'imagens',

			'where' => array('id_imovel' => $imovel->id, 'destaque' => 1),

			'limit' => 1

		);



		$this->select->set($config);

		$img_dest = $this->select->resultado();



		if( !empty($img_dest) ) {

			$imagem_principal = '/assets/uploads/imovel/'.$imovel->id_usuario.'/'.trim($img_dest[0]->arquivo);

			$arq = $img_dest[0]->arquivo;

		} else

		if($num_img > 0) {

			$imagem_principal = '/assets/uploads/imovel/'.$imovel->id_usuario.'/'.trim($img[0]->arquivo);

			$arq = $img[0]->arquivo;

		} else {

			$imagem_principal = '/assets/images/sem_imagem.jpg';

			$arq = 'assets/images/sem_imagem.jpg';

		}



		if(file_exists( $_SERVER['DOCUMENT_ROOT'].$imagem_principal )) {

			$imagem_principal = base_url().$imagem_principal;

		} else {

			$imagem_principal = $arq;

		}



		$this->title = $imovel->titulo;

		$this->keywords = "";

		$this->description = "";



		/* Dados de Contato */

		

		$id_usuario = $imovel->id_usuario;

		

		$config = array(

					'campos' => 'tipo, perfil',

					'tabela' => 'usuarios',

					'where' => array('id' => $id_usuario)

				);

		

		$this->select->set($config);

		$user = $this->select->resultado();

		$tabela = $this->util->tabelaPerfil($user[0]->tipo);

		

		if ($imovel->usarDadosContato == 1 || $user[0]->tipo == 3 ) {

			$config = array(

				'campos' => 'nome, email, telefone, melhor_horario',

				'tabela' => $tabela,

				'where' => array('id' => $user[0]->perfil)

			);

			

			$this->select->set($config);

			$contato = $this->select->resultado();

			$nome = $contato[0]->nome;

			$contato[0]->nome = "<p><b>Nome: </b> ".$nome." </p>";

			$r[0]->email=$contato[0]->email;



		}else{

			$contato[0]->nome = "";

			$contato[0]->email = $imovel->email;

			$contato[0]->telefone = $imovel->fone;

			$contato[0]->melhor_horario = $imovel->melhor_horario;

		}



		$link_busca = $this->linkBusca($imovel);

		if( !empty($_SERVER[ 'QUERY_STRING' ]) ) {

			$link_busca = "classificados?".$_SERVER[ 'QUERY_STRING' ];

		}

		

		//Mapa

		

		$mapa_google = '';

		

		if($imovel->mapa == 1) {

			

			$config = array(



				'campos' => 'end.endereco_logradouro as "rua", ba.bairro_descricao as "bairro", cid.cidade_descricao as "cidade", est.uf_sigla as "estado", imo.complemento',

	

				'tabela' => 'imoveis imo',

	

				'join' => array(

	

							array('cep_endereco end','end.endereco_codigo = imo.rua','left'),

	

							array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left'),

							

							array('cep_cidade cid','cid.cidade_codigo = imo.cidade','left'),

							

							array('cep_uf est','est.uf_codigo = imo.estado','left')

	

						),

	

				'where' => array('imo.id' => $imovel->id)

	

			);



			$this->select->set($config);



			$m = $this->select->resultado();

			$mapa_codigo = $imovel->cep.'+'.$m[0]->rua.'+'.$imovel->numero.'+'.$m[0]->cidade.'+'.$m[0]->estado;

			$texto_end = $m[0]->rua.' '.$imovel->numero.', '.$m[0]->bairro.', '.$m[0]->cidade.' '.$m[0]->estado.' - Complemento: '.$m[0]->complemento;

			$mapa_google = '

			

			<div class="row">

			

				<h2 class="preto2" style="padding-left: 20px;"> LOCALIZAÇÃO - <a class="ativar_mapa"><span class="mapa_texto">Visualizar</span> Mapa do Endereço</a> </h2>

				

				<h2 class="preto2" style="padding-left: 20px;">'.$texto_end.'</h2>

				<div class="columns small-12 medium-12 large-12">

					

					<div class="mapa box_mapa box_mapa_hidden">

					

						<iframe width="970" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q='.$mapa_codigo.'&key=AIzaSyBEKFcYT22HGZOWyb2eNpykzJVCIgkf2uE"></iframe>

					

					</div>

					

				</div>

				

			</div>';

		

		}

		

		//Número de Referencia para imobiliarias

		$num_referencia = '';

		if(!empty($imovel->referencia_xml)) {

			$num_referencia = '<p><b>Nº Referência:</b> '.$imovel->referencia_xml.'</p>';

		}

		

		//Botão de Favoritar 

		$usuario = $this->session->userdata('login');

		

		if(!empty($usuario)) { //Verifica se está logado

		

			$logado = 1;

			

			//Verifica se o usuário tem esse imóvel favoritado

			$id_usuario = $usuario['id'];

			

			$config = array(

				'campos' => 'id',

				'tabela' => 'imoveis_favoritos',

				'where' => array('id_usuario' => $id_usuario, 'id_imovel' => $imovel->id)

			);

		

			$this->select->set($config);

			$fav = $this->select->total();

			

			if($fav > 0) {

				$f_add = 'display: none';

				$f_remove = '';		

			} else {

				$f_add = '';

				$f_remove = 'display: none';		

			}

			

		} else {

			$f_add = '';

			$f_remove = 'display: none';

			$logado = 0;

		}



		$data = array(

			"condominio" => $condo,

			"slug" => $slug,

			"dados" => $r,

			"titulo_pagina" => $titulo_pagina,

			"numero_referencia" => $num_referencia,

			"finalidade_texto" => $finalidade[0]->tipo,

			"valor_br" => $valor,

			"valor_m2" => $valor_m2,

			"area" => $area,

			"imagem_principal" => $imagem_principal,

			"galeria" => $img,

			"contato" => $contato,

			"link_busca" => $link_busca,

			"titulo_tipo" => $titulo_tipo,

			"barra_tipo" => $barra_tipo,

			"mapa_google" => $mapa_google,

			"wish_add" => $f_add,

			"wish_remover" => $f_remove,

			"logado" => $logado,

			"cep" => $imovel->cep,

			"id_usuario" => $id_usuario

		);

		//print_r($data);die();

		$this->load( 'imovel', $data );



	}

	

	public function linkBusca( $i ) {

	

		$tipo = $i->tipo;

		$id_tipo_imovel = $i->id_tipo_imovel;

		$finalidade = $i->finalidade;

		$estado = $i->estado;

		$cidade = $i->cidade;

		$condominio = $i->condominio;

		

		$link = 'classificados?tipo='.$tipo.'&tipo_imovel='.$id_tipo_imovel.'&finalidade='.$finalidade.'&estado='.$estado.'&cidade[]='.$cidade.'&condominio='.$condominio;;

		

		return $link;

	

	}

	

	public function adicionarFavorito() {

		

		$id_imovel = $this->input->post('id_imovel', TRUE);

		

		$usuario = $this->session->userdata('login');

		

		$id_usuario = $usuario['id'];

		

		//Remove o vinculo de favorito para esse imovel

		$this->db->query('DELETE FROM imoveis_favoritos WHERE id_usuario = "'.$id_usuario.'" AND id_imovel = "'.$id_imovel.'" ');

		

		$this->db->query('INSERT INTO imoveis_favoritos (id_usuario, id_imovel, data_cadastro) VALUES("'.$id_usuario.'", "'.$id_imovel.'", "'.time().'") ');

			

	}

	

	public function removerFavorito() {

		

		$id_imovel = $this->input->post('id_imovel', TRUE);

		

		$usuario = $this->session->userdata('login');

		

		$id_usuario = $usuario['id'];

		

		//Remove o vinculo de favorito para esse imovel

		$this->db->query('DELETE FROM imoveis_favoritos WHERE id_usuario = "'.$id_usuario.'" AND id_imovel = "'.$id_imovel.'" ');

			

	}

	

}