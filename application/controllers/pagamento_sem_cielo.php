<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pagamento extends Main_Controller {

	private $valor;
	private $numPagamento;

	public function index() {

		$this->checaSessao();

		$this->calculaValor();

		if($this->valor > 0){
			$this->pagseguro();
		} else {
			redirect( base_url().'painel-proprietario' );
		}
		
	}

	public function OLDcalculaValor() {

		$us = $this->session->userdata('login');
		
		$precos = array(
			'campos' => 'preco_normal, preco_destaque, preco_super_destaque, preco_feirao, preco_destaque_feirao, preco_super_destaque_feirao',
			'tabela' => 'usuarios',
			'where' => array('id' => $us['id'])
		);
		
		$this->select->set($precos);
		$preco = $this->select->resultado();
		
		$preco_normal = $preco[0]->preco_normal;
		$preco_destaque = $preco[0]->preco_destaque;
		$preco_super_destaque = $preco[0]->preco_super_destaque;
		$preco_feirao = $preco[0]->preco_feirao;
		$preco_destaque_feirao = $preco[0]->preco_destaque_feirao;
		$preco_super_destaque_feirao = $preco[0]->preco_super_destaque_feirao;

		
		$p = $this->session->userdata('publicar');
		
		$preco_total = 0;
		
		foreach($p as $val) {
		
			$id = $val['id'];
			
			
			$preco = $this->util->moeda2br($preco_normal);
			$preco_total += $preco_normal;

			
			if($val['feirao'] == 1){
				$preco = $preco_feirao;
				

				$preco_total += $preco_feirao;
			}

			if($val['destaque'] == 1){
				$preco = $preco_destaque;
			

				$preco_total += $preco_destaque;
			}

			if($val['destaque_feirao'] == 1){
				$preco = $preco_destaque_feirao;
				

				$preco_total += $preco_destaque_feirao;
			}


			if($val['super_destaque'] == 1){
				$preco = $preco_super_destaque;
			

				$preco_total += $preco_super_destaque;
			}
			
			if($val['super_destaque_feirao'] == 1){
				$preco = $preco_super_destaque_feirao;
			

				$preco_total += $preco_super_destaque_feirao;	
			}

		}
		
		$this->valor = $preco_total;

	}

	public function calculaValor() {

		$us = $this->session->userdata('login');
		
		$precos = array(
			'campos' => 'preco_normal, preco_destaque, preco_super_destaque, preco_feirao, preco_destaque_feirao, preco_super_destaque_feirao',
			'tabela' => 'usuarios',
			'where' => array('id' => $us['id'])
		);
		
		$this->select->set($precos);
		$preco = $this->select->resultado();
		
		$preco_normal = $preco[0]->preco_normal;
		$preco_destaque = $preco[0]->preco_destaque;
		$preco_super_destaque = $preco[0]->preco_super_destaque;
		$preco_feirao = $preco[0]->preco_feirao;
		$preco_destaque_feirao = $preco[0]->preco_destaque_feirao;
		$preco_super_destaque_feirao = $preco[0]->preco_super_destaque_feirao;

		
		$p = $this->session->userdata('publicar');
		
		$preco_total = 0;
		
		foreach($p as $k => $val) {
		
			$id = $val['id'];
			
			$preco = $this->util->moeda2br($preco_normal);
				

				$preco_total += $preco_normal;

			
			if($val['feirao'] == 1){
				$preco = $preco_feirao;
				

				$preco_total += $preco_feirao;
			}

			if($val['destaque'] == 1){
				$preco = $preco_destaque;
			

				$preco_total += $preco_destaque;
			}

			if($val['destaque_feirao'] == 1){
				$preco = $preco_destaque_feirao;
				

				$preco_total += $preco_destaque_feirao;
			}


			if($val['super_destaque'] == 1){
				$preco = $preco_super_destaque;
				

				$preco_total += $preco_super_destaque;
			}
			
			if($val['super_destaque_feirao'] == 1){
				$preco = $preco_super_destaque_feirao;
				

				$preco_total += $preco_super_destaque_feirao;	
			}	

			$data_expira = 	$this->DataExpira($us['id']);

			if($preco == 0) {
				$data = array(
					'status' => 3,
					'super_destaque' => $val['super_destaque'],
					'destaque' => $val['destaque'],
					'super_destaque_feirao' => $val['super_destaque_feirao'],
					'destaque_feirao' => $val['destaque_feirao'],
					'feirao' => $val['feirao'],
					'aprovado' => 0,
					'data_expira' => $data_expira
				);
				
				$this->master_model->update_form('imoveis', $data, $id);
				unset($p[$k]);
			} else {
				$preco_total += $preco;	
			}

		}
		
		$newdata = array(
			'publicar' => $p
		);
		$this->session->set_userdata($newdata);
		
		$this->valor = $preco_total;

	}

	private function alterarStatusImovel() {
		/* Update no status do imovel */
		$config = array(
			'campos' => 'id_imovel, destaque, super_destaque, feirao, destaque_feirao, super_destaque_feirao',
			'tabela' => 'pagamentos_imoveis',
			'where' => array('id_pagamento' => $this->numPagamento)
		);
			
		
		$this->select->set($config);
		$imoveis = $this->select->resultado();
				
		foreach($imoveis as $val) {	
			$dados = array(
				'status' => 2,
				'destaque' => $val->destaque,
				'super_destaque' => $val->super_destaque,
				'feirao' => $val->feirao,
				'destaque_feirao' => $val->destaque_feirao,
				'super_destaque_feirao' => $val->super_destaque_feirao

			);

			$id_imovel = $val->id_imovel;

			$this->master_model->update_form('imoveis', $dados, $id_imovel);
		}
	
		
	}

	private function salvarPagamento(){

		/* Salvar dados do pagamento */
		
		$login = $this->session->userdata( "login" );
		
		$id_usuario = $login['id'];
		$data_cadastro = time();
		
		$dados = array(
			
			'id_usuario' => $id_usuario,
			'valor' => $this->valor,
			'data_cadastro' => $data_cadastro,
			'tipo' => 0
		
		);
		
		$id_pag = $this->master_model->cadastrar_form('pagamentos',$dados);
		
		$this->numPagamento = $id_pag;


			/* Salvar dados de imóveis */

		$publicar = $this->session->userdata( "publicar" );
		
		foreach($publicar as $v) {
		
			$id_imovel = $v['id'];
			$destaque = $v['destaque'];
			$super_destaque = $v['super_destaque'];
			$feirao = $v['feirao'];
			$destaque_feirao = $v['destaque_feirao'];
			$super_destaque_feirao = $v['super_destaque_feirao'];
			$preco = $v['preco'];
			$data_cadastro = time();
			
			$dados = array(
			
				'id_pagamento' => $id_pag,
				'id_imovel' => $id_imovel,
				'preco' => $v['preco'],
				'destaque' => $destaque,
				'super_destaque' => $super_destaque,
				'feirao' => $feirao,
				'destaque_feirao' => $destaque_feirao,
				'super_destaque_feirao' => $super_destaque_feirao,
				'data_cadastro' => $data_cadastro
			
			);
			
			$id_pag_mov = $this->master_model->cadastrar_form('pagamentos_imoveis',$dados);
		}
	}

	public function pagseguro() {

		$this->salvarPagamento();

		require_once "PagSeguroLibrary/PagSeguroLibrary.php";

		$test = false;

		$email = "pagseguro@novoninho.com.br";
		$token = "BBE82C5011E341D495A9F306AFD95E28";

		// $email = "luca@sorocabacom.com.br";
		// $token = "2255ACF11C1142C0823A0FA2B13B4B10";

		$paymentRequest = new PagSeguroPaymentRequest();

		$loginUser = $this->session->userdata( "login" );
		$idUser = $loginUser['id'];

		$paymentRequest->setReference( $this->numPagamento.'X'.$idUser );
		
		$publicar = $this->session->userdata( "publicar" );

		foreach( $publicar as $p ) {

			$paymentRequest->addItem( $p['id'], 'Anuncio - "'.$p['titulo'].'"', 1, $p['preco'] );

		}

		$login = $this->session->userdata( "login" );
		
		//$paymentRequest->setSender();

		$paymentRequest->setCurrency("BRL");
		$paymentRequest->setShippingType(1);

		$paymentRequest->setExtraAmount( 0 );

		$shipping = new PagSeguroShipping();
		$type = new PagSeguroShippingType(3); // objeto PagSeguroShippingType
		$shipping->setType($type);
		$shipping -> setCost( 0 );

		$paymentRequest->setShipping($shipping);

		$paymentRequest->setRedirectUrl( base_url()."painel-proprietario" );

		$credentials = new PagSeguroAccountCredentials(
			$email, // e-mail
			$token // token
		);

		try {

			$this->alterarStatusImovel();

			redirect( $paymentRequest->register( $credentials ) );

		} catch( PagSeguroServiceException $e ) {

			if( $test ) {
				
				echo "Error - <br/>";
				echo "<pre>";
				print_r( $e );
				echo "</pre>";

			} else {

				redirect( base_url().'painel-proprietario' ); //Se for corretor ele vai mandar pro painel de corretor

			}

		}

	}

	public function retorno_pagseguro() {
		
			error_reporting(E_ALL);

			require_once "PagSeguroLibrary/PagSeguroLibrary.php";
	
			$email = "pagseguro@novoninho.com.br";
			$token = "BBE82C5011E341D495A9F306AFD95E28";
	
			// $email = "luca@sorocabacom.com.br";
			// $token = "2255ACF11C1142C0823A0FA2B13B4B10";
	
			$pagseguros = array(
				'pagseguro_email' => $email,
				'pagseguro_token' => $token
			);
	
			$data_pedido = array(
				"dataStatus" => time()
			);
	
			$credentials = new PagSeguroAccountCredentials($pagseguros['pagseguro_email'], $pagseguros['pagseguro_token']);
			$test = false;
	
			if($test){
				$_POST['notificationCode'] = '583DE522-6CCA-494C-A4BC-9D748D5DF5BB';
				$_POST['notificationType'] = 'transaction';
			}
	
			if(isset($_POST['notificationType']) && isset($_POST['notificationCode'])){
	
				$code = $_POST['notificationCode'];
				$type = $_POST['notificationType'];
	
				if($test){
					$transaction = PagSeguroTransactionSearchService::searchByCode($credentials, $code);
					//$transaction = PagSeguroNotificationService::checkTransaction($credentials, $code);
				}else {
					if ($type == 'transaction') {
						$transaction = PagSeguroNotificationService::checkTransaction($credentials, $code);
					}
	
				}
				
				$transactionId = $transaction->getCode();
				$transactionDate = substr($transaction->getDate(), 0, 10);
				$transactionLastEventDate = substr($transaction->getLastEventDate(), 0, 10);
				$transactionCode = $transaction->getCode();
				$transactionReference = $transaction->getReference();
	
				$status = $transaction->getStatus();
				$transactionStatus = $status->getValue();
	
				$paymentMethod = $transaction->getPaymentMethod();
				$type = $paymentMethod->getType();
				$transactionPaymentMethodType = $type->getValue();
	
				$code = $paymentMethod->getCode();
				$transactionPaymentMethodCode = $code->getValue();
	
				$transactionGrossAmount = $transaction->getGrossAmount();
				$transactionInstallmentCount = $transaction->getInstallmentCount();
	
				$InfSender = $transaction->getSender();
				$transactionSenderEmail	= $InfSender->getEmail();
				$transactionSenderName	= $InfSender->getName();
				
				$Infshipping 		= $transaction->getShipping();  
				$Infaddress 		= $Infshipping->getAddress();
				
				$CliNome 			= $InfSender 	-> getName();
				$CliEmail 			= $InfSender 	-> getEmail();
				$CliTelefone 		= $InfSender 	-> getPhone()	-> getareaCode() . $InfSender -> getPhone() -> getNumber();
				
				$CliEndereco 		= $Infaddress 	-> getStreet();
				$CliNumero 			= $Infaddress 	-> getNumber();
				$CliComplemento 	= $Infaddress 	-> getComplement();
				$CliBairro 			= $Infaddress 	-> getDistrict();
				$CliCidade 			= $Infaddress 	-> getCity();
				$CliEstado 			= $Infaddress 	-> getState();
				$CliCEP 			= $Infaddress 	-> getPostalCode();
	
				$shipping = $transaction -> getShipping();
				$transactionShippingType = $shipping->getType()->getTypeFromValue();//getType();
				$transactionShippingCost = $shipping->getCost();
	
				$transactionItemCount = 0;
				$items = $transaction->getItems();
				
				$Extras 			= '0,00';
				$TipoFrete 			= 'FR';
				$ValorFrete 		= '0,00';
				$Anotacao 			= '';
				
				switch ($transactionPaymentMethodType) {
					case 1:
						$TipoPagamento = "Cartão de crédito";
						break;
					case 2:
						$TipoPagamento = "Boleto";
						break;
					case 3:
						$TipoPagamento = "Débito online (TEF)";
						break;
					case 4:
						$TipoPagamento = "Saldo PagSeguro";
						break;
					case 5:
						$TipoPagamento = "Oi Paggo";
						break;
				}
				
				$dados_retorno = $transactionReference;
				$DR = explode('X', $dados_retorno);
				$id_pagamento = $DR[0];
				$id_usuario = $DR[1];
	
				/* Update na Tabela de Pagamentos */
				
				$data = array(	
					'hash' => $transactionCode
				);
	
				$this->master_model->update_form('pagamentos', $data, $id_pagamento);
	
				$s = NULL;
				switch ($transactionStatus) {
					case 1:
						$statusPagamento = "Aguardando pagamento";
					break;
					case 2:
						$statusPagamento = "Em análise";
					break;
					case 3:
						$statusPagamento = "Pago";
						$s = 'pago';
					break;
					case 4:
						$statusPagamento = "Pago";
						$s = 'pago';
					break;
					case 5:
						$statusPagamento = "Em disputa";
					break;
					case 6:
						$statusPagamento = "Devolvida";
					break;
					case 7:
						$statusPagamento = "Cancelada";
						$s = 'cancelado';
					break;
				}
				
				/* Update na tabela do pagseguro */
				
				$resultado = $this->db->query("SELECT p.* FROM `pagseguro` p WHERE p.TransacaoID = '".$transactionId."'");
				
				/* ========================= */	
				
				if($resultado->num_rows() == 0){		
					$query = $this->db->query("
					INSERT into pagseguro (
						data_cadastro,
						usuario,
						TransacaoID,
						Referencia,
						Extras,
						TipoFrete, 
						ValorFrete, 
						Anotacao,
						DataTransacao,
						TipoPagamento, 
						StatusTransacao,
						Status,
						
						CliNome, 
						CliEmail, 
						CliEndereco, 
						CliNumero, 
						CliComplemento, 
						CliBairro, 
						CliCidade, 
						CliEstado, 
						CliCEP, 
						CliTelefone
					) VALUES (
						'".time()."',
						'".$id_pagamento."',
						'".$transactionId."',
						'".$transactionReference."',
						'".$Extras."',
						'".$TipoFrete."',
						'".$ValorFrete."',
						'".$Anotacao."',
						'".$transactionDate."',
						'".$TipoPagamento."',
						'".$statusPagamento."',
						'".$transactionStatus."',
						
						'".$CliNome."',
						'".$CliEmail."',
						'".$CliEndereco."',
						'".$CliNumero."',
						'".$CliComplemento."',
						'".$CliBairro."',
						'".$CliCidade."',
						'".$CliEstado."',
						'".$CliCEP."',
						'".$CliTelefone."'
					)");
				}
				
				$resultado = $this->db->query("UPDATE `pagseguro` SET StatusTransacao = '".$statusPagamento."' WHERE TransacaoID = '".$transactionId."';");
	
				/* Checagem do tipo de usuario */
				//Se for corretor, ja aprovar e mudar o status do imovel para aparecer no site
				
				$config = array(
					'campos' => 'us.tipo',
					'tabela' => 'pagamentos pag',
					'join' => array(
							array('usuarios us','us.id = pag.id_usuario','left')
						),
					'where' => array('pag.id' => $id_pagamento)
				);
		
				$this->select->set($config);
				$r = $this->select->resultado();
				
				$tipo = $r[0]->tipo; 
				//$s = 'pago';
				if($s == 'pago'){ //se estiver pago
	
					//if($tipo == 2) { //Corretor, não precisa de avaliação
					//	$aprovado = 1;
					//} else {
						$aprovado = 0; //agora corretor tambem precisa de avaliação
					//}
	
					$this->db->query("UPDATE pagamentos SET pago = 1 WHERE id = ".$id_pagamento); //muda o status do pagto na tabela Pagamentos
	
					$data_expira = 	$this->DataExpira($id_usuario);
					
					/* Update nos dados da tabela de imoveis por pagamentos */
					
					$this->db->query("UPDATE pagamentos_imoveis SET data_expira = $data_expira WHERE id_pagamento = ".$id_pagamento);
					
					/* Update nos imoveis */
					
					$config = array(
						'campos' => 'id_imovel, destaque, super_destaque, feirao, super_destaque_feirao, destaque_feirao',
						'tabela' => 'pagamentos_imoveis',
						'where' => array('id_pagamento' => $id_pagamento)
					);
					
					$this->select->set($config);
					$imoveis = $this->select->resultado();
					
					foreach($imoveis as $val) {
					
						$id_imo = $val->id_imovel;
						$destaque = $val->destaque;
						$super_destaque = $val->super_destaque;
						$feirao = $val->feirao;
						$destaque_feirao = $val->destaque_feirao;
						$super_destaque_feirao = $val->super_destaque_feirao;
						
						$data = array(
							'status' => 3,
							'destaque' => $destaque,
							'super_destaque' => $super_destaque,
							'feirao' => $feirao,
							'destaque_feirao' => $destaque_feirao,
							'super_destaque_feirao' => $super_destaque_feirao,
							'aprovado' => $aprovado,
							'data_expira' => $data_expira
						);
						
						$this->master_model->update_form('imoveis', $data, $id_imo);
					
					}
				} else if($s == 'cancelado') { //se estiver cancelado
	
					$this->db->query("UPDATE pagamentos SET pago = 2 WHERE id = ".$id_pagamento); //muda o status do pagto na tabela Pagamentos
	
					$config = array(
						'campos' => 'id_imovel',
						'tabela' => 'pagamentos_imoveis',
						'where' => array('id_pagamento' => $id_pagamento)
					);
					
					$this->select->set($config);
					$imoveis = $this->select->resultado();
					
					foreach($imoveis as $val) {
					
						$id_imo = $val->id_imovel;
						
						$data = array(
							'status' => 4
						);
						
						$this->master_model->update_form('imoveis', $data, $id_imo);
					
					}
				}
	
				// redirect( base_url() );
	
			} else {
	
				redirect( base_url() );
	
			}

	}

	public function checaSessao() {
	
		$sessao = $this->session->userdata('login');
		// var_dump($sessao);die;
		if(!isset($sessao) || $sessao['logado'] == 0) {
			redirect(base_url());
		}


		$p = $this->session->userdata('publicar');

		if( !$p ) {

			if( $sessao['tipo'] == '1' ) {

				$painel = 'painel-proprietario';

			} else
			if( $sessao['tipo'] == '2' ) {

				$painel = 'painel-corretor';

			}

			redirect( base_url() .$painel);

		}
	
	}
	
	public function DataExpira($id_usuario) {
		
		$query = array(
			'campos' => 'expiracao',
			'tabela' => 'usuarios',
			'where' => array('id' => $id_usuario)
		);
		
		$this->select->set($query);
		$dados = $this->select->resultado();
		
		$dt_ex = $dados[0]->expiracao;
		
		if(empty($dt_ex)) { //Se a data de expiração do usuario for vazia
			
			$query = array(
				'campos' => 'expiracao',
				'tabela' => 'config'
			);
			
			$this->select->set($query);
			$ex = $this->select->resultado();
			
			$dt_ex = $ex[0]->expiracao;
			
		}
	
		$hoje = time();
	
		$expira = mktime(0,0,0,date('m',$hoje), date('d', $hoje)+$dt_ex, date('Y', $hoje));
		
		return $expira;
		
	}

}