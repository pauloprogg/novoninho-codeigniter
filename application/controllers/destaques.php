<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Destaques extends Main_Controller {

	public function index() {

		

		$this->title = "Destaques";
		$this->keywords = "Destaques, Aqui";
		$this->description = "Essa é a Destaques";
		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller

		$data = array(
			
		);

		$this->load( 'destaques/index', $data );

	}

	public function canvas() {
		/*$imagens = $this->input->post('img_val');

		$count = 0;
		foreach($imagens as $img){
			$filteredData=substr($img, strpos($img, ",")+1);
	 
			//Decode the string
			$unencodedData=base64_decode($filteredData);
			 
			//Save the image
			file_put_contents('assets/uploads/destaques/destaques_'.md5($count.time()).'.png', $unencodedData);
			$count++;
		}*/
		// $img = '123123';

		$src = $this->input->post('img_val'); 
		
		$decoded = ""; 
		for ($i=0; $i < ceil(strlen($src)/256); $i++) 
		   $decoded = $decoded . base64_decode(substr($src,$i*256,256)); 
	    
	    $decoded;

		$dados = array(
			'img' => $decoded,
		);

		$this->load( 'destaques/canvas', $dados );
	}

}