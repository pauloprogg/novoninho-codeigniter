<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xml extends Main_Controller {

	//=========================== Modelo Viva Real - http://manual.vivareal.com/home
	public function xmlVivareal() {
		
		set_time_limit(0);
		
		$l = $this->session->userdata('login');
		$id_usuario = $l['id'];
		
		if(file_exists($_FILES["xml"]["tmp_name"])) {
			
			$time = time();
			$tmp_name = $_FILES["xml"]["tmp_name"];
			$name = $_FILES["xml"]["name"];
			
			$tamanho_arquivo = $_FILES["xml"]["size"];
			$tamanho_maximo = 10485760; //10MB
			
			if($tamanho_arquivo <= $tamanho_maximo) {
			
				$f = explode('.', $name);
				$ext = end($f);
				
				if($ext == 'xml') { //Verifica se é xml
				
					/* Salvar Arquivo XML */
					$uploads_dir = $_SERVER['DOCUMENT_ROOT'].'/novoninho/assets/uploads/xml/'.$id_usuario;
					
					if(!file_exists($uploads_dir)) {
						mkdir($uploads_dir);	
					}
					
					$nome_arquivo = $time.$name;
					if(move_uploaded_file($tmp_name, $uploads_dir.'/'.$nome_arquivo)) {
					
						/* Lendo o arquivo xml */
						$xml = simplexml_load_file(base_url().'assets/uploads/xml/'.$id_usuario.'/'.$nome_arquivo);
						
						//Validação do número de imóveis do xml
						$imoveis = $xml -> Listings -> Listing;
						$total_xml = count($imoveis);
						
						if($total_xml > 0) { //Tipo de xml válido
						
													
							//Verifica se o número de imóveis do xml não excedeu o limite
							$config = array(
								'campos' => 'id',
								'tabela' => 'imoveis',
								'where' => array('id_usuario' => $id_usuario)
							);
							
							$this->select->set($config);
							$total_imo = $this->select->total();
							
							//Total de imóveis publicados + total de imóveis do xml
							$total = $total_xml + $total_imo;
							
							//Puxa o número limite de imóveis da imobiliaria
							$config = array(
								'campos' => 'num_anuncios, tipo_xml',
								'tabela' => 'usuarios',
								'where' => array('id' => $id_usuario)
							);
							
							$this->select->set($config);
							$usuario = $this->select->resultado();
							$us_num = $usuario[0]->num_anuncios;
							
							if($total <= $us_num) {
								
								//Salvar registro no log de importações
								$dados = array(
									'id_usuario' => $id_usuario,
									'data_cadastro' => time(),
									'tipo_xml' => $usuario[0]->tipo_xml,
									'arquivo' => $nome_arquivo
								);
						
								$log = $this->master_model->cadastrar_form('log_xml',$dados);
							
								// Salvar dados do XML

								//removendo imoveis cadastrados via xml
								$this->db->query("DELETE FROM `imoveis` WHERE `id_usuario` = ".$id_usuario." AND `arquivo_xml` IS NOT NULL");
								
								foreach($imoveis as $imo) {
									
									$ref = $imo->ListingID; //Número de Referencia do xml
									
									//Verifica se o imovel ja esta no sistema e o deleta para atualizar seus dados
									/*
									$config = array(
										'campos' => 'id',
										'tabela' => 'imoveis',
										'where' => array('id_usuario' => $id_usuario, 'referencia_xml' => $ref)
									);
								
									$this->select->set($config);
									$total_ref = $this->select->total();
								
									if($total_ref > 0) { //Se achar o imovel de referencia, exclui o imovel
										
										$imovel_ref = $this->select->resultado();
										
										$this->master_model->delete_form('imoveis', $imovel_ref[0]->id, 'id');
										$this->master_model->delete_form('imagens', $imovel_ref[0]->id, 'id_imovel');
										
									}
									*/
									/* Dados Padrões */
									
									$data_cadastro = time();
									$data_expira = 0;
									$ativo = 1;
									$id_usuario = $id_usuario;
									$destaque = 0;
									$status = 3; //Publicado
									$aprovado = 1; //Aprovado
									$mapa = 0;
									$xml = 1;
									
									$titulo = $imo->Title;
		
									/* Checagem de Slug */
									
									$slug = $this->util->slug($titulo);	
									$slug = $this->checaSlug($slug, 'imoveis');	
						
									$descricao = $imo->Details->Description;
									
									//==== Tipo da Transação
									$tipo = 1; //Tipo Padrão
									$tipo_transacao = $imo->TransactionType;
									
									if(strstr($tipo_transacao, 'Rent')) {
										$tipo = 2;
									}
									if (strstr($tipo_transacao, 'Sale')) {
										$tipo = 1;
									}
									//========================
									
									//================== Tipo do Imóvel e Finalidade
									
									$tipo_imovel = 5; //Padrão
									$finalidade = 2; //Padrão
									$n = array();
									$n = explode('/', str_replace(' ', '', $imo->Details->PropertyType));
									
									$condominio = 1;
									for($i = 0; $i < count($n); $i++) { //Pula o primeiro valor, que é a finalidade
									
										if(!empty($n[$i])) {

										//==== Condominio Residencial / Apartamento
										if('apartamento'== $n[$i] || 'Apartamento'== $n[$i] || 'Condominio'== $n[$i] || 'Condo'== $n[$i] || 'Condomínio'== $n[$i] || 'condo'==$n[$i] || 'condominio'==$n[$i] || 'condom&iacute;nio'==$n[$i] || 'Condom&iacute;nio'==$n[$i]) {
											$condominio = 0;	
										}
									
											$num = 0;
										
											//Tipo do imovel
											$config = array(
												'campos' => 'id',
												'tabela' => 'tipo_imovel',
												'where' => 'tipo LIKE "%'.$n[$i].'%"'
											);
										
											$this->select->set($config);
											$num = $this->select->total();
											
											if($num > 0) {
											
												$t = $this->select->resultado();
												$tipo_imovel = $t[0]->id;
												continue;
												
											}
										
										}
										
									}
									
									//==== ID da Finalidade - Primeiro valor da array de PropertyType
									$config = array(
										'campos' => 'id',
										'tabela' => 'finalidade',
										'where' => 'tipo LIKE "%'.$n[0].'%"'
									);
								
									$this->select->set($config);
									$num = $this->select->total();
									
									if($num > 0) {
									
										$f = $this->select->resultado();
										$finalidade = $f[0]->id;
										
									}
									
									//=============================================
									
									//==== Garagem, dormitorios, banheiros e suites
									
									$dormitorios = 0;
									$suites = 0;
									$banheiros = 0;
									$garagem = 0;
									
									if(!empty($imo->Details->Bedrooms)) {
										$dormitorios = $imo->Details->Bedrooms;
									}
									
									if(!empty($imo->Details->Suites)) {
										$suites = $imo->Details->Suites;
									}
									
									if(!empty($imo->Details->Bathrooms)) {
										$banheiros = $imo->Details->Bathrooms;
									}
									
									if(!empty($imo->Details->Garage)) {
										$garagem = $imo->Details->Garage;
									}
									
									//===============================================
									
									//Area em metros 2
									
									$area_m2 = 0;
									if(!empty($imo->Details->ConstructedArea)) {
										$area_m2 = $imo->Details->ConstructedArea;
									}
									
									//Valor
									
									if($tipo_transacao == 'For Rent') {
										$valor = $imo->Details->RentalPrice;
									} else if ($tipo_transacao == 'For Sale') {
										$valor = $imo->Details->ListPrice;
									} else {
									
										$valor = 0;
										
										if(!empty($imo->Details->RentalPrice)) {
											$valor = $imo->Details->RentalPrice;
										}
										
										if(!empty($imo->Details->ListPrice)) {
											$valor = $imo->Details->ListPrice;
										}
										
									}
									
									//======== Dados de Endereço
									
									$estado = $_POST['estado'];
									$cidade = $_POST['cidade'];
									
									//Cep
									$config = array(
										'campos' => 'cidade_cep',
										'tabela' => 'cep_cidade',
										'where' => array('cidade_codigo' => $cidade)
									);
								
									$this->select->set($config);
									$cid = $this->select->resultado();
									
									$cep = $cid[0]->cidade_cep;
									
									//==============================
									
									/* Dados de Contato */
									
									$config = array(
										'campos' => '*',
										'tabela' => 'dados_imobiliaria',
										'where' => array('id' => $l['perfil'])
									);
								
									$this->select->set($config);
									$r = $this->select->resultado();
									
									$telefone = $r[0]->telefone;
									$email = $r[0]->email;
									
									$dados = array(
										
										'data_cadastro' => $data_cadastro,
										'ativo' => $ativo,
										'id_usuario' => $id_usuario,
										'status' => $status,
										'aprovado' => $aprovado,
										'destaque' => $destaque,
										'slug' => $slug,
										'titulo' => (string)$titulo,
										'descricao1' => (string)trim($descricao),
										'tipo' => $tipo,
										'id_tipo_imovel' => $tipo_imovel,
										'finalidade' => $finalidade,
										'dormitorios' => $dormitorios,
										'suites' => $suites,
										'garagem' => $garagem,
										'banheiros' => $banheiros,
										'usarDadosContato' => 2,
										'mapa' => $mapa,
										'area_m2' => $area_m2,
										'cep' => $cep,
										'cidade' => $cidade,
										'estado' => $estado,
										'condominio' => $condominio,
										'valor' => $valor,
										'fone' => $telefone,
										'email' => $email,
										'referencia_xml' => $ref,
										'arquivo_xml' => $nome_arquivo
										
									);
							
									$id_imovel = $this->master_model->cadastrar_form('imoveis',$dados);
									
									/* Fotos */
									$x = 1;
									$total_imagens = 10;
									if (!is_dir($_SERVER['DOCUMENT_ROOT'].'/novoninho/assets/uploads/imovel/'.$id_usuario)) {
										mkdir($_SERVER['DOCUMENT_ROOT'].'/novoninho/assets/uploads/imovel/'.$id_usuario);
									}
									foreach($imo->Media->Item as $foto) {
										if($x <= $total_imagens) {
											
											$destaque = 0;
											
											if($x == 1) {
												$destaque = 1;	
											}
											
											$foto = explode('?', $foto);
											$foto = $foto[0];
											$ex = explode('/', $foto);
											$nome_imagem = end($ex);
											
											$img = explode('.', $nome_imagem);
											
											$nome_imagem = $img[0]."-".md5(uniqid(rand(),true)).'.'.$img[1];
											
											$nova_imagem = $_SERVER['DOCUMENT_ROOT'].'/novoninho/assets/uploads/imovel/'.$id_usuario.'/'.$nome_imagem;
											if(file_put_contents($nova_imagem, file_get_contents($foto))) {
												//Salvar registro da imagem
												$dados = array(
													'ativo' => 1,
													'data_cadastro' => time(),
													'usuario' => $id_usuario,
													'id_imovel' => $id_imovel,
													'destaque' => $destaque,
													'arquivo' => $nome_imagem
												);
										
												$imagens = $this->master_model->cadastrar_form('imagens',$dados);	
											}
											
										}
										
										$x++;
										
									}
								
								}
								$msg = 'Imóveis importados com sucesso!';
								$status = 1;
							
							} else { //Excedeu o numero limite
							
								$num_restante = $us_num - $total_imo;
								
								$msg = 'Número de imóveis do xml ( '.$total_xml.' ) excedeu o número de imóveis restantes ( '.$num_restante.' ), suba um xml com menos imóveis ou entre em contato conosco.';
								$status = 0;
								
							}
							
						} else {
							
							$msg = 'Formato de arquivo xml inválido.';
							$status = 0;
							
						}
						
						// ==================================
						
					} else {
						
						$msg = 'Erro ao salvar o arquivo';
						$status = 0;
						
					}
					
				} else {
					
					$msg = 'Extensão do arquivo inválida.';
					$status = 0;
					
				}
				
			} else {
			
				$msg = 'Tamanho do arquivo excede o tamanho máximo (5MB).';
				$status = 0;
				
			}

		} else {
		
			$msg = 'Erro no arquivo.';
			$status = 0;
			
		}
		
		$json = '{"status" : '.$status.', "msg" : "'.$msg.'"}';
		
		header('Content-type: text/json');
		header('Content-type: application/json');

		echo $json;
		exit;
		
	}
	
	//=========================== Modelo I-Value - http://integracao.valuegaia.com.br/
	public function xmlIvalue() {

		set_time_limit(0);
		ini_set('memory_limit','1000M');

		$l = $this->session->userdata('login');

		$id_usuario = $l['id'];

		if(isset($_FILES["xml"]["tmp_name"]) && file_exists($_FILES["xml"]["tmp_name"])) {

			$time = time();

			$tmp_name = $_FILES["xml"]["tmp_name"];

			$name = $_FILES["xml"]["name"];

			$tamanho_arquivo = $_FILES["xml"]["size"];

			$tamanho_maximo = 838860800; //100MB

			if($tamanho_arquivo <= $tamanho_maximo) {

				$f = explode('.', $name);

				$ext = end($f);

				if($ext == 'xml') { //Verifica se é xml

					/* Salvar Arquivo XML */
					$uploads_dir = $_SERVER['DOCUMENT_ROOT'].'/assets/uploads/xml/'.$id_usuario;

					if(!file_exists($uploads_dir)) {
						mkdir($uploads_dir);	
					}

					$nome_arquivo = $time.$name;

					if(move_uploaded_file($tmp_name, $uploads_dir.'/'.$nome_arquivo)) {

						/* Lendo o arquivo xml */
						$xml = simplexml_load_file(base_url().'assets/uploads/xml/'.$id_usuario.'/'.$nome_arquivo);
						//Validação do número de imóveis do xml
						$imoveis = $xml->Imoveis->Imovel;
						$total_xml = count($imoveis);
						if($total_xml > 0) { //Tipo de xml válido
							//Verifica se o número de imóveis do xml não excedeu o limite
							$config = array(
								'campos' => 'id',
								'tabela' => 'imoveis',
								'where' => array('id_usuario' => $id_usuario)
							);
							$this->select->set($config);
							$total_imo = $this->select->total();
							//Total de imóveis publicados + total de imóveis do xml
							$total = $total_xml + $total_imo;
							//Puxa o número limite de imóveis da imobiliaria
							$config = array(
								'campos' => 'num_anuncios, tipo_xml',
								'tabela' => 'usuarios',
								'where' => array('id' => $id_usuario)
							);
							$this->select->set($config);
							$usuario = $this->select->resultado();
							$us_num = $usuario[0]->num_anuncios;
							if($total <= $us_num) {

								// Salvar dados do XML
								//Salvar registro no log de importações
								$dados = array(
									'id_usuario' => $id_usuario,
									'data_cadastro' => time(),
									'tipo_xml' => $usuario[0]->tipo_xml,
									'arquivo' => $nome_arquivo
								);
								$log = $this->master_model->cadastrar_form('log_xml',$dados);

								//removendo imoveis cadastrados via xml
								$this->db->query("DELETE FROM `imoveis` WHERE `id_usuario` = ".$id_usuario." AND arquivo_xml IS NOT NULL " );

								$this->load->helper('file');

								$files = delete_files("/assets/uploads/imovel/".$id_usuario."/"); // get all file names

								/* Pega os dados de Tipo do imóvel */

								$config = array(
									'campos' => 'tipo, id',
									'tabela' => 'tipo_imovel'
								);
								$this->select->set($config);
								$tipo_imovel = $this->select->resultado();

								$array_tipo_imovel = array();

								foreach($tipo_imovel as $t) {
									$array_tipo_imovel[$t->id] = $t->tipo;
								}

								/* ============================== */

								/* Pega os dados de Fianlidade do imóvel */

								$config = array(
									'campos' => 'tipo, id',
									'tabela' => 'finalidade'
								);
								$this->select->set($config);
								$finalidades = $this->select->resultado();

								$array_finalidade = array();

								foreach($finalidades as $f) {
									$array_finalidade[$f->id] = $f->tipo;
								}

								/* ============================== */
								
								foreach($imoveis as $imo) {

									$ref = trim($imo->CodigoImovel); //Número de Referencia do xml

									/*
									//Verifica se o imovel ja esta no sistema e o deleta para atualizar seus dados
									$config = array(
										'campos' => 'id',
										'tabela' => 'imoveis',
										'where' => 'id_usuario = '.$id_usuario.' AND referencia_xml = "'.$ref.'"'
									);
									$this->select->set($config);
									$total_ref = $this->select->total();
									if($total_ref > 0) { //Se achar o imovel de referencia, exclui o imovel
										$imovel_ref = $this->select->resultado();
										$this->master_model->delete_form('imoveis', $imovel_ref[0]->id, 'id');
										$this->master_model->delete_form('imagens', $imovel_ref[0]->id, 'id_imovel');
									}
									*/
									/* Dados Padrões */
	
									$data_cadastro = time();
									$data_expira = 0;
									$ativo = 1;
									$id_usuario = $id_usuario;
									$destaque = 0;
									$status = 3; //Publicado
									$aprovado = 1; //Aprovado
									$mapa = 0;
									$xml = 1;
									$tipo = 1; //Tipo Venda Padrão
									$titulo = $imo->TituloImovel;
									$slug = $this->util->slug($titulo);	
									$slug = $this->checaSlug($slug, 'imoveis');	

									//==== Finalidade e tipo do imóvel
									$tipo_imovel = $imo->TipoImovel;

									if( in_array( $tipo_imovel, $array_tipo_imovel ) ) {
										$tipo_imovel = array_search($tipo_imovel,$array_tipo_imovel);
									} else { //Tipo de imovel padrão (Casa)
										$tipo_imovel = 5;
									}

									$finalidade = $imo->Finalidade;
									
									if( in_array( $finalidade, $array_finalidade ) ) {
										$finalidade = array_search($finalidade,$array_finalidade);
									} else { //Tipo de imovel padrão (Residencial)
										$finalidade = 2;
									}

									//================================

									//==== Dados de endereço baseado no cep
									$cep = $imo->CEP;
									if(empty($cep)) {
										$cep = 0;
									}
									
									$numero = 0; 

									if(!empty($imo->Numero)) {
										$numero = $imo->Numero;
									}
									
									$config = array(
										'campos' => 'endereco_codigo, bairro_codigo',
										'tabela' => 'cep_endereco',
										'where' => array('endereco_cep' => $cep)
									);
								
									$this->select->set($config);
									$num = $this->select->total();
									
									if($num > 0) { //Se achar os dados vinculados ao CEP
									
										$da = $this->select->resultado();
										
										$endereco = $da[0]->endereco_codigo;
										$bairro = $da[0]->bairro_codigo;
										
										$config = array(
											'campos' => 'cidade_codigo',
											'tabela' => 'cep_bairro',
											'where' => array('bairro_codigo' => $bairro)
										);
									
										$this->select->set($config);
										$da = $this->select->resultado();
										
										$cidade = $da[0]->cidade_codigo;
										
										$config = array(
											'campos' => 'uf_codigo',
											'tabela' => 'cep_cidade',
											'where' => array('cidade_codigo' => $cidade)
										);
									
										$this->select->set($config);
										$da = $this->select->resultado();
										
										$estado = $da[0]->uf_codigo;
										
									} else { //Procura dado por dado pelo codigo no banco

										//Estado
										$estado = (String)$imo->Estado;

										$config = array(
											'campos' => 'uf_codigo',
											'tabela' => 'cep_uf',
											'where' => array('uf_sigla' => $estado)
										);
									
										$this->select->set($config);
										$da_estado = $this->select->resultado();

										if(isset($da_estado[0]->uf_codigo)) {
											$estado = $da_estado[0]->uf_codigo;
										} else {
											$estado = 0;
										}

										//Cidade
										$cidade = (String)$imo->Cidade;

										$config = array(
											'campos' => 'cidade_codigo',
											'tabela' => 'cep_cidade',
											'where' => array('uf_codigo' => $estado, 'cidade_descricao' => $cidade)
										);
									
										$this->select->set($config);
										$da_cidade = $this->select->resultado();

										if(isset($da_cidade[0]->cidade_codigo)) {
											$cidade = $da_cidade[0]->cidade_codigo;
										} else {
											$cidade = 0;
										}

										//Bairro 
										$bairro = (String)$imo->Bairro;

										$config = array(
											'campos' => 'bairro_codigo',
											'tabela' => 'cep_bairro',
											'where' => array('cidade_codigo' => $cidade, 'bairro_descricao' => $bairro)
										);
									
										$this->select->set($config);
										$da_bairro = $this->select->resultado();

										if(isset($da_bairro[0]->bairro_codigo)) {
											$bairro = $da_bairro[0]->bairro_codigo;
										} else {
											$bairro = 0;
										}

										//Endereco 
										$endereco = (String)$imo->Endereco;

										$config = array(
											'campos' => 'endereco_codigo',
											'tabela' => 'cep_endereco',
											'where' => array('bairro_codigo' => $bairro, 'endereco_logradouro' => $endereco)
										);
									
										$this->select->set($config);
										$da_endereco = $this->select->resultado();

										if(isset($da_endereco[0]->bairro_codigo)) {
											$endereco = $da_endereco[0]->endereco_codigo;
										} else {
											$endereco = 0;
										}

									}
									
									$complemento = '';
									if(!empty($imo->ComplementoEndereco)) {
										$complemento = $imo->ComplementoEndereco;
									}
									
									//=====================================
										
									if (!empty($imo->PrecoVenda) || $imo->PrecoVenda != "") {
										$valor = $imo->PrecoVenda;
									}else{
										$valor = $imo->PrecoLocacao;
										$tipo = 2; //Tipo locação
									}
				
									$area_m2 = $imo->AreaUtil;
									
									//==== Garagem, dormitorios, banheiros e suites
									
									$dormitorios = 0;
									$suites = 0;
									$banheiros = 0;
									$garagem = 0;
									
									if(!empty($imo->QtdDormitorios)) {
										$dormitorios = $imo->QtdDormitorios;
									}
									$condominio = 1;
									if(!empty($imo->NomeCondominio) AND $imo->NomeCondominio != "") {
										$condominio = 0;
									}else{
										$aux = array('apartamento','Apartamento','Condominio','Condo','Condomínio','condo','condominio','condomínio','Condomínio');
										foreach ($aux as $aux2) {
											$domain = strstr($imo->SubTipoImovel, $aux2);
											if($domain != FALSE){
												$condominio = 0;
											}
										}
									}
									
									if(!empty($imo->QtdSuites)) {
										$suites = $imo->QtdSuites;
									}
									
									if(!empty($imo->QtdBanheiros)) {
										$banheiros = $imo->QtdBanheiros;
									}
									
									if(!empty($imo->QtdVagas)) {
										$garagem = $imo->QtdVagas;
									}
									
									//===============================================
									
									/* Dados de Contato */
									
									$config = array(
										'campos' => '*',
										'tabela' => 'dados_imobiliaria',
										'where' => array('id' => $l['perfil'])
									);
								
									$this->select->set($config);
									$r = $this->select->resultado();
									
									$telefone = $r[0]->telefone;
									$email = $r[0]->email;
									
									$dados = array(
										
										'data_cadastro' => $data_cadastro,
										'ativo' => $ativo,
										'id_usuario' => $id_usuario,
										'status' => $status,
										'aprovado' => $aprovado,
										'destaque' => $destaque,
										'slug' => $slug,
										'titulo' => (string)$titulo,
										'descricao1' => (string)$titulo,
										'tipo' => $tipo,
										'id_tipo_imovel' => $tipo_imovel,
										'finalidade' => $finalidade,
										'dormitorios' => $dormitorios,
										'suites' => $suites,
										'garagem' => $garagem,
										'banheiros' => $banheiros,
										'usarDadosContato' => 2,
										'mapa' => $mapa,
										'area_m2' => $area_m2,
										'cep' => $cep,
										'rua' => $endereco,
										'numero' => $numero,
										'complemento' => (string)$complemento,
										'bairro' => $bairro,
										'cidade' => $cidade,
										'estado' => $estado,
										'condominio' => $condominio, //Padrão Não
										'valor' => $valor,
										'fone' => $telefone,
										'email' => $email,
										'referencia_xml' => (string)$ref,
										'arquivo_xml' => $nome_arquivo
										
									);

									//print_r($dados);
							
									$id_imovel = $this->master_model->cadastrar_form('imoveis',$dados);
									
									/* Fotos */
									$x = 1;
									$total_imagens = 10;

									/*
									if (!is_dir($_SERVER['DOCUMENT_ROOT'].'/assets/uploads/imovel/'.$id_usuario)) {
										mkdir($_SERVER['DOCUMENT_ROOT'].'/assets/uploads/imovel/'.$id_usuario);
									}
									*/

									if(isset($imo->Fotos->Foto)) {
										foreach($imo->Fotos->Foto as $foto) {
											
											if($x <= $total_imagens) {
												
												$destaque = 0;
												
												if($foto->Principal == 1) {
													$destaque = 1;	
												}
												
												/*
												$ex = explode('.', $foto->URLArquivo);
												$tipoImg = end($ex);
												*/

												/*
												$tipoImg = '';
												if(empty($tipoImg)) {
													$tipoImg = 'jpg';
												}

												$nome_img = $foto->NomeArquivo;
												
												$nome_imagem = $nome_img."-".md5(uniqid(rand(),true)).'.'.$tipoImg;
												
												$nova_imagem = $_SERVER['DOCUMENT_ROOT'].'/assets/uploads/imovel/'.$id_usuario.'/'.$nome_imagem;

												if(file_put_contents($nova_imagem, file_get_contents($foto->URLArquivo))) {
													//Salvar registro da imagem
													$dados = array(
														'ativo' => 1,
														'data_cadastro' => time(),
														'usuario' => $id_usuario,
														'id_imovel' => $id_imovel,
														'destaque' => $destaque,
														'arquivo' => $nome_imagem
													);
											
													$imagens = $this->master_model->cadastrar_form('imagens',$dados);	
												}
												*/

												$dados = array(
													'ativo' => 1,
													'data_cadastro' => time(),
													'usuario' => $id_usuario,
													'id_imovel' => $id_imovel,
													'destaque' => $destaque,
													'arquivo' => (String)$foto->URLArquivo
												);
										
												$imagens = $this->master_model->cadastrar_form('imagens',$dados);
												
											}
											
											$x++;
											
										}
									}
								
								}
								
								$msg = 'Imóveis importados com sucesso!';
								$status = 1;
							
							} else { //Excedeu o numero limite
							
								$num_restante = $us_num - $total_imo;
								
								$msg = 'Número de imóveis do xml ( '.$total_xml.' ) excedeu o número de imóveis restantes ( '.$num_restante.' ), suba um xml com menos imóveis ou entre em contato conosco.';
								$status = 0;
								
							}
							
						} else {
							
							$msg = 'Formato de arquivo xml inválido.';
							$status = 0;
							
						}
						
						// ==================================
						
					} else {
						
						$msg = 'Erro ao salvar o arquivo';
						$status = 0;
						
					}
					
				} else {
					
					$msg = 'Extensão do arquivo inválida.';
					$status = 0;
					
				}
				
			} else {
			
				$msg = 'Tamanho do arquivo excede o tamanho máximo (5MB).';
				$status = 0;
				
			}

		} else {
		
			$msg = 'Erro no arquivo.';
			$status = 0;
			
		}
		
		$json = '{"status" : '.$status.', "msg" : "'.$msg.'"}';
		
		header('Content-type: text/json');
		header('Content-type: application/json');

		echo $json;
		exit;
		
	}
	
	public function checaSlug($slug, $tabela) {
		
		$query = $this->db->query("SELECT id FROM ".$tabela." WHERE slug LIKE '".$slug."%'");
	
		if ($query->num_rows() > 0) {

			$slug = $slug.'-'.$query->num_rows();
			
			return $this->checaSlug($slug, $tabela);

		} else {
			
			return $slug;
			
		}

	}
	
}