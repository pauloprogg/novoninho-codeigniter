<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Painel_imobiliaria extends Main_Controller {

	private $pagina = "painel-imobiliaria";

	private $tabela = "imoveis";

	public function index( $pag = 1 ) {

		$this->checaSessao();
		$this->limparFotos();
		$this->title = "Painel de Controle";
		$this->keywords = "Painel de Controle, Aqui";
		$this->description = "Painel de Controle";
		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller

		ini_set('memory_limit','1000M');
		
		$sessao = $this->session->userdata('login');
		
		$msg_erro =  $this->session->userdata('msg_erro');
		
		$mensagem_erro = '';
		
		if(!empty($msg_erro)) {
			
			$mensagem_erro = '<div class="small-12 form_result_footer" style="text-align: center;"><div data-alert="" class="alert-box alert radius">'.$msg_erro.'<a href="javascript:void(0)" class="close">×</a></div></div>';
			$this->session->unset_userdata('msg_erro');
						
		}
		//print_r($msg_erro);
		$id_usuario = $sessao['id'];
		
		$config = array(
			'campos' => '*',
			'tabela' => 'usuarios',
			'where' => array('id' => $id_usuario)
		);
		
		$this->select->set($config);
		$ativo = $this->select->resultado();
		
		$ativo = $ativo[0]->ativo;

		$estrutura_xml = '';
		
		if($ativo == 1) {
			
			//========================= Configurações de importação de xml
			
			$config = array(
				'campos' => 'tipo_xml',
				'tabela' => 'usuarios',
				'where' => array('id' => $id_usuario)
			);
		
			$this->select->set($config);
			$tipo_xml = $this->select->resultado();
			$tipo_xml = $tipo_xml[0]->tipo_xml;
			
			if(empty($tipo_xml)) { //Nao exibe botao de importar
				$btn_importar = '';	
				
			} else { //Exibe o conteudo
			
				$btn_importar = '<a class="botao_azul" id="enviar_xml" style="margin-left: 20px;">Importar XML</a>';
				
				//Ve qual o tipo 	
				
				switch($tipo_xml) {
					
					case '1': //I-Value 
					
						$estrutura_xml = $this->parser->parse( 'painelimobiliaria/xml/ivalue', array('base_url' => base_url()), true );
					
					break;
					
					case '2': //Viva Real 
					
						//Estado
						$config = array(
							'campos' => 'uf.uf_codigo, uf.uf_descricao',
							'tabela' => 'cep_uf uf',
							'join' => array(
								array('cep_cidade c','c.uf_codigo = uf.uf_codigo','left')
							),
							'where' => 'c.cidade_cep <> ""',
							'orderBy' => array('uf.uf_descricao' => 'ASC')
						);
						$this->select->set($config);
						$estado = $this->select->resultado();
				
						$select_estado = '<select name="estado" id="estado" class="t1 estado_xml">';
				
						foreach($estado as $val ) {
				
							$select_estado .= '<option value="'.$val->uf_codigo.'">'.$val->uf_descricao.'</option>';
				
						}
				
						$select_estado .= '</select>'; 
					
						//------ Cidade

						$config = array(
							'campos' => 'cidade_codigo, cidade_descricao',
							'tabela' => 'cep_cidade',
							'where' => 'uf_codigo = '.$estado[0]->uf_codigo.' AND cidade_cep <> ""'
						);
				
						$this->select->set($config);
						$cidade = $this->select->resultado();
				
						$select_cidade = '<select name="cidade" id="cidade" class="t1 cidade_xml">';
				
						foreach($cidade as $val ) {
				
								$select_cidade .= '<option value="'.$val->cidade_codigo.'">'.$val->cidade_descricao.'</option>';
				
						}
				
						$select_cidade .= '</select>'; 

						$estrutura_xml = $this->parser->parse( 'painelimobiliaria/xml/vivareal', array('base_url' => base_url(), 'select_estado' => $select_estado, 'select_cidade' => $select_cidade), true );
					
					break;
					
				}
				
			}
			
			//=========================================
			
			/* Pega o total de imóveis */
			$config = array(
				'campos' => 'id, titulo, destaque, ativo, feirao, super_destaque',
				'tabela' => 'imoveis',
				'where' => array('id_usuario' => $id_usuario)
			);
			
			$this->select->set($config);
			$num = $this->select->total();
			/* ========================================= */

			/* CONFIGURAÇÕES DE PAGINAÇÃO */
			$total_imo = $num;
			$por_page = 20;
			$pagina = $pag;
			$pages = $total_imo / $por_page;

			$pages = ceil($pages);

			$pages = (int) $pages;

			if($pagina == 1) {
				// $offset = 1;

				$offset = 0;
			} else {
				// $offset = ($pagina * $por_page) + 1;

				$offset = ($pagina-1) * $por_page;
			}

			// echo $limit.' - '.$offset;




			$config = array(
				'campos' => 'id, titulo, destaque, ativo, feirao, super_destaque, destaque_feirao, super_destaque_feirao',
				'tabela' => 'imoveis',
				'where' => array('id_usuario' => $id_usuario),
				'orderBy' => array('titulo' => 'ASC'),
				'limit' => $por_page,
				'offset' => $offset
				
			);

			if(!empty($_GET['tipo'])){
				
				$config['where']['tipo'] = $_GET['tipo'];			
			}

			if(!empty($_GET['destaque'])){
				$config['where']['destaque'] = $_GET['destaque'];			
			}

			if(!empty($_GET['super_destaque'])){
				$config['where']['super_destaque'] = $_GET['super_destaque'];			
			}

			if(!empty($_GET['feirao'])){
				$config['where']['feirao'] = $_GET['feirao'];			
			}


			if(!empty($_GET['destaque_feirao'])){
				$config['where']['destaque_feirao'] = $_GET['destaque_feirao'];			
			}

			if(!empty($_GET['super_destaque_feirao'])){
				$config['where']['super_destaque_feirao'] = $_GET['super_destaque_feirao'];			
			}

			if(!empty($_GET['tipo_imovel'])){
				$config['where']['id_tipo_imovel'] = $_GET['tipo_imovel'];			
			}
			
			if(!empty($_GET['dormitorios'])){
				$config['where']['dormitorios'] = $_GET['dormitorios'];			
			}

			if(!empty($_GET['finalidade'])){
				$config['where']['finalidade'] = $_GET['finalidade'];			
			}

			if(!empty($_GET['valor'])){
				$config['where']['valor >='] = $_GET['valor'];		
			}

			if(!empty($_GET['referencia_xml'])){
				$config['where']['referencia_xml'] = $_GET['referencia_xml'];
			}

			if(!empty($_GET['id'])){
				$config['where']['id'] = $_GET['id'];
			}

			

			

			
			$this->select->set($config);
			$resultado = $this->select->resultado();
			



			$config = array(
				'campos' => 'id, expira_em,nome, responsavel',
				'tabela' => 'dados_imobiliaria',
				'where' => array('id' => $sessao['perfil'])
			);
			
			$this->select->set($config);
			$dados_imobiliaria = $this->select->resultado();
			$dados_imobiliaria = $dados_imobiliaria[0];

			//****************gera quantos dias faltam para expirar as publicacoes
			$date = date_create();
			date_timestamp_set($date, $dados_imobiliaria->expira_em);
			$data_final = date_format($date, 'd/m/Y');

			$data_inicial = date_create();
			$data_inicial = date_format($data_inicial, 'd/m/Y');


			// Cria uma função que retorna o timestamp de uma data no formato DD/MM/AAAA
			function geraTimestamp($data) {
			$partes = explode('/', $data);
				return mktime(0, 0, 0, $partes[1], $partes[0], $partes[2]);
			}

			// Usa a função criada e pega o timestamp das duas datas:
			$time_inicial = geraTimestamp($data_inicial);
			$time_final = geraTimestamp($data_final);

			// Calcula a diferença de segundos entre as duas datas:
			$diferenca = $time_final - $time_inicial; // 19522800 segundos

			// Calcula a diferença de dias
			$dias = (int)floor( $diferenca / (60 * 60 * 24)); // 225 dias
			
			if($dias <= 0){
				$dias = '<b style="color:red">Suas publicações expiraramm!</b>';
			}else{
				$dias = 'Faltam : <b>'.$dias.'</b> dias para suas publicações expirarem.';
			}
			//****************

			if($num > 0) {
			
				$lista_imoveis = '';
				
				foreach($resultado as $v) {


				//---------------------Busca imagem

					$imgImovel = array(

						'campos' => 'arquivo',

						'tabela' => 'imagens',

						'where' => array('id_imovel' => $v->id, 'destaque' => 1)

					);

					$this->select->set($imgImovel);

					$img = $this->select->resultado();
					$folder = base_url() ."../assets/uploads/imovel/".$id_usuario."/";
					$imgOriginal = $folder.$img[0]->arquivo;
				

				
					if($v->ativo == 0) {
						$status = 'status_oculto';
						$oculto = 'Mostrar';
					} else {
						$oculto = 'Ocultar';
						$status = 'status_publicado';
						$status_feirao = '';
						$status_normal = '';
						$status_destaque_feirao = '';
						$status_super_destaque_feirao = '';
						$status_destaque = '';
						$status_super_destaque = '';

						if($v->feirao == 1){
							$status_feirao = 'status_okay';	
						}else{
							$status_normal = 'status_okay';
						}
						if($v->destaque_feirao == 1){
							$status_destaque_feirao = 'status_okay';
						}
						if($v->super_destaque_feirao == 1){
							$status_super_destaque_feirao = 'status_okay';	
						}


						if($v->destaque == 1){
							$status_destaque = 'status_okay';
						}

						if($v->super_destaque == 1){
							$status_super_destaque = 'status_okay';
						}
						
					}
				
					$lista_imoveis .= '<div class="linha '.$status.'">
										
										<div class="columns small-2 medium-2 large-2">
											<img src="'.$imgOriginal.'">
										</div>

										<div class="columns small-10 medium-10 large-10">
											<div class="columns small-9 medium-9 large-9">
												
												<span>'.$v->titulo.'</span>
												
											</div>
											<div class="columns small-1 medium-1 large-1 center">
												<a href="'.base_url().'painel-imobiliaria/editar/'.$v->id.'" alt="Editar Imóvel" title="Editar Imóvel"><img src="{base_url}assets/images/editar.png"></a>
											</div>
											
											<div class="columns small-1 medium-1 large-1">
												<label><a id="'.$v->id.'" class="imobiliaria_ocultar">'.$oculto.'</a></label>
											</div>

											<div class="columns small-1 medium-1 large-1 center">
												<a id="'.$v->id.'" class="deletar_imovel" caminho="painel-imobiliaria" alt="Deletar Imóvel" title="Deletar Imóvel">
													<img src="{base_url}assets/images/deletar.png">
												</a>
											</div>
										</div>
										
										<div class="columns small-10 medium-10 large-10 acoes">
										
											<div class="row">
																								
												<div class="columns small-6 medium-6 large-6">
													
													<fieldset>
														<legend>Feirão</legend>
														<div class="columns small-3 medium-3 large-3 '.$status_feirao.'">
															<label>
															<a id="'.$v->id.'" class="imobiliaria_feirao">Feirão</a>

															</label>
														</div>
														
														<div class="columns small-4 medium-4 large-4 '.$status_destaque_feirao.'">
															<label>
																<a id="'.$v->id.'" class="imobiliaria_destaque_feirao">Destaque</a>
																
															</label>
														</div>

														<div class="columns small-5 medium-5 large-5 '.$status_super_destaque_feirao.'">
															<label>
																<a id="'.$v->id.'" class="imobiliaria_super_destaque_feirao">Super Destaque</a>
																
															</label>
														</div>
													</fieldset>
												</div>

												<div class="columns small-6 medium-6 large-6">

													<!-- Normal -->
													<fieldset>
														<legend>Normal</legend>
														
														<div class="columns small-3 medium-3 large-3 status_okay">
															<label><a id="'.$v->id.'" class="imobiliaria_feirao" >Normal</a></label>
														</div>
														
														<div class="columns small-4 medium-4 large-4 '.$status_destaque.'">
															<label>
																<a id="'.$v->id.'" class="imobiliaria_destaque">Destaque</a>
																
															</label>
														</div>

														<div class="columns small-5 medium-5 large-5 '.$status_super_destaque.'">
															<label>
																<a id="'.$v->id.'" class="imobiliaria_super_destaque">Super Destaque</a>
																
															</label>
														</div>
													</fieldset>

												</div>
											
									</div>
											
										</div>
										
									</div>';
				
				}
			
			} else {
			
				$lista_imoveis = '<div class="linha">
			
									<div class="columns small-12 medium-12 large-12">
										Não Existem Imóveis Cadastrados.
									</div>
		
								</div>';
			
			}
			
			$estrutura = $this->parser->parse( 'templates/imobiliaria/ativo', array('base_url' => base_url(), 'lista_imoveis' => $lista_imoveis, 'botao_importar' => $btn_importar, 'diasFaltam' => $dias, 'pages' => $pages, 'pagina' =>$this->pagina), true );
			
			//Modal lista de xml
			//$lista_xml = $this->parser->parse( 'templates/painelimobiliaria/lista_xml', array('base_url' => base_url()), true );
			
		} else {
			
			$estrutura = $this->parser->parse( 'templates/imobiliaria/inativo', array('base_url' => base_url()), true );
		
		}

		$data = array(
			'estrutura' => $estrutura,
			'mensagem_erro' => $mensagem_erro,
			'estrutura_xml' => $estrutura_xml
		);

		$this->load( 'painelimobiliaria/imoveis', $data );

	}

	public function donwloadXml(){

		set_time_limit(0);
		ini_set('memory_limit','2000M');

		//$url = 'http://www.valuegaia.com.br/integra/midia.ashx?midia=GaiaWebServiceImovel&p=72u7bsK8d7ieRtdP346%2f1l1DXcxNTZuHbizjsOIrDcggvRnVXTfPOvRhP5s4aoPxnaSZIkfMSmuwLFnDSxRinCLMVqhi22buye8gNWf7K5IzrM%2bHVLd2CQ%3d%3d';

		$url = $_POST['urlIntegracao'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url );
		unset($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'SOROCABACOM');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		// $buffer = ;
		// $aquivoNome= date('d-m-Y_h-i-s')."imoveisXML.xml";
		$this->load->helper('download');
		force_download(date('d-m-Y_h-i-s')."imoveisXML.xml", curl_exec($ch));
		curl_close($ch);

		/*
		//Oficial
		$caminhoFile= $_SERVER['DOCUMENT_ROOT'].'/assets/downloads/'.$aquivoNome;
		//sites em produção 
		//$caminhoFile= $_SERVER['DOCUMENT_ROOT'].'/novoninho/assets/downloads/'.$aquivoNome;
		$fp = fopen($caminhoFile, "w");
		$fw = fwrite($fp, $buffer);

		fclose($caminhoFile);
		$data = array(
			'link' => base_url().'assets/downloads/'.$aquivoNome,
		);

		//direct(base_url().'assets/downloads/'.$aquivoNome);
		$this->load( 'painelimobiliaria/Xml_ivalue', $data );
		*/
		
	}

	
	public function editarperfil() {
	
		$this->checaSessao();

		$this->title = "Painel de Controle";
		$this->keywords = "Painel de Controle, Aqui";
		$this->description = "Painel de Controle";
		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller

		$d = $this->session->userdata('login');

		$config = array(
			'campos' => '*',
			'tabela' => 'usuarios',
			'where' => array('id' => $d['id'])
		);
		
		$this->select->set($config);
		$dados_usuario = $this->select->resultado();
		//$dados_usuario = $d_usuario[0];
		
		$tabela = $this->util->tabelaPerfil($d['tipo']);
		
		$config = array(
			'campos' => '*',
			'tabela' => $tabela,
			'where' => array('id' => $d['perfil'])
		);
		
		$this->select->set($config);
		$r = $this->select->resultado();

		//respostas como soube	
		$query = array(
			'campos' => 'texto',
			'tabela' => 'como_soube',
			'where' => array()
		);	
		$this->select->set($query);
		$respostas = $this->select->resultado();
		$selectResp = '<label>Como Soube?</label>
						<select name="como_soube" class="campo"><option value="">Não selecionado</option>';
		$select='';
		foreach ($respostas as $resposta) {
			if ($r[0]->como_soube == $resposta->texto) {
				$select='selected';
			}else{$select='';}
			$selectResp.='<option value="'.$resposta->texto.'" '.$select.'>'.$resposta->texto.'</value>';
		}
		$selectResp.='</select>';


		//*******seleciona usuario
		$user = array(
			'campos' => '*',
			'tabela' =>'usuarios',
			'where' => array('perfil' => $d['perfil'], 'tipo' => $d['tipo'])
		);
		$this->select->set($user);
		$userdados = $this->select->resultado();
		$newsletter='';
		if($userdados[0]->newsletter == 0){
			$newsletter = '<label>
								<input type="checkbox" name="newsletter" id="newsletter" value="1">
								<span><span style="font-weight: bold;color: #008cba;">Aceito e quero receber novidades do Novo Ninho</span></span>
							</label>';
		}else{
			$newsletter = '<label style="display:none;">
								<input type="checkbox" name="newsletter" id="newsletter" value="1" checked="checked">
								<span><span style="font-weight: bold;color: #008cba;">Aceito e quero receber novidades do Novo Ninho</span></span>
							</label>';
		}

		$modal_termos = $this->parser->parse( 'templates/modal_termos', array(
			'base_url' => base_url(), 'titulo' => ''/* $t[0]->titulo */, 'texto' => ''/* $t[0]->texto */
			), true );

		//vendedores
		$query = array(
			'campos' => 'nome',
			'tabela' => 'vendedores',
			'where' => array('ativo' => 1)
		);
		
		$this->select->set($query);
		$vendedores = $this->select->resultado();
		$selectVendedor = '<label>Vendedor</label>
						<select name="vendedor" class="campo"><option value="">Não selecionado</option>';
		$select='';
		foreach ($vendedores as $vendedor) {
			if ($r[0]->vendedor == $vendedor->nome) {
				$select='selected';
			}else{$select='';}
			$selectVendedor.='<option value="'.$vendedor->nome.'" '.$select.'>'.$vendedor->nome.'</value>';
		}
		$selectVendedor.='</select>';

		$classe_mascara = '';
		$check_cpf = '';	
		$check_cnpj = '';
		if($r[0]->check_cpf_cnpj == 1) {
			$classe_mascara = 'cpf';
			$check_cpf = 'checked';	
		} else {
			$classe_mascara = 'cnpj';
			$check_cnpj = 'checked';	
		}

		$data = array(
			"dados" => $r,
			"classe_mascara" => $classe_mascara,
			"dados_usuario" => $dados_usuario,
			"check_cnpj" => $check_cnpj,
			"selectResp" => $selectResp,
			"check_cpf" => $check_cpf,
			"selectVendedor" => $selectVendedor,
			"newsletter" => $newsletter,
			"modal_termos" => $modal_termos

		);

		$this->load( 'painelimobiliaria/editarperfil', $data );

	}
	
	public function cadastrar() {
	
		$this->checaSessao();
		
		$this->checaLimite();
		$config = array(
			'campos' => 'tipo, id',
			'tabela' => 'tipo_imovel'
		);
		
		$this->select->set($config);
		$tipo_imovel = $this->select->resultado();
		
		$config = array(
			'campos' => 'tipo, id',
			'tabela' => 'finalidade'
		);
		
		$this->select->set($config);
		$finalidade = $this->select->resultado();

		$cidade_sessao = $this->session->userdata('cidade');
		$estado_sessao = $this->session->userdata('estado');

		$config = array(
			'campos' => 'uf_codigo as "value", uf_sigla as "label"',
			'tabela' => 'cep_uf'
		);
		
		$this->select->set($config);
		$estados = $this->select->resultado();

		$config = array(
			'campos' => 'cidade_codigo as "value", cidade_descricao as "label"',
			'tabela' => 'cep_cidade',
			'where' => array('uf_codigo' => $estado_sessao)
		);
		
		$this->select->set($config);
		$cidades = $this->select->resultado();

		$this->title = "Painel de Controle";
		$this->keywords = "Painel de Controle, Aqui";
		$this->description = "Painel de Controle";
		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller

		$caminho = 'painel-imobiliaria'; //Retorno do formulario de cadastro
		
		$l = $this->session->userdata('login');
		
		$id_usuario = $l['id'];
		
		$formulario = $this->parser->parse( 'painelimobiliaria/cadastro-imovel', array(
			'id_usuario' => $id_usuario,
			'base_url' => base_url(),
			"caminho" => $caminho,
			"tipo_imovel" => $tipo_imovel,
			"finalidade" => $finalidade,
			"estados" => $estados,
			"cidades" => $cidades,
			"estado_selected" => $estado_sessao,
			"cidade_selected" => $cidade_sessao
		), true );
		
		$data = array(
			"formulario_cadastro" => $formulario
		);

		$this->load( 'painelimobiliaria/cadastrar', $data );

	}
	
	public function editar( $id ) {
	
		$this->checaSessao();
		
		$l = $this->session->userdata('login');
		
		$id_usuario = $l['id'];
		
		$config = array(
			'campos' => '*',
			'tabela' => 'imoveis',
			'where' => array('id' => $id, 'id_usuario' => $id_usuario)
		);
	
		$this->select->set($config);
		$total = $this->select->total();
		$r = $this->select->resultado();
		
		if($total == 0) {
			redirect(base_url().'painel-imobiliaria');
		}
		
		$i = $r[0];
		
		/* Validação de Status */
		
		//if($i->status == 1) {
		
			$form = 'editar_imovel_imobiliaria';
			$envio = 'editImovel';
			$bloqueado = '';
			$bloqueadoarea='';
		
		/*} else {
		
			$form = 'editar_imovel_bloqueado';
			$envio = 'editImovelBloqueado';
			// $bloqueado = 'readonly="readonly"';
			$bloqueado = 'disabled="disabled"';
			$bloqueadoarea='disabled';
		
		}*/
		
		$config = array(
			'campos' => 'tipo as "nome_tipo", id as "id_tipo"',
			'tabela' => 'tipo_imovel'
		);
		
		$this->select->set($config);
		$tipo_imovel = $this->select->resultado();
		
		$config = array(
			'campos' => 'tipo, id',
			'tabela' => 'finalidade'
		);
		
		$this->select->set($config);
		$finalidade = $this->select->resultado();
		
		$valor_imovel = $this->util->moeda2br($i->valor);

		/* Configuração dos Campos */
		
		//------- Radio Tipo
		$check_venda = '';
		$check_locacao = '';
		$check_temporada = '';
		
		if($i->tipo == 1) { $check_venda = 'checked="checked"'; }
		if($i->tipo == 2) { $check_locacao = 'checked="checked"'; }
		if($i->tipo == 3) { $check_temporada = 'checked="checked"'; }
		
		$radio_tipo = '<div class="row">
				
							<div class="columns small-4 medium-4 large-4">
								<input type="radio" name="tipo_imovel" id="tipo_imovel" value="1" class="radio_tipo" '.$bloqueado.' '.$check_venda.'><span>Venda</span>
							</div>
							
							<div class="columns small-4 medium-4 large-4">
								<input type="radio" name="tipo_imovel" id="tipo_imovel" value="2" class="radio_tipo" '.$bloqueado.' '.$check_locacao.'><span>Locação</span>
							</div>
							
							<div class="columns small-4 medium-4 large-4">
								<input type="radio" name="tipo_imovel" id="tipo_imovel" value="3" class="radio_tipo" '.$bloqueado.' '.$check_temporada.'><span>Temporada</span>
							</div>
							
						</div>';
		//----------
						
		//----- Select Tipo de Imóvel
		$select_tipo_imovel = '<select name="tipo" id="tipo" '.$bloqueado.'>';
		
		foreach($tipo_imovel as $val ) {
				
				$check = '';
				if($val->id_tipo == $i->id_tipo_imovel) $check = 'selected="selected"';
		
				$select_tipo_imovel .= '<option value="'.$val->id_tipo.'" '.$check.'>'.$val->nome_tipo.'</option>';
				
		}
		
		$select_tipo_imovel .= '</select>'; 
		
		//----- Select Finalidade
		
		$select_finalidade = '<select name="finalidade" id="finalidade" '.$bloqueado.'>';
		
		foreach($finalidade as $val ) {
				
				$check = '';
				if($val->id == $i->finalidade) $check = 'selected="selected"';
		
				$select_finalidade .= '<option value="'.$val->id.'" '.$check.'>'.$val->tipo.'</option>';
				
		}
		
		$select_finalidade .= '</select>'; 
	
		//----------
		
		//----- Select Dormitorioes
		
		$select_dormitorios = '<select name="dormitorios" id="dormitorios" '.$bloqueado.'>';
		
		for($x = 0;$x <= 10;$x++) {
				
				$check = '';
				if($x == $i->dormitorios) $check = 'selected="selected"';
		
				$select_dormitorios .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';
				
		}
		
		$select_dormitorios .= '</select>'; 
	
		//----------
		
		//----- Select Suites
		
		$select_suites = '<select name="suites" id="suites" '.$bloqueado.'>';
		
		for($x = 0;$x <= 10;$x++) {
				
				$check = '';
				if($x == $i->suites) $check = 'selected="selected"';
		
				$select_suites .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';
				
		}
		
		$select_suites .= '</select>'; 
	
		//----------
		
		//----- Select Garagem
		
		$select_garagem = '<select name="garagem" id="garagem" '.$bloqueado.'>';
		
		for($x = 0;$x <= 10;$x++) {
				
				$check = '';
				if($x == $i->garagem) $check = 'selected="selected"';
		
				$select_garagem .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';
				
		}
		
		$select_garagem .= '</select>'; 
	
		//----------
		
		//----- Select Banheiros
		
		$select_banheiros = '<select name="banheiros" id="banheiros" '.$bloqueado.'>';
		
		for($x = 0;$x <= 10;$x++) {
				
				$check = '';
				if($x == $i->banheiros) $check = 'selected="selected"';
		
				$select_banheiros .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';
				
		}
		
		$select_banheiros .= '</select>'; 
	
		//----------
		
		//------- Radio Mapa
		$check_mapa_sim = '';
		$check_mapa_nao = '';
		
		if($i->mapa == 1) { $check_mapa_sim = 'checked="checked"'; }
		if($i->mapa == 0) { $check_mapa_nao = 'checked="checked"'; }
		
		$radio_mapa = '<div class="columns small-6 medium-6 large-6">
							<input type="radio" name="mapa" id="mapa" value="1" class="radio_tipo" '.$check_mapa_sim.' ><span>Sim</span>
						</div>
					
						<div class="columns small-6 medium-6 large-6">
							<input type="radio" name="mapa" id="mapa" value="0" class="radio_tipo" '.$check_mapa_nao.' ><span>Não</span>
						</div>';
		//----------
		
		//------- Radio Condominio
		$check_cond_sim = '';
		$check_cond_nao = '';
		
		if($i->condominio == 0) { $check_cond_sim = 'checked="checked"'; }
		if($i->condominio == 1) { $check_cond_nao = 'checked="checked"'; }
		
		$radio_condominio = '<div class="columns small-6 medium-6 large-6">
							<input type="radio" name="condominio" id="condominio" value="0" class="radio_tipo" '.$check_cond_sim.' '.$bloqueado.'><span>Sim</span>
						</div>
					
						<div class="columns small-6 medium-6 large-6">
							<input type="radio" name="condominio" id="condominio" value="1" class="radio_tipo" '.$check_cond_nao.' '.$bloqueado.'><span>Não</span>
						</div>';
		//----------
		
		/* Galeria de Fotos */
		
		$config = array(
			'campos' => 'id as "id_imagem", arquivo, destaque',
			'tabela' => 'imagens',
			'where' => array('id_imovel' => $id)
		);
		
		$this->select->set($config);
		$imagens = $this->select->resultado();
		
		foreach($imagens as $x => $val) {

			if($val->destaque == 1) {
				$checked = 'checked="checked"';
			} else {
				$checked = '';
			}

			$arquivo = trim($val->arquivo);
			$path = '/assets/uploads/imovel/'.$id_usuario.'/';

			if(file_exists(getcwd().$path.$arquivo)) {
				$arquivo = $path.$arquivo;
			}

			$imagens[$x]->arquivo = $arquivo;
			$imagens[$x]->destaque_texto = $checked;
		
		}		
		
		//Dados do endereço
		
		$cep = str_replace('-', '', $i->cep);
		
		$config = array(
			'campos' => 'r.endereco_logradouro as "rua", b.bairro_descricao as "bairro", c.cidade_descricao as "cidade", e.uf_sigla as "estado"',
			'tabela' => 'cep_endereco r',
			'join' => array(
				array('cep_bairro b','b.bairro_codigo = r.bairro_codigo','left'),
				array('cep_cidade c','c.cidade_codigo = b.cidade_codigo','left'),
				array('cep_uf e','e.uf_codigo = c.uf_codigo','left')
			),
			'where' => array('r.endereco_cep' => $cep)
		);
		
		$this->select->set($config);
		$end_total = $this->select->total();
		
		if($end_total > 0) { //Cep normal
			
			$dados_endereco = $this->select->resultado();
			
			$dados_end = $dados_endereco[0];
			
			$texto_rua = $dados_end->rua;
			$texto_bairro = $dados_end->bairro;
			
		} else { //Procura de cep padrao da cidade
			
			$config = array(
				'campos' => 'c.cidade_descricao as "cidade", e.uf_sigla as "estado"',
				'tabela' => 'cep_cidade c',
				'join' => array(
					array('cep_uf e','e.uf_codigo = c.uf_codigo','left')
				),
				'where' => array('c.cidade_cep' => $i->cep)
			);
			
			$this->select->set($config);
			$dados_endereco = $this->select->resultado();
			
			//$dados_end = $dados_endereco[0];
			
			$texto_rua = '';
			$texto_bairro = '';
			
		}

		$dadosCadastrosOption ='';

		$dadosCadastrosOption ='<div class="columns small-12 medium-6 large-6">
									<input type="radio" name="usar_contato" '.$bloqueado.' id="usar_contato" value="1" class="radio_tipo usar_contato" '.($r[0]->usarDadosContato == 1 ? 'checked="checked"' : '' ).'><span>Sim</span>
								</div>
								<div class="columns small-12 medium-6 large-6">
									<input type="radio" name="usar_contato" '.$bloqueado.' id="usar_contato" value="2" class="radio_tipo usar_contato" '.($r[0]->usarDadosContato == 0 ? 'checked="checked"' : '' ).'><span>Não</span>
								</div>';

		//=======================================

		/* Configuração de cidades e estados */
		$cidade_selected = $i->cidade;
		$estado_selected = $i->estado;

		$config = array(
			'campos' => 'uf_codigo as "value", uf_sigla as "label"',
			'tabela' => 'cep_uf'
		);
		
		$this->select->set($config);
		$estados = $this->select->resultado();

		$config = array(
			'campos' => 'cidade_codigo as "value", cidade_descricao as "label"',
			'tabela' => 'cep_cidade',
			'where' => array('uf_codigo' => $estado_selected)
		);
		
		$this->select->set($config);
		$cidades = $this->select->resultado();

		$this->title = "Painel de Controle";
		$this->keywords = "Painel de Controle, Aqui";
		$this->description = "Painel de Controle";
		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller

		$caminho = 'painel-imobiliaria'; //Retorno do formulario de cadastro
		
		$formulario = $this->parser->parse( 'painelimobiliaria/editar-imovel', array(
			'base_url' => base_url(),
			"caminho" => $caminho,
			"dados" => $r,
			"form" => $form, 
			"envio" => $envio,
			"bloqueado" => $bloqueado,
			"bloqueadoarea" => $bloqueadoarea,
			"select_tipo_imovel" => $select_tipo_imovel,
			"radio_tipo" => $radio_tipo,
			"select_finalidade" => $select_finalidade,
			"select_dormitorios" => $select_dormitorios,
			"select_suites" => $select_suites,
			"select_garagem" => $select_garagem,
			"select_banheiros" => $select_banheiros,
			"radio_mapa" => $radio_mapa,
			"radio_condominio" => $radio_condominio,
			"valor_imovel" => $valor_imovel,
			"texto_rua" => $texto_rua,
			"texto_bairro" => $texto_bairro,

			"estados" => $estados,
			"cidades" => $cidades,
			"cidade_selected" => $cidade_selected,
			"estado_selected" => $estado_selected,

			"imagens" => $imagens,
			"dadosCadastrosOption" => $dadosCadastrosOption
		), true );

		$data = array(
			"formulario_editar" => $formulario
		);

		$this->load( 'painelimobiliaria/editar', $data );

	}
	
	public function editPerfil() {
	
		$id = $this->input->post('id', TRUE);
		$creci = $this->input->post('creci', TRUE);
		$nome = $this->input->post('imobiliaria', TRUE);
		$responsavel = $this->input->post('responsavel', TRUE);
		$endereco = $this->input->post('endereco', TRUE);
		$numero = $this->input->post('numero', TRUE);
		$complemento = $this->input->post('complemento', TRUE);
		$cidade = $this->input->post('cidade', TRUE);
		$estado = $this->input->post('estado', TRUE);
		$email = $this->input->post('email', TRUE);
		$cpf_cnpj = $this->input->post('cpf_cnpj', TRUE);
		$check_cpf_cnpj = $this->input->post('check_cpf_cnpj', TRUE);
		$telefone = $this->input->post('telefone', TRUE);
		$celular = $this->input->post('celular', TRUE);
		$como_soube = $this->input->post('como_soube', TRUE);
		$vendedor = $this->input->post('vendedor', TRUE);
		$contrato = $this->input->post('contrato', TRUE);
		$newsletter =  $this->input->post('newsletter', TRUE);

		$importacao_automatica =  $this->input->post('importacao_automatica', TRUE);
		$link_ivalue =  $this->input->post('link_ivalue', TRUE);
		
		$melhor_horario = $this->input->post('melhor_horario', TRUE);
		
		$senha = nl2br($this->input->post('senha', TRUE));
		$confirmar_senha = nl2br($this->input->post('confirmar_senha', TRUE));
		
		$d = $this->session->userdata('login');
		
		//Validação de Email
		$validacao_email = array(
			'campos' => 'login',
			'tabela' => 'usuarios',
			'where' => array('login' => $email,'id <> '.$d['id']  => NULL)
		);
		
		$this->select->set($validacao_email);
		$total = $this->select->total();
		
		if($total > 0) { //Se existem Emails de usuário iguais
		
			$status = 1;
			$msg = "Email já cadastrado no sistema, use outro email.";
			
		} else {
			$tabela = $this->util->tabelaPerfil($d['tipo']);
			
			$data= array(
			
				'creci' => $creci,
				'nome' => $nome,
				'responsavel' => $responsavel,
				'endereco' => $endereco,
				'numero' => $numero,
				'complemento' => $complemento,
				'cidade' => $cidade,
				'estado' => $estado,
				'celular' => $celular,
				'email' => $email,
				'cpf_cnpj' => $cpf_cnpj,
				'check_cpf_cnpj' => $check_cpf_cnpj,
				'telefone' => $telefone,
				'melhor_horario' => $melhor_horario,
				'como_soube' => $como_soube,
				'vendedor' => $vendedor
			);
			
			$id_perfil = $this->master_model->update_form($tabela,$data,$id);

			//salvando checkbox contrato e newsletter
			$data= array(
				'contrato' => $contrato,
				'newsletter' => $newsletter,
				'login' => $email,
				'importacao_automatica' => $importacao_automatica,
				'link_ivalue' => $link_ivalue
			);
			$id_usuario = $d['id'];
			$ret = $this->master_model->update_form('usuarios',$data,$id_usuario);


			
			if(!empty($senha) && $senha == $confirmar_senha) {
			
				$data= array(
			
					'senha' => md5($senha)
			
				);
				
				$id_usuario = $d['id'];
				
				$ret = $this->master_model->update_form('usuarios',$data,$id_usuario);
				
			}
			$status = 0;
			$msg = "Dados atualizados.";
		}
		header('Content-type: text/json');
		header('Content-type: application/json');

		$json = '{"status" : '.$status.', "msg" : "'.trim($msg).'"}';

		echo $json;
		exit;
	}
	
	public function minhalista() {
		
		$this->checaSessao();

		$this->title = "Minha Lista";
		$this->keywords = "Minha Lista";
		$this->description = "Minha Lista";
		
		$sessao = $this->session->userdata('login');
		
		$id_usuario = $sessao['id'];
		
		$config = array(

			'campos' => 'imo.id, imo.slug, imo.titulo, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.valor',

			'tabela' => 'imoveis imo',

			'join' => array(

						array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'),

						array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left'),
						
						array('imoveis_favoritos fa','fa.id_imovel = imo.id','left')

					),

			'where' => array('imo.status' => 3, 'imo.ativo' => 1, 'fa.id_usuario' => $id_usuario)

		);

		$this->select->set($config);

		$num = $this->select->total();
		
		$lista = '';
		
		if($num == 0) {
			
			$lista = '<h2 class="text-center favoritos_vazio">Não existem imóveis na sua lista de Favoritos.</h2>';
			
		} else {
			
			$r = $this->select->resultado();
			
			$lista = '<ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">';
			
			foreach($r as $val) {

				$config = array(

					'campos' => 'arquivo',

					'tabela' => 'imagens',

					'where' => array('id_imovel' => $val->id, 'destaque' => 1)

				);

				$this->select->set($config);
				$num_destaque = $this->select->total();
				$img_dest = $this->select->resultado();

				if($num_destaque > 0) {

					$imagem = base_url().'assets/uploads/imovel/'.$id_usuario.'/'.$img_dest[0]->arquivo;

				} else {

					$config = array(

						'campos' => 'arquivo',

						'tabela' => 'imagens',

						'where' => array('id_imovel' => $val->id, 'destaque' => 1),

						'limit' => 1

					);

					$this->select->set($config);
					$num_img = $this->select->total();
					$img = $this->select->resultado();

					if($num_img > 0) {

						$imagem = base_url().'assets/uploads/imovel/'.$id_usuario.'/'.$img[0]->arquivo;

					} else {
						
						$config = array(

							'campos' => 'arquivo',
	
							'tabela' => 'imagens',
	
							'where' => array('id_imovel' => $val->id)
	
						);
	
						$this->select->set($config);
						$num_img = $this->select->total();
						$img = $this->select->resultado();
						
						if($num_img > 0) {
							$imagem = base_url().'assets/uploads/imovel/'.$id_usuario.'/'.$img[0]->arquivo;
						} else {
							$imagem = base_url().'assets/images/sem_imagem.jpg';	
						}

					}

				}

				$valor = $this->util->moeda2br($val->valor);
				
				if($val->tipo == 1) { $tipo = 'status_venda.png'; }
				if($val->tipo == 2) { $tipo = 'status_locacao.png'; }
				if($val->tipo == 3) { $tipo = 'status_temporada.png'; }

				$lista .= '<li>
								<div class="locacao_item">

									<div class="imagem">

										<a href="'.base_url().'imovel/'.$val->slug.'">

											<img class="imovel" src="'.$imagem.'">

											<img class="status" src="'.base_url().'assets/images/'.$tipo.'">

										</a>

									</div>

									<div class="dados">

										<a href="'.base_url().'imovel/'.$val->slug.'">

											<h2>#'.$val->id.' - '.$val->titulo.'</h2>

										</a>

										<p><b>Tipo:</b> '.$val->texto_tipo.'</p>

										<p><b>Local:</b> '.$val->endereco.'</p>

										<p><b>Valor:</b> R$ '.$valor.' </p>

									</div>

								</div>

							</li>';

			}
			
			$lista .= '</ul>';
		
		}
		
		$data = array(
			'lista' => $lista
		);
		
		$this->load( 'painelimobiliaria/minhalista', $data );
			
	}
	
	public function doDestaque() {
	
		$id = $this->input->post('id', TRUE);
	
		/* Checagem de Sessão e ID vinculado ao usuario */
		$this->checaSessao();
	
		$l = $this->session->userdata('login');
		
		$id_usuario = $l['id'];
		
		/* Numero de Destaque de Usuario */
		$config = array(
			'campos' => 'num_destaque',
			'tabela' => 'usuarios',
			'where' => array('id' => $id_usuario)
		);
	
		$this->select->set($config);
		$num_d = $this->select->resultado();
		
		$num_destaque = $num_d[0]->num_destaque;
		
		$config = array(
			'campos' => '*',
			'tabela' => 'imoveis',
			'where' => array('id' => $id, 'id_usuario' => $id_usuario)
		);
	
		$this->select->set($config);
		$total = $this->select->total();
		$r = $this->select->resultado();
		
		if($total == 0) {
			redirect(base_url().'painel-imobiliaria');
		}
		/* ------- */
		
		if($r[0]->destaque == 0) {
		
			/* Checagem de Destaques */
			$destaque = 1;
			
				$config = array(
					'campos' => 'id',
					'tabela' => 'imoveis',
					'where' => array('id_usuario' => $id_usuario, 'destaque' => 1)
				);
		
				$this->select->set($config);
				$total_d = $this->select->total();
			
			if($num_destaque <= $total_d) {
			
				$msg = 'Você ja ultrapassou seu número de Anúncios Destaque permitidos ('.$num_destaque.')';
				$status = 1; //Erro
				
			} else {
			
				$msg = '';
				$status = 0;
				
				$data= array(
			
					'destaque' => $destaque
				
				);
				
				$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
				
			}
			
		} else {
		
			$destaque = 0;
			
			$data= array(
			
				'destaque' => $destaque
			
			);
			
			$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
			
			$msg = '';
			$status = 0;
			
		}

		$json = '{"status" : '.$status.', "msg" : "'.$msg.'"}';
		
		header('Content-type: text/json');
		header('Content-type: application/json');

		echo $json;
		exit;

	
	}

	public function doSuperDestaque() {
	
		$id = $this->input->post('id', TRUE);
	
		/* Checagem de Sessão e ID vinculado ao usuario */
		$this->checaSessao();
	
		$l = $this->session->userdata('login');
		
		$id_usuario = $l['id'];
		
		/* Numero de Destaque de Usuario */
		$config = array(
			'campos' => 'num_super_destaque',
			'tabela' => 'usuarios',
			'where' => array('id' => $id_usuario)
		);
	
		$this->select->set($config);
		$num_d = $this->select->resultado();
		
		$num_super_destaque = $num_d[0]->num_super_destaque;
		
		$config = array(
			'campos' => '*',
			'tabela' => 'imoveis',
			'where' => array('id' => $id, 'id_usuario' => $id_usuario)
		);
	
		$this->select->set($config);
		$total = $this->select->total();
		$r = $this->select->resultado();
		
		if($total == 0) {
			redirect(base_url().'painel-imobiliaria');
		}
		/* ------- */
		
		if($r[0]->super_destaque == 0) {

			/* Checagem de Destaques */
			$super_destaque = 1;
			
				$config = array(
					'campos' => 'super_destaque',
					'tabela' => 'imoveis',
					'where' => array('id_usuario' => $id_usuario, 'super_destaque' => 1)
				);
		
				$this->select->set($config);
				$total_d = $this->select->total();

			if($num_super_destaque <= 1) {
				
				$msg = 'Você ja ultrapassou seu número de Anúncios Super Destaque permitidos ('.$num_super_destaque.')'.$total_d;
				$status = 1; //Erro
				
			} else {
			
				$msg = '';
				$status = 0;
				
				$data= array(
			
					'super_destaque' => $super_destaque
				
				);
				
				$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
				
			}
			
		} else {
		
			$super_destaque = 0;
			
			$data= array(
			
				'super_destaque' => $super_destaque
			
			);
			
			$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
			
			$msg = '';
			$status = 0;
			
		}

		$json = '{"status" : '.$status.', "msg" : "'.$msg.'"}';
		
		header('Content-type: text/json');
		header('Content-type: application/json');

		echo $json;
		exit;

	
	}


	public function doDestaqueFeirao() {
	
		$id = $this->input->post('id', TRUE);
	
		/* Checagem de Sessão e ID vinculado ao usuario */
		$this->checaSessao();
	
		$l = $this->session->userdata('login');
		
		$id_usuario = $l['id'];
		
		/* Numero de Destaque de Usuario */
		$config = array(
			'campos' => 'num_destaque_feirao',
			'tabela' => 'usuarios',
			'where' => array('id' => $id_usuario)
		);
	
		$this->select->set($config);
		$num_d = $this->select->resultado();
		
		$num_destaque_feirao = $num_d[0]->num_destaque_feirao;
		
		$config = array(
			'campos' => '*',
			'tabela' => 'imoveis',
			'where' => array('id' => $id, 'id_usuario' => $id_usuario)
		);
	
		$this->select->set($config);
		$total = $this->select->total();
		$r = $this->select->resultado();
		
		if($total == 0) {
			redirect(base_url().'painel-imobiliaria');
		}
		/* ------- */
		
		if($r[0]->destaque_feirao == 0) {
		
			/* Checagem de Destaques */
			$destaque_feirao = 1;
			
				$config = array(
					'campos' => 'id',
					'tabela' => 'imoveis',
					'where' => array('id_usuario' => $id_usuario, 'destaque_feirao' => 1)
				);
		
				$this->select->set($config);
				$total_d = $this->select->total();
			
			if($num_destaque_feirao <= $total_d) {
			
				$msg = 'Você ja ultrapassou seu número de Anúncios Destaque permitidos ('.$num_destaque_feirao.')';
				$status = 1; //Erro
				
			} else {
			
				$msg = '';
				$status = 0;
				
				$data= array(
			
					'destaque_feirao' => $destaque_feirao
				
				);
				
				$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
				
			}
			
		} else {
		
			$destaque_feirao = 0;
			
			$data= array(
			
				'destaque_feirao' => $destaque_feirao
			
			);
			
			$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
			
			$msg = '';
			$status = 0;
			
		}

		$json = '{"status" : '.$status.', "msg" : "'.$msg.'"}';
		
		header('Content-type: text/json');
		header('Content-type: application/json');

		echo $json;
		exit;

	
	}

	public function doSuperDestaqueFeirao() {
	
		$id = $this->input->post('id', TRUE);
	
		/* Checagem de Sessão e ID vinculado ao usuario */
		$this->checaSessao();
	
		$l = $this->session->userdata('login');
		
		$id_usuario = $l['id'];
		
		/* Numero de Destaque de Usuario */
		$config = array(
			'campos' => 'num_super_destaque_feirao',
			'tabela' => 'usuarios',
			'where' => array('id' => $id_usuario)
		);
	
		$this->select->set($config);
		$num_d = $this->select->resultado();
		
		$num_super_destaque_feirao = $num_d[0]->num_super_destaque_feirao;
		
		$config = array(
			'campos' => '*',
			'tabela' => 'imoveis',
			'where' => array('id' => $id, 'id_usuario' => $id_usuario)
		);
	
		$this->select->set($config);
		$total = $this->select->total();
		$r = $this->select->resultado();
		
		if($total == 0) {
			redirect(base_url().'painel-imobiliaria');
		}
		/* ------- */
		
		if($r[0]->super_destaque_feirao == 0) {

			/* Checagem de Destaques */
			$super_destaque_feirao = 1;
			
				$config = array(
					'campos' => 'super_destaque_feirao',
					'tabela' => 'imoveis',
					'where' => array('id_usuario' => $id_usuario, 'super_destaque_feirao' => 1)
				);
		
				$this->select->set($config);
				$total_d = $this->select->total();

			if($num_super_destaque_feirao <= 1) {
				
				$msg = 'Você ja ultrapassou seu número de Anúncios Super Destaque permitidos ('.$num_super_destaque_feirao.')'.$total_d;
				$status = 1; //Erro
				
			} else {
			
				$msg = '';
				$status = 0;
				
				$data= array(
			
					'super_destaque_feirao' => $super_destaque_feirao
				
				);
				
				$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
				
			}
			
		} else {
		
			$super_destaque = 0;
			
			$data= array(
			
				'super_destaque_feirao' => $super_destaque_feirao
			
			);
			
			$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
			
			$msg = '';
			$status = 0;
			
		}

		$json = '{"status" : '.$status.', "msg" : "'.$msg.'"}';
		
		header('Content-type: text/json');
		header('Content-type: application/json');

		echo $json;
		exit;

	
	}
	
	public function doFeirao() {
	
		$id = $this->input->post('id', TRUE);
	
		/* Checagem de Sessão e ID vinculado ao usuario */
		$this->checaSessao();
	
		$l = $this->session->userdata('login');
		
		$id_usuario = $l['id'];
		
		/* Numero de Destaque de Usuario */
		$config = array(
			'campos' => 'num_feirao',
			'tabela' => 'usuarios',
			'where' => array('id' => $id_usuario)
		);
	
		$this->select->set($config);
		$num_d = $this->select->resultado();
		
		$num_feirao = $num_d[0]->num_feirao;
		
		$config = array(
			'campos' => '*',
			'tabela' => 'imoveis',
			'where' => array('id' => $id, 'id_usuario' => $id_usuario)
		);
	
		$this->select->set($config);
		$total = $this->select->total();
		$r = $this->select->resultado();
		
		if($total == 0) {
			redirect(base_url().'painel-imobiliaria');
		}
		/* ------- */
		
		if($r[0]->feirao == 0) {

			/* Checagem de Destaques */
			$feirao = 1;
			
				$config = array(
					'campos' => 'feirao',
					'tabela' => 'imoveis',
					'where' => array('id_usuario' => $id_usuario, 'feirao' => 1)
				);
		
				$this->select->set($config);
				$total_d = $this->select->total();

			if($num_feirao <= 1) {
				
				$msg = 'Você ja ultrapassou seu número de Anúncios Super Destaque permitidos ('.$num_feirao.')'.$total_d;
				$status = 1; //Erro
				
			} else {
			
				$msg = '';
				$status = 0;
				
				$data= array(
			
					'feirao' => $feirao
				
				);
				
				$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
				
			}
			
		} else {
		
			$feirao = 0;
			
			$data= array(
			
				'feirao' => $feirao
			
			);
			
			$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
			
			$msg = '';
			$status = 0;
			
		}

		$json = '{"status" : '.$status.', "msg" : "'.$msg.'"}';
		
		header('Content-type: text/json');
		header('Content-type: application/json');

		echo $json;
		exit;

	
	}

	// public function doFeirao() {
	
	// 	$id = $this->input->post('id', TRUE);
	
	// 	/* Checagem de Sessão e ID vinculado ao usuario */
	// 	$this->checaSessao();
	
	// 	$l = $this->session->userdata('login');
		
	// 	$id_usuario = $l['id'];
		
		
	// 	$config = array(
	// 		'campos' => 'num_feirao',
	// 		'tabela' => 'usuarios',
	// 		'where' => array('id' => $id_usuario)
	// 	);
	
	// 	$this->select->set($config);
	// 	$num_f = $this->select->resultado();
		
	// 	$num_feirao = $num_f[0]->num_feirao;
		
	// 	$config = array(
	// 		'campos' => '*',
	// 		'tabela' => 'imoveis',
	// 		'where' => array('id' => $id, 'id_usuario' => $id_usuario)
	// 	);
	
	// 	$this->select->set($config);
	// 	$total = $this->select->total();
	// 	$r = $this->select->resultado();
		
	// 	if($total == 0) {
	// 		redirect(base_url().'painel-imobiliaria');
	// 	}
	// 	/* ------- */
		
	// 	if($r[0]->feirao == 0) {
		
	// 		$feirao = 1;
			
	// 			$config = array(
	// 				'campos' => 'id',
	// 				'tabela' => 'imoveis',
	// 				'where' => array('id_usuario' => $id_usuario, 'feirao' => 1)
	// 			);
		
	// 			$this->select->set($config);
	// 			$total_d = $this->select->total();
			
	// 		if($num_feirao ===  0 ) {
			
	// 			$msg = 'Você não está aprovado para feirão';
	// 			$status = 1; //Erro
				
	// 		} else {
			
	// 			$msg = '';
	// 			$status = 0;
				
	// 			$data= array(
			
	// 				'feirao' => $feirao
				
	// 			);
				
	// 			$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
				
	// 		}
			
	// 	} else {
		
	// 		$feirao = 0;
			
	// 		$data= array(
			
	// 			'feirao' => $feirao
			
	// 		);
			
	// 		$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
			
	// 		$msg = '';
	// 		$status = 0;
			
	// 	}

	// 	$json = '{"status" : '.$status.', "msg" : "'.$msg.'"}';
		
	// 	header('Content-type: text/json');
	// 	header('Content-type: application/json');

	// 	echo $json;
	// 	exit;

	
	// }
	
	public function doOcultar() {
	
		$id = $this->input->post('id', TRUE);
	
		/* Checagem de Sessão e ID vinculado ao usuario */
		$this->checaSessao();
	
		$l = $this->session->userdata('login');
		
		$id_usuario = $l['id'];
		
		$config = array(
			'campos' => '*',
			'tabela' => 'imoveis',
			'where' => array('id' => $id, 'id_usuario' => $id_usuario)
		);
	
		$this->select->set($config);
		$total = $this->select->total();
		$r = $this->select->resultado();
		
		if($total == 0) {
			redirect(base_url().'painel-imobiliaria');
		}
		/* ------- */
		
		if($r[0]->ativo == 0) {
			$ativo = 1;
		} else {
			$ativo = 0;
		}
		
		$data= array(
		
			'ativo' => $ativo
		
		);
		
		$id_imovel = $this->master_model->update_form('imoveis',$data,$id);
	
	}
	
	public function xml() {
		
		/* Salvar Arquivo XML */
		$uploads_dir = $_SERVER['DOCUMENT_ROOT'].'/novoninho/assets/uploads/xml';
		
		$time = time();
		$tmp_name = $_FILES["xml"]["tmp_name"];
        $name = $_FILES["xml"]["name"];
        move_uploaded_file($tmp_name, "$uploads_dir/".$time.$name);
		
		/* Lendo o arquivo xml */
		
		$xml = simplexml_load_file(base_url().'assets/uploads/xml/'.$time.$name);
		
		$this->salvarDadosXml($xml);
	
	}
	
	public function salvarDadosXml( $xml ) {
	
		foreach($xml as $x) {
		
			$l = $this->session->userdata('login');
		
			$tipo = 1; //Venda Padrão
			$titulo = $x->TituloImovel;
		
			/* Dados Padrões */
			
			$ativo = 1;
			$id_usuario = $l['id'];
			$destaque = 0;
			$status = 3; //Publicado
			$aprovado = 1; //Aprovado
			$slug = $this->util->slug($titulo);
			
			$tipo_imovel = $x->TipoImovel;
			
			/* ID do tipo */
			$config = array(
				'campos' => 'id',
				'tabela' => 'tipo_imovel',
				'where' => array('tipo' => '"'.$tipo_imovel.'"')
			);
		
			$this->select->set($config);
			$num = $this->select->total();
			
			if($num > 0) {
			
				$t = $this->select->resultado();
				$tipo_imovel = $t[0]->id;
				
			} else { //Tipo de imovel padrão (Casa)
			
				$tipo_imovel = 5;
			
			}
			
			$finalidade = $x->Finalidade;
			
			/* ID da Finalidade */
			$config = array(
				'campos' => 'id',
				'tabela' => 'finalidade',
				'where' => array('tipo' => '"'.$finalidade.'"')
			);
		
			$this->select->set($config);
			$num = $this->select->total();
			
			if($num > 0) {
			
				$f = $this->select->resultado();
				$finalidade = $f[0]->id;
				
			} else { //Tipo de imovel padrão (Residencial)
			
				$finalidade = 2;
			
			}
			
			$cep = $x->CEP;
			
			/* Dados de Endereço (Em Base do Cep) */
			
			$numero = $x->Numero;
			
			$config = array(
				'campos' => 'endereco_codigo, bairro_codigo',
				'tabela' => 'cep_endereco',
				'where' => array('endereco_cep' => $cep)
			);
		
			$this->select->set($config);
			$num = $this->select->total();
			
			if($num > 0) {
			
				$da = $this->select->resultado();
				
				$endereco = $da[0]->endereco_codigo;
				$bairro = $da[0]->bairro_codigo;
				
				$config = array(
					'campos' => 'cidade_codigo',
					'tabela' => 'cep_bairro',
					'where' => array('bairro_codigo' => $bairro)
				);
			
				$this->select->set($config);
				$da = $this->select->resultado();
				
				$cidade = $da[0]->cidade_codigo;
				
				$config = array(
					'campos' => 'uf_codigo',
					'tabela' => 'cep_cidade',
					'where' => array('cidade_codigo' => $cidade)
				);
			
				$this->select->set($config);
				$da = $this->select->resultado();
				
				$estado = $da[0]->uf_codigo;
				
			}
			
			$complemento = $x->ComplementoEndereco;
			
			$valor = $x->PrecoVenda;
			
			$area_m2 = $x->AreaTotal;
			
			$dormitorios = $x->QtdDormitorios;
			$suites = $x->QtdSuites;
			$garagem = $x->QtdVagas;
			
			/* Fotos */
			// foreach($x->Imovel->Fotos as $foto) {
				// $this->SalvarImagem($foto);
			// }
			
			/* Dados de Contato */
			$tabela = $this->util->tabelaPerfil($l['tipo']);
		
			/* --------------- */
			
			$config = array(
				'campos' => '*',
				'tabela' => $tabela,
				'where' => array('id' => $l['perfil'])
			);
		
			$this->select->set($config);
			$r = $this->select->resultado();
			
			$telefone = $r[0]->telefone;
			$email = $r[0]->email;
			
			$dados = array(
		
				'ativo' => $ativo,
				'id_usuario' => $id_usuario,
				'status' => $status,
				'aprovado' => $aprovado,
				'destaque' => $destaque,
				'slug' => $slug,
				'titulo' => (string)$titulo,
				'descricao1' => (string)$titulo,
				'tipo' => $tipo,
				'id_tipo_imovel' => $tipo_imovel,
				'finalidade' => $finalidade,
				'dormitorios' => $dormitorios,
				'suites' => $suites,
				'garagem' => $garagem,
				'area_m2' => $area_m2,
				'cep' => $cep,
				'rua' => $endereco,
				'numero' => $numero,
				'complemento' => (string)$complemento,
				'bairro' => $bairro,
				'cidade' => $cidade,
				'estado' => $estado,
				'condominio' => 1, //Padrão Não
				'valor' => $valor,
				'fone' => $telefone,
				'email' => $email
				
			);
	
			$id_imovel = $this->master_model->cadastrar_form('imoveis',$dados);
		
		}
		
		redirect(base_url().'painel-imobiliaria');
	
	}
	
	public function checaSessao() {
	
		$sessao = $this->session->userdata('login');
		
		if(!isset($sessao) || $sessao['logado'] == 0) {
			redirect(base_url());
		} else {
		
			if($sessao['tipo'] == 1 && $this->uri->segment(1) != 'painel-proprietario') {
				redirect(base_url().'painel-proprietario');
			}
			
			if($sessao['tipo'] == 2 && $this->uri->segment(1) != 'painel-corretor') {
				redirect(base_url().'painel-corretor');
			}
			
			if($sessao['tipo'] == 3 && $this->uri->segment(1) != 'painel-imobiliaria') {
				redirect(base_url().'painel-imobiliaria');
			}
		
		}
	
	}
	
	public function checaLimite() {
		
		$sessao = $this->session->userdata('login');
		
		$config = array(
				'campos' => 'num_anuncios',
				'tabela' => 'usuarios',
				'where' => array('id' => $sessao['id'])
			);
			
		$this->select->set($config);
		$r = $this->select->resultado();
		
		$num_anuncios = $r[0]->num_anuncios;
		
		$config = array(
				'campos' => 'id',
				'tabela' => 'imoveis',
				'where' => array('id_usuario' => $sessao['id'])
			);
			
		$this->select->set($config);
			
		$r = $this->select->resultado();
		
		$total_imoveis = $this->select->total();
		
		if($num_anuncios <= $total_imoveis) {
			$this->session->set_userdata('msg_erro', 'Você alcançou seu limite de imóveis!');
			
			redirect(base_url().'painel-imobiliaria');
		}
		
	}
	
	public function limparFotos() {
	
		$d = $this->session->userdata('login');
		
		$id_usuario = $d['id'];
		
		$this->db->query("DELETE FROM imagens WHERE usuario = ".$id_usuario." AND id_imovel = 0");
	
	}

	/* Função referente ao cron de importacao dos clientes */
	public function importacaoImoveisCron() {

		set_time_limit(0);
		ini_set('memory_limit','2000M');
		error_reporting(E_ALL);

		/* Pega os dados de Tipo do imóvel */
		$config = array(
			'campos' => 'tipo, id',
			'tabela' => 'tipo_imovel'
		);
		$this->select->set($config);
		$tipo_imovel = $this->select->resultado();
		$array_tipo_imovel = array();
		foreach($tipo_imovel as $t) {
			$array_tipo_imovel[$t->id] = $t->tipo;
		}
		/* ============================== */

		/* Pega os dados de Fianlidade do imóvel */
		$config = array(
			'campos' => 'tipo, id',
			'tabela' => 'finalidade'
		);
		$this->select->set($config);
		$finalidades = $this->select->resultado();
		$array_finalidade = array();
		foreach($finalidades as $f) {
			$array_finalidade[$f->id] = $f->tipo;
		}
		/* ============================== */

		$config = array(
			'campos' => '*',
			'tabela' => 'usuarios',
			'where' => array('importacao_automatica' => 1, 'link_ivalue != ""' => NULL, 'ativo' => 1)
		);
		$this->select->set($config);
		$imobiliarias = $this->select->resultado();

		foreach($imobiliarias as $imo) {

			//$url = 'http://www.valuegaia.com.br/integra/midia.ashx?midia=GaiaWebServiceImovel&p=hhghkn4dRcQgpAUNL%2fWUqA%3d%3d';
			$url = $imo->link_ivalue;

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url );
			unset($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, 'SOROCABACOM');
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

			$data = curl_exec($ch);
			curl_close($ch);
			$xml = simplexml_load_string($data);

			$imoveis = $xml->Imoveis->Imovel;

			$id_usuario = $imo->id;
			$l['perfil'] = $imo->perfil;
			
			$count = 1;

			/* Guarda os imóveis destaque do usuario */
			$config = array(
				'campos' => 'referencia_xml, id',
				'tabela' => 'imoveis',
				'where' => array('destaque' => 1, 'id_usuario' => $id_usuario)
			);
			$this->select->set($config);
			$imo_d = $this->select->resultado();

			$imoveis_destaque = array();
			foreach($imo_d as $v) {
				$imoveis_destaque[] = $v->referencia_xml;
			}
			/* =================================== */

			$this->db->query("DELETE FROM `imoveis` WHERE `id_usuario` = ".$id_usuario." AND arquivo_xml IS NOT NULL " );
			
			foreach($imoveis as $imo) {
				$ref = trim($imo->CodigoImovel); //Número de Referencia do xml

				/*
				//Verifica se o imovel ja esta no sistema e o deleta para atualizar seus dados
				$config = array(
					'campos' => 'id',
					'tabela' => 'imoveis',
					'where' => 'id_usuario = '.$id_usuario.' AND referencia_xml = "'.$ref.'"'
				);
				$this->select->set($config);
				$total_ref = $this->select->total();
				if($total_ref > 0) { //Se achar o imovel de referencia, exclui o imovel
					$imovel_ref = $this->select->resultado();
					$this->master_model->delete_form('imoveis', $imovel_ref[0]->id, 'id');
					$this->master_model->delete_form('imagens', $imovel_ref[0]->id, 'id_imovel');
				}
				*/

				/* Dados Padrões */
				$data_cadastro = time();
				$data_expira = 0;
				$ativo = 1;
				$id_usuario = $id_usuario; //Teste;
				$status = 3; //Publicado
				$aprovado = 1; //Aprovado
				$mapa = 0;
				$xml = 1;
				$tipo = 1; //Tipo Venda Padrão
				$titulo = $imo->TituloImovel;
				$slug = $this->util->slug($titulo);	
				$slug = $this->checaSlug($slug, 'imoveis');	

				// Verifica se o imovel é destaque
				$destaque = 0;
				if(in_array($ref, $imoveis_destaque)) {
					$destaque = 1;
				}

				//==== Finalidade e tipo do imóvel
				$tipo_imovel = $imo->TipoImovel;
				if( in_array( $tipo_imovel, $array_tipo_imovel ) ) {
					$tipo_imovel = array_search($tipo_imovel,$array_tipo_imovel);
				} else { //Tipo de imovel padrão (Casa)
					$tipo_imovel = 5;
				}
				$finalidade = $imo->Finalidade;
				
				if( in_array( $finalidade, $array_finalidade ) ) {
					$finalidade = array_search($finalidade,$array_finalidade);
				} else { //Tipo de imovel padrão (Residencial)
					$finalidade = 2;
				}
				//================================

				//==== Dados de endereço baseado no cep
				$cep = $imo->CEP;
				if(empty($cep)) {
					$cep = 0;
				}
				
				$numero = 0; 
				if(!empty($imo->Numero)) {
					$numero = $imo->Numero;
				}
				
				$config = array(
					'campos' => 'endereco_codigo, bairro_codigo',
					'tabela' => 'cep_endereco',
					'where' => array('endereco_cep' => $cep)
				);
			
				$this->select->set($config);
				$num = $this->select->total();
				
				if($num > 0) { //Se achar os dados vinculados ao CEP
				
					$da = $this->select->resultado();
					
					$endereco = $da[0]->endereco_codigo;
					$bairro = $da[0]->bairro_codigo;
					
					$config = array(
						'campos' => 'cidade_codigo',
						'tabela' => 'cep_bairro',
						'where' => array('bairro_codigo' => $bairro)
					);
				
					$this->select->set($config);
					$da = $this->select->resultado();
					
					$cidade = $da[0]->cidade_codigo;
					
					$config = array(
						'campos' => 'uf_codigo',
						'tabela' => 'cep_cidade',
						'where' => array('cidade_codigo' => $cidade)
					);
				
					$this->select->set($config);
					$da = $this->select->resultado();
					
					$estado = $da[0]->uf_codigo;
					
				} else { //Procura dado por dado pelo codigo no banco
					//Estado
					$estado = (String)$imo->Estado;
					$config = array(
						'campos' => 'uf_codigo',
						'tabela' => 'cep_uf',
						'where' => array('uf_sigla' => $estado)
					);
				
					$this->select->set($config);
					$da_estado = $this->select->resultado();
					if(isset($da_estado[0]->uf_codigo)) {
						$estado = $da_estado[0]->uf_codigo;
					} else {
						$estado = 0;
					}
					//Cidade
					$cidade = (String)$imo->Cidade;
					$config = array(
						'campos' => 'cidade_codigo',
						'tabela' => 'cep_cidade',
						'where' => array('uf_codigo' => $estado, 'cidade_descricao' => $cidade)
					);
				
					$this->select->set($config);
					$da_cidade = $this->select->resultado();
					if(isset($da_cidade[0]->cidade_codigo)) {
						$cidade = $da_cidade[0]->cidade_codigo;
					} else {
						$cidade = 0;
					}
					//Bairro 
					$bairro = (String)$imo->Bairro;
					$config = array(
						'campos' => 'bairro_codigo',
						'tabela' => 'cep_bairro',
						'where' => array('cidade_codigo' => $cidade, 'bairro_descricao' => $bairro)
					);
				
					$this->select->set($config);
					$da_bairro = $this->select->resultado();
					if(isset($da_bairro[0]->bairro_codigo)) {
						$bairro = $da_bairro[0]->bairro_codigo;
					} else {
						$bairro = 0;
					}
					//Endereco 
					$endereco = (String)$imo->Endereco;
					$config = array(
						'campos' => 'endereco_codigo',
						'tabela' => 'cep_endereco',
						'where' => array('bairro_codigo' => $bairro, 'endereco_logradouro' => $endereco)
					);
				
					$this->select->set($config);
					$da_endereco = $this->select->resultado();
					if(isset($da_endereco[0]->bairro_codigo)) {
						$endereco = $da_endereco[0]->endereco_codigo;
					} else {
						$endereco = 0;
					}
				}
				
				$complemento = '';
				if(!empty($imo->ComplementoEndereco)) {
					$complemento = $imo->ComplementoEndereco;
				}
				
				//=====================================
					
				if (!empty($imo->PrecoVenda) || $imo->PrecoVenda != "") {
					$valor = $imo->PrecoVenda;
				}else{
					$valor = $imo->PrecoLocacao;
					$tipo = 2; //Tipo locação
				}
				$area_m2 = $imo->AreaUtil;
				
				//==== Garagem, dormitorios, banheiros e suites
				
				$dormitorios = 0;
				$suites = 0;
				$banheiros = 0;
				$garagem = 0;
				
				if(!empty($imo->QtdDormitorios)) {
					$dormitorios = $imo->QtdDormitorios;
				}
				$condominio = 1;
				if(!empty($imo->NomeCondominio) AND $imo->NomeCondominio != "") {
					$condominio = 0;
				}else{
					$aux = array('apartamento','Apartamento','Condominio','Condo','Condomínio','condo','condominio','condomínio','Condomínio');
					foreach ($aux as $aux2) {
						$domain = strstr($imo->SubTipoImovel, $aux2);
						if($domain != FALSE){
							$condominio = 0;
						}
					}
				}
				
				if(!empty($imo->QtdSuites)) {
					$suites = $imo->QtdSuites;
				}
				
				if(!empty($imo->QtdBanheiros)) {
					$banheiros = $imo->QtdBanheiros;
				}
				
				if(!empty($imo->QtdVagas)) {
					$garagem = $imo->QtdVagas;
				}
				
				//===============================================
				
				/* Dados de Contato */
				
				$config = array(
					'campos' => '*',
					'tabela' => 'dados_imobiliaria',
					'where' => array('id' => $l['perfil']) //TESTE EMPRESA
				);
			
				$this->select->set($config);
				$r = $this->select->resultado();
				
				$telefone = $r[0]->telefone;
				$email = $r[0]->email;
				
				$dados = array(
					
					'data_cadastro' => $data_cadastro,
					'ativo' => $ativo,
					'id_usuario' => $id_usuario,
					'status' => $status,
					'aprovado' => $aprovado,
					'destaque' => $destaque,
					'slug' => $slug,
					'titulo' => (string)$titulo,
					'descricao1' => (string)$titulo,
					'tipo' => $tipo,
					'id_tipo_imovel' => $tipo_imovel,
					'finalidade' => $finalidade,
					'dormitorios' => $dormitorios,
					'suites' => $suites,
					'garagem' => $garagem,
					'banheiros' => $banheiros,
					'usarDadosContato' => 2,
					'mapa' => $mapa,
					'area_m2' => $area_m2,
					'cep' => $cep,
					'rua' => $endereco,
					'numero' => $numero,
					'complemento' => (string)$complemento,
					'bairro' => $bairro,
					'cidade' => $cidade,
					'estado' => $estado,
					'condominio' => $condominio, //Padrão Não
					'valor' => $valor,
					'fone' => $telefone,
					'email' => $email,
					'referencia_xml' => (string)$ref,
					'arquivo_xml' => ''
					
				);
		
				$id_imovel = $this->master_model->cadastrar_form('imoveis',$dados);
				
				/* Fotos */
				$x = 1;
				$total_imagens = 10;

				if(isset($imo->Fotos->Foto)) {
					foreach($imo->Fotos->Foto as $foto) {
						
						if($x <= $total_imagens) {
							
							$destaque = 0;
							
							if($foto->Principal == 1) {
								$destaque = 1;	
							}
							
							$dados = array(
								'ativo' => 1,
								'data_cadastro' => time(),
								'usuario' => $id_usuario,
								'id_imovel' => $id_imovel,
								'destaque' => $destaque,
								'arquivo' => (String)$foto->URLArquivo
							);
					
							$imagens = $this->master_model->cadastrar_form('imagens',$dados);
							
						}
						
						$x++;
						
					}
				}

				$count++;

				/*
				if($count > 30) {
					die;''
				}
				*/

				echo $count.' - ';
			
			}

		}
		
		$msg = 'Imóveis importados com sucesso!';
		$status = 1;

	}

	public function checaSlug($slug, $tabela) {
		
		$query = $this->db->query("SELECT id FROM ".$tabela." WHERE slug LIKE '".$slug."%'");
	
		if ($query->num_rows() > 0) {
			$slug = $slug.'-'.$query->num_rows();
			
			return $this->checaSlug($slug, $tabela);
		} else {
			
			return $slug;
			
		}
	}
}