<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Main_Controller {

	public function index() {

		$this->title = "Home";
		$this->keywords = "";
		$this->description = "";

		$data = array();
		$data[ "destaques" ] = $this->Destaques();
		$data[ "venda" ] = $this->Venda();
		$data[ "locacao" ] = $this->Locacao();
		$data[ "temporada" ] = $this->Temporada();

		$this->load( 'home', $data );

	}
}