<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sobre extends Main_Controller {

	public function index() {

		$this->title = "Sobre";
		$this->keywords = "Sobre, Aqui";
		$this->description = "Essa é a página Sobre";

		$this->setMenuSidebarSobre();
		$sidebar = $this->getMenuSidebar();

		/*
			O menu lateral pode ser chamado dessa forma se for estatico
			porem se for dinâmico (ex.: categorias de prdutos) ele pode
			ser setado no proprio metodo do controller.
			Ex.:

			$this->menu_sidebar = array(	
				array(
					"class_active" => "",
					"href" => base_url() ."produtos/categoria-1",
					"label"=> "Categorias 1",
					"submenu" => ""
				),	
				array(
					"class_active" => "",
					"href" => base_url() ."produtos/categoria-2",
					"label"=> "Categorias 2",
					"submenu" => ""
				),	
				array(
					"class_active" => "",
					"href" => base_url() ."produtos/categoria-3",
					"label"=> "Categorias 3",
					"submenu" => ""
				),
			);
			
			$sidebar = $this->getMenuSidebar();

		*/

		$data = array(
			"sidebar" => $sidebar
		);

		$this->load( 'sobre/index', $data );

	}

	public function o_que_fazemos() {

		$this->title = "O que fazemos";
		$this->keywords = "O que fazemos, Aqui";
		$this->description = "Essa é a página O que fazemos";

		$this->setMenuSidebarSobre();
		$sidebar = $this->getMenuSidebar();	

		$data = array(
			"sidebar" => $sidebar
		);

		$this->load( 'sobre/o-que-fazemos', $data );

	}
}