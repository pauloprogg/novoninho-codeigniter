<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticia extends Main_Controller {

	public function show( $slug ) {
	
		$config = array(
			'campos' => 'titulo, imagem, texto',
			'tabela' => 'noticias',
			'where' => array('slug' => $slug)
		);
		
		$this->select->set($config);
		
		$val = $r = $this->select->total();
		
		if(empty($slug) || $val == 0) {
			redirect( base_url().'noticias' );	
		}
		
		$r = $this->select->resultado();
		
		$noticia = $r[0];

		$this->title = $noticia->titulo;
		$this->keywords = "";
		$this->description = "";
		$this->image = ""; // O default é setado no controller Main_Controller

		#CSS especifico
		// $this->css[] = array( "href" => base_url()."assets/css/css_especifico_1.css" );
		// $this->css[] = array( "href" => base_url()."assets/css/css_especifico_2.css" );

		#JS especifico
		// $this->js[] = array( "src" => base_url()."assets/js/js_especifico_1.js" );
		// $this->js[] = array( "src" => base_url()."assets/js/js_especifico_2.js" );

		#JS inline especifico
		// $this->js_inline[] = array( "script" => "console.log(\"oi\");" );
		// $this->js_inline[] = array( "script" => "console.log(\"oi 2\");" );


		#campos = campos da tabela - obrigatorio
		#tabela = nome da tabela - obrigatorio
		#where = pode ser array ex array('campo' => 'valor') ou uma string personalizada

		#join = array ex:
				#array(
					#array('comments c','c.id = blogs.id','left'),
					#array('comments','comments.id = blogs.id','left')
				#);

		#orderBy = array ex array('campo' => 'ASC')
		#limit = inteiro - é obrigatorio caso use paginação
		#group = pode ser array ex array("title", "date") ou uma string

		// $config = array(
		// 			'campos' => 'titulo, titulo, texto, arquivo',
		// 			'tabela' => 'noticias',
		// 			'limit' => 1
		// 		);

		// $this->select->set($config);

		// # nome da pagina
		// # numero de link por pagina
		// # usando a paginação tem que chamar ela antes de resultados
		// $paginacao = $this->select->paginacao( "home", 3);

		// #retorna o resultado
		// $resultado = $this->select->resultado();

		// # Exibe ultima query
		// #echo $this->db->last_query();

		// # Retorna o total de registro
		// $total = $this->select->total();

		$data = array(
			'titulo' => $noticia->titulo,
			'texto' => $noticia->texto,
			'imagem' => $noticia->imagem
		);

		$this->load( 'noticia', $data );

	}
}