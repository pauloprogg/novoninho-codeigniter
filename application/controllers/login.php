<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Main_Controller {



	public function logar() {

		$email = $this->input->post('login_email', TRUE);
		$senha = $this->input->post('login_senha', TRUE);
		
		$senha = md5($senha);
			
		$query = array(
			'campos' => 'u.*',
			'tabela' => 'usuarios u',
			'where' => array('login' => $email, 'senha' => $senha)
		);
		
		$this->select->set($query);
		$total = $this->select->total();
		
		if($total > 0) {
		
			$dados = $this->select->resultado();
		
			$tabela = $this->util->tabelaPerfil($dados[0]->tipo);
		
			$config = array(
				'campos' => 'nome, email',
				'tabela' => $tabela,
				'where' => array('id' => $dados[0]->perfil)
			);
			
			$this->select->set($config);
			$contato = $this->select->resultado();
				
			if($dados[0]->ativo == 0 && $dados[0]->tipo != 3) {
			
				$status = 1;
				$msg = 'Sua conta foi desativada, entre em contato conosco pela página de Contato';	
				$caminho = '';
				
			} else {
			
				$status = 0;
				
				$msg = "Login efetivado com sucesso, redirecionando para o seu Painel de Controle";

				$caminho = $this->util->caminhoPainel($dados[0]->tipo);
				
				$newdata = array(
					'login' => array(
					   'id' => $dados[0]->id, //Id do usuário
					   'nome' => $contato[0]->nome,
					   'email' => $contato[0]->email,
					   'perfil' => $dados[0]->perfil, //Id do perfil vinculado
					   'tipo' => $dados[0]->tipo, //Tipo do usuario (1 - Proprietario, 2 - Corretor, 3 - Imobiliaria)
					   'logado' => 1
					)
				);

				$this->session->set_userdata($newdata);
				
			}
			
		} else {
		
			$status = 1;
			$msg = 'Email ou Senha inválidos, verifique os dados e tente novamente!';
			$caminho = '';
		
		}
			
		header('Content-type: text/json');
		header('Content-type: application/json');

		$json = '{"status" : '.$status.', "msg" : "'.trim($msg).'", "caminho" : "'.trim($caminho).'"}';

		echo $json;
		exit;

	}
	
	public function logout() {
	
		$this->session->unset_userdata('login');
		redirect(base_url());
		
	}
	
	public function esqueciSenha() {
	
		$email = $this->input->post('email', TRUE);
		
		$config = array(

			'campos' => '*',
			'tabela' => 'usuarios',
			'where' => array( 'login' => $email )

		);
		
		$this->select->set($config);
		$dados_email = $this->select->resultado();

		if( !empty( $dados_email ) ) {
		
			$nova_senha = $this->util->geraSenha( 8 );

			$data = array(
				'senha' => md5( $nova_senha )
			);

			$u = $this->master_model->update_form('usuarios',$data,$dados_email[0]->id);
		
			$tabela = $this->util->tabelaPerfil($dados_email[0]->tipo);
		
			$config = array(
				'campos' => 'nome, email',
				'tabela' => $tabela,
				'where' => array('id' => $dados_email[0]->perfil)
			);
			
			$this->select->set($config);
			$contato = $this->select->resultado();

			if( $u ) {

				$msg = '

					<html>

						<body style="padding: 30px 0">

							<table width="800" border="0" align="center" cellpadding="15" cellspacing="0">

							<tr>

								<td height="100" valign="top"  style="border: 1px solid #ccc;">

									<h2 style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #02436B; padding: 0; margin: 0 0 10px 0;">Dados do contato</h2>

									<table width="775" border="0" cellspacing="0" cellpadding="6">

										<tr>

											<td >

												<p style="font-family:Arial, Helvetica, sans-serif; font-size: 12px; color:#02436B; padding:0; margin:0;"> 

													Olá, '.$contato[0]->nome.' segue abaixo a sua nova senha</p>

												</td>

											</tr>



											<tr>

												<td ><p style="font-family:Arial, Helvetica, sans-serif; font-size: 12px; color:#02436B; padding:0; margin:0;"><strong>Email:</strong> 

													'.$email.'</p>

												</td>

											</tr>



											<tr>

												<td ><p style="font-family:Arial, Helvetica, sans-serif; font-size: 12px; color:#02436B; padding:0; margin:0;"><strong>Senha:</strong> 

													'.$nova_senha.'</p>

												</td>

											</tr>

										</table>



									</td>

								</tr>

							</table>



							<br />						



						</body>

					</html>

				';		

				$this->load->library('email');	

				$config['protocol'] = 'mail';

				$config['charset'] = 'utf-8';

				$config['wordwrap'] = TRUE;

				$config['mailtype'] = "html";

				$this->email->initialize($config);

				$data = array();

				$to = $email;

				$ca = 'Novo Ninho';			

				$this->email->from( 'contato@novoninho.com', $ca );

				$this->email->to($to);

				$this->email->subject('Novo Ninho | Recuperação de senha');

				$this->email->message($msg);	

				$this->email->send();
		
			}
			
			$status = 0;
		
		} else {
		
			$status = 1;
		
		}
		
		echo $status;
		
	}

	public function trocarCidade() {
		$newdataCidade =  $this->input->post('cidade', TRUE);
		$newdataEstado =  $this->input->post('estado', TRUE);
		$newdataFeirao =  $this->input->post('feirao', TRUE);

		$this->session->set_userdata("cidade", $newdataCidade);
		$this->session->set_userdata("estado", $newdataEstado);
		$this->session->set_userdata("feirao", $newdataFeirao);

		$result = array(
			'success' => true,
			'feirao' => false
		);
		if(!empty($newdataFeirao)){
			$result['feirao'] = true;
		}

		echo json_encode($result);
		/*die;
		$this->load->helper('url');
		redirect('/classificados', 'refresh');*/

	}
}