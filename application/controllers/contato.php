﻿<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contato extends Main_Controller {

	public function index() {

		$config_texto = array(
			'campos' => 'texto1, texto2',
			'tabela' => 'pagina_contato'
		);

		$this->select->set($config_texto);
		$texto = $this->select->resultado();

		$this->title = "Contato";
		$this->keywords = "Contato, Aqui";
		$this->description = "Essa é a Contato";
		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller

		$data = array(
			'texto1' => $texto[0]->texto1,
			'texto2' => $texto[0]->texto2
		);

		$this->load( 'contato', $data );

	}

	public function send() {
		require_once('phpmailer/class.phpmailer.php');
		require_once 'phpmailer_dados.php';

		$nome = $this->input->post('nome', TRUE);
		$email = $this->input->post('email', TRUE);
		$assunto = $this->input->post('assunto', TRUE);
		$nImovel = $this->input->post('nImovel', TRUE);
		$mensagem = nl2br($this->input->post('mensagem', TRUE));
		$nImovel = 'Imóvel numero : '.$nImovel;
		$data = array(
			'nome' => $nome,
			'email' => $email,
			'assunto' => $assunto,
			'mensagem' => '<br>'.$nImovel.'<br>'.$mensagem,
			'data' => time()
		);

		$this->db->insert(
			"contato",
			$data);

		$msg = $this->parser->parse( 'templates/email/contatoSite', $data);

		//$to = 'no-reply@novoninho.com.br';
		// $to = 'luca@sorocabacom.com.br';
		$to = 'contato@novoninho.com.br';
		//$to = 'marcus@sorocabacom.com.br';

		$mailer->AddAddress($to);
		//$mailer->AddAddress('fernando@sorocabacom.com.br');

		$mailer->Subject = utf8_decode('Email de Contato | '.$assunto);

		$mailer->Body = utf8_decode($msg);

		if(!$mailer->Send()){
			die($mailer->ErrorInfo);
		}else{
			return true;
		}
	}

	public function contatoImovel() {

		require_once('phpmailer/class.phpmailer.php');
		require_once 'phpmailer_dados.php';

		$email_contato = $this->input->post('email_contato', TRUE);
		$imovel_titulo = $this->input->post('imovel_titulo', TRUE);

		$slug = $this->input->post('slug', TRUE);
		$nome = $this->input->post('nome', TRUE);
		$email = $this->input->post('email', TRUE);
		$mensagem = nl2br($this->input->post('mensagem', TRUE));

		$config = array(
			'campos' => 'imo.*, tip.tipo as "texto_tipo"',
			'tabela' => 'imoveis imo',
			'join' => array(
						array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left')
					),
			'where' => array('imo.slug' => $slug),
			'limit' => 1
		);
		$this->select->set($config);
		$r = $this->select->resultado();
		$imovel = $r[0];

		if($imovel->condominio == 0) { $condo = 'Sim'; }
		if($imovel->condominio == 1) { $condo = 'Não'; }

		if($imovel->tipo == 1) { $t = 'Venda'; $titulo_tipo = 'titulo_vermelho'; $barra_tipo = 'barra-vermelho'; }
		if($imovel->tipo == 2) { $t = 'Locação'; $titulo_tipo = 'titulo_azul'; $barra_tipo = 'barra-azul'; }
		if($imovel->tipo == 3) { $t = 'Temporada'; $titulo_tipo = 'titulo_cinza'; $barra_tipo = 'barra-cinza'; }

			//finalidade
		$config = array(
			'campos' => 'tipo',
			'tabela' => 'finalidade',
			'where' => array('id' => $imovel->finalidade)
		);

		$this->select->set($config);
		$finalidade = $this->select->resultado();

		$titulo_pagina = $imovel->texto_tipo.' // '.$t;

		$valor = $this->util->moeda2br($imovel->valor);

		if( $imovel->area_m2 > 0 ) {
			$valor_m2 = $imovel->valor / $imovel->area_m2;
			$valor_m2 = $this->util->moeda2br($valor_m2);

		} else {
			$valor_m2 = 0;
		}

		$area = $this->util->moeda2br($imovel->area_m2);
		$area = str_replace('.', '', $area);
		/* Galeria */

		$config = array(
			'campos' => 'arquivo',
			'tabela' => 'imagens',
			'where' => array('id_imovel' => $imovel->id),
			'orderBy' => array( 'destaque' => 'DESC', 'id' => 'DESC')
		);

		$this->select->set($config);
		$num_img = $this->select->total();
		$img = $this->select->resultado();

		/* Verifica se a imagem existe */
		foreach($img as $x => $val) {

			$arquivo = trim($val->arquivo);
			$path = '/assets/uploads/imovel/'.$imovel->id_usuario.'/';

			if(file_exists(getcwd().$path.$arquivo)) {
				$img[$x]->arquivo = base_url().$path.$arquivo;
			} else {
				$img[$x]->arquivo = $arquivo;
			}

		}

		$config = array(
			'campos' => 'arquivo',
			'tabela' => 'imagens',
			'where' => array('id_imovel' => $imovel->id, 'destaque' => 1),
			'limit' => 1
		);

		$this->select->set($config);
		$img_dest = $this->select->resultado();

		if( !empty($img_dest) ) {
			$imagem_principal = '/assets/uploads/imovel/'.$imovel->id_usuario.'/'.trim($img_dest[0]->arquivo);
			$arq = $img_dest[0]->arquivo;
		} else
		if($num_img > 0) {
			$imagem_principal = '/assets/uploads/imovel/'.$imovel->id_usuario.'/'.trim($img[0]->arquivo);
			$arq = $img[0]->arquivo;
		} else {
			$imagem_principal = '/assets/images/sem_imagem.jpg';
			$arq = 'assets/images/sem_imagem.jpg';
		}

		if(file_exists( $_SERVER['DOCUMENT_ROOT'].$imagem_principal )) {
			$imagem_principal = base_url().$imagem_principal;
		} else {
			$imagem_principal = $arq;
		}

		// echo $imagem_principal;die;
		$this->title = $imovel->titulo;
		$this->keywords = "";
		$this->description = "";

		$data = array(
			'nome' => $nome,
			'email' => $email,
			'assunto' => 'Interesse em Imóvel - Novo Ninho',
			'mensagem' => $mensagem,
			"condominio" => $condo,
			"dados" => $r,
			"numero_referencia" => $imovel->referencia_xml,
			"finalidade_texto" => $finalidade[0]->tipo,
			"valor_br" => $valor,
			"valor_m2" => $valor_m2,
			"area" => $area,
			"imagem_principal" => $imagem_principal,
		);

		$msg = $this->parser->parse( 'templates/email/contato', $data, true);

		$to = $email_contato;
		$mailer->AddAddress($to);

		//$mailer->AddAddress('fernando@sorocabacom.com.br');

		$mailer->Subject = utf8_decode('Interesse em Imóvel | '.$imovel_titulo);

		$mailer->Body = utf8_decode($msg);

		if(!$mailer->Send()){
			echo "bad";
			return false;
		}else{
			echo 'sucesso';
			return true;
		}

	}

}