<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Painel_corretor extends Main_Controller {

private $pagina = "painel-corretor";

	private $tabela = "imoveis";

	public function index() {

	

		$this->checaSessao();

		$this->limparFotos();



		$this->title = "Painel de Controle";

		$this->keywords = "Painel de Controle, Aqui";

		$this->description = "Painel de Controle";

		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller

		

		$sessao = $this->session->userdata('login');

		

		$id_usuario = $sessao['id'];

		

		$this->db->cache_delete_all();

		

		$config = array(

			'campos' => 'id, titulo, status, destaque, feirao, super_destaque, data_expira, aprovado',

			'tabela' => 'imoveis',

			'where' => array('id_usuario' => $id_usuario)

		);

		if(!empty($_GET['tipo'])){
				
				$config['where']['tipo'] = $_GET['tipo'];			
			}

			if(!empty($_GET['destaque'])){
				$config['where']['destaque'] = $_GET['destaque'];			
			}

			if(!empty($_GET['super_destaque'])){
				$config['where']['super_destaque'] = $_GET['super_destaque'];			
			}

			if(!empty($_GET['feirao'])){
				$config['where']['feirao'] = $_GET['feirao'];			
			}


			if(!empty($_GET['destaque_feirao'])){
				$config['where']['destaque_feirao'] = $_GET['destaque_feirao'];			
			}

			if(!empty($_GET['super_destaque_feirao'])){
				$config['where']['super_destaque_feirao'] = $_GET['super_destaque_feirao'];			
			}

			if(!empty($_GET['tipo_imovel'])){
				$config['where']['id_tipo_imovel'] = $_GET['tipo_imovel'];			
			}
			
			if(!empty($_GET['dormitorios'])){
				$config['where']['dormitorios'] = $_GET['dormitorios'];			
			}

			if(!empty($_GET['finalidade'])){
				$config['where']['finalidade'] = $_GET['finalidade'];			
			}

			if(!empty($_GET['valor'])){
				$config['where']['valor >='] = $_GET['valor'];		
			}

			if(!empty($_GET['referencia_xml'])){
				$config['where']['referencia_xml'] = $_GET['referencia_xml'];
			}

			if(!empty($_GET['id'])){
				$config['where']['id'] = $_GET['id'];
			}

		

		$this->select->set($config);

		$num = $this->select->total();

		$resultado = $this->select->resultado();

		

		$botao_divulgar = '';

		

		if($num > 0) {

		

			$lista_imoveis = '';

			

			foreach($resultado as $v) {


				$imgImovel = array(

					'campos' => 'arquivo',

					'tabela' => 'imagens',

					'where' => array('id_imovel' => $v->id, 'destaque' => 1)

				);

				$this->select->set($imgImovel);

				$img = $this->select->resultado();
				$folder = base_url() ."../assets/uploads/imovel/".$id_usuario."/";
				$imgOriginal = $folder.$img[0]->arquivo;

			

				if($v->status == 1) {

					$lista_imoveis .= $this->parser->parse( 'templates/status_imovel/aguardando_pagamento', array( "base_utl" => base_url(), "id" => $v->id, "titulo" => $v->titulo, "imgOriginal" => $imgOriginal, "caminho" => 'painel-corretor' ), true );

				} else if($v->status == 2) {

					$lista_imoveis .= $this->parser->parse( 'templates/status_imovel/aguardando_aprovacao', array( "base_utl" => base_url(), "id" => $v->id, "titulo" => $v->titulo, "imgOriginal" => $imgOriginal, "caminho" => 'painel-corretor' ), true );

				} else if($v->status == 3 && $v->aprovado == 0) {

					$lista_imoveis .= $this->parser->parse( 'templates/status_imovel/pago', array( "base_utl" => base_url(), "id" => $v->id, "titulo" => $v->titulo, "imgOriginal" => $imgOriginal, "caminho" => 'painel-corretor' ), true );

				} else if($v->status == 3 && $v->aprovado == 1) {

				

					// if($v->destaque == 0) {

					// 	$d = 'status_publicado';

					// } else {

					// 	$d = 'status_destaque';

					// }

					if(($v->feirao == 1) && ($v->destaque == 1)){

						$d = 'status_destaque_feirao';

					}elseif($v->destaque == 1) {

						$d = 'status_destaque';

					}elseif($v->feirao == 1){

						$d = 'status_feirao';

					}elseif($v->super_destaque == 1){

						$d = 'status_super_destaque';

					}elseif(($v->feirao == 0) && ($v->destaque == 0) && ($v->super_destaque == 0)) {

						$d = 'status_publicado';

					}

					

					$data = date('d/m/Y' , $v->data_expira);

				

					$lista_imoveis .= $this->parser->parse( 'templates/status_imovel/publicado', array( "base_utl" => base_url(), "id" => $v->id, "titulo" => $v->titulo, "imgOriginal" => $imgOriginal, "data_expira" => $data, "status" => $d, "caminho" => 'painel-corretor' ), true );

					

				}

			

			}

		

		} else {

		

			$lista_imoveis = '<div class="linha">

		

								<div class="columns small-12 medium-12 large-12">

									Não Existem Imóveis Cadastrados.

								</div>

	

							</div>';

		

		}

		

		/*//Checagem para aparecer o botão de divulgar

		$config = array(

			'campos' => 'id',

			'tabela' => 'imoveis',

			'where' => array('status' => 1, 'id_usuario' => $id_usuario)

		);

		*/

		$totalVenda = $this->db->query("SELECT id FROM imoveis WHERE status<>3 and id_usuario=".$id_usuario);

		

		if($totalVenda->num_rows > 0) {

			

			$botao_divulgar = '<div class="row">

			

								<div class="columns small-12 medium-6 large-6 left text-left pagar selecionar_todos">

									

									<input type="checkbox" id="select_todos"> Selecionar Todos

									

								</div>

		

								<div class="columns small-12 medium-6 large-6 pagar">

									

									<a id="enviar_publicar" action="{base_url}painel/salvarDadosPublicar" caminho="painel-corretor" class="botao_azul">Próximo</a>

									

								</div>

								

							</div>';

			

		}



		$data = array(

			'lista_imoveis' => $lista_imoveis,

			'botao_divulgar' => $botao_divulgar

		);



		$this->load( 'painelcorretor/imoveis', $data );



	}

	

	public function editarperfil() {

	

		$this->checaSessao();



		$this->title = "Painel de Controle";

		$this->keywords = "Painel de Controle, Aqui";

		$this->description = "Painel de Controle";

		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller



		$data = array();

		

		$d = $this->session->userdata('login');

		

		$tabela = $this->util->tabelaPerfil($d['tipo']);

		

		$config = array(

			'campos' => '*',

			'tabela' => $tabela,

			'where' => array('id' => $d['perfil'])

		);

		$this->select->set($config);

		$r = $this->select->resultado();

		

		//*****respostas como soube		

		$query = array(

			'campos' => 'texto',

			'tabela' => 'como_soube',

			'where' => array()

		);

		$this->select->set($query);

		$respostas = $this->select->resultado();

		

		//*******seleciona usuario

		$user = array(

			'campos' => '*',

			'tabela' =>'usuarios',

			'where' => array('perfil' => $d['perfil'], 'tipo' => $d['tipo'])

		);

		$this->select->set($user);

		$userdados = $this->select->resultado();

		$newsletter='';

		if($userdados[0]->newsletter == 0){

			$newsletter = '<label>

								<input type="checkbox" name="newsletter" id="newsletter" value="1">

								<span><span style="font-weight: bold;color: #008cba;">Aceito e quero receber novidades do Novo Ninho</span></span>

							</label>';

		}else{

			$newsletter = '<label style="display:none;">

								<input type="checkbox" name="newsletter" id="newsletter" value="1" checked="checked">

								<span><span style="font-weight: bold;color: #008cba;">Aceito e quero receber novidades do Novo Ninho</span></span>

							</label>';

		}

		$modal_termos = $this->parser->parse( 'templates/modal_termos', array(

			'base_url' => base_url(), 'titulo' => $t[0]->titulo, 'texto' => $t[0]->texto

			), true );









		$selectResp = '<label>Como Soube?</label>

						<select name="como_soube" class="campo"><option value="">Não selecionado</option>';

		$select='';

		foreach ($respostas as $resposta) {

			if ($r[0]->como_soube == $resposta->texto) {

				$select='selected';

			}else{$select='';}

			$selectResp.='<option value="'.$resposta->texto.'" '.$select.'>'.$resposta->texto.'</value>';

		}

		$selectResp.='</select>';







		//vendedores

		$query = array(

			'campos' => 'nome',

			'tabela' => 'vendedores',

			'where' => array('ativo' => 1)

		);

		

		$this->select->set($query);

		$vendedores = $this->select->resultado();

		$selectVendedor = '<label>Vendedor</label>

						<select name="vendedor" class="campo"><option value="">Não selecionado</option>';

		$select='';

		foreach ($vendedores as $vendedor) {

			if ($r[0]->vendedor == $vendedor->nome) {

				$select='selected';

			}else{$select='';}

			$selectVendedor.='<option value="'.$vendedor->nome.'" '.$select.'>'.$vendedor->nome.'</value>';

		}

		$selectVendedor.='</select>';





//**************************		

		$classe_mascara = '';

		$check_cpf = '';	

		$check_cnpj = '';

		if($r[0]->check_cpf_cnpj == 1) {

			$classe_mascara = 'cpf';

			$check_cpf = 'checked';	

		} else {

			$classe_mascara = 'cnpj';

			$check_cnpj = 'checked';	

		}



		$data = array(

			"dados" => $r,

			"classe_mascara" => $classe_mascara,

			"check_cnpj" => $check_cnpj,

			"selectResp" => $selectResp,

			"check_cpf" => $check_cpf,

			"selectVendedor" => $selectVendedor,

			"newsletter" => $newsletter,

			"modal_termos" => $modal_termos

		);



		$this->load( 'painelcorretor/editarperfil', $data );



	}

	

	public function cadastrar() {

	

		$this->checaSessao();

		

		$config = array(

			'campos' => 'tipo, id',

			'tabela' => 'tipo_imovel'

		);

		

		$this->select->set($config);

		$tipo_imovel = $this->select->resultado();

		

		$config = array(

			'campos' => 'tipo, id',

			'tabela' => 'finalidade'

		);

		

		$this->select->set($config);

		$finalidade = $this->select->resultado();



		$this->title = "Painel de Controle";

		$this->keywords = "Painel de Controle, Aqui";

		$this->description = "Painel de Controle";

		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller



		$caminho = 'painel-corretor'; //Retorno do formulario de cadastro

		

		$l = $this->session->userdata('login');

		

		$id_usuario = $l['id'];

		

		$formulario = $this->parser->parse( 'templates/imovel/cadastrar', array(

			'id_usuario' => $id_usuario,

			'base_url' => base_url(),

			"caminho" => $caminho,

			"tipo_imovel" => $tipo_imovel,

			"finalidade" => $finalidade

		), true );

		

		$data = array(

			"formulario_cadastro" => $formulario

		);



		$this->load( 'painelcorretor/cadastrar', $data );



	}

	

	public function editar( $id ) {

	

		$this->checaSessao();

		

		$l = $this->session->userdata('login');

		

		$id_usuario = $l['id'];

		

		$config = array(

			'campos' => '*',

			'tabela' => 'imoveis',

			'where' => array('id' => $id, 'id_usuario' => $id_usuario)

		);

	

		$this->select->set($config);

		$total = $this->select->total();

		$r = $this->select->resultado();

		

		if($total == 0) {

			redirect(base_url().'painel-corretor');

		}



		$i = $r[0];

		

		/* Validação de Status */

		

		if($i->status == 1) {

		

			$form = 'editar_imovel';

			$envio = 'editImovel';

			$bloqueado = '';

			$bloqueadoarea='';

		

		} else {

		

			$form = 'editar_imovel_bloqueado';

			$envio = 'editImovelBloqueado';

			// $bloqueado = 'readonly="readonly"';

			$bloqueado = 'disabled="disabled"';

			$bloqueadoarea='disabled';

		

		}

		

		$config = array(

			'campos' => 'tipo as "nome_tipo", id as "id_tipo"',

			'tabela' => 'tipo_imovel'

		);

		

		$this->select->set($config);

		$tipo_imovel = $this->select->resultado();

		

		$config = array(

			'campos' => 'tipo, id',

			'tabela' => 'finalidade'

		);

		

		$this->select->set($config);

		$finalidade = $this->select->resultado();

		

		$valor_imovel = $this->util->moeda2br($i->valor);

		

		/* Configuração dos Campos */

		

		//------- Radio Tipo

		$check_venda = '';

		$check_locacao = '';

		$check_temporada = '';

		

		if($i->tipo == 1) { $check_venda = 'checked="checked"'; }

		if($i->tipo == 2) { $check_locacao = 'checked="checked"'; }

		if($i->tipo == 3) { $check_temporada = 'checked="checked"'; }

		

		$radio_tipo = '<div class="row">

				

							<div class="columns small-4 medium-4 large-4">

								<input type="radio" name="tipo_imovel" id="tipo_imovel" value="1" class="radio_tipo" '.$bloqueado.' '.$check_venda.'><span>Venda</span>

							</div>

							

							<div class="columns small-4 medium-4 large-4">

								<input type="radio" name="tipo_imovel" id="tipo_imovel" value="2" class="radio_tipo" '.$bloqueado.' '.$check_locacao.'><span>Locação</span>

							</div>

							

							<div class="columns small-4 medium-4 large-4">

								<input type="radio" name="tipo_imovel" id="tipo_imovel" value="3" class="radio_tipo" '.$bloqueado.' '.$check_temporada.'><span>Temporada</span>

							</div>

							

						</div>';

		//----------

						

		//----- Select Tipo de Imóvel

		$select_tipo_imovel = '<select name="tipo" id="tipo" '.$bloqueado.'>';

		

		foreach($tipo_imovel as $val ) {

				

				$check = '';

				if($val->id_tipo == $i->id_tipo_imovel) $check = 'selected="selected"';

		

				$select_tipo_imovel .= '<option value="'.$val->id_tipo.'" '.$check.'>'.$val->nome_tipo.'</option>';

				

		}

		

		$select_tipo_imovel .= '</select>'; 

		

		//----- Select Finalidade

		

		$select_finalidade = '<select name="finalidade" id="finalidade" '.$bloqueado.'>';

		

		foreach($finalidade as $val ) {

				

				$check = '';

				if($val->id == $i->finalidade) $check = 'selected="selected"';

		

				$select_finalidade .= '<option value="'.$val->id.'" '.$check.'>'.$val->tipo.'</option>';

				

		}

		

		$select_finalidade .= '</select>'; 

	

		//----------

		

		//----- Select Dormitorioes

		

		$select_dormitorios = '<select name="dormitorios" id="dormitorios" '.$bloqueado.'>';

		

		for($x = 0;$x <= 10;$x++) {

				

				$check = '';

				if($x == $i->dormitorios) $check = 'selected="selected"';

		

				$select_dormitorios .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';

				

		}

		

		$select_dormitorios .= '</select>'; 

	

		//----------

		

		//----- Select Suites

		

		$select_suites = '<select name="suites" id="suites" '.$bloqueado.'>';

		

		for($x = 0;$x <= 10;$x++) {

				

				$check = '';

				if($x == $i->suites) $check = 'selected="selected"';

		

				$select_suites .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';

				

		}

		

		$select_suites .= '</select>'; 

	

		//----------

		

		//----- Select Garagem

		

		$select_garagem = '<select name="garagem" id="garagem" '.$bloqueado.'>';

		

		for($x = 0;$x <= 10;$x++) {

				

				$check = '';

				if($x == $i->garagem) $check = 'selected="selected"';

		

				$select_garagem .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';

				

		}

		

		$select_garagem .= '</select>'; 

	

		//----------

		

		//----- Select Banheiros

		

		$select_banheiros = '<select name="banheiros" id="banheiros" '.$bloqueado.'>';

		

		for($x = 0;$x <= 10;$x++) {

				

				$check = '';

				if($x == $i->banheiros) $check = 'selected="selected"';

		

				$select_banheiros .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';

				

		}

		

		$select_banheiros .= '</select>'; 

	

		//----------

		

		//------- Radio Mapa

		$check_mapa_sim = '';

		$check_mapa_nao = '';

		

		if($i->mapa == 1) { $check_mapa_sim = 'checked="checked"'; }

		if($i->mapa == 0) { $check_mapa_nao = 'checked="checked"'; }

		

		$radio_mapa = '<div class="columns small-6 medium-6 large-6">

							<input type="radio" name="mapa" id="mapa" value="1" class="radio_tipo" '.$check_mapa_sim.' ><span>Sim</span>

						</div>

					

						<div class="columns small-6 medium-6 large-6">

							<input type="radio" name="mapa" id="mapa" value="0" class="radio_tipo" '.$check_mapa_nao.' ><span>Não</span>

						</div>';

		//----------

		

		//------- Radio Condominio

		$check_cond_sim = '';

		$check_cond_nao = '';

		

		if($i->condominio == 0) { $check_cond_sim = 'checked="checked"'; }

		if($i->condominio == 1) { $check_cond_nao = 'checked="checked"'; }

		

		$radio_condominio = '<div class="columns small-6 medium-6 large-6">

							<input type="radio" name="condominio" id="condominio" value="0" class="radio_tipo" '.$check_cond_sim.' '.$bloqueado.'><span>Sim</span>

						</div>

					

						<div class="columns small-6 medium-6 large-6">

							<input type="radio" name="condominio" id="condominio" value="1" class="radio_tipo" '.$check_cond_nao.' '.$bloqueado.'><span>Não</span>

						</div>';

		//----------

		

		/* Galeria de Fotos */

		

		$config = array(

			'campos' => 'id as "id_imagem", arquivo, destaque',

			'tabela' => 'imagens',

			'where' => array('id_imovel' => $id)

		);

		

		$this->select->set($config);

		$imagens = $this->select->resultado();

		

		foreach($imagens as $x => $val) {



			if($val->destaque == 1) {

				$checked = 'checked="checked"';

			} else {

				$checked = '';

			}

			

			$imagens[$x]->destaque_texto = $checked;

		

		}	

		

		//Dados do endereço

		

		$cep = str_replace('-', '', $i->cep);

		

		$config = array(

			'campos' => 'r.endereco_logradouro as "rua", b.bairro_descricao as "bairro", c.cidade_descricao as "cidade", e.uf_sigla as "estado"',

			'tabela' => 'cep_endereco r',

			'join' => array(

				array('cep_bairro b','b.bairro_codigo = r.bairro_codigo','left'),

				array('cep_cidade c','c.cidade_codigo = b.cidade_codigo','left'),

				array('cep_uf e','e.uf_codigo = c.uf_codigo','left')

			),

			'where' => array('r.endereco_cep' => $cep)

		);

		

		$this->select->set($config);

		$end_total = $this->select->total();

		

		if($end_total > 0) { //Cep normal

			

			$dados_endereco = $this->select->resultado();

			

			$dados_end = $dados_endereco[0];

			

			$texto_rua = $dados_end->rua;

			$texto_bairro = $dados_end->bairro;

			

		} else { //Procura de cep padrao da cidade

			

			$config = array(

				'campos' => 'c.cidade_descricao as "cidade", e.uf_sigla as "estado"',

				'tabela' => 'cep_cidade c',

				'join' => array(

					array('cep_uf e','e.uf_codigo = c.uf_codigo','left')

				),

				'where' => array('c.cidade_cep' => $i->cep)

			);

			

			$this->select->set($config);

			$dados_endereco = $this->select->resultado();

			

			$dados_end = $dados_endereco[0];

			

			$texto_rua = '';

			$texto_bairro = '';

			

		}



		$dadosCadastrosOption ='';



		$dadosCadastrosOption ='<div class="columns small-12 medium-6 large-6">

									<input type="radio" name="usar_contato" '.$bloqueado.' id="usar_contato" value="1" class="radio_tipo usar_contato" '.($r[0]->usarDadosContato == 1 ? 'checked="checked"' : '' ).'><span>Sim</span>

								</div>

								<div class="columns small-12 medium-6 large-6">

									<input type="radio" name="usar_contato" '.$bloqueado.' id="usar_contato" value="2" class="radio_tipo usar_contato" '.($r[0]->usarDadosContato == 0 ? 'checked="checked"' : '' ).'><span>Não</span>

								</div>';



		 

		//=======================================

		

		$this->title = "Painel de Controle";

		$this->keywords = "Painel de Controle, Aqui";

		$this->description = "Painel de Controle";

		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller

		

		$caminho = 'painel-corretor'; //Retorno do formulario de cadastro

		

		$formulario = $this->parser->parse( 'templates/imovel/editar', array(

			'base_url' => base_url(),

			"caminho" => $caminho,

			"dados" => $r,

			"form" => $form, 

			"envio" => $envio,

			"bloqueado" => $bloqueado,

			"bloqueadoarea" => $bloqueadoarea,

			"select_tipo_imovel" => $select_tipo_imovel,

			"radio_tipo" => $radio_tipo,

			"select_finalidade" => $select_finalidade,

			"select_dormitorios" => $select_dormitorios,

			"select_suites" => $select_suites,

			"select_garagem" => $select_garagem,

			"select_banheiros" => $select_banheiros,

			"radio_mapa" => $radio_mapa,

			"radio_condominio" => $radio_condominio,

			"valor_imovel" => $valor_imovel,

			"texto_rua" => $texto_rua,

			"texto_bairro" => $texto_bairro,

			"texto_cidade" => $dados_end->cidade,

			"texto_estado" => $dados_end->estado,

			"imagens" => $imagens,

			"dadosCadastrosOption" => $dadosCadastrosOption

		), true );



		$data = array(

			"formulario_editar" => $formulario

		);



		$this->load( 'painelcorretor/editar', $data );



	}

	

	public function publicar() {

	

		$this->checaSessao();

		

		/* Pegar preços do usuarios */

		

		$us = $this->session->userdata('login');

		

		$precos = array(

			'campos' => 'preco_normal, preco_destaque, expiracao, preco_feirao, preco_super_destaque, preco_destaque_feirao, preco_super_destaque_feirao',

			'tabela' => 'usuarios',

			'where' => array('id' => $us['id'])

		);

		

		$this->select->set($precos);

		$p = $this->select->resultado();

		

		$preco_normal = $p[0]->preco_normal;

		$preco_destaque = $p[0]->preco_destaque;

		$preco_super_destaque = $p[0]->preco_super_destaque;
		
		$preco_feirao = $p[0]->preco_feirao;

		$preco_destaque_feirao = $p[0]->preco_destaque_feirao;

		$preco_super_destaque_feirao = $p[0]->preco_super_destaque_feirao;
		



		$dias = $p[0]->expiracao;

		

		$p = $this->session->userdata('publicar');

		

		if(empty($p)) {

			redirect(base_url().'painel-corretor');

		}

		

		//$this->session->unset_userdata('publicar');

		

		$imov = '';

		$c = 1;

		

		$preco_total = 0;

		

		foreach($p as $val) {

		

			$id = $val['id'];

			

			$preco = $this->util->moeda2br($preco_normal);
				$texto_anuncio[] = 'Anúncio Normal';

				$preco_total += $preco_normal;

			
			if($val['feirao'] == 1){
				$preco = $preco_feirao;
				$texto_anuncio[] = 'Feirao';

				$preco_total += $preco_feirao;
			}

			if($val['destaque'] == 1){
				$preco = $preco_destaque;
				$texto_anuncio[] = 'Destaque';

				$preco_total += $preco_destaque;
			}

			if($val['destaque_feirao'] == 1){
				$preco = $preco_destaque_feirao;
				$texto_anuncio[] = 'Destaque Feirão';

				$preco_total += $preco_destaque_feirao;
			}


			if($val['super_destaque'] == 1){
				$preco = $preco_super_destaque;
				$texto_anuncio[] = 'Super Destaque';

				$preco_total += $preco_super_destaque;
			}
			
			if($val['super_destaque_feirao'] == 1){
				$preco = $preco_super_destaque_feirao;
				$texto_anuncio[] = 'Super Destaque Feirão';

				$preco_total += $preco_super_destaque_feirao;	
			}


			$texto_anuncio = implode(', ', $texto_anuncio);

			

			/* Dados do Imóvel */

			

			$dados = array(

				'campos' => 'imo.*, tip.tipo as "texto_tipo"',

				'tabela' => 'imoveis imo',

				'join' => array(

					array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left')

				),

				'where' => array('imo.id' => $id)

			);

			

			$this->select->set($dados);

			$d = $this->select->resultado();

			

			$da = $d[0];

			

			if($da->tipo == 1) { $tipo = 'Venda'; }

			if($da->tipo == 2) { $tipo = 'Locacao'; }

			if($da->tipo == 3) { $tipo = 'Temporada'; }

			

			$valor = $this->util->moeda2br($da->valor);

			

			$imov .= '<div class="row linha" data-id="'.$c.'">

					

						<div class="columns small-6 medium-6 large-6 coluna">

							<label class="end">'.$da->titulo.'</label>

						</div>

						

						<div class="columns small-2 medium-2 large-2 coluna">

							<label class="normal">'.$texto_anuncio.'</label>

						</div>

						

						<div class="columns small-2 medium-2 large-2 coluna">

							<label class="normal">Duração '.$dias.' Dias</label>

						</div>

						

						<div class="columns small-2 medium-2 large-2">

						

							<div class="row">

								<div class="columns small-9 medium-9 large-9 coluna">

									'.($preco > 0 ? '<label class="valor">R$ '.$preco : '<label class="valor green">GRÁTIS').'</label>

								</div>

								

								<div class="columns small-3 medium-3 large-3 div_titulo">

									<img src="{base_url}assets/images/seta_baixo.png" class="seta1">

									<img src="{base_url}assets/images/seta_cima.png" class="seta2">

								</div>

							</div>

						</div>

						

					</div>	

		

					<div class="row div_conteudo" id="'.$c.'">

					

						<div class="columns small-12 medium-12 large-12">

							<label><strong>Imóvel:</strong> '.$tipo.'</label>

						</div>

						

						<div class="columns small-12 medium-12 large-12">

							<label><strong>Tipo:</strong> '.$da->texto_tipo.'</label>

						</div>

						

						<div class="columns small-12 medium-12 large-12">

							<label><strong>Dormitórios:</strong> '.$da->dormitorios.'</label>

						</div>

						

						<div class="columns small-12 medium-12 large-12">

							<label><strong>Suítes:</strong> '.$da->suites.'</label>

						</div>

						

						<div class="columns small-12 medium-12 large-12">

							<label><strong>Garagem:</strong> '.$da->garagem.'</label>

						</div>

						

						<div class="columns small-12 medium-12 large-12">

							<label><strong>Valor:</strong> R$ '.$valor.'</label>

						</div>

						

					</div>';

			

			$c++;

			

		}

		

		$total = $this->util->moeda2br($preco_total);



		$this->title = "Painel de Controle";

		$this->keywords = "Painel de Controle, Aqui";

		$this->description = "Painel de Controle";

		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller



		$data = array(

			'lista' => $imov,

			'total' => $total

		);



		$this->load( 'painelcorretor/publicar', $data );



	}

	

	public function editPerfil() {

	

		

		$id = $this->input->post('id', TRUE);

		$creci = $this->input->post('creci', TRUE);

		$nome = $this->input->post('nome', TRUE);

		$cpf_cnpj = $this->input->post('cpf_cnpj', TRUE);

		$check_cpf_cnpj = $this->input->post('check_cpf_cnpj', TRUE);

		$email = $this->input->post('email', TRUE);

		$telefone = $this->input->post('telefone', TRUE);

		$melhor_horario = $this->input->post('melhor_horario', TRUE);

		$senha = nl2br($this->input->post('senha', TRUE));

		$confirmar_senha = nl2br($this->input->post('confirmar_senha', TRUE));

		$como_soube = $this->input->post('como_soube', TRUE);

		$vendedor = $this->input->post('vendedor', TRUE);

		$contrato = $this->input->post('contrato', TRUE);

		$newsletter =  $this->input->post('newsletter', TRUE);

		

		$d = $this->session->userdata('login');

		

		//Validação de Email

		$validacao_email = array(

			'campos' => 'login',

			'tabela' => 'usuarios',

			'where' => array('login' => $email,'id <> '.$d['id']  => NULL)

		);

		

		$this->select->set($validacao_email);

		$total = $this->select->total();

		

		if($total > 0) { //Se existem Emails de usuário iguais

		

			$status = 1;

			$msg = "Email já cadastrado no sistema, use outro email.";

			

		} else {

			$tabela = $this->util->tabelaPerfil($d['tipo']);

			

			$data= array(

			

				'creci' => $creci,

				'nome' => $nome,

				'cpf_cnpj' => $cpf_cnpj,

				'check_cpf_cnpj' => $check_cpf_cnpj,

				'email' => $email,

				'telefone' => $telefone,

				'melhor_horario' => $melhor_horario,

				'como_soube' => $como_soube,

				'vendedor' => $vendedor

			

			);

			

			$id_perfil = $this->master_model->update_form($tabela,$data,$id);

			



			//salvando checkbox contrato e newsletter

			$data= array(

				'contrato' => $contrato,

				'newsletter' => $newsletter,

				'login' => $email

			);

			$id_usuario = $d['id'];

			$ret = $this->master_model->update_form('usuarios',$data,$id_usuario);

			

			

			if(!empty($senha) && $senha == $confirmar_senha) {

			

				$data= array(

			

					'senha' => md5($senha)

			

				);

				

				$id_usuario = $d['id'];

				

				$ret = $this->master_model->update_form('usuarios',$data,$id_usuario);

				

			}

			$status = 0;

			$msg = "Dados atualizados.";

		}

	header('Content-type: text/json');

	header('Content-type: application/json');



	$json = '{"status" : '.$status.', "msg" : "'.trim($msg).'"}';



	echo $json;

	exit;

	}

	

	public function minhalista() {

		

		$this->checaSessao();



		$this->title = "Minha Lista";

		$this->keywords = "Minha Lista";

		$this->description = "Minha Lista";

		

		$sessao = $this->session->userdata('login');

		

		$id_usuario = $sessao['id'];

		

		$config = array(



			'campos' => 'imo.id_usuario, imo.id, imo.slug, imo.titulo, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.valor',



			'tabela' => 'imoveis imo',



			'join' => array(



						array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'),



						array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left'),

						

						array('imoveis_favoritos fa','fa.id_imovel = imo.id','left')



					),



			'where' => array('imo.status' => 3, 'imo.ativo' => 1, 'fa.id_usuario' => $id_usuario)



		);



		$this->select->set($config);



		$num = $this->select->total();

		

		$lista = '';

		

		if($num == 0) {

			

			$lista = '<h2 class="text-center favoritos_vazio">Não existem imóveis na sua lista de Favoritos.</h2>';

			

		} else {

			

			$r = $this->select->resultado();

			

			$lista = '<ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">';

			

			foreach($r as $val) {



				$config = array(



					'campos' => 'arquivo',



					'tabela' => 'imagens',



					'where' => array('id_imovel' => $val->id, 'destaque' => 1)



				);



				$this->select->set($config);

				$num_destaque = $this->select->total();

				$img_dest = $this->select->resultado();



				if($num_destaque > 0) {



					$imagem = base_url().'assets/uploads/imovel/'.$val->id_usuario.'/'.$img_dest[0]->arquivo;



				} else {



					$config = array(



						'campos' => 'arquivo',



						'tabela' => 'imagens',



						'where' => array('id_imovel' => $val->id, 'destaque' => 1),



						'limit' => 1



					);



					$this->select->set($config);

					$num_img = $this->select->total();

					$img = $this->select->resultado();



					if($num_img > 0) {



						$imagem = base_url().'assets/uploads/imovel/'.$id_usuario.'/'.$img[0]->arquivo;



					} else {

						

						$config = array(



							'campos' => 'arquivo',

	

							'tabela' => 'imagens',

	

							'where' => array('id_imovel' => $val->id)

	

						);

	

						$this->select->set($config);

						$num_img = $this->select->total();

						$img = $this->select->resultado();

						

						if($num_img > 0) {

							$imagem = base_url().'assets/uploads/imovel/'.$id_usuario.'/'.$img[0]->arquivo;

						} else {

							$imagem = base_url().'assets/images/sem_imagem.jpg';	

						}



					}



				}



				$valor = $this->util->moeda2br($val->valor);

				

				if($val->tipo == 1) { $tipo = 'status_venda.png'; }

				if($val->tipo == 2) { $tipo = 'status_locacao.png'; }

				if($val->tipo == 3) { $tipo = 'status_temporada.png'; }



				$lista .= '<li>

								<div class="locacao_item">



									<div class="imagem">



										<a href="'.base_url().'imovel/'.$val->slug.'">



											<img class="imovel" src="'.$imagem.'">



											<img class="status" src="'.base_url().'assets/images/'.$tipo.'">



										</a>



									</div>



									<div class="dados">



										<a href="'.base_url().'imovel/'.$val->slug.'">



											<h2>#'.$val->id.' - '.$val->titulo.'</h2>



										</a>



										<p><b>Tipo:</b> '.$val->texto_tipo.'</p>



										<p><b>Local:</b> '.$val->endereco.'</p>



										<p><b>Valor:</b> R$ '.$valor.' </p>



									</div>



								</div>



							</li>';



			}

			

			$lista .= '</ul>';

		

		}

		

		$data = array(

			'lista' => $lista

		);

		

		$this->load( 'painelcorretor/minhalista', $data );

			

	}

	

	/*---- Funções ----*/

	

	public function checaSessao() {

	

		$sessao = $this->session->userdata('login');

		

		if(!isset($sessao) || $sessao['logado'] == 0) {

			redirect(base_url());

		} else {

		

			if($sessao['tipo'] == 1 && $this->uri->segment(1) != 'painel-proprietario') {

				redirect(base_url().'painel-proprietario');

			}

			

			if($sessao['tipo'] == 2 && $this->uri->segment(1) != 'painel-corretor') {

				redirect(base_url().'painel-corretor');

			}

			

			if($sessao['tipo'] == 3 && $this->uri->segment(1) != 'painel-imobiliaria') {

				redirect(base_url().'painel-imobiliaria');

			}

		

		}

	

	}

	

	public function limparFotos() {

	

		$d = $this->session->userdata('login');

		

		$id_usuario = $d['id'];

		

		$this->db->query("DELETE FROM imagens WHERE usuario = ".$id_usuario." AND id_imovel = 0");

	

	}

	

}