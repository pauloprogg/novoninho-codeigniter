<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anuncie_imobiliaria extends Main_Controller {

	public function index() {

		$config = array(
			'campos' => 'texto1',
			'tabela' => 'pagina_imobiliaria'
		);
		
		$this->select->set($config);
		$r = $this->select->resultado();
	
		$this->title = "Anuncie Imobiliária";
		$this->keywords = "Anuncie Imobiliária, Aqui";
		$this->description = "Essa é a Anuncie Imobiliária";
		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller

		$modal_bemvindo = $this->parser->parse( 'templates/modal_bemvindo', array(
							'base_url' => base_url()
						), true );
						
		$query = array(
			'campos' => 'titulo, texto',
			'tabela' => 'termos',
			'where' => array('tipo_usuario' => 3)
		);
		
		$this->select->set($query);
		$t = $this->select->resultado();
						
		$modal_termos = $this->parser->parse( 'templates/modal_termos', array(
			'base_url' => base_url(), 'titulo' => $t[0]->titulo, 'texto' => $t[0]->texto
		), true );
		
		$data = array(
			"texto" => $r[0]->texto1,
			"modal_bemvindo" => $modal_bemvindo,
			"modal_termos" => $modal_termos
		);

		$this->load( 'anuncie_imobiliaria', $data );

	}
	
	public function cadastrar_imobiliaria() {

		$nome = $this->input->post('nome', TRUE);
		$creci = $this->input->post('creci', TRUE);
		$responsavel = $this->input->post('responsavel', TRUE);
		$endereco = $this->input->post('endereco', TRUE);
		$numero = $this->input->post('numero', TRUE);
		$complemento = $this->input->post('complemento', TRUE);
		$como_soube = $this->input->post('como_soube', TRUE);
		$vendedor = $this->input->post('vendedor', TRUE);
		$cidade = $this->input->post('cidade', TRUE);
		$estado = $this->input->post('estado', TRUE);
		$telefone = $this->input->post('telefone', TRUE);
		$celular = $this->input->post('celular', TRUE);
		$email = $this->input->post('email', TRUE);
		$cpf_cnpj = $this->input->post('cpf_cnpj', TRUE);
		$check_cpf_cnpj = $this->input->post('check_cpf_cnpj', TRUE);
		$melhor_horario = $this->input->post('melhor_horario', TRUE);
		$senha = nl2br($this->input->post('senha', TRUE));
		$data_cadastro = time();
		$newsletter = $this->input->post('newsletter', TRUE);
		
		$tipo = 3; //Imobiliária
		
		//Validação de Email
		$validacao_email = array(
			'campos' => 'login',
			'tabela' => 'usuarios',
			'where' => array('login' => $email)
		);
		
		$this->select->set($validacao_email);
		$total = $this->select->total();
		
		if($total > 0) { //Se existem Emails de usuário iguais
		
			$status = 1;
			$msg = "Email ja cadastrado no sistema, use outro email.";
			
		} else {
			//Validação de cpf
			$validacao_cpf = array(
				'campos' => 'cpf_cnpj',
				'tabela' => 'dados_imobiliaria',
				'where' => array('cpf_cnpj' => $cpf_cnpj)
			);
			
			$this->select->set($validacao_cpf);
			$total = $this->select->total();
			
			if($total > 0 && $cpf_cnpj!='') { 
				$status = 1;
					$msg = "CPF/CNPJ já cadastrado no sistema.";
			}else{
			
				$senha = md5($senha);
				
				$ins = array(

					'nome' => $nome,
					'creci' => $creci,
					'responsavel' => $responsavel,
					'endereco' => $endereco,
					'numero' => $numero,
					'complemento' => $complemento,
					'cidade' => $cidade,
					'como_soube' => $como_soube,
					'vendedor' => $vendedor,
					'estado' => $estado,
					'telefone' => $telefone,
					'celular' => $celular,
					'melhor_horario' => $melhor_horario,
					'email'=> $email,
					'cpf_cnpj'=> $cpf_cnpj,
					'check_cpf_cnpj' => $check_cpf_cnpj,
					
				);
				
				$id_perfil = $this->master_model->cadastrar_form('dados_imobiliaria',$ins);
				
				//Inserção de usuario
				
				/* Precos padrão dos anuncios */

				$precos = array(
					'campos' => 'expiracao',
					'tabela' => 'config'
				);
				
				$this->select->set($precos);
				$p = $this->select->resultado();
				
				$expiracao = $p[0]->expiracao;
				
				$ins = array(

					'ativo'=> 0,
					'login'=> $email,
					'senha' => $senha,
					'tipo' => $tipo,
					'perfil' => $id_perfil,
					'data_cadastro' => $data_cadastro,
					'expiracao' => $expiracao,
					'newsletter' => $newsletter

				);
				
				$id_usuario = $this->master_model->cadastrar_form('usuarios',$ins);
				
				$this->Logar($id_usuario);
				
				$status = 0;
				$msg = "Cadastro Realizado com Sucesso";
			
			}
		}
		header('Content-type: text/json');
		header('Content-type: application/json');

		$json = '{"status" : '.$status.', "msg" : "'.trim($msg).'"}';

		echo $json;
		exit;

	}
	
	public function Logar( $id ) {
	
		$query = array(
			'campos' => 'u.*',
			'tabela' => 'usuarios u',
			'where' => array('id' => $id)
		);
		
		$this->select->set($query);
		$dados = $this->select->resultado();
		
		$tabela = $this->util->tabelaPerfil($dados[0]->tipo);
	
		$config = array(
			'campos' => 'nome, email',
			'tabela' => $tabela,
			'where' => array('id' => $dados[0]->perfil)
		);
		
		$this->select->set($config);
		$contato = $this->select->resultado();
		
		$newdata = array(
			'login' => array(
			   'id' => $dados[0]->id, //Id do usuário
			   'nome' => $contato[0]->nome,
			   'email' => $contato[0]->email,
			   'perfil' => $dados[0]->perfil, //Id do perfil vinculado
			   'tipo' => $dados[0]->tipo, //Tipo do usuario (1 - Proprietario, 2 - Corretor, 3 - Imobiliaria)
			   'logado' => 1
			)
		);
		
		$this->session->set_userdata($newdata);
	
	}
	
}