<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Painel extends Main_Controller {

	

	public function cadastrarImovel() {

		

		$titulo = nl2br($this->input->post('titulo', TRUE));

		$descricao1 = nl2br($this->input->post('descricao1', TRUE));

		$tipo_imovel = $this->input->post('tipo_imovel', TRUE);

		$tipo = $this->input->post('tipo', TRUE);

		$finalidade = $this->input->post('finalidade', TRUE);

		$dormitorios = $this->input->post('dormitorios', TRUE);

		$suites = $this->input->post('suites', TRUE);

		$garagem = $this->input->post('garagem', TRUE);

		$banheiros = $this->input->post('banheiros', TRUE);

		$area_m2 = $this->input->post('area_m2', TRUE);

		$mapa = $this->input->post('mapa', TRUE);

		$cep = $this->input->post('cep', TRUE);

		$rua = $this->input->post('id_rua', TRUE);

		$numero = $this->input->post('numero', TRUE);

		$complemento = $this->input->post('complemento', TRUE);

		$bairro = $this->input->post('id_bairro', TRUE);

		$cidade = $this->input->post('id_cidade', TRUE);

		$estado = $this->input->post('id_estado', TRUE);

		$condominio = $this->input->post('condominio', TRUE);

		$valor = $this->input->post('valor', TRUE);

		$usar_contato = $this->input->post('usar_contato', TRUE);

		$telefone = $this->input->post('telefone', TRUE);

		$email = $this->input->post('email', TRUE);

		$descricao2 = $this->input->post('descricao2', TRUE);

		$descricao3 = $this->input->post('descricao3', TRUE);

		

		//Numero de Referencia 

		if(isset($_POST['numero_referencia'])) {

			$num_ref = $this->input->post('numero_referencia', TRUE);;	

		} else {

			$num_ref = NULL;	

		}

		

		$data_cadastro = time();

	

		/* Dados da Sessao */

		$l = $this->session->userdata('login');

		$id_usuario = $l['id'];

		

		//Checagem de imagem de capa

		$config = array(

			'campos' => 'destaque',

			'tabela' => 'imagens',

			'where' => array('usuario' => $id_usuario, 'id_imovel' => 0, 'destaque' => 1)

		);

		$this->select->set($config);

		$imovel_capa = $this->select->total();

		

		if($imovel_capa == 0) { //Não selecionou uma capa

			

			$status_cadastro = 1;

			$msg = 'Selecione uma imagem de capa!';

			

		} else {

			

			$status_cadastro = 0;

			$msg = '';

	

			$tabela = $this->util->tabelaPerfil($l['tipo']);

	

			/* --------------- */

	

			if($usar_contato == 1) { //Usar dados padrões de contato

				$usarDadosContato=1;

				$config = array(

	

					'campos' => '*',

	

					'tabela' => $tabela,

	

					'where' => array('id' => $l['perfil'])

	

				);

	

				$this->select->set($config);

	

				$r = $this->select->resultado();

	

				$telefone = $r[0]->telefone;

	

				$email = $r[0]->email;

	

			}else{

				$usarDadosContato=0;

			}

	

			$valor = $this->util->moeda2us($valor);

	

			$area_m2 = $this->util->moeda2us($area_m2);

	

			/* Dados Padrões */

	

			if($l['tipo'] == 3) { //Imobiliaria

	

				$status = 3;

	

				$aprovado = 1;

	

			} else {

	

				$status = 1;

	

				$aprovado = 0;

	

			}

	

			$destaque = 0;

	

			$slug = $this->util->slug($titulo);

	

			$slug = $this->checaSlug($slug, 'imoveis');

	

			/* -------------- */

	

			$dados = array(

				'ativo' => 1,

	

				'aprovado' => $aprovado,

	

				'id_usuario' => $id_usuario,

	

				'status' => $status,

	

				'destaque' => $destaque,

	

				'slug' => $slug,

	

				'titulo' => $titulo,

	

				'descricao1' => $descricao1,

	

				'tipo' => $tipo_imovel,

	

				'id_tipo_imovel' => $tipo,

	

				'finalidade' => $finalidade,

	

				'dormitorios' => $dormitorios,

	

				'suites' => $suites,

	

				'garagem' => $garagem,

	

				'area_m2' => $area_m2,

	

				'banheiros' => $banheiros,

	

				'mapa' => $mapa,

	

				'cep' => $cep,

	

				'rua' => $rua,

	

				'numero' => $numero,

	

				'complemento' => $complemento,

	

				'bairro' => $bairro,

	

				'cidade' => $cidade,

	

				'estado' => $estado,

	

				'condominio' => $condominio,

	

				'valor' => $valor,

	

				'fone' => $telefone,

	

				'email' => $email,

	

				'descricao2' => $descricao2,

	

				'descricao3' => $descricao3,

	

				'data_cadastro' => time(),

				

				'referencia_xml' => $num_ref,



				'usarDadosContato' => $usarDadosContato

			);

	

			$id_imovel = $this->master_model->cadastrar_form('imoveis',$dados);

	

			/* Vincular as Imagens */

	

			$this->db->query("UPDATE imagens SET id_imovel = $id_imovel WHERE usuario = ".$id_usuario." AND id_imovel = 0");

		

		}

		

		header('Content-type: text/json');

		header('Content-type: application/json');

		$json = '{"status" : '.$status_cadastro.', "msg" : "'.trim($msg).'"}';

		echo $json;

		exit;

	}

	

	public function editImovel() {

	

		$id_imovel = $this->input->post('id_imovel', TRUE);

		

		$titulo = nl2br($this->input->post('titulo', TRUE));

		$descricao1 = nl2br($this->input->post('descricao1', TRUE));

		$tipo_imovel = $this->input->post('tipo_imovel', TRUE);

		$tipo = $this->input->post('tipo', TRUE);

		$finalidade = $this->input->post('finalidade', TRUE);

		$dormitorios = $this->input->post('dormitorios', TRUE);

		$suites = $this->input->post('suites', TRUE);

		$garagem = $this->input->post('garagem', TRUE);

		$banheiros = $this->input->post('banheiros', TRUE);

		$area_m2 = $this->input->post('area_m2', TRUE);

		$mapa = $this->input->post('mapa', TRUE);

		$cep = $this->input->post('cep', TRUE);

		$rua = $this->input->post('id_rua', TRUE);

		$numero = $this->input->post('numero', TRUE);

		$complemento = $this->input->post('complemento', TRUE);

		$bairro = $this->input->post('id_bairro', TRUE);

		$cidade = $this->input->post('id_cidade', TRUE);

		$estado = $this->input->post('id_estado', TRUE);

		$condominio = $this->input->post('condominio', TRUE);

		$valor = $this->input->post('valor', TRUE);

		

		$telefone = $this->input->post('telefone', TRUE);

		$email = $this->input->post('email', TRUE);

		

		$descricao2 = $this->input->post('descricao2', TRUE);

		$descricao3 = $this->input->post('descricao3', TRUE);

		$usar_contato = $this->input->post('usar_contato', TRUE);

		

		$l = $this->session->userdata('login');

		$id_usuario = $l['id'];

	

		$tabela = $this->util->tabelaPerfil($l['tipo']);

		

		if($usar_contato == 1) { //Usar dados padrões de contato

			$usarDadosContato=1;

			$config = array(



				'campos' => '*',



				'tabela' => $tabela,



				'where' => array('id' => $l['perfil'])



			);



			$this->select->set($config);



			$r = $this->select->resultado();



			$telefone = $r[0]->telefone;



			$email = $r[0]->email;



		}else{

			$usarDadosContato=0;

		}

		//Numero de Referencia 

		if(isset($_POST['numero_referencia'])) {

			$num_ref = $this->input->post('numero_referencia', TRUE);;	

		} else {

			$num_ref = NULL;	

		}

		

		$valor = $this->util->moeda2us($valor);

		$area_m2 = $this->util->moeda2us($area_m2);

		

		/* Dados da Sessao */

		

		$l = $this->session->userdata('login');

		

		$id_usuario = $l['id'];

		

		$slug = $this->util->slug($titulo);

		$slug = $this->checaSlug($slug, 'imoveis');

		

		$dados = array(

		

			'slug' => $slug,

			'titulo' => $titulo,

			'descricao1' => $descricao1,

			'tipo' => $tipo_imovel,

			'id_tipo_imovel' => $tipo,

			'finalidade' => $finalidade,

			'dormitorios' => $dormitorios,

			'suites' => $suites,

			'garagem' => $garagem,

			'banheiros' => $banheiros,

			'mapa' => $mapa,

			'area_m2' => $area_m2,

			'cep' => $cep,

			'rua' => $rua,

			'numero' => $numero,

			'complemento' => $complemento,

			'bairro' => $bairro,

			'cidade' => $cidade,

			'estado' => $estado,

			'condominio' => $condominio,

			'valor' => $valor,

			'fone' => $telefone,

			'email' => $email,

			'descricao2' => $descricao2,

			'descricao3' => $descricao3,

			'referencia_xml' => $num_ref,

			'usarDadosContato' => $usarDadosContato

			

		);

	

		$this->master_model->update_form('imoveis', $dados, $id_imovel);

		

		/* Vincular as Imagens */

		

		$this->db->query("UPDATE imagens SET id_imovel = $id_imovel WHERE usuario = ".$id_usuario." AND id_imovel = 0");

		

	}

	

	public function editImovelBloqueado() {

	

		$id_imovel = $this->input->post('id_imovel', TRUE);

		$mapa = $this->input->post('mapa', TRUE);

		$descricao1 = nl2br($this->input->post('descricao1', TRUE));	

		$descricao2 = $this->input->post('descricao2', TRUE);

		$descricao3 = $this->input->post('descricao3', TRUE);

		

		//Numero de Referencia 

		if(isset($_POST['numero_referencia'])) {

			$num_ref = $this->input->post('numero_referencia', TRUE);;	

		} else {

			$num_ref = NULL;	

		}

		

		/* Dados da Sessao */

		

		$l = $this->session->userdata('login');

		

		$id_usuario = $l['id'];

		

		$dados = array(

			'mapa' => $mapa,

			'descricao1' => $descricao1,

			'descricao2' => $descricao2,

			'descricao3' => $descricao3,

			'referencia_xml' => $num_ref

			

		);

	

		$this->master_model->update_form('imoveis', $dados, $id_imovel);

		

		/* Vincular as Imagens */

		

		$this->db->query("UPDATE imagens SET id_imovel = $id_imovel WHERE usuario = ".$id_usuario." AND id_imovel = 0");

	

	}

	

	public function deletar() { //Deletar Imóvel

	

		$id = $this->input->post('id', TRUE);

		$caminho = $this->input->post('caminho', TRUE);

		

		$l = $this->session->userdata('login');

		

		$id_usuario = $l['id'];

		

		$config = array(

			'campos' => 'id',

			'tabela' => 'imoveis',

			'where' => array('id' => $id, 'id_usuario' => $id_usuario)

		);

	

		$this->select->set($config);

		$total = $this->select->total();

		

		if($total == 0) {

			redirect(base_url().$caminho);

		}

	

		$this->master_model->delete_form('imoveis', $id, 'id');

		$this->master_model->delete_form('imagens', $id, 'id_imovel');

	

	}

	

	public function cep() {

		

		$result_j = false;

		$ceps = NULL;

		$resultado = NULL;

		$url = NULL;

		$json = '';

		

		if(isset($_POST['cep'])){

			

			$cep = str_replace('-', '', $_POST['cep']);

			$cep = (int)$cep;

			

			$query = $this->db->query('SELECT e.endereco_logradouro, e.endereco_codigo as endereco_id, 

			b.bairro_descricao, b.bairro_codigo as bairro_id,

			c.cidade_descricao,  c.cidade_codigo as cidade_id,

			u.uf_sigla, u.uf_codigo as uf_id

			FROM cep_endereco e 

			LEFT JOIN cep_bairro b 	ON e.bairro_codigo = b.bairro_codigo

			LEFT JOIN cep_cidade c 	ON b.cidade_codigo = c.cidade_codigo

			LEFT JOIN cep_uf u 		ON u.uf_codigo = c.uf_codigo

			WHERE e.endereco_cep = "'.$this->util->escape($cep).'";');

			

			if($query->num_rows() == 0){ //Puxar o cep geral da cidade

			

				$query_s = $this->db->query('

				SELECT e.endereco_logradouro, e.endereco_codigo as endereco_id, 

				b.bairro_descricao, b.bairro_codigo as bairro_id,

				u.uf_sigla, u.uf_codigo as uf_id, 

				c.cidade_descricao, c.cidade_codigo as cidade_id	

				

				FROM cep_cidade c

				LEFT JOIN cep_uf u 		 ON u.uf_codigo = c.uf_codigo

				LEFT JOIN cep_bairro b 	 ON c.cidade_codigo = b.cidade_codigo

				LEFT JOIN cep_endereco e ON b.bairro_codigo = e.bairro_codigo

				WHERE c.cidade_cep = "'.$this->util->escape($_POST['cep']).'";');

				

				$d = $query_s->result();

				

				if($query_s->num_rows() > 0){

					$resultado = 2;

					$resultado_txt = 'Sucesso - cep único';	

					$dados = $d[0];

				} else {

					$resultado = 0;

					$resultado_txt = 'Serviço indisponível/cep inválido';

				}

				

			} else {

				

				$resultado = 1;

				$resultado_txt = 'Sucesso - cep completo';	

				$d = $query->result();

				$dados = $d[0];

				

			}

			

			$result_j = true;

			

			if($resultado != 0){

				

				$uf			= $dados->uf_sigla;

				$cidade 	= $dados->cidade_descricao;

				$cidade		= ucwords(mb_strtolower($cidade,'UTF-8'));

				

				$bairro 	= $dados->bairro_descricao;

				$bairro		= ucwords(mb_strtolower($bairro,'UTF-8'));

					

				$logradouro = $dados->endereco_logradouro;

				$logradouro = ucwords(mb_strtolower($logradouro,'UTF-8'));

				

				$logradouro_id	= $dados->endereco_id;

				$bairro_id	= $dados->bairro_id;

				$cidade_id	= $dados->cidade_id;

				$uf_id		= $dados->uf_id;

					

				$json = '{"result" : '.$result_j.', "resultado" : "'.$resultado.'", "cep" : "'.$ceps.'", "resultado_mensagem" : "'.$resultado_txt.'", "rua" : "'.$logradouro.'", "bairro" : "'.$bairro.'", "cidade" : "'.$cidade.'", "estado" : "'.$uf.'", "rua_id" : "'.$logradouro_id.'", "bairro_id" : "'.$bairro_id.'", "cidade_id" : "'.$cidade_id.'", "estado_id" : "'.$uf_id.'"}';

				

			}  else {

		

				$json = '{"result" : '.$result_j.', "resultado" : "'.$resultado.'"}';

				

			}

			

		}

		

		header('Content-type: text/json');

		header('Content-type: application/json');

		echo $json;

		exit;

	}

	

	public function imagem() {

	

		$tabela = "imagens";

		switch ( $this->input->post('method', true) ) {

			

			case '1': //insere a imagem

			

				unset( $_POST['method'] );

				

				$d = $this->session->userdata('login');

				

				$data = array(

					'id_imovel' => 0,

					'arquivo' => $_POST['arquivo'],

					'data_cadastro' => time(),

					'ativo' => 1,

					'usuario' => $d['id']

				);

				

				$id = $this->master_model->cadastrar_form($tabela,$data);

				echo $id;

			break;

			case '3': //deleta a imagem	

				$this->master_model->delete_form($tabela, $_POST["id"], 'id');

			break;

			default:

				# code...

				break;

		}

	

	}

	

	public function imagemDestaque() {

	

		$id_foto = $this->input->post('id_foto', TRUE);

		$id_imovel = $this->input->post('id_imovel', TRUE);

		

		$us = $this->session->userdata('login');

		

		$id_usuario = $us['id'];

		

		/* Retirar Destaques Vinculados ao Imovel */

		

		$this->db->query("UPDATE imagens SET destaque = 0 WHERE usuario = ".$id_usuario." AND id_imovel = ".(!!$id_imovel ? $id_imovel : 0)."");

		

		/* Imagem Destaque */

		

		$this->db->query("UPDATE imagens SET destaque = 1 WHERE usuario = ".$id_usuario." AND id = ".$id_foto."");

	

	}

	

	public function salvarDadosPublicar() {

		$dados = $this->input->post('itens', TRUE);

		

		$us = $this->session->userdata('login');

		

		$precos = array(

			'campos' => 'preco_normal, preco_destaque, preco_super_destaque, preco_feirao, preco_destaque_feirao, preco_super_destaque_feirao',

			'tabela' => 'usuarios',

			'where' => array('id' => $us['id'])

		);

		

		$this->select->set($precos);

		$preco = $this->select->resultado();

		

		$preco_normal = $preco[0]->preco_normal;

		$preco_destaque = $preco[0]->preco_destaque;

		$preco_super_destaque = $preco[0]->preco_super_destaque;

		$preco_feirao = $preco[0]->preco_feirao;
		
		$preco_destaque_feirao = $preco[0]->preco_destaque_feirao;

		$preco_super_destaque_feirao = $preco[0]->preco_super_destaque_feirao;


		foreach($dados as $x => $val) {

			

			if($dados[$x]['destaque'] == 0) {

				$dados[$x]['preco'] = $preco_normal;

			} else {

				$dados[$x]['preco'] = $preco_destaque;

			}	

			

			$precos = array(

				'campos' => 'titulo',

				'tabela' => 'imoveis',

				'where' => array('id' => $val['id'])

			);

			

			$this->select->set($precos);

			$titulo = $this->select->resultado();

			

			$dados[$x]['titulo'] = $titulo[0]->titulo;

				

		}

		

		$newdata = array(

			'publicar' => $dados

		);

		$this->session->set_userdata($newdata);

		

		$d = $this->session->userdata('publicar');

		

	}

	

	public function visualizarImovel() {

		

		extract($_POST);

		if(!isset($id_imovel)) {

			$id_imovel = 0;

		}

		

		$config = array(

			'campos' => 'MAX(id) as "num"',

			'tabela' => 'imoveis'

		);

		

		$this->select->set($config);

		

		$num_imoveis = $this->select->resultado();

		

		$usuario = $this->session->userdata('login');

		

		$id_usuario = $usuario['id'];

		

		if($condominio == 0) { $condo = 'Sim'; } 

		if($condominio == 1) { $condo = 'Não'; } 

		if($tipo_imovel == 1) { $t = 'Venda'; } 

		if($tipo_imovel == 2) { $t = 'Locação'; } 

		if($tipo_imovel == 3) { $t = 'Temporada'; } 

		

		$config = array(

			'campos' => 'tipo',

			'tabela' => 'tipo_imovel',

			'where' => array('id' => $tipo)

		);

		

		$this->select->set($config);

		$tipo_texto = $this->select->resultado();

		

		$titulo_pagina = $tipo_texto[0]->tipo.' // '.$t;

		

		if( $area_m2 > 0 ) {

			$valor_temp = $this->util->moeda2us($valor);

			$area_m2 = $this->util->moeda2us($area_m2);

			$valor_m2 = $valor_temp / $area_m2;

		} else {

		

			$valor_m2 = 0;

		

		}

		/* Galeria */

		$config = array(

			'campos' => 'arquivo',

			'tabela' => 'imagens',

			'where' => array('id_imovel' => $id_imovel, 'usuario' => $id_usuario),

			'orderBy' => array( 'destaque' => 'DESC', 'id' => 'DESC')

		);

		$this->select->set($config);

		$num_img = $this->select->total();

		$img = $this->select->resultado();



		/* Verifica se a imagem existe */

		foreach($img as $x => $val) {



			$arquivo = trim($val->arquivo);

			$path = '/assets/uploads/imovel/'.$id_usuario.'/';



			if(file_exists(getcwd().$path.$arquivo)) {

				$img[$x]->arquivo = base_url().$path.$arquivo;

			} else {

				$img[$x]->arquivo = $arquivo;

			}



		}

		

		$config = array(

			'campos' => 'arquivo',

			'tabela' => 'imagens',

			'where' => array('id_imovel' => $id_imovel, 'usuario' => $id_usuario, 'destaque' => 1),

			'limit' => 1

		);

		$this->select->set($config);

		$img_dest = $this->select->resultado();

		if( !empty($img_dest) ) {

			$imagem_principal = base_url().'assets/uploads/imovel/'.$id_usuario.'/'.$img_dest[0]->arquivo;

			$arq = $img_dest[0]->arquivo;

		} else

		if($num_img > 0) {

			$imagem_principal = base_url().'assets/uploads/imovel/'.$id_usuario.'/'.$img[0]->arquivo;

			$arq = $img[0]->arquivo;

		} else {

			$imagem_principal = base_url().'assets/images/sem_imagem.jpg';

			$arq = 'assets/images/sem_imagem.jpg';

		}



		if(file_exists(getcwd().$imagem_principal)) {

			$imagem_principal = base_url().$imagem_principal;

		} else {

			$imagem_principal = $arq;

		}



		/* Dados de Contato */

		

		$config = array(

			'campos' => 'tipo, perfil',

			'tabela' => 'usuarios',

			'where' => array('id' => $id_usuario)

		);

		

		$this->select->set($config);

		$user = $this->select->resultado();

		

		$tabela = $this->util->tabelaPerfil($user[0]->tipo);

	

		$config = array(

			'campos' => 'nome, email, telefone, melhor_horario',

			'tabela' => $tabela,

			'where' => array('id' => $user[0]->perfil)

		);

		

		$this->select->set($config);

		$contato = $this->select->resultado();

		

		if( $valor == '' ) {

			$valor = '0,00';	

		}

		

		//Mapa

		

		$mapa_google = '';

		

		/*

		if($imovel->mapa == 1) {

			

			$config = array(



				'campos' => 'end.endereco_logradouro as "rua", ba.bairro_descricao as "bairro", cid.cidade_descricao as "cidade", est.uf_sigla as "estado", imo.complemento',

	

				'tabela' => 'imoveis imo',

	

				'join' => array(

	

							array('cep_endereco end','end.endereco_codigo = imo.rua','left'),

	

							array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left'),

							

							array('cep_cidade cid','cid.cidade_codigo = imo.cidade','left'),

							

							array('cep_uf est','est.uf_codigo = imo.estado','left')

	

						),

	

				'where' => array('imo.id' => $imovel->id)

	

			);



			$this->select->set($config);



			$m = $this->select->resultado();

			$mapa_codigo = $imovel->cep.'+'.$m[0]->rua.'+'.$imovel->numero.'+'.$m[0]->cidade.'+'.$m[0]->estado;

			$texto_end = $m[0]->rua.' '.$imovel->numero.', '.$m[0]->bairro.', '.$m[0]->cidade.' '.$m[0]->estado.' - Complemento: '.$m[0]->complemento;

			$mapa_google = '

			

			<div class="row">

			

				<h2 class="preto2" style="padding-left: 20px;"> LOCALIZAÇÃO - <a class="ativar_mapa"><span class="mapa_texto">Visualizar</span> Mapa do Endereço</a> </h2>

				

				<h2 class="preto2" style="padding-left: 20px;">'.$texto_end.'</h2>

				<div class="columns small-12 medium-12 large-12">

					

					<div class="mapa box_mapa box_mapa_hidden">

					

						<iframe width="970" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q='.$mapa_codigo.'&key=AIzaSyBEKFcYT22HGZOWyb2eNpykzJVCIgkf2uE"></iframe>

					

					</div>

					

				</div>

				

			</div>';

		

		}

		*/



		$Formcontato['email'] = $contato[0]->email;

		$Formcontato['telefone'] = $contato[0]->telefone;

		$mostraNome = '<p><b>Nome: </b> '.$contato[0]->nome.' </p>';

		if ($usar_contato == 2) {

			$mostraNome = '';

			$Formcontato['email'] = $email;

			$Formcontato['telefone'] = $telefone;

		}

		

		$data = array(

			"id_next_imovel" => $num_imoveis[0]->num+1,

			"condominio" => $condo,

			"id_imovel" => $id_imovel,

			"dados" => array($_POST),

			"titulo_pagina" => $titulo_pagina,

			"valor_br" => $valor,

			"valor_m2" => $this->util->moeda2br($valor_m2),

			"area" => $this->util->moeda2br($area_m2),

			"imagem_principal" => $imagem_principal,

			"galeria" => $img,

			"telefone_user" => $Formcontato['telefone'],

			"email_user" => $Formcontato['email'],

			"nome_user" => $mostraNome,

			"horario_user" => $contato[0]->melhor_horario,

			"mapa_google" => $mapa_google,

			"base_url" => base_url(),

			"anuncios" => $this->Anuncios(),

			"id_usuario" => $id_usuario,

			//"NusaContato" => $Formcontato['NusaContato']

		);

		print_r($dados['imagem_principal']);
		exit();

		

		$html = $this->parser->parse( 'templates/visualizar_imovel', $data, true );

		

		echo $html;

		

		/*$json = '';

		

		header('Content-type: text/json');

		header('Content-type: application/json');

		echo $json;

		exit;*/

			

	}

	

	public function AnunciosVisualizar() {

		$sessionCidade = $this->session->userdata('cidade');

		

		if(empty($sessionCidade)) {

			$where = 'ativo = 1';

		} else {

			$where = 'ativo = 1 AND cidade = '.$sessionCidade.' OR todos = 1';

		}

		$config = array(

			'campos' => 'link, imagem',

			'tabela' => 'anuncios',

			'orderBy' => array('RAND()' => '' ),

			'limit' => 6,

			'where' => $where

		);

		$this->select->set($config);

		$r = $this->select->resultado();

		return $this->parser->parse( 'templates/anuncios', array('base_url' => base_url(), 'dados' => $r), true );

	

	}

	

	/*-----------------*/

	

	public function checaSlug($slug, $tabela) {

		

		$query = $this->db->query("SELECT id FROM ".$tabela." WHERE slug LIKE '".$slug."%'");

	

		if ($query->num_rows() > 0) {



			$slug = $slug.'-'.$query->num_rows();

			

			return $this->checaSlug($slug, $tabela);



		} else {

			

			return $slug;

			

		}



	}

	

}