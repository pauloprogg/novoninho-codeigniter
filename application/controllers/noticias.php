<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends Main_Controller {

	public function index() {
		
		$sessionCidade = $this->session->userdata('cidade');
		
		if(empty($sessionCidade)) {
			$where = 'ativo = 1';
		} else {
			$where = 'ativo = 1 AND cidade = '.$sessionCidade.' OR todos = 1';
		}
	
		$config = array(
			'campos' => 'titulo, imagem, resumo, slug',
			'tabela' => 'noticias',
			'where' => $where,
			'orderBy' => array('id' => 'ASC')
		);

		$this->select->set($config);
		/*$total = $this->select->total();
		
		if($total == 0) {
			
			$config = array(
				'campos' => 'titulo, imagem, resumo, slug',
				'tabela' => 'noticias',
				'orderBy' => array('id' => 'ASC')
			);
	
			$this->select->set($config);
			
		}
		*/
		$paginacao = $this->select->paginacao( "noticias", 3);
		$resultado = $this->select->resultado();
		// echo $this->db->last_query();die;
		$this->title = "Noticias";
		$this->keywords = "";
		$this->description = "";
		$this->image = ""; // O default é setado no controller Main_Controller

		$data = array(
			"dados" => $resultado,
			"paginacao" => $paginacao
		);

		$this->load( 'noticias/index', $data );

	}
	
	public function show( $slug ) {
	
		$config = array(
			'campos' => 'titulo, imagem, texto',
			'tabela' => 'noticias',
			'where' => array('slug' => $slug)
		);
		
		$this->select->set($config);
		
		$val = $r = $this->select->total();
		
		if(empty($slug) || $val == 0) {
			redirect( base_url().'noticias' );	
		}
		
		$r = $this->select->resultado();
		
		$noticia = $r[0];

		$this->title = $noticia->titulo;
		$this->keywords = "";
		$this->description = "";
		$this->image = ""; // O default é setado no controller Main_Controller

		$data = array(
			'titulo' => $noticia->titulo,
			'texto' => $noticia->texto,
			'imagem_noticia' => $noticia->imagem
		);

		$this->load( 'noticias/show', $data );

	}
	
}