<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function validaCPF($cpf = null) {

    // Verifica se um número foi informado
    if(empty($cpf)) {
        return false;
    }

    // Elimina possivel mascara
    $cpf = ereg_replace('[^0-9]', '', $cpf);
    $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

    // Verifica se o numero de digitos informados é igual a 11
    if (strlen($cpf) != 11) {
        return false;
    }
    // Verifica se nenhuma das sequências invalidas abaixo
    // foi digitada. Caso afirmativo, retorna falso
    else if ($cpf == '00000000000' ||
        $cpf == '11111111111' ||
        $cpf == '22222222222' ||
        $cpf == '33333333333' ||
        $cpf == '44444444444' ||
        $cpf == '55555555555' ||
        $cpf == '66666666666' ||
        $cpf == '77777777777' ||
        $cpf == '88888888888' ||
        $cpf == '99999999999') {
        return false;
     // Calcula os digitos verificadores para verificar se o
     // CPF é válido
     } else {

        for ($t = 9; $t < 11; $t++) {

            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }

        return true;
    }
}

class Cadastrar extends Main_Controller {

	public function proprietario() {

		$this->title = "Cadastrar Proprietário";
		$this->keywords = "Cadastrar Proprietário, Aqui";
		$this->description = "Essa é a Cadastrar Proprietário";
		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller

		$modal_bemvindo = $this->parser->parse( 'templates/modal_bemvindo', array(
			'base_url' => base_url()
		), true );

		$query = array(
			'campos' => 'titulo, texto',
			'tabela' => 'termos',
			'where' => array('tipo_usuario' => 1)
		);

		$this->select->set($query);
		$t = $this->select->resultado();

		if(!isset($t[0])){
			$t[0] = (object)array(
				'titulo' => '',
				'texto' => ''
			);
		}

		$modal_termos = $this->parser->parse( 'templates/modal_termos', array(
			'base_url' => base_url(),
			'titulo' => $t[0]->titulo,
			'texto' => $t[0]->texto
		), true );

		$data = array(
			"modal_bemvindo" => $modal_bemvindo,
			"modal_termos" => $modal_termos
		);

		$this->load( 'cadastrar_proprietario', $data );

	}

	public function corretor() {

		$this->title = "Cadastrar Proprietário";
		$this->keywords = "Cadastrar Proprietário, Aqui";
		$this->description = "Essa é a Cadastrar Proprietário";
		$this->image = "assets/images/apple-touch-icon-114x114.png"; // O default é setado no controller Main_Controller

		$modal_bemvindo = $this->parser->parse( 'templates/modal_bemvindo', array(
							'base_url' => base_url()
						), true );

		$query = array(
			'campos' => 'titulo, texto',
			'tabela' => 'termos',
			'where' => array('tipo_usuario' => 2)
		);

		$this->select->set($query);
		$t = $this->select->resultado();

		// $modal_termos = $this->parser->parse( 'templates/modal_termos', array(
		// 	'base_url' => base_url(), 'titulo' => $t[0]->titulo, 'texto' => $t[0]->texto
		// ), true );

		$data = array(
			"modal_bemvindo" => $modal_bemvindo,
			//"modal_termos" => $modal_termos
		);

		$this->load( 'cadastrar_corretor', $data );

	}

	public function cadastrar_proprietario() {

		$nome = $this->input->post('nome', TRUE);
		$email = $this->input->post('email', TRUE);
		$cpf_cnpj = $this->input->post('cpf_cnpj', TRUE);
		$check_cpf_cnpj = $this->input->post('check_cpf_cnpj', TRUE);
		$melhor_horario = $this->input->post('melhor_horario', TRUE);
		$como_soube = $this->input->post('como_soube', TRUE);
		$vendedor = $this->input->post('vendedor', TRUE);
		$telefone = $this->input->post('telefone', TRUE);
		$senha = nl2br($this->input->post('senha', TRUE));
		$anunciante = nl2br($this->input->post('anunciante', TRUE));
		$data_cadastro = time();
		$newsletter = $this->input->post('newsletter', TRUE);

		$tipo = 1; //Proprietário
		//Validação de cpf
		$validacao_cpf = array(
			'campos' => 'cpf_cnpj',
			'tabela' => 'dados_proprietario',
			'where' => array('cpf_cnpj' => $cpf_cnpj)
		);

		$this->select->set($validacao_cpf);
		$total = $this->select->total();

		if($total > 0 && $cpf_cnpj!='') {
			$status = 1;
				$msg = "CPF/CNPJ já cadastrado no sistema.";
		}else{
			//Validação de Email
			$validacao_email = array(
				'campos' => 'login',
				'tabela' => 'usuarios',
				'where' => array('login' => $email)
			);

			$this->select->set($validacao_email);
			$total = $this->select->total();

			if($total > 0) { //Se existem Emails de usuário iguais

				$status = 1;
				$msg = "Email ja cadastrado no sistema, use outro email.";

			} else {

				//Inserção no perfil de proprietario

				$senha = md5($senha);

				$ins = array(

					'nome'=> $nome,
					'email'=> $email,
					'melhor_horario' => $melhor_horario,
					'como_soube' => $como_soube,
					'telefone' => $telefone,
					'cpf_cnpj' => $cpf_cnpj,
					'check_cpf_cnpj' => $check_cpf_cnpj,
					'anunciante' => $anunciante,
					'vendedor' => $vendedor

				);

				$id_perfil = $this->master_model->cadastrar_form('dados_proprietario',$ins);

				/* Precos padrão dos anuncios */

				$precos = array(
					'campos' => 'preco_normal, preco_destaque, preco_super_destaque, expiracao, preco_web, preco_feirao,preco_destaque_feirao, preco_super_destaque_feirao',
					'tabela' => 'config'
				);

				$this->select->set($precos);
				$p = $this->select->resultado();

				$preco_normal = $p[0]->preco_normal;
				$preco_destaque = $p[0]->preco_destaque;
				$preco_super_destaque = $p[0]->preco_super_destaque;
				$preco_feirao = $p[0]->preco_feirao;
				$preco_destaque_feirao = $p[0]->preco_destaque_feirao;
				$preco_super_destaque_feirao = $p[0]->preco_super_destaque_feirao;
				$expiracao = $p[0]->expiracao;
				$preco_web = $p[0]->preco_web;

				//Inserção de usuario

				$ins = array(

					'ativo'=> 1,
					'login'=> $email,
					'senha' => $senha,
					'tipo' => $tipo,
					'perfil' => $id_perfil,
					'preco_normal' => $preco_normal,
					'preco_web' => $preco_web,
					'preco_destaque' => $preco_destaque,
					'preco_super_destaque' => $preco_super_destaque,
					'preco_feirao' => $preco_feirao,
					'preco_destaque_feirao' => $preco_destaque_feirao,
					'preco_super_destaque_feirao' => $preco_super_destaque_feirao,
					'expiracao' => $expiracao,
					'data_cadastro' => $data_cadastro,
					'newsletter' => $newsletter

				);

				$id_usuario = $this->master_model->cadastrar_form('usuarios',$ins);

				$this->Logar($id_usuario);

				$status = 0;
				$msg = "Cadastro Realizado com Sucesso";

			}
		}


		header('Content-type: text/json');
		header('Content-type: application/json');

		$json = '{"status" : '.$status.', "msg" : "'.trim($msg).'"}';

		echo $json;
		exit;

	}

	public function cadastrar_corretor() {

		$creci = $this->input->post('creci', TRUE);
		$nome = $this->input->post('nome', TRUE);
		$email = $this->input->post('email', TRUE);
		$cpf_cnpj = $this->input->post('cpf_cnpj', TRUE);
		$como_soube = $this->input->post('como_soube', TRUE);
		$vendedor = $this->input->post('vendedor', TRUE);
		$check_cpf_cnpj = $this->input->post('check_cpf_cnpj', TRUE);
		$melhor_horario = $this->input->post('melhor_horario', TRUE);
		$telefone = $this->input->post('telefone', TRUE);
		$senha = nl2br($this->input->post('senha', TRUE));
		$data_cadastro = time();
		$newsletter = $this->input->post('newsletter', TRUE);

		$tipo = 2; //Corretor

		//Validação de cpf
		$validacao_cpf = array(
			'campos' => 'cpf_cnpj',
			'tabela' => 'dados_corretor',
			'where' => array('cpf_cnpj' => $cpf_cnpj)
		);

		$this->select->set($validacao_cpf);
		$total = $this->select->total();

		if($total > 0 && $cpf_cnpj!='') {
			$status = 1;
				$msg = "CPF/CNPJ já cadastrado no sistema.";
		}else{
			//Validação de Email
			$validacao_email = array(
				'campos' => 'login',
				'tabela' => 'usuarios',
				'where' => array('login' => $email)
			);

			$this->select->set($validacao_email);
			$total = $this->select->total();

			if($total > 0) { //Se existem Emails de usuário iguais

				$status = 1;
				$msg = "Email ja cadastrado no sistema, use outro email.";

			} else {

				$senha = md5($senha);

				//Inserção no perfil de corretor

				$ins = array(

					'nome'=> $nome,
					'email'=> $email,
					'cpf_cnpj'=> $cpf_cnpj,
					'como_soube' => $como_soube,
					'vendedor' => $vendedor,
					'check_cpf_cnpj' => $check_cpf_cnpj,
					'melhor_horario' => $melhor_horario,
					'telefone' => $telefone,
					'creci' => $creci

				);

				$id_perfil = $this->master_model->cadastrar_form('dados_corretor',$ins);

				/* Precos padrão dos anuncios */

				$precos = array(
					'campos' => 'preco_normal, preco_destaque, preco_super_destaque, expiracao, preco_web, preco_feirao, preco_destaque_feirao, preco_super_destaque_feirao',
					'tabela' => 'config'
				);

				$this->select->set($precos);
				$p = $this->select->resultado();

				$preco_normal = $p[0]->preco_normal;
				$preco_destaque = $p[0]->preco_destaque;
				$preco_super_destaque = $p[0]->preco_super_destaque;

				$preco_feirao = $p[0]->preco_feirao;

				$preco_destaque_feirao = $p[0]->preco_destaque_feirao;
				$preco_super_destaque_feirao = $p[0]->preco_super_destaque_feirao;
				$preco_web = $p[0]->preco_web;
				$expiracao = $p[0]->expiracao;

				//Inserção de usuario

				$ins = array(

					'ativo'=> 1,
					'login'=> $email,
					'senha' => $senha,
					'tipo' => $tipo,
					'perfil' => $id_perfil,
					'preco_normal' => $preco_normal,
					'preco_destaque' => $preco_destaque,
					'preco_super_destaque' => $preco_super_destaque,
					'preco_feirao' => $preco_feirao,
					'preco_destaque_feirao' => $preco_destaque_feirao,
					'preco_super_destaque_feirao' => $preco_super_destaque_feirao,
					'preco_web' => $preco_web,
					'expiracao' => $expiracao,
					'data_cadastro' => $data_cadastro,
					'newsletter' => $newsletter

				);

				$id_usuario = $this->master_model->cadastrar_form('usuarios',$ins);

				$this->Logar($id_usuario);

				$status = 0;
				$msg = "Cadastro Realizado com Sucesso";

			}
		}

		header('Content-type: text/json');
		header('Content-type: application/json');

		$json = '{"status" : '.$status.', "msg" : "'.trim($msg).'"}';

		echo $json;
		exit;

	}

	public function Logar( $id ) {

		$query = array(
			'campos' => 'u.*',
			'tabela' => 'usuarios u',
			'where' => array('id' => $id)
		);

		$this->select->set($query);
		$dados = $this->select->resultado();

		$tabela = $this->util->tabelaPerfil($dados[0]->tipo);

		$config = array(
			'campos' => 'nome, email',
			'tabela' => $tabela,
			'where' => array('id' => $dados[0]->perfil)
		);

		$this->select->set($config);
		$contato = $this->select->resultado();

		$newdata = array(
			'login' => array(
			   'id' => $dados[0]->id, //Id do usuário
			   'nome' => $contato[0]->nome,
			   'email' => $contato[0]->email,
			   'perfil' => $dados[0]->perfil, //Id do perfil vinculado
			   'tipo' => $dados[0]->tipo, //Tipo do usuario (1 - Proprietario, 2 - Corretor, 3 - Imobiliaria)
			   'logado' => 1
			)
		);

		$this->session->set_userdata($newdata);

	}

}