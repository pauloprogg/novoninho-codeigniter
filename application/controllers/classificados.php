<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Classificados extends Main_Controller {



	public function __construct() {

		parent::__construct();

		ini_set('memory_limit','128M');

	}



	public function index() {

		if($this->input->get('feirao', TRUE) != '') {
			$this->session->unset_userdata('feirao');
			$this->session->set_userdata( 'feirao', $this->input->get('feirao', TRUE));
		}


		$this->title = "Classificados";

		$this->keywords = "";

		$this->description = "";



		$get = $this->input->get();

		

		$dados = $this->DadosPesquisa($get);

		

		if($get) { //Pesquisa Realizada



			$resultado = $this->ResultadoDados($dados);

			

		} else { //Sem Pesquisa

		

			$resultado = $this->ResultadoLimpo();

			

		}

			

		$formulario = $this->Formulario($dados);

		

		$data = array(

			"formulario" => $formulario,

			"resultado" => $resultado

		);



		$data[ "destaques" ] = $this->Destaques();



		$this->load( 'classificados', $data );



	}

	

	public function DadosPesquisa( $g ) {

		

		$dados = array();

		

		// Tipo

		if(isset($g['tipo'])) {

			$dados['tipo'] = $g['tipo'];

		} else {

			$dados['tipo'] = 1;

		}



		// Tipo de Imovel

		if(!empty($g['tipo_imovel'])) {

			$dados['id_tipo_imovel'] = $g['tipo_imovel'];

		}



		// Finalidade

		if(!empty($g['finalidade'])) {

			$dados['finalidade'] = $g['finalidade'];

		}


		//Feirao

		if(!empty($g['feirao'])) {

			$dados['feirao'] = $g['feirao'];

			$this->session->unset_userdata('feirao');
			$this->session->set_userdata( 'feirao', $dados['feirao']);
		}

		

		// Dormitorios

		

		//---- Minimo

		if(isset($g['dorm_min'])) {

			$dados['dorm_min'] = $g['dorm_min'];

		} else {

			$dados['dorm_min'] = '';

		}

		

		//---- Maximo

		if(isset($g['dorm_max'])) {

			$dados['dorm_max'] = $g['dorm_max'];

		} else {

			$dados['dorm_max'] = '';

		}

		

		// Suites

		

		//---- Minimo

		if(isset($g['suite_min'])) {

			$dados['suite_min'] = $g['suite_min'];

		} else {

			$dados['suite_min'] = '';

		}

		

		//---- Maximo

		if(isset($g['suite_max'])) {

			$dados['suite_max'] = $g['suite_max'];

		} else {

			$dados['suite_max'] = '';

		}

		

		// Garagem

		

		//---- Minimo

		if(isset($g['gar_min'])) {

			$dados['gar_min'] = $g['gar_min'];

		} else {

			$dados['gar_min'] = '';

		}

		

		//---- Maximo

		if(isset($g['gar_max'])) {

			$dados['gar_max'] = $g['gar_max'];

		} else {

			$dados['gar_max'] = '';

		}

		

		// Valor

		

		//---- Minimo

		if(isset($g['val_min'])) {

			$dados['val_min'] = $g['val_min'];

		} else {

			$dados['val_min'] = '';

		}

		

		//---- Maximo

		if(isset($g['val_max'])) {

			$dados['val_max'] = $g['val_max'];

		} else {

			$dados['val_max'] = '';

		}

		

		// Proprietario

		if(isset($g['proprietario'])) {

			$dados['proprietario'] = $g['proprietario'];

		} else {

			$dados['proprietario'] = '';

		}

		

		// Imobiliaria

		if(isset($g['imobiliaria'])) {

			$dados['imobiliaria'] = $g['imobiliaria'];

		} else {

			$dados['imobiliaria'] = '';

		}

		

		// id_imobiliaria

		if(isset($g['id_imobiliaria'])) {

			$dados['id_imobiliaria'] = $g['id_imobiliaria'];

		} else {

			$dados['id_imobiliaria'] = '';

		}

		

		// Condominio

		if(isset($g['condominio'])) {

			$dados['condominio'] = $g['condominio'];

		} else {

			$dados['condominio'] = 2;

		}




		

		// Cidade

		if(isset($g['cidade'])) {

			$dados['cidade'] = $g['cidade'];

		} else {

			$dados['cidade'] = '';

		}

		

		// Estado

		if(isset($g['estado'])) {

			$dados['estado'] = $g['estado'];

		} else {

			$dados['estado'] = 1;

		}

		

		//Get dos campos de cep

		//Cidade

			if(isset($g['cep_cidade'])) {

				$dados['cep_cidade'] = $g['cep_cidade'];

			} else {

				$dados['cep_cidade'] = '';

			}

			

			if(isset($g['id_cidade'])) {

				$dados['id_cidade'] = $g['id_cidade'];

			} else {

				$dados['id_cidade'] = '';

			}

		

		//Estado

			if(isset($g['cep_estado'])) {

				$dados['cep_estado'] = $g['cep_estado'];

			} else {

				$dados['cep_estado'] = '';

			}

			

			if(isset($g['id_estado'])) {

				$dados['id_estado'] = $g['id_estado'];

			} else {

				$dados['id_estado'] = '';

			}

		

		//Rua

			if(isset($g['cep_rua'])) {

				$dados['cep_rua'] = $g['cep_rua'];

			} else {

				$dados['cep_rua'] = '';

			}

			

			if(isset($g['id_rua'])) {

				$dados['id_rua'] = $g['id_rua'];

			} else {

				$dados['id_rua'] = '';

			}

			

		

		//Bairro

			if(isset($g['cep_bairro'])) {

				$dados['cep_bairro'] = $g['cep_bairro'];

			} else {

				$dados['cep_bairro'] = '';

			}

			

			if(isset($g['id_bairro'])) {

				$dados['id_bairro'] = $g['id_bairro'];

			} else {

				$dados['id_bairro'] = '';

			}

		

		//Cep

			if(isset($g['cep'])) {

				$dados['cep'] = $g['cep'];

			} else {

				$dados['cep'] = '';

			}

			

		//Usar Cep

		if(isset($g['usar_cep'])) {

			$dados['usar_cep'] = $g['usar_cep'];

		} else {

			$dados['usar_cep'] = 0;

		}

		

		//Bairros

		if(isset($g['bairro'])) {

			$dados['bairro'] = $g['bairro'];

		} else {

			$dados['bairro'] = '';

		}

		

		//Numero 

		if(isset($g['numero'])) {

			$dados['numero'] = $g['numero'];

		} else {

			$dados['numero'] = '';

		}

		

		return $dados;

		

	}

	

	public function Formulario( $d ) {

	

		// print_r($d);

		

		$config = array(

			'campos' => 'tipo as "nome_tipo", id as "id_tipo"',

			'tabela' => 'tipo_imovel'

		);

		

		$this->select->set($config);

		$tipo_imovel = $this->select->resultado();

		

		$config = array(

			'campos' => 'tipo, id',

			'tabela' => 'finalidade'

		);



		$this->select->set($config);

		$finalidade = $this->select->resultado();

		

		/* Configuração dos Campos */

		

		//------- Radio Tipo

		$check_venda = '';

		$check_locacao = '';

		$check_temporada = '';

		

		if($d['tipo'] == 1) { $check_venda = 'checked="checked"'; }

		if($d['tipo'] == 2) { $check_locacao = 'checked="checked"'; }

		if($d['tipo'] == 3) { $check_temporada = 'checked="checked"'; }

		

		$radio_tipo = '<div class="columns small-4 medium-4 large-4">

							<input type="radio" name="tipo" id="tipo" value="1" class="radio_tipo" '.$check_venda.'"><span>Venda</span>

						</div>

						

						<div class="columns small-4 medium-4 large-4">

							<input type="radio" name="tipo" id="tipo" value="2" class="radio_tipo" '.$check_locacao.'><span>Locação</span>

						</div>

						

						<div class="columns small-4 medium-4 large-4">	

							<input type="radio" name="tipo" id="tipo" value="3" class="radio_tipo" '.$check_temporada.'><span>Temporada</span>

						</div>';

		//----------

		

		//----- Select Tipo de Imóvel

		$select_tipo_imovel = '<select name="tipo_imovel" id="tipo_imovel">

								<option value="">Todos</option>';

		

		foreach($tipo_imovel as $val ) {

				

				$check = '';

				if(isset($d['id_tipo_imovel'])) {

					if($val->id_tipo == $d['id_tipo_imovel']) $check = 'selected="selected"';

				}

		

				$select_tipo_imovel .= '<option value="'.$val->id_tipo.'" '.$check.'>'.$val->nome_tipo.'</option>';

				

		}

		

		$select_tipo_imovel .= '</select>'; 

		

		//----- Select Finalidade

		

		$select_finalidade = '<select name="finalidade" id="finalidade">

								<option value="">Todas</option>';

		

		foreach($finalidade as $val ) {

				

				$check = '';

				if(isset($d['finalidade'])) {

					if($val->id == $d['finalidade']) $check = 'selected="selected"';

				}

		

				$select_finalidade .= '<option value="'.$val->id.'" '.$check.'>'.$val->tipo.'</option>';

				

		}

		

		$select_finalidade .= '</select>'; 

	

		//----- Select Feirão

		

		$select_feirao = '<select name="feirao" id="feirao">
							<option value="1">Sim</option>
							<option value="0">Não</option>
							<option value="">Ambos</option>
						</select>';


		//----- Dormitorios

		

		$select_dormitorios_min = '';

		

		for($x = 1;$x <= 10;$x++) {

				

				$check = '';

				if($x == $d['dorm_min']) $check = 'selected="selected"';

		

				$select_dormitorios_min .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';

				

		}

		

		$select_dormitorios_max = '';

		

		for($x = 1;$x <= 10;$x++) {

				

				$check = '';

				if($x == $d['dorm_max']) $check = 'selected="selected"';

		

				$select_dormitorios_max .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';

				

		}

	

		//----------

		

		//----- Suites

		

		$select_suites_min = '';

		

		for($x = 1;$x <= 10;$x++) {

				

				$check = '';

				if($x == $d['suite_min']) $check = 'selected="selected"';

		

				$select_suites_min .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';

				

		}

		

		$select_suites_max = '';

		

		for($x = 1;$x <= 10;$x++) {

				

				$check = '';

				if($x == $d['suite_max']) $check = 'selected="selected"';

		

				$select_suites_max .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';

				

		}

	

		//----------

		

		//----- Garagem

		

		$select_garagem_min = '';

		

		for($x = 1;$x <= 10;$x++) {

				

				$check = '';

				if($x == $d['gar_min']) $check = 'selected="selected"';

		

				$select_garagem_min .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';

				

		}

		

		$select_garagem_max = '';

		

		for($x = 1;$x <= 10;$x++) {

				

				$check = '';

				if($x == $d['gar_max']) $check = 'selected="selected"';

		

				$select_garagem_max .= '<option value="'.$x.'" '.$check.'>'.$x.'</option>';

				

		}

	

		//----------

		

		//----- Valor

		

		$valor_minimo = $d['val_min'];

		$valor_maximo = $d['val_max'];

		

		//------ Falar Com 

		

		$ch_prop = '';

		$ch_imo = '';

		

		if($d['proprietario'] == 1) { $ch_prop = 'checked="checked"'; }

		if($d['imobiliaria'] == 1) { $ch_imo = 'checked="checked"'; }

		

		$check_falarcom = '<div class="columns small-4 medium-4 large-4">

					<input type="checkbox" class="check" name="proprietario" value="1" '.$ch_prop.'> Proprietário

				</div>

				

				<div class="columns small-4 medium-4 large-4">

					<input type="checkbox" class="check" name="imobiliaria" value="1" '.$ch_imo.'> Imobiliária

				</div>';

				

		//------ Imobiliária

		

		$config = array(

			'campos' => 'u.id, d.nome as "texto"',

			'tabela' => 'usuarios u',

			'where' => 'u.tipo = 3 AND u.ativo=1',

			'join' => array(

				array('dados_imobiliaria d','d.id = u.perfil','left')

			),

		);



		$this->select->set($config);

		$total = $this->select->total();

		$imobiliaria = $this->select->resultado();

		

		$select_imobiliaria = '';

		

		if($total > 0) {

			

			foreach($imobiliaria as $val ) {

					

					$check = '';

					if($val->id == $d['id_imobiliaria']) $check = 'selected="selected"';

			

					$select_imobiliaria .= '<option value="'.$val->id.'" '.$check.'>'.$val->texto.'</option>';

					

			}

		

		}

		

		//------ Condominio

		

		$condominio = array(

			array(

				'val' => 2,

				'label' => 'Ambos'

			),

			array(

				'val' => 1,

				'label' => 'Não'

			),

			array(

				'val' => 0,

				'label' => 'Sim'

			),

		);



		$check_condominio = '<select name="condominio" class="columns small-12 medium-8 large-8">';



		foreach($condominio as $c ) {



			$check = '';

			if($c['val'] == $d['condominio']) $check = 'selected="selected"';



			$check_condominio .= '<option value="'.$c['val'].'" '.$check.'>'.$c['label'].'</option>';



		}



		$check_condominio .= '</select>';



		//------ Estado

		

		$config = array(

			'campos' => 'uf_codigo, uf_descricao',

			'tabela' => 'cep_uf',

			'orderBy' => array('uf_descricao' => 'ASC')

		);

		

		$this->select->set($config);

		$estado = $this->select->resultado();

		

		$select_estado = '<select name="estado" id="estado" class="t1 select_estado">';

		

		$paramEstado = $this->input->get('estado', true);

	

		foreach($estado as $val ) {



				$check = '';



				if (empty($paramEstado)){

					if($val->uf_codigo == $this->session->userdata('estado')) $check = 'selected="selected"';

				} else {

					if($val->uf_codigo == $d['estado']) $check = 'selected="selected"';

				}

				$select_estado .= '<option value="'.$val->uf_codigo.'" '.$check.'>'.$val->uf_descricao.'</option>';

				

		}

		

		$select_estado .= '</select>'; 

		

		//------ Cidade

		$paramCidade = $this->input->get('cidade', true);

		if (empty($paramCidade)){

			$whereEstado = $this->session->userdata('estado');

		} else {

			$whereEstado = $d['estado'];

		}





		$config = array(

			'campos' => 'cidade_codigo, cidade_descricao',

			'tabela' => 'cep_cidade',

			'where' => array('uf_codigo' => $whereEstado),

			'orderBy' => array('cidade_descricao' => 'ASC')

		);

		

		$this->select->set($config);

		$cidade = $this->select->resultado();

		

		$select_cidade = '<select name="cidade[]" id="cidade" class="t1 select_cidade classificados_cidade" multiple="multiple">';

		

		foreach($cidade as $val ) {

				

				$check = '';



				if (empty($paramCidade)){

					if($val->cidade_codigo == $this->session->userdata('cidade')) $check = 'selected="selected"';

				} else {

					if(in_array($val->cidade_codigo, $d['cidade'])) $check = 'selected="selected"';

				}

				$select_cidade .= '<option value="'.$val->cidade_codigo.'" '.$check.'>'.$val->cidade_descricao.'</option>';

				

		}

		

		$select_cidade .= '</select>'; 

		

		

		/*

		$select_cidade = '<div class="lista_cidades cidade_classificados">

							<ul>';

		

		foreach($cidade as $val ) {

				

			$check = '';



			if (empty($paramCidade)){

				if($val->cidade_codigo == $this->session->userdata('cidade')) $check = 'checked="checked"';

			} else {

				if(in_array($val->cidade_codigo, $d['cidade'])) $check = 'checked="checked"';

			}

			

			$select_cidade .= '<li><label><input type="checkbox" class="check check_cidade" id="cidade" name="cidade[]" value="'.$val->cidade_codigo.'" '.$check.'>'.$val->cidade_descricao.'</label></li>';

			

		}

		

		$select_cidade .= '</ul>

						</div>';

		*/

		

		//------- Bairros

		

		$estrutura_bairros = '';

		

		if(empty($d['cidade'])) { //NÃO EXISTE GET DE CIDADE, PEGA DA SESSAO

			

			$mostra_bairro = 'div.classificados div.lista_bairros { display: block; }';

			

			$c_bairro = $this->session->userdata('cidade');

			

			$config = array(

				'campos' => 'bairro_codigo, bairro_descricao',

				'tabela' => 'cep_bairro',

				'where' => array('cidade_codigo' => $c_bairro)

			);

			

			$this->select->set($config);

			$bairros = $this->select->resultado();

			

			foreach($bairros as $k => $val ) {

					

				$check = '';

					

				if(!empty($d['bairro'])) {

					foreach($d['bairro'] as $b) {

					

						if($val->bairro_codigo == $b) $check = 'checked="checked"';

					

					}

				}

					

				$estrutura_bairros .= '<li><input type="checkbox" class="check check-bairros" id="bairro_'.$k.'" name="bairro[]" value="'.$val->bairro_codigo.'" '.$check.'>'.$val->bairro_descricao.'</li>';

					

			}



		} else {

		

			if(count($d['cidade']) > 1) { //Varias cidades, esconde o bairro

			

				$mostra_bairro = 'div.classificados div.lista_bairros { display: none; }';

			

			} else {

				

				$mostra_bairro = 'div.classificados div.lista_bairros { display: block; }';

			

				$c_bairro = $d['cidade'][0];

			

				$config = array(

					'campos' => 'bairro_codigo, bairro_descricao',

					'tabela' => 'cep_bairro',

					'where' => array('cidade_codigo' => $c_bairro)

				);

				

				$this->select->set($config);

				$bairros = $this->select->resultado();

				

				foreach($bairros as $k => $val ) {

						

					$check = '';

						

					if(!empty($d['bairro'])) {

						foreach($d['bairro'] as $b) {

						

							if($val->bairro_codigo == $b) $check = 'checked="checked"';

						

						}

					}

						

					$estrutura_bairros .= '<li><input type="checkbox" class="check check-bairros" id="bairro_'.$k.'" name="bairro[]" value="'.$val->bairro_codigo.'" '.$check.'>'.$val->bairro_descricao.'</li>';

						

				}

				

			}

		

		}

		

		//Campos de cep

		$cep = $d['cep'];

		

		$cep_rua = $d['cep_rua'];

		$id_rua = $d['id_rua'];

		

		$cep_bairro = $d['cep_bairro'];

		$id_bairro = $d['id_bairro'];

		

		$cep_cidade = $d['cep_cidade'];

		$id_cidade = $d['id_cidade'];

		

		$cep_estado = $d['cep_estado'];

		$id_estado = $d['id_estado'];

		

		//Usar Cep

		$usar_cep = $d['usar_cep'];

		

		//Numero de Referencia 

		if(!empty($d['numero'])) {

			$numero = $d['numero'];	

		} else {

			$numero = '';

		}

		

		//Estilo para mostar as opções de cep

		if($d['usar_cep'] == 1) {

			$estilo_cep = '

				div.classificados div.semcep { display: none; }

				div.classificados div.comcep { display: block; }

				div.classificados div.lista_bairros { display: none; }';

			$texto_cep = 'Pesquisa por Cidade/Estado';

		} else {

			

			$estilo_cep = '

				div.classificados div.semcep { display: block; }

				div.classificados div.comcep { display: none; }';

			$estilo_cep .= $mostra_bairro;

			

			$texto_cep = 'Pesquisa por Endereço';

		}

		

		//Estilo CSS

		$estilo = '<style>';

		

		$estilo .= $estilo_cep;

		

		$estilo .= '</style>';

	

		$d = $this->parser->parse( 'templates/classificados/formulario', array( 

			"radio_tipo" => $radio_tipo,

			"select_tipo_imovel" => $select_tipo_imovel,

			"select_finalidade" => $select_finalidade,

			"select_dormitorios_min" => $select_dormitorios_min,

			"select_dormitorios_max" => $select_dormitorios_max,

			"select_suites_min" => $select_suites_min,

			"select_suites_max" => $select_suites_max,

			"select_garagem_min" => $select_garagem_min,

			"select_garagem_max" => $select_garagem_max,

			"valor_minimo" => $valor_minimo,

			"valor_maximo" => $valor_maximo,

			"check_falarcom" => $check_falarcom,

			"select_imobiliaria" => $select_imobiliaria,

			"check_condominio" => $check_condominio,

			"select_estado" => $select_estado,

			"select_cidade" => $select_cidade,

			"select_feirao" => $select_feirao,

			"estrutura_bairros" => $estrutura_bairros,

			"cep" => $cep,

			"cep_rua" => $cep_rua,

			"id_rua" => $id_rua,

			"cep_bairro" => $cep_bairro,

			"id_bairro" => $id_bairro,

			"cep_cidade" => $cep_cidade,

			"id_cidade" => $id_cidade,

			"cep_estado" => $cep_estado,

			"id_estado" => $id_estado,

			"usar_cep" => $usar_cep,

			"estilo" => $estilo,

			"texto_cep" => $texto_cep,

			"numero" => $numero

		), true );

		

		return $d;

	

	}

	

	public function ResultadoLimpo() {

		

		$data = array();

		$data[ "base_utl" ] = base_url();

		$data[ "venda" ] = $this->Venda();

		$data[ "locacao" ] = $this->Locacao();

		$data[ "temporada" ] = $this->Temporada();

		

		$d = $this->parser->parse( 'templates/classificados/resultado_limpo', $data, true );

	

		return $d;

	

	}

	

	public function ResultadoDados( $d ) {

		

		$join = array(); //Where para tipo de usuario por imovel, proprietario e imobiliaria

		$join[] = array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'); //Where padrao para o tipo do imovel

		$join[] = array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left'); //Where padrao para o endereço

		$join[] = array('cep_cidade cid','cid.cidade_codigo = imo.cidade','left'); //Where padrao para o endereço da cidade

		$join[] = array('usuarios user','user.id = imo.id_usuario','left'); //Where padrao para o vinculo de usuario

	

		$where = array(

			'imo.ativo' => 1, //Ativo

			'imo.status' => 3, //Publicado

			'imo.aprovado' => 1 //Imóvel Aprovado

		);

	

		// Tipo

		$where['imo.tipo'] = $d['tipo'];

		

		if($d['tipo'] == 1) { 

			$class_tipo = 'venda'; 

			$texto_tipo = 'Venda'; 

			$cor_titulo = 'titulo_vermelho'; 

			$barra_tipo = 'barra-vermelho'; 

			$div_tipo = 'venda_item';

			$imagem_tipo = 'status_venda';

		}

		

		if($d['tipo'] == 2) { 

			$class_tipo = 'locacao'; 

			$texto_tipo = 'Locacao'; 

			$cor_titulo = 'titulo_azul'; 

			$barra_tipo = 'barra-azul'; 

			$div_tipo = 'locacao_item';

			$imagem_tipo = 'status_locacao';

		}

		

		if($d['tipo'] == 3) { 

			$class_tipo = 'temporada'; 

			$texto_tipo = 'Temporada'; 

			$cor_titulo = 'titulo_cinza'; 

			$barra_tipo = 'barra-cinza'; 

			$div_tipo = 'temporada_item';

			$imagem_tipo = 'status_temporada';

		}



		// Tipo de Imovel

		if(isset($d['id_tipo_imovel'])) {

			$where['imo.id_tipo_imovel'] = $d['id_tipo_imovel'];

		}	



		// // Finalidade

		if(isset($d['finalidade'])) {

			$where['imo.finalidade'] = $d['finalidade'];

		}


		// Feirao

		if(isset($d['feirao'])) {
			$where['imo.feirao '] = $d['feirao'];
		}

		

		// // Dormitorios

		

		// //---- Minimo

		if(!empty($d['dorm_min'])) {

			$where['imo.dormitorios >= '.$d['dorm_min']] = null;

		}

		

		// //---- Maximo

		if(!empty($d['dorm_max'])) {

			$where['imo.dormitorios <= '.$d['dorm_max']] = null;

		}

		

		// // Suites

		

		// //---- Minimo

		if(!empty($d['suite_min'])) {

			$where['imo.suites >= '.$d['suite_min']] = null;

		}

		

		// //---- Maximo

		if(!empty($d['suite_max'])) {

			$where['imo.suites <= '.$d['suite_max']] = null;

		}

		

		// // Garagem

		

		// //---- Minimo

		if(!empty($d['gar_min'])) {

			$where['imo.garagem >= '.$d['gar_min']] = null;

		}

		

		// //---- Maximo

		if(!empty($d['gar_max'])) {

			$where['imo.garagem <= '.$d['gar_max']] = null;

		}

		

		// // Valor

		

		// //---- Minimo

		if(!empty($d['val_min'])) {

			$val_min = $this->util->moeda2us($d['val_min']);

			$where['imo.valor >= '.$val_min] = null;

		}

		

		// //---- Maximo

		if(!empty($d['val_max'])) {

			$val_max = $this->util->moeda2us($d['val_max']);

			$where['imo.valor <= '.$val_max] = null;

		}

		

		if(isset($d['proprietario']) && $d['proprietario'] == 1 && isset($d['imobiliaria']) && $d['imobiliaria'] == 1) {

			//Não volta nada

		} else if(isset($d['proprietario']) && $d['proprietario'] == 1) { // Proprietario

			$where['user.tipo IN (1, 2)'] = NULL;

		} else if(isset($d['imobiliaria']) && $d['imobiliaria'] == 1) { // Imobiliaria

			$where['user.tipo'] = 3; 

		}

		

		// id_imobiliaria

		if(isset($d['id_imobiliaria']) && $d['id_imobiliaria'] != '') {

			$where['imo.id_usuario'] = $d['id_imobiliaria'];	

		}

		

		//Condominio

		if(isset($d['condominio']) && $d['condominio'] != '' && $d['condominio'] != 2) {

			$where['imo.condominio'] = $d['condominio'];

		}

		

		//Aqui ele verifica se a pessoa está usando a pesquisa por cep ou não, se usar a por cep, o where só sera feito pelo cep, senão, mantera o padrao, pela cidade, pela rua e pelo bairro

		

		switch($d['usar_cep']) {

			

			case 0: //Nao usa o cep

			

				// Cidade

				if(!empty($d['cidade'])) {

					$cidades = implode(',', $d['cidade']);

					//$where['imo.cidade'] = $d['cidade'];

					$where['imo.cidade IN ('.$cidades.')'] = NULL;

				}

				

				// // Estado

				if(!empty($d['estado'])) {

					$where['imo.estado'] = $d['estado'];

				}

				

				// //Bairros

				if(isset($d['bairro']) && is_array($d['bairro']) ) {

					

					if($d['bairro'][0] != '' ) {

						$bairros = implode(',', $d['bairro']);

						$where['imo.bairro IN ('.$bairros.')'] = NULL;

					}



				}

						

			break;

			

			case 1: //Usa o cep

			

				// Cep

				if(!empty($d['cep'])) {

					$where['imo.cep'] = $d['cep'];

				}

				

			break;

			

		}

		

		//Numero 

		if(!empty($d['numero'])) {

			$where = array(); //Deve prevalescer acima de todos os filtros

			$where['imo.ativo'] = 1;

			$where['imo.status'] = 3;

			$where['imo.aprovado'] = 1;

			$where['imo.id'] = $d['numero'];

		}

		

		$config = array(

			'campos' => 'imo.id_usuario  as "usuario", imo.id, imo.slug, imo.titulo, imo.dormitorios, imo.garagem, imo.suites, imo.area_m2, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.valor, cid.cidade_descricao as "cidade"',

			'tabela' => 'imoveis imo',

			'join' => $join,

			'where' => $where,

		);

		

		$this->select->set($config);

		$numero = $this->select->total();



		$config['limit'] = '15';

		$config['offset'] = '0';



		if( !!$per_page = $this->input->get('per_page', TRUE) ) {

			$config['offset'] = $config['limit'] * ($per_page - 1);

		}

		$this->select->set($config);



		// var_dump($config['offset']);

		$paginacao = $this->select->paginacao("classificados", 5);	

		// var_dump($paginacao);die();

		$resultado = $this->select->resultado();

				

		$lista_pesquisa = '';

		

		if($resultado > 0) {

			

			foreach($resultado as $val) {

			

				$config = array(

					'campos' => 'arquivo',

					'tabela' => 'imagens',

					'where' => array('id_imovel' => $val->id, 'destaque' => 1),

					'limit' => 1

				);

				

				$this->select->set($config);

				$num_img = $this->select->total();

				$img = $this->select->resultado();

				

				if($num_img > 0) {

					$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;

				} else {

					

					$config = array(

						'campos' => 'arquivo',

						'tabela' => 'imagens',

						'where' => array('id_imovel' => $val->id),

						'limit' => 1

					);

					

					$this->select->set($config);

					$num_img = $this->select->total();

					$img = $this->select->resultado();

					

					if($num_img > 0) {

						$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;

					} else {

						$imagem = base_url().'assets/images/sem_imagem.jpg';

					}

					

				}



				/* Verifica se a imagem existe, se não, exibe o arquivo todo */

				$arquivo = '';

				if(isset($img[0])) {

					$arquivo = trim($img[0]->arquivo);

				}

				$path = '/assets/uploads/imovel/'.$val->usuario.'/';



				if( substr($arquivo, 0, 4) == 'http' ) {

					$imagem = $arquivo;

				}

				

				$valor = $this->util->moeda2br($val->valor);

			

				if(empty($val->endereco)) {

					$endereco = $val->cidade;

				} else {

					$endereco = $val->endereco;

				}

			

				$lista_pesquisa .= '<li>

										<div class="'.$div_tipo.'">

											

											<div class="imagem">

												<a href="'.base_url().'imovel/'.$val->slug.'?'.$_SERVER[ 'QUERY_STRING' ].'">

													<img class="imovel" src="'.$imagem.'">

													<img class="status" src="'.base_url().'assets/images/'.$imagem_tipo.'.png">

												</a>

											</div>

											

											<div class="dados">



												<a href="'.base_url().'imovel/'.$val->slug.'">

													<h2>#'.$val->id.' - '.$val->titulo.'</h2>

												</a>

												<p><b>Tipo:</b> '.$val->texto_tipo.'</p>

												<p><b>Área útil:</b> '.$val->area_m2.' (m²)</p>

												<p><b>Garagem:</b> '.$val->garagem.'</p>

												<p><b>Local:</b> '.$endereco.'</p>

												<p><b>Número de Quartos:</b> '.$val->dormitorios.'</p>

												<p><b>Número de Suítes:</b> '.$val->suites.'</p>

												<p><b>Valor:</b> R$ '.$valor.' </p>



											</div>

									

										</div>

									</li>';

			

			}

			

		}

		// var_dump($paginacao);

		$data_view = array( 

			"base_utl" => base_url(),

			"class_tipo" => $class_tipo,

			"cor_tipo" => $cor_titulo,

			"barra_tipo" => $barra_tipo,

			"texto_tipo" => $texto_tipo,

			"lista_pesquisa" => $lista_pesquisa,

			"paginacao" => $paginacao

		);



		$d = $this->parser->parse( 'templates/classificados/resultado_dados', $data_view, true );

	

		return $d;

	

	}

	

	public function montarCidades() {

	

		$uf = $this->input->post('uf', TRUE);

		

		$config = array(

			'campos' => 'cidade_codigo, cidade_descricao',

			'tabela' => 'cep_cidade',

			'where' => array('uf_codigo' => $uf)

		);

		

		$this->select->set($config);

		$cidade = $this->select->resultado();

		

		$html = '';

		

		foreach($cidade as $val) {

		

			$html .= '<option value="'.$val->cidade_codigo.'">'.$val->cidade_descricao.'</option>';

		

		}

		

		echo $html;

	

	}

	public function montarFeirao() {

	

		$id_cidade = $this->input->post('id_cidade', TRUE);

		

		$config = array(

			'campos' => 'id_cidade, feirao',

			'tabela' => 'feirao_cidade',

			'where' => array('id_cidade' => $id_cidade)

		);

		

		$this->select->set($config);

		$feirao = $this->select->resultado();

		

		$html = '';

		foreach($feirao as $val) {
			
			if($val->feirao == 1){
			

				$html .= '<option value="1">Sim</option>
						  <option value="0">Não</option>';
			}

			if(empty($val->feirao)){
				$html .= '<option value="0">Não</option>';
			}
		}

		echo $html;

	

	}

	

	public function montarCidadesSessao() {

	

		$uf = $this->input->post('uf', TRUE);

		

		/*

		$query = $this->db->query('

			SELECT cidade FROM imoveis

			UNION

			SELECT cidade FROM anuncios

			UNION

			SELECT cidade FROM banner_empreendimento

			UNION

			SELECT cidade FROM banner_topo

			UNION

			SELECT cidade FROM imobiliarias

			UNION

			SELECT cidade FROM noticias

			UNION

			SELECT cidade FROM revistas'

		);

		*/



		$query = $this->db->query('

			SELECT cidade FROM anuncios where cidade is not null

			UNION

			

			select id_cidade from regiao_cidade rc inner join banner_empreendimento bt on bt.regiao = rc.id_regiao where id_cidade != 0 and id_cidade is not null


			UNION

			select id_cidade from regiao_cidade rc inner join banner_topo bt on bt.regiao = rc.id_regiao where id_cidade != 0 and id_cidade is not null

			UNION

			SELECT cidade FROM imobiliarias where cidade is not null

			UNION

			SELECT cidade FROM noticias where cidade is not null

			UNION

			SELECT cidade FROM revistas where cidade is not null' 


		);

		

		$c = array();

		

		foreach ($query->result() as $r) {

			$c[] = $r->cidade;

	    }

		

		$c = implode(',', $c); //Todas os campos de cidades separado por virgula

		$c2 = explode(',', $c); //Transformar em array para tirar os valores iguais

		$c2 = array_unique($c2);

		

		$cidades = implode(',', $c2);

		

		$query = $this->db->query('SELECT cidade_codigo, cidade_descricao FROM cep_cidade WHERE uf_codigo = '.$uf.' AND cidade_codigo IN ('.$cidades.')');

		

		$html = '';

		

		foreach($query->result() as $val) {

		

			$html .= '<option value="'.$val->cidade_codigo.'">'.$val->cidade_descricao.'</option>';

		

		}

		

		echo $html;

	

	}

	

	public function montarCidadesXml() {

	

		$uf = $this->input->post('uf', TRUE);

		

		$config = array(

			'campos' => 'cidade_codigo, cidade_descricao',

			'tabela' => 'cep_cidade',

			'where' => 'uf_codigo = '.$uf.' AND cidade_cep <> ""'

		);

		

		$this->select->set($config);

		$cidade = $this->select->resultado();

		

		$html = '';

		

		foreach($cidade as $val) {

		

			$html .= '<option value="'.$val->cidade_codigo.'">'.$val->cidade_descricao.'</option>';

		

		}

		

		echo $html;

	

	}

	

	public function montarCidadesClassificados() {

	

		$uf = $this->input->post('uf', TRUE);

		

		$config = array(

			'campos' => 'cidade_codigo, cidade_descricao',

			'tabela' => 'cep_cidade',

			'where' => array('uf_codigo' => $uf),

			'orderBy' => array('cidade_descricao' => 'ASC')

		);

		

		$this->select->set($config);

		$cidade = $this->select->resultado();

		

		$html = '<ul>';

		

		foreach($cidade as $val ) {

			

			$html .= '<li><label><input type="checkbox" class="check check_cidade" id="cidade" name="cidade[]" value="'.$val->cidade_codigo.'" '.$check.'>'.$val->cidade_descricao.'</label></li>';

			

		}

		

		$html .= '</ul>';

		

		echo $html;

	

	}

	

	public function MontarBairros() {

	

		$cidade = $this->input->post('cidade', TRUE);

		

		$config = array(

			'campos' => 'bairro_codigo, bairro_descricao',

			'tabela' => 'cep_bairro',

			'where' => array('cidade_codigo' => $cidade)

		);

		

		$this->select->set($config);

		$bairro = $this->select->resultado();

		

		$html = '<li><input type="checkbox" class="check" id="bairro" name="bairro[]" value="">Todos os Bairros</li>';

		

		foreach($bairro as $k => $val) {

		

			$html .= '<li><input type="checkbox" class="check check-bairros" id="bairro_'.$k.'" name="bairro[]" value="'.$val->bairro_codigo.'">'.$val->bairro_descricao.'</li>';

		

		}

		

		echo $html;

	

	}

	

}