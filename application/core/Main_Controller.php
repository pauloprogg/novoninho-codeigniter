<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_Controller extends CI_Controller {

	public $title = "";
	public $keywords = "";
	public $description = "";
	public $image = "assets/images/apple-touch-icon-114x114.png";
	public $site_name = "Novo Ninho";
	public $author = "Fazul Agência Web";

	public $css = array();
	public $js = array();
	public $js_inline = array();

	public $translate = array();
	public $sufixo = "";
	public $sufixo_sql = "";

	public $menu_header = array();
	public $menu_footer = array();
	public $menu_sidebar = array();

	public function __construct() {

		global $CFG;
		parent::__construct();

		if( $CFG->config['language_active'] ) {
			$this->lang->load('main', $this->config->item('language'));
			$this->sufixo = SUFIXO;
			$this->sufixo_sql = SUFIXO_SQL;
		}

	}
		
	public function head() {

		$data = array(
			array(
				"title" => $this->title,
				"keywords" => $this->keywords,
				"description" => $this->description,
				"image" => $this->image,
				"site_name" => $this->site_name,
				"author" => $this->author,
				"css" => $this->css
			)
		);

		return $data;

	}

	public function setMenuHeader() {

		$subSobre =
		// $subSobre = "";
		/* Set Menu Header aqui */
		$this->menu_header = array(
			array(
				"class_active" => "",
				"last_span" => "",
				"href" => base_url() ."home",
				"label"=> "Home",
				"submenu" => ""
			),
			array(
				"class_active" => "",
				"last_span" => "",
				"href" => base_url() ."quemsomos",
				"label"=> "Quem Somos",
				"submenu" => ""
			),
			array(
				"class_active" => "",
				"last_span" => "",
				"href" => base_url() ."classificados",
				"label"=> "Classificados",
				"submenu" => ""
			),
			array(
				"class_active" => "",
				"last_span" => "",
				"href" => base_url() ."empreendimentos",
				"label"=> "Empreendimentos",
				"submenu" => ""
			),
			array(
				"class_active" => "",
				"last_span" => "",
				"href" => base_url() ."imobiliarias",
				"label"=> "Imobiliárias",
				"submenu" => ""
			),
			array(
				"class_active" => "",
				"last_span" => "",
				"href" => base_url() ."anuncieaqui",
				"label"=> "Anuncie Aqui",
				"submenu" => ""
			),
			array(
				"class_active" => "",
				"last_span" => "",
				"href" => base_url() ."multimidia",
				"label"=> "Multimídia",
				"submenu" => ""
			),
			array(
				"class_active" => "",
				"last_span" => "",
				"href" => base_url() ."noticias",
				"label"=> "Notícias",
				"submenu" => ""
			)
			// array(
			// 	"class_active" => "",
			// 	"last_span" => "last",
			// 	"href" => base_url() ."painel/minhalista",
			// 	"label"=> "Minha Lista",
			// 	"submenu" => ""
			// )
		);

		$sessao = $this->session->userdata('login');

		if( !!$sessao ) {

			$id_usuario = $sessao['id'];
			$tipo_usuario = $sessao['tipo'];

			$this->menu_header[] = array(
				"class_active" => "",
				"last_span" => "",
				"href" => base_url() ."contato",
				"label"=> "Contato",
				"submenu" => ""
			);

			if($tipo_usuario == 1) { // proprietario

				$url = base_url() ."painel-proprietario/minhalista";

			} else
			if($tipo_usuario == 2) { // corretor

				$url = base_url() ."painel-corretor/minhalista";

			} else { // imobiliaria

				$url = base_url() ."painel-imobiliaria/minhalista";

			}

			$this->menu_header[] = array(
				"class_active" => "",
				"last_span" => "last",
				"href" => $url,
				"label"=> "Minha lista",
				"submenu" => ""
			);


		} else {

			$this->menu_header[] = array(
				"class_active" => "",
				"last_span" => "last",
				"href" => base_url() ."contato",
				"label"=> "Contato",
				"submenu" => ""
			);

		}

		$key = 0;

		foreach( $this->menu_header as $menu ) {
			$isActive = ( base_url().$this->uri->segment(1) == $menu[ "href" ] );
			$isHome = ( base_url().$this->uri->segment(1) == base_url() && $menu[ "href" ] == base_url()."home" );

			if( $isActive || $isHome ) {
				$this->menu_header[ $key ][ "class_active" ] .= "active";
				// break;
			}

			if( !empty( $menu[ "submenu" ] ) ) {

				$submenu = "<ul class=\"submenu\">";
				$submenu .= $this->parser->parse( 'templates/submenu_header', array( "submenu" => $menu[ "submenu" ] ), true );
				$submenu .= "</ul>";

				$this->menu_header[ $key ][ "submenu" ] = $submenu;

			}

			$key++;

		}

		$data = array(
			"menu" => $this->menu_header,
			"menu_mobile" => $this->menu_header
		);

		return $this->menu_header = $this->parser->parse( 'templates/menu_header', $data, true );

	}

	public function setMenuHeaderPainel() {

		$subSobre =
		// $subSobre = "";
		/* Set Menu Header aqui */

		$this->menu_header = array(
			array(
				"class_active" => "",
				"last_span" => "",
				"href" => base_url().$this->uri->segment(1)."/editarperfil",
				"label"=> "Editar seus Dados",
				"submenu" => ""
			),
			
			array(
				"class_active" => "",
				"last_span" => "",
				"href" => base_url().$this->uri->segment(1)."/minhalista",
				"label"=> "Minha Lista",
				"submenu" => ""
			)
		);
		//Checagem para ver se o usuario é um proprietario anunciante para esconder o menu de imovel
		
		$sessao = $this->session->userdata('login');
		
		$id_usuario = $sessao['id'];
		$tipo_usuario = $sessao['tipo'];
		if($tipo_usuario == 1) {
			
			$config = array(
				'campos' => 'd.anunciante',
				'tabela' => 'usuarios u',
				'join' => array(
						array('dados_proprietario d','d.id = u.perfil','left')
					),
				'where' => array('u.id' => $id_usuario)
			);
			$this->select->set($config);
			$r = $this->select->resultado();
			
			if($r[0]->anunciante == 1) { //Proprietario anunciante
				
				$this->menu_header[] = array(
	
					"class_active" => "",
	
					"last_span" => "",
	
					"href" => base_url().$this->uri->segment(1)."/imoveis",
	
					"label"=> "Imoveis",
	
					"submenu" => ""
	
				);
				
			}
			
		} else {
		
			$this->menu_header[] = array(
				"class_active" => "",
				"last_span" => "",
				"href" => base_url().$this->uri->segment(1)."/imoveis",
				"label"=> "Imoveis",
				"submenu" => ""
			);
			
		}
		
		//Colocando o item do menu sair no final
		$this->menu_header[] = array(
			"class_active" => "",
			"last_span" => "",
			"href" => base_url()."login/logout",
			"label"=> "Sair",
			"submenu" => ""
		);
		$key = 0;
		
		foreach( $this->menu_header as $menu ) {
			$isActive = ( base_url().$this->uri->segment(1) == $menu[ "href" ] );
			$isHome = ( base_url().$this->uri->segment(1) == base_url() && $menu[ "href" ] == base_url()."home" );

			if( $isActive || $isHome ) {
				$this->menu_header[ $key ][ "class_active" ] .= "active";
				// break;
			}

			if( !empty( $menu[ "submenu" ] ) ) {

				$submenu = "<ul class=\"submenu\">";
				$submenu .= $this->parser->parse( 'templates/submenu_header', array( "submenu" => $menu[ "submenu" ] ), true );
				$submenu .= "</ul>";

				$this->menu_header[ $key ][ "submenu" ] = $submenu;

			}

			$key++;

		}

		$data = array(
			"menu" => $this->menu_header,
			"menu_mobile" => $this->menu_header
		);

		return $this->menu_header = $this->parser->parse( 'templates/menu_header', $data, true );

	}
	public function modalCidade() {
		$data = '';
		
		//Puxando os estados que tem dados cadastrados
		
		/*
		$query = $this->db->query('
			SELECT estado FROM imoveis
			UNION
			SELECT estado FROM anuncios
			UNION
			SELECT estado FROM banner_empreendimento
			UNION
			SELECT estado FROM banner_topo
			UNION
			SELECT estado FROM imobiliarias
			UNION
			SELECT estado FROM noticias
			UNION
			SELECT estado FROM revistas
			GROUP BY estado'
		);
		*/

		$query = $this->db->query('
			SELECT estado FROM anuncios
			UNION
			SELECT estado FROM banner_empreendimento
			UNION
			SELECT estado FROM banner_topo
			UNION
			SELECT estado FROM imobiliarias
			UNION
			SELECT estado FROM noticias
			UNION
			SELECT estado FROM revistas
			GROUP BY estado'
		);
		
		$m = array();
		
		foreach ($query->result() as $r) {
			$m[] = $r->estado;
		}
		
		$meses = implode(',',array_filter($m));
		
		//Estado
		$config = array(
			'campos' => 'uf_codigo, uf_descricao',
			'tabela' => 'cep_uf',
			
			'where' => 'uf_codigo IN ('.$meses.')'
		);
		$this->select->set($config);
		$estado = $this->select->resultado();

		$select_estado = '<select name="estado" id="estado" class="t1 estado_sessao">
							<option value="">Selecione um estado</option>';

		foreach($estado as $val ) {

			$select_estado .= '<option value="'.$val->uf_codigo.'">'.$val->uf_descricao.'</option>';

		}

		$select_estado .= '</select>'; 

		//------ Cidade

		$config = array(
			'campos' => 'cidade_codigo, cidade_descricao',
			'tabela' => 'cep_cidade'
		);

		$this->select->set($config);
		$cidade = $this->select->resultado();

		$select_cidade = '<select name="cidade" id="cidade" class="t1 cidade">';

		foreach($cidade as $val ) {

				$select_cidade .= '<option value="'.$val->cidade_codigo.'">'.$val->cidade_descricao.'</option>';

		}

		$select_cidade .= '</select>'; 

		
		// ---------------------------------------------
		$config = array(
			'campos' => 'id_cidade, feirao',
			'tabela' => 'feirao_cidade'
		);

		$this->select->set($config);
		$feirao_cidade = $this->select->resultado();

		
		if($this->session->userdata('feirao') === false ){
			$this->select->set(
						array(
							'campos' => 'feirao',
							'tabela' => 'config'
					));
				
				$config_data = $this->select->resultado();
				$this->session->set_userdata( 'feirao', $config_data[0]->feirao);
		}
			
		$feirao = $this->session->userdata('feirao');
		$select_feirao = '';
		if ($feirao == '1') {
		
			$select_feirao = '<select name="feirao" id="feirao" class="t1 feirao" disabled="disabled">';

					$select_feirao .= '<option value="'.$feirao.'"> Sim </option>';
					$select_feirao .= '<option value="0"> Não </option>';

			$select_feirao .= '</select>'; 
		
		}
		else{
			$sessionFeirao = '0';
		}
		/**/
		// ------------------------------------------

		$data = array(
				'select_cidade' => $select_cidade,
				'select_estado' => $select_estado,
				'select_feirao' => $select_feirao,
				'base_url' => base_url()
			);

		$mostrarModal = '';
		$sessionCidade = $this->session->userdata('cidade');
		if (empty($sessionCidade) ) {
			$mostrarModal = $this->parser->parse( 'templates/modal_cidade', $data, true );
		}

		return $mostrarModal;
	}
	public function modalCidadeAgain() {
		
		$mostrarModal = '';
		
		$sessionCidade = $this->session->userdata('cidade');
		
		if ($sessionCidade) {
		
			$id_cidade = $this->session->userdata('cidade');
			$id_estado = $this->session->userdata('estado');
	
			$data = '';
			
			//Puxando os estados que tem dados cadastrados
			
			/*
			$query = $this->db->query('
				SELECT estado FROM imoveis
				UNION
				SELECT estado FROM anuncios
				UNION
				SELECT estado FROM banner_empreendimento
				UNION
				SELECT estado FROM banner_topo
				UNION
				SELECT estado FROM imobiliarias
				UNION
				SELECT estado FROM noticias
				UNION
				SELECT estado FROM revistas
				GROUP BY estado'
			);
			*/

			$query = $this->db->query('
				SELECT estado FROM anuncios
				UNION
				SELECT estado FROM banner_empreendimento
				UNION
				SELECT estado FROM banner_topo
				UNION
				SELECT estado FROM imobiliarias
				UNION
				SELECT estado FROM noticias
				UNION
				SELECT estado FROM revistas
				GROUP BY estado'
			);
			
			$m = array();
			
			foreach ($query->result() as $r) {
				$m[] = $r->estado;
			}
			
			$meses = implode(',',array_filter($m));
			
			//Puxando as cidades selecionadas
			
			/*
			$query = $this->db->query('
				SELECT cidade FROM imoveis
				UNION
				SELECT cidade FROM anuncios
				UNION
				SELECT cidade FROM banner_empreendimento
				UNION
				SELECT cidade FROM banner_topo
				UNION
				SELECT cidade FROM imobiliarias
				UNION
				SELECT cidade FROM noticias
				UNION
				SELECT cidade FROM revistas'
			);
			*/

			$query = $this->db->query('
				SELECT cidade FROM anuncios where cidade is not null
				UNION
				
				select id_cidade from regiao_cidade rc inner join banner_empreendimento bt on bt.regiao = rc.id_regiao where id_cidade != 0 and id_cidade is not null

				UNION
				select id_cidade from regiao_cidade rc inner join banner_topo bt on bt.regiao = rc.id_regiao where id_cidade != 0 and id_cidade is not null
				UNION
				SELECT cidade FROM imobiliarias where cidade is not null
				UNION
				SELECT cidade FROM noticias where cidade is not null
				UNION
				SELECT cidade FROM revistas where cidade is not null' 
			);
			
			$c = array();

			foreach ($query->result() as $r) {
				$c[] = $r->cidade;
			}
			
			
			$c = implode(',', $c); //Todas os campos de cidades separado por virgula
			$c2 = explode(',', $c); //Transformar em array para tirar os valores iguais
			$c2 = array_unique($c2);
			
			$cidades = implode(',', $c2);
				
			//Estado
	
			$config = array(
	
				'campos' => 'uf_codigo, uf_descricao',
	
				'tabela' => 'cep_uf',
				
				'where' => 'uf_codigo IN ('.$meses.')'
	
			);
	
			$this->select->set($config);
	
			$estado = $this->select->resultado();
	
			$select_estado = '<select name="estado" id="estado" class="t1 estado_sessao">
								<option value="">Selecione um estado</option>';
	
			foreach($estado as $val ) {
	
				$check = '';
				
				if($val->uf_codigo == $id_estado) {
					$check = 'selected="seleted"';
				}
	
				$select_estado .= '<option value="'.$val->uf_codigo.'" '.$check.'>'.$val->uf_descricao.'</option>';

			}
	
			$select_estado .= '</select>'; 
	
			$config = array(
	
				'campos' => 'cidade_codigo, cidade_descricao', 	
				'tabela' => 'cep_cidade',
				
				'where' => 'cidade_codigo IN ('.$cidades.') AND uf_codigo = '.$id_estado
	
			);

			$this->select->set($config);
	
			$cidade = $this->select->resultado();

			$select_cidade = '<select name="cidade" id="cidade" class="t1 cidade">';

			foreach($cidade as $val ) {
	
				$check = '';
				
				if($val->cidade_codigo == $id_cidade) {
					$check = 'selected="seleted"';
				}
	
				$select_cidade .= '<option value="'.$val->cidade_codigo.'" '.$check.'>'.$val->cidade_descricao.'</option>';	
	
			}
	
			$select_cidade .= '</select>'; 
			
			if($this->session->userdata('feirao') === false ){
				$this->select->set(
							array(
								'campos' => 'feirao',
								'tabela' => 'config'
						));
					
					$config_data = $this->select->resultado();
					$this->session->set_userdata( 'feirao', $config_data[0]->feirao);
			}
				
			$feirao = $this->session->userdata('feirao');
			$select_feirao = '';
			if ($feirao == '1') {
			
				$select_feirao = '<select name="feirao" id="feirao" class="t1 feirao">';

						$select_feirao .= '<option value="'.$feirao.'"> Sim </option>';
						$select_feirao .= '<option value="0"> Não </option>';

				$select_feirao .= '</select>'; 
			
			}
			else{
				$sessionFeirao = '0';
			}
				
			$data = array(
					'select_feirao' => $select_feirao,
					'select_cidade' => $select_cidade,
	
					'select_estado' => $select_estado,
	
					'base_url' => base_url()
	
				);
	
			$mostrarModal = $this->parser->parse( 'templates/modal_cidade_again', $data, true );
		}
		return $mostrarModal;
	}
	public function header() {

		 //Estado
		 $config = array(
			'campos' => 'uf_codigo, uf_descricao',
			'tabela' => 'cep_uf'
		 );

		 $this->select->set($config);
		 $estado = $this->select->resultado();

		 $select_estado = '<select name="estado" id="estado" class="t1 estado">';

		 foreach($estado as $val ) {

			$select_estado .= '<option value="'.$val->uf_codigo.'">'.$val->uf_descricao.'</option>';

		 }

		 $select_estado .= '</select>'; 

		 //------ Cidade

		 $config = array(
			'campos' => 'cidade_codigo, cidade_descricao',
			'tabela' => 'cep_cidade'
		 );

		 $this->select->set($config);
		 $cidade = $this->select->resultado();

		 $select_cidade = '<select name="cidade" id="cidade" class="t1 cidade">';

		 foreach($cidade as $val ) {

				$select_cidade .= '<option value="'.$val->cidade_codigo.'">'.$val->cidade_descricao.'</option>';

		 }

		 $select_cidade .= '</select>'; 

		$texto_header = $this->setHeaderLogin();

		$sessionFeirao = $this->session->userdata('feirao');
	
		//Banner do topo
		$sessionCidade = $this->session->userdata('cidade');

		$config = array(
			'campos' => 'id_regiao as id',
			'tabela' => 'regiao_cidade',
			'where' => '(id_cidade = "'.$sessionCidade.'" OR id_cidade LIKE "%,'.$sessionCidade.'" OR id_cidade LIKE "'.$sessionCidade.',%" OR id_cidade LIKE "%,'.$sessionCidade.',%")'
		);
		$this->select->set($config);
		$regiao = $this->select->resultado();

		$config = array(
			'campos' => 'imagem, link',
			'tabela' => 'banner_topo',
			'orderBy' => array('RAND()' => '' ),
			'limit' => 6,
			//'where' => array('ativo' => 1, 'cidade' => $sessionCidade)
			'where' => 'ativo = 1 AND feirao = "'.$sessionFeirao.'"'
		);
		if(!empty($regiao)){
			$config['where'] .= ' AND (regiao = "'.$regiao[0]->id.'" OR regiao LIKE "%,'.$regiao[0]->id.'" OR regiao LIKE "'.$regiao[0]->id.',%" OR regiao LIKE "%,'.$regiao[0]->id.',%" OR cidade = "'.$sessionCidade.'")';
		}
		$this->select->set($config);
		$r = $this->select->resultado(); 
		
		$data = array(
			array(
				"logo" => "apple-touch-icon-114x114.png",
				"menu" => $this->setMenuHeader(),
				"banners" => $r,
				"texto_header" => $texto_header,
				"modalCidade" => $this->modalCidade(),
				
				"modalCidadeAgain" => $this->modalCidadeAgain(),
				"select_estado" => $select_estado,
				"select_cidade" => $select_cidade
			)
		);
		return $data;
	}

	public function headerPainel() {

		$texto_header = $this->setHeaderLogin();

		$sessionCidade = $this->session->userdata('cidade');

		$config = array(
			'campos' => 'imagem, link',
			'tabela' => 'banner_topo',
			'orderBy' => array('RAND()' => '' ),
			'limit' => 6,
			'where' => 'ativo = 1 AND (cidade = '.$sessionCidade.' OR cidade LIKE "%,'.$sessionCidade.'" OR cidade LIKE "'.$sessionCidade.',%" OR cidade LIKE "%,'.$sessionCidade.',%")'
		);

		$this->select->set($config);
		$r = $this->select->resultado();

		//checa se a cidade escolhida na sessão não retorna resultados. Se não retornar, exibe tudo
		if (empty($r)){
			$config = array(
				'campos' => 'imagem, link',
				'tabela' => 'banner_topo',
				'orderBy' => array('RAND()' => '' ),
				'limit' => 6,
				'where' => array('ativo' => 1)
			);

			$this->select->set($config);
			$r = $this->select->resultado();
		}

		$data = array(
			array(
				"logo" => "apple-touch-icon-114x114.png",
				"menu" => $this->setMenuHeaderPainel(),
				"banners" => $r,
				"texto_header" => $texto_header,
				"modalCidade" => "",
				
				"modalCidadeAgain" => $this->modalCidadeAgain()
			)
		);

		return $data;

	}

	public function setHeaderLogin() {

		//Checagem de Sessão

		if($this->session->userdata('login')) {

			$d = $this->session->userdata('login');

			$tabela = $this->util->tabelaPerfil($d['tipo']);

			$config = array(
				'campos' => 'nome',
				'tabela' => $tabela,
				'where' => array('id' => $d['perfil'])
			);

			$this->select->set($config);
			$r = $this->select->resultado();

			$nome = $r[0]->nome;
			$caminho = $this->util->caminhoPainel($d['tipo']);

			$texto_header = '
				<span class="texto_login"><a href="'.base_url().$caminho.'">Olá <b>'.$nome.'</b></a></span>
				<span class="logout"><a href="'.base_url().'login/logout">Sair</a></span>
			';

		} else {
			// $texto_header = '<span><a href="#" data-reveal-id="modalCadastro">Entrar</a></span>';
			
			$texto_header = '
				<form action="'. base_url() .'login/logar" class="form-login" id="formulario_login" method="post">
					<div class="small-12 medium-5 large-5 columns .small-only-text-center">
						<input type="text" name="login_email" id="login_email" placeholder="Usuário/Login"/>
					</div>
					<div class="small-12 medium-5 large-5 columns .small-only-text-center">
						<input type="password" name="login_senha" id="login_senha" placeholder="senha"/>
						<span class="esqueciSenha"><a href="#" data-reveal-id="modalCadastro">Esqueci minha senha</a></span>
					</div>
					<div class="small-12 medium-2 large-2 columns .small-only-text-center">
						<button type="submit">Entrar</button>
					</div>
				</form>
			';
			/*
			$texto_header = '<form action="'. base_url() .'login/logar" class="form-login" id="formulario_login" method="post">';
			$texto_header .= '<input type="text" name="login_email" id="login_email" placeholder="Usuário"/>';
			$texto_header .= '<input type="password" name="login_senha" id="login_senha" placeholder="senha"/>';
			$texto_header .= '<button type="submit">Entrar</button>';
			$texto_header .= '<span class="esqueciSenha"><a href="#" data-reveal-id="modalCadastro">Esqueci minha senha</a></span>';
			$texto_header .= '</form>';
			*/
		}

		return $texto_header;

	}

	public function getMenuSidebar() {

		foreach( $this->menu_sidebar as $key => $menu ) {

			$isActive = ( rtrim( base_url().$this->uri->segment(1).'/'.$this->uri->segment(2), "/" ) == rtrim( $menu[ "href" ], "/" ) );

			if( $isActive ) {

				$this->menu_sidebar[ $key ][ "class_active" ] .= "active";

			}

			if( !empty( $menu[ "submenu" ] ) ) {

				$submenu = "<ul class=\"submenu\">";
				$submenu .= $this->parser->parse( 'templates/submenu_sidebar', array( "submenu" => $menu[ "submenu" ] ), true );
				$submenu .= "</ul>";

				$this->menu_sidebar[ $key ][ "submenu" ] = $submenu;

			}

		}

		$data = array(
			"menu" => $this->menu_sidebar
		);

		return $this->menu_sidebar = $this->parser->parse( 'templates/menu_sidebar', $data, true );

	}

	public function setMenuSidebarSobre() {

		$this->menu_sidebar = array(
			array(
				"class_active" => "",
				"href" => base_url() ."sobre",
				"label"=> "Quem somos",
				"submenu" => ""
			),
			array(
				"class_active" => "",
				"href" => base_url() ."sobre/o-que-fazemos",
				"label"=> "O que fazemos",
				"submenu" => ""
			)
		);

	}

	public function setMenuFooter() {

		/* Set Menu Footer aqui */
		$this->menu_footer = array(
			array(
				"class_active" => "",
				"href" => base_url() ."home",
				"label"=> "Home"
			),

			array(
				"class_active" => "",
				"href" => base_url() ."noticias",
				"label"=> "Noticias"
			),

		);

		foreach( $this->menu_footer as $key => $menu ) {

			$isActive = ( base_url().$this->uri->segment(1) == $menu[ "href" ] );
			$isHome = ( base_url().$this->uri->segment(1) == base_url() && $menu[ "href" ] == base_url()."home" );

			if( $isActive || $isHome ) {
				$this->menu_footer[ $key ][ "class_active" ] .= "active";
				break;
			}

		}

		$data = array(
			"menu" => $this->menu_footer
		);

		return $this->menu_footer = $this->parser->parse( 'templates/menu_footer', $data, true );;

	}

	public function footer() {

		$data = array(
			array(
				"menu" => $this->setMenuFooter(),
				"copy_right" => date("Y") ." &copy;",
				"js" => $this->js,
				"js_inline" => $this->js_inline
			)
		);

		return $data;

	}

	public function getTranslate() {

		global $LANG;
		$this->translate = $LANG->language;

	}

	public function Noticias() {
		
		$sessionFeirao = $this->session->userdata('feirao');
		$sessionCidade = $this->session->userdata('cidade');
		
		if(empty($sessionCidade)) {
			$sessionCidade = 0;
		}
		
		$where = 'ativo = 1 AND cidade = '.$sessionCidade.' OR todos = 1 AND feirao = '.$sessionFeirao;
		
		$config = array(
			'campos' => 'titulo, resumo, imagem, slug',
			'tabela' => 'noticias',
			'where' => $where,
			'limit' => 4
		);
		$this->select->set($config);
		$r = $this->select->resultado();
		return $this->parser->parse( 'templates/noticias', array('base_url' => base_url(), 'dados' => $r), true );

	}

	public function Anuncios() {
		
		$sessionFeirao = $this->session->userdata('feirao');		
		$sessionCidade = $this->session->userdata('cidade');
		
		if(empty($sessionCidade)) {
			$sessionCidade = 0;
		}
		
		/*
		if(empty($sessionCidade)) {
			$where = 'ativo = 1';
		} else {
			$where = 'ativo = 1 AND cidade = '.$sessionCidade.' OR todos = 1';
		}
		*/
		
		$where = 'ativo = 1 AND (cidade = "'.$sessionCidade.'" OR cidade LIKE "%,'.$sessionCidade.'" OR cidade LIKE "'.$sessionCidade.',%" OR cidade LIKE "%,'.$sessionCidade.',%") AND (feirao = "'.$sessionFeirao.'")';
		//$where = 'ativo = 1 AND cidade IN ('.$sessionCidade.') OR todos = 1';
		
		$config = array(
			'campos' => 'link, imagem',
			'tabela' => 'anuncios',
			'orderBy' => array('RAND()' => '' ),
			'limit' => 6,
			'where' => $where
		);
		$this->select->set($config);
		$r = $this->select->resultado();
		return $this->parser->parse( 'templates/anuncios', array('base_url' => base_url(), 'dados' => $r), true );

	}

	public function bannerHeader() {
				
		$sessionCidade = $this->session->userdata('cidade');
	
		$sessionFeirao = $this->session->userdata('feirao');
		//select quais regios tem essa cidade, pegar id da regiao e buscar na query
		$config = array(
			'campos' => 'id_regiao as id',
			'tabela' => 'regiao_cidade',
			'where' => '(id_cidade = "'.$sessionCidade.'" OR id_cidade LIKE "%,'.$sessionCidade.'" OR id_cidade LIKE "'.$sessionCidade.',%" OR id_cidade LIKE "%,'.$sessionCidade.',%")'
		);
		$this->select->set($config);
		$regiao = $this->select->resultado();
		
		$config = array(
			'campos' => 'link, imagem, titulo, legenda',
			'tabela' => 'banner_empreendimento',
			'orderBy' => array('RAND()' => '' ),
			'limit' => 6,
			//'where' => array('ativo' => 1, 'cidade' => $sessionCidade)
			//'where' => 'ativo = 1 AND (cidade = "'.$sessionCidade.'" OR cidade LIKE "%,'.$sessionCidade.'" OR cidade LIKE "'.$sessionCidade.',%" OR cidade LIKE "%,'.$sessionCidade.',%") AND (feirao = "'.$sessionFeirao.'")'
			'where' => 'ativo = 1 AND (feirao = "'.$sessionFeirao.'")'
		);

		if(!empty($regiao)){
			$config['where'] .= ' AND (regiao = "'.$regiao[0]->id.'" OR regiao LIKE "%,'.$regiao[0]->id.'" OR regiao LIKE "'.$regiao[0]->id.',%" OR regiao LIKE "%,'.$regiao[0]->id.',%" OR cidade = "'.$sessionCidade.'")';
		}
		$this->select->set($config);
		$r = $this->select->resultado();
		
		
		//checa se a cidade escolhida na sessão não retorna resultados. Se não retornar, exibe tudo
		/*
		if (empty($r)) {
			$config = array(
				'campos' => 'link, imagem, titulo, legenda',
				'tabela' => 'banner_empreendimento',
				'orderBy' => array('RAND()' => '' ),
				'limit' => 6,
				'where' => array('ativo' => 1)
			);

			$this->select->set($config);
			$r = $this->select->resultado();

		}
		*/
		return $this->parser->parse( 'templates/banner_header', array('base_url' => base_url(), 'dados' => $r), true );
	}
	public function Destaques() {

		$sessionFeirao = $this->session->userdata('feirao');
				
		$sessionCidade = $this->session->userdata('cidade');

		$config = array(
			'campos' => 'imo.id_usuario as "usuario", imo.area_m2, imo.garagem, imo.id, imo.slug, imo.titulo, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.dormitorios, imo.suites, imo.valor, cid.cidade_descricao as "cidade"',
			'tabela' => 'imoveis imo',
			'join' => array(
						array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'),
						array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left'),
						array('cep_cidade cid','cid.cidade_codigo = imo.cidade','left'),
						array('usuarios user','user.id = imo.id_usuario','left')
					),
			//'where' => array('imo.status' => 3, 'imo.destaque' => 1, 'user.tipo' => 3, 'imo.cidade' => $sessionCidade, 'imo.ativo' => 1, 'imo.feirao' => $sessionFeirao),
			'where' => 'imo.status = 3 AND (cidade = "'.$sessionCidade.'" OR cidade LIKE "%,'.$sessionCidade.'" OR cidade LIKE "'.$sessionCidade.',%" OR cidade LIKE "%,'.$sessionCidade.',%") AND imo.ativo = 1 AND (user.tipo = 3 AND imo.destaque = 1 OR imo.super_destaque = 1)',
			'orderBy' => array('RAND()' => ''),
			'limit' => 16
		);
		
		if($sessionFeirao == '1'){
			$config['where'] .= ' AND feirao = "'.$sessionFeirao.'"';
		}

		$this->select->set($config);
		$num = $this->select->total();
		$r = $this->select->resultado();

		//checa se a cidade escolhida na sessão não retorna resultados. Se não retornar, exibe tudo
		/*
		if(empty($r)){
			$config = array(
				'campos' => 'imo.id, imo.slug, imo.titulo, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.dormitorios, imo.suites, imo.valor',
				'tabela' => 'imoveis imo',
				'join' => array(
							array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'),
							array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left'),
							array('usuarios user','user.id = imo.id_usuario','left')
						),
				'where' => array('imo.status' => 3, 'imo.destaque' => 1, 'user.tipo' => 3, 'imo.ativo' => 1),
				'orderBy' => array('RAND()' => ''),
				'limit' => 16
			);

			$this->select->set($config);
			$num = $this->select->total();
			$r = $this->select->resultado();
		}
		*/

		$destaques = '';

		if($num > 0) {

			foreach($r as $val) {

				$config = array(
					'campos' => 'arquivo',
					'tabela' => 'imagens',
					'where' => array('id_imovel' => $val->id, 'destaque' => 1)
				);

				$this->select->set($config);
				$num_destaque = $this->select->total();
				$img = $this->select->resultado();

				if($num_destaque > 0) {

					$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
					
				} else {

					$config = array(
						'campos' => 'arquivo',
						'tabela' => 'imagens',
						'where' => array('id_imovel' => $val->id, 'destaque' => 1),
						'limit' => 1
					);
					
					$this->select->set($config);
					$num_img = $this->select->total();
					$img = $this->select->resultado();

					if($num_img > 0) {
						$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
					} else {
						
						$config = array(
							'campos' => 'arquivo',
	
							'tabela' => 'imagens',
	
							'where' => array('id_imovel' => $val->id),
	
							'limit' => 1
	
						);
						
						$this->select->set($config);
						$num_img = $this->select->total();
						
						$img = $this->select->resultado();
						
						if($num_img > 0) {
							$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
						
						} else {
							$imagem = base_url().'assets/images/sem_imagem.jpg';
						
						}
					}
					
				}

				/* Verifica se a imagem existe, se não, exibe o arquivo todo */
				$arquivo = trim($img[0]->arquivo);
				$path = '/assets/uploads/imovel/'.$val->usuario.'/';

				if( substr($arquivo, 0, 4) == 'http' ) {
					$imagem = $arquivo;
				}

				if($val->tipo == 1) { $tipo_img = 'status_venda.png'; } //Venda
				if($val->tipo == 2) { $tipo_img = 'status_locacao.png'; } //Locação
				if($val->tipo == 3) { $tipo_img = 'status_temporada.png'; } //Temporada

				$valor = $this->util->moeda2br($val->valor);
				
				if(empty($val->endereco)) {
					$endereco = $val->cidade;
				} else {
					$endereco = $val->endereco;
				}

				$destaques .= '<li>
								<div class="destaque">
									
									<div class="imagem">
										<a href="'.base_url().'imovel/'.$val->slug.'">
											<img class="imovel" src="'.$imagem.'">
											<img class="status" src="'.base_url().'assets/images/'.$tipo_img.'">
										</a>
									</div>
									
									<div class="dados">
										<a href="'.base_url().'imovel/'.$val->slug.'">
											<h2>#'.$val->id.' - '.$val->titulo.'</h2>
										</a>
										<p><b>Tipo:</b> '.$val->texto_tipo.'</p>
										<p><b>Área útil:</b> '.$val->area_m2.' (m<sup>2</sup>)</p>
										<p><b>Garagem:</b> '.$val->garagem.'</p>
										<p><b>Local:</b> '.$endereco.'</p>
										<p><b>Número de Quartos:</b> '.$val->dormitorios.'</p>
										<p><b>Número de Suítes:</b> '.$val->suites.'</p>
										<p><b>Valor:</b> R$ '.$valor.' </p>
									</div>
								
								</div>
							</li>';

			}

		} 
		return $this->parser->parse( 'templates/destaques', array('base_url' => base_url(), 'lista_destaques' => $destaques), true );
	}

	public function DestaquesExport() {
		
		$ids = $this->input->get('ids');
		if(!empty($ids)) {
			$where = array('imo.id IN ('.$ids.')' => NULL);
		} else {
			$where = array('imo.status' => 3, 'imo.destaque' => 1, 'user.tipo' => 3, 'imo.ativo' => 1);
		}
		$config = array(
			'campos' => 'user.tipo as "tipo_usuario", user.perfil as "usuario_perfil", user.id as "usuario", imo.id, imo.slug, imo.titulo, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.dormitorios, imo.suites, imo.valor, cid.cidade_descricao as "cidade"',
			'tabela' => 'imoveis imo',
			'join' => array(
						array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'),
						array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left'),
						array('cep_cidade cid','cid.cidade_codigo = imo.cidade','left'),
						array('usuarios user','user.id = imo.id_usuario','left')
					),
			'where' => $where,
			'orderBy' => array('imo.id' => 'DESC'),
			// 'limit' => '16'
		);
		$this->select->set($config);
		$num = $this->select->total();
		$r = $this->select->resultado();
		//checa se a cidade escolhida na sessão não retorna resultados. Se não retornar, exibe tudo
		/*
		if(empty($r)){
			$config = array(
				'campos' => 'imo.id, imo.slug, imo.titulo, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.dormitorios, imo.suites, imo.valor',
				'tabela' => 'imoveis imo',
				'join' => array(
							array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'),
							array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left'),
							array('usuarios user','user.id = imo.id_usuario','left')
						),
				'where' => array('imo.status' => 3, 'imo.destaque' => 1, 'user.tipo' => 3, 'imo.ativo' => 1),
				'orderBy' => array('RAND()' => ''),
				'limit' => 16
			);
			$this->select->set($config);
			$num = $this->select->total();
			$r = $this->select->resultado();
		}
		*/
		$destaques = '';
		if($num > 0) {
			$count = 1;
			foreach($r as $val) {
				switch($val->tipo_usuario) {
					case 1: //Proprietario
						$tab_dados = 'dados_proprietario';
					break;
					case 2: //Corretor
						$tab_dados = 'dados_corretor';
					break;
					case 3: //Imobiliaria
						$tab_dados = 'dados_imobiliaria';
					break;
				}
				$config = array(
					'campos' => 'd.telefone, d.email',
					'tabela' => $tab_dados.' d',
					'where' => array('d.id' => $val->usuario_perfil)
				);
				$this->select->set($config);
				$user_dados = $this->select->resultado();
				$config = array(
					'campos' => 'arquivo',
					'tabela' => 'imagens',
					'where' => array('id_imovel' => $val->id, 'destaque' => 1)
				);
				$this->select->set($config);
				$num_destaque = $this->select->total();
				$img_dest = $this->select->resultado();
				if($num_destaque > 0) {
					$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img_dest[0]->arquivo;
					
				} else {
					$config = array(
						'campos' => 'arquivo',
						'tabela' => 'imagens',
						'where' => array('id_imovel' => $val->id, 'destaque' => 1),
						'limit' => 1
					);
					
					$this->select->set($config);
					$num_img = $this->select->total();
					$img = $this->select->resultado();
					if($num_img > 0) {
						$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
					} else {
						
						$config = array(
							'campos' => 'arquivo',
	
							'tabela' => 'imagens',
	
							'where' => array('id_imovel' => $val->id),
	
							'limit' => 1
	
						);
						
						$this->select->set($config);
						$num_img = $this->select->total();
						
						$img = $this->select->resultado();
						
						if($num_img > 0) {
							$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
						
						} else {
							$imagem = base_url().'assets/images/sem_imagem.jpg';
						
						}
					}
					
				}
				if($val->tipo == 1) { $tipo_img = 'status_venda.png'; } //Venda
				if($val->tipo == 2) { $tipo_img = 'status_locacao.png'; } //Locação
				if($val->tipo == 3) { $tipo_img = 'status_temporada.png'; } //Temporada
				$valor = $this->util->moeda2br($val->valor);
				
				if(empty($val->endereco)) {
					$endereco = $val->cidade;
				} else {
					$endereco = $val->endereco;
				}
				$destaques .= '
								<div class="columns small-6 container_destaque">
									<div class="destaque">
										
										<div class="imagem">
											<a href="'.base_url().'imovel/'.$val->slug.'">
												<img class="imovel" src="'.$imagem.'">
												<img class="status" src="'.base_url().'assets/images/'.$tipo_img.'">
											</a>
										</div>
										
										<div class="dados">
											<a href="'.base_url().'imovel/'.$val->slug.'">
												<h2>#'.$val->id.' - '.$val->titulo.'</h2>
											</a>
											<p><b>Tipo:</b> '.$val->texto_tipo.'</p>
											<p><b>Local:</b> '.$endereco.'</p>
											<p><b>Número de Quartos:</b> '.$val->dormitorios.'</p>
											<p><b>Número de Suítes:</b> '.$val->suites.'</p>
											<p><b>Valor:</b> R$ '.$valor.' </p>
											<p><b>Email:</b> '.$user_dados[0]->email.' </p>
											<p><b>Telefone:</b> '.$user_dados[0]->telefone.' </p>
										</div>
										<a href="javascript:void(0)" class="remover">&times;</a>
										
									</div>	
								</div>';
				if($count%2 == 0){
					// $destaques .= '<div class="clearfix"></div>';
				}
				$count++;
			}
		} 
		return $this->parser->parse( 'templates/destaquesExport', array('base_url' => base_url(), 'lista_destaques' => $destaques), true );
	}

	public function Venda() {
		
		$sessionFeirao = $this->session->userdata('feirao');
		$sessionCidade = $this->session->userdata('cidade');

		$config = array(
			'campos' => 'imo.id_usuario as "usuario", imo.area_m2, imo.garagem, imo.id, imo.slug, imo.titulo, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.valor, cid.cidade_descricao as "cidade"',
			'tabela' => 'imoveis imo',
			'join' => array(
						array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'),
						array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left'),
						array('cep_cidade cid','cid.cidade_codigo = imo.cidade','left')
					),
			 // 'where' => array('imo.status' => 3, 'imo.destaque' => 1, 'imo.tipo' => 1, 'imo.cidade' => $sessionCidade, 'imo.ativo' => 1, 'imo.aprovado' => 1, 'imo.feirao' => $sessionFeirao),
			'where' => 'imo.status = 3 AND (cidade = "'.$sessionCidade.'" OR cidade LIKE "%,'.$sessionCidade.'" OR cidade LIKE "'.$sessionCidade.',%" OR cidade LIKE "%,'.$sessionCidade.',%") AND imo.tipo = 1 AND imo.ativo = 1 AND imo.aprovado = 1 AND (imo.destaque = 1 OR imo.super_destaque = 1)',
			'orderBy' => array('RAND()' => ''),
			'limit' => 16
		);
		if($sessionFeirao == '1'){
			$config['where'] .= ' AND feirao = "'.$sessionFeirao.'"';
		}

		$this->select->set($config);
		$num = $this->select->total();
		$r = $this->select->resultado();

		/*
		if(empty($r)){
			$config = array(
				'campos' => 'imo.id, imo.slug, imo.titulo, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.valor',
				'tabela' => 'imoveis imo',
				'join' => array(
							array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'),
							array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left')
						),
				'where' => array('imo.status' => 3, 'imo.destaque' => 1, 'imo.tipo' => 1, 'imo.ativo' => 1),
				'orderBy' => array('RAND()' => ''),
				'limit' => 16
			);

			$this->select->set($config);
			$num = $this->select->total();
			$r = $this->select->resultado();

		}
		*/
		
		$vendas = '';

		if($num > 0) {

			foreach($r as $val) {

				$config = array(
					'campos' => 'arquivo',
					'tabela' => 'imagens',
					'where' => array('id_imovel' => $val->id, 'destaque' => 1)
				);

				$this->select->set($config);
				$num_destaque = $this->select->total();
				$img = $this->select->resultado();

				if($num_destaque > 0) {

					$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
					
				} else {

					$config = array(
						'campos' => 'arquivo',
						'tabela' => 'imagens',
						'where' => array('id_imovel' => $val->id, 'destaque' => 1),
						'limit' => 1
					);
					
					$this->select->set($config);
					$num_img = $this->select->total();
					$img = $this->select->resultado();

					if($num_img > 0) {
						$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
					} else {
						
						$config = array(
							'campos' => 'arquivo',
	
							'tabela' => 'imagens',
	
							'where' => array('id_imovel' => $val->id),
	
							'limit' => 1
	
						);
	
						
	
						$this->select->set($config);
	
						$num_img = $this->select->total();
	
						$img = $this->select->resultado();
						
						if($num_img > 0) {
							$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
							
						} else {
							$imagem = base_url().'assets/images/sem_imagem.jpg';
						
						}
						
					}
					
				}

				/* Verifica se a imagem existe, se não, exibe o arquivo todo */
				$arquivo = trim($img[0]->arquivo);
				$path = '/assets/uploads/imovel/'.$val->usuario.'/';

				if( substr($arquivo, 0, 4) == 'http' ) {
					$imagem = $arquivo;
				}

				$valor = $this->util->moeda2br($val->valor);
				
				if(empty($val->endereco)) {
					$endereco = $val->cidade;
				} else {
					$endereco = $val->endereco;
				}

				$vendas .= '<li>
								<div class="venda_item">
									
									<div class="imagem">
										<a href="'.base_url().'imovel/'.$val->slug.'">
											<img class="imovel" src="'.$imagem.'">
											<img class="status" src="'.base_url().'assets/images/status_venda.png">
										</a>
									</div>
									
									<div class="dados">
										<a href="'.base_url().'imovel/'.$val->slug.'">
											<h2>#'.$val->id.' - '.$val->titulo.'</h2>
										</a>
										<p><b>Tipo:</b> '.$val->texto_tipo.'</p>
										<p><b>Área útil:</b> '.$val->area_m2.' (m<sup>2</sup>)</p>
										<p><b>Garagem:</b> '.$val->garagem.'</p>
										<p><b>Local:</b> '.$endereco.'</p>
										<p><b>Valor:</b> R$ '.$valor.' </p>
									</div>
								
								</div>
							</li>';

			}

		} 

		return $this->parser->parse( 'templates/venda', array('base_url' => base_url(), 'lista_vendas' => $vendas), true );

	}

	public function Locacao() {
		
		
			// $this->select->set(
			// 		array(
			// 			'campos' => 'feirao',
			// 			'tabela' => 'config'
			// 	));
			
			// $config_data = $this->select->resultado();
			// $this->session->set_userdata( 'feirao', array( 'value' => $config_data[0]->feirao));
		
		$sessionFeirao = $this->session->userdata('feirao');
		$sessionCidade = $this->session->userdata('cidade');

		$config = array(
			'campos' => 'imo.id_usuario as "usuario", imo.area_m2, imo.garagem, imo.id, imo.slug, imo.titulo, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.valor, cid.cidade_descricao as "cidade"',
			'tabela' => 'imoveis imo',
			'join' => array(
						array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'),
						array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left'),
						array('cep_cidade cid','cid.cidade_codigo = imo.cidade','left')
					),
			// 'where' => array('imo.status' => 3, 'imo.destaque' => 1, 'imo.tipo' => 2, 'imo.cidade' => $sessionCidade, 'imo.ativo' => 1, 'imo.aprovado' => 1, 'imo.feirao' => $sessionFeirao),
			'where' => 'imo.status = 3 AND (cidade = "'.$sessionCidade.'" OR cidade LIKE "%,'.$sessionCidade.'" OR cidade LIKE "'.$sessionCidade.',%" OR cidade LIKE "%,'.$sessionCidade.',%") AND imo.tipo = 2 AND imo.ativo = 1 AND imo.aprovado = 1 AND (imo.destaque = 1 OR imo.super_destaque = 1)',
			'orderBy' => array('RAND()' => ''),
			'limit' => 16
		);

		if($sessionFeirao == '1'){
			$config['where'] .= ' AND feirao = "'.$sessionFeirao.'"';
		}

		$this->select->set($config);
		$num = $this->select->total();
		$r = $this->select->resultado();
		
		/*
		if(empty($r)){
			$config = array(
				'campos' => 'imo.id, imo.slug, imo.titulo, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.valor',
				'tabela' => 'imoveis imo',
				'join' => array(
							array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'),
							array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left')
						),
				'where' => array('imo.status' => 3, 'imo.destaque' => 1, 'imo.tipo' => 2, 'imo.ativo' => 1),
				'orderBy' => array('RAND()' => ''),
				'limit' => 16
			);

			$this->select->set($config);
			$num = $this->select->total();
			$r = $this->select->resultado();
		}
		*/

		$locacao = '';

		if($num > 0) {

			foreach($r as $val) {

				$config = array(
					'campos' => 'arquivo',
					'tabela' => 'imagens',
					'where' => array('id_imovel' => $val->id, 'destaque' => 1)
				);

				$this->select->set($config);
				$num_destaque = $this->select->total();
				$img = $this->select->resultado();

				if($num_destaque > 0) {

					$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
					
				} else {

					$config = array(
						'campos' => 'arquivo',
						'tabela' => 'imagens',
						'where' => array('id_imovel' => $val->id, 'destaque' => 1),
						'limit' => 1
					);
					
					$this->select->set($config);
					$num_img = $this->select->total();
					$img = $this->select->resultado();

					if($num_img > 0) {
						$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
					} else {
						
						$config = array(
							'campos' => 'arquivo',
	
							'tabela' => 'imagens',
	
							'where' => array('id_imovel' => $val->id)
	
						);
	
						$this->select->set($config);
	
						$num_img = $this->select->total();
	
						$img = $this->select->resultado();
						
						if($num_img > 0) {
							$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
						} else {
							$imagem = base_url().'assets/images/sem_imagem.jpg';	
						}
					}
					
				}

				/* Verifica se a imagem existe, se não, exibe o arquivo todo */
				$arquivo = trim($img[0]->arquivo);
				$path = '/assets/uploads/imovel/'.$val->usuario.'/';

				if( substr($arquivo, 0, 4) == 'http' ) {
					$imagem = $arquivo;
				}

				$valor = $this->util->moeda2br($val->valor);
				
				if(empty($val->endereco)) {
					$endereco = $val->cidade;
				} else {
					$endereco = $val->endereco;
				}

				$locacao .= '<li>
								<div class="locacao_item">
									
									<div class="imagem">
										<a href="'.base_url().'imovel/'.$val->slug.'">
											<img class="imovel" src="'.$imagem.'">
											<img class="status" src="'.base_url().'assets/images/status_locacao.png">
										</a>
									</div>
									
									<div class="dados">
										<a href="'.base_url().'imovel/'.$val->slug.'">
											<h2>#'.$val->id.' - '.$val->titulo.'</h2>
										</a>
										<p><b>Tipo:</b> '.$val->texto_tipo.'</p>
										<p><b>Área útil:</b> '.$val->area_m2.' (m<sup>2</sup>)</p>
										<p><b>Garagem:</b> '.$val->garagem.'</p>
										<p><b>Local:</b> '.$endereco.'</p>
										<p><b>Valor:</b> R$ '.$valor.' </p>
									</div>
								
								</div>
							</li>';

			}

		} 

		return $this->parser->parse( 'templates/locacao', array('base_url' => base_url(), 'lista_locacao' => $locacao), true );

	}

	public function Temporada() {
		
			// $this->select->set(
			// 		array(
			// 			'campos' => 'feirao',
			// 			'tabela' => 'config'
			// 	));
			
			// $config_data = $this->select->resultado();
			// $this->session->set_userdata( 'feirao', array( 'value' => $config_data[0]->feirao));
		
		$sessionFeirao = $this->session->userdata('feirao');
		$sessao = $this->session->userdata('login');

		$id_usuario = $sessao['id'];
		
		$sessionCidade = $this->session->userdata('cidade');
		$config = array(
			'campos' => 'imo.id_usuario as "usuario", imo.area_m2, imo.garagem, imo.id, imo.slug, imo.titulo, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.valor, cid.cidade_descricao as "cidade"',
			'tabela' => 'imoveis imo',
			'join' => array(
						array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'),
						array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left'),
						array('cep_cidade cid','cid.cidade_codigo = imo.cidade','left')
					),
			//'where' => array('imo.status' => 3, 'imo.destaque' => 1, 'imo.tipo' => 3, 'imo.cidade' => $sessionCidade, 'imo.ativo' => 1, 'imo.aprovado' => 1, 'imo.feirao' => $sessionFeirao),
			'where' => 'imo.status = 3 AND (cidade = "'.$sessionCidade.'" OR cidade LIKE "%,'.$sessionCidade.'" OR cidade LIKE "'.$sessionCidade.',%" OR cidade LIKE "%,'.$sessionCidade.',%") AND imo.tipo = 3 AND imo.ativo = 1 AND imo.aprovado = 1 AND (imo.destaque = 1 OR imo.super_destaque = 1)',
			'orderBy' => array('RAND()' => ''),
			'limit' => 16
		);

		if($sessionFeirao == '1'){
			$config['where'] .= ' AND feirao = "'.$sessionFeirao.'"';
		}

		$this->select->set($config);
		$num = $this->select->total();
		$r = $this->select->resultado();
		
		/*
		if(empty($r)){
			$config = array(
				'campos' => 'imo.id, imo.slug, imo.titulo, imo.tipo, tip.tipo as "texto_tipo", ba.bairro_descricao as "endereco", imo.valor',
				'tabela' => 'imoveis imo',
				'join' => array(
							array('tipo_imovel tip','tip.id = imo.id_tipo_imovel','left'),
							array('cep_bairro ba','ba.bairro_codigo = imo.bairro','left')
						),
				'where' => array('imo.status' => 3, 'imo.destaque' => 1, 'imo.tipo' => 3, 'imo.ativo' => 1),
				'orderBy' => array('RAND()' => ''),
				'limit' => 16
			);

			$this->select->set($config);
			$num = $this->select->total();
			$r = $this->select->resultado();
		}
		*/
		
		$temporada = '';

		if($num > 0) {

			foreach($r as $val) {

				$config = array(
					'campos' => 'arquivo',
					'tabela' => 'imagens',
					'where' => array('id_imovel' => $val->id, 'destaque' => 1)
				);

				$this->select->set($config);
				$num_destaque = $this->select->total();
				$img = $this->select->resultado();

				if($num_destaque > 0) {

					$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
					
				} else {

					$config = array(
						'campos' => 'arquivo',
						'tabela' => 'imagens',
						'where' => array('id_imovel' => $val->id, 'destaque' => 1),
						'limit' => 1
					);
					
					$this->select->set($config);
					$num_img = $this->select->total();
					$img = $this->select->resultado();

					if($num_img > 0) {
						$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
					} else {
						
						$config = array(
	
							'campos' => 'arquivo',
	
							'tabela' => 'imagens',
	
							'where' => array('id_imovel' => $val->id),
	
							'limit' => 1
	
						);
	
						
	
						$this->select->set($config);
	
						$num_img = $this->select->total();
	
						$img = $this->select->resultado();
						
						if($num_img > 0) {
							$imagem = base_url().'assets/uploads/imovel/'.$val->usuario.'/'.$img[0]->arquivo;
						
						} else {
							$imagem = base_url().'assets/images/sem_imagem.jpg';
							
						}
					}
					
				}

				/* Verifica se a imagem existe, se não, exibe o arquivo todo */
				$arquivo = trim($img[0]->arquivo);
				$path = '/assets/uploads/imovel/'.$val->usuario.'/';

				if( substr($arquivo, 0, 4) == 'http' ) {
					$imagem = $arquivo;
				}

				$valor = $this->util->moeda2br($val->valor);
				
				if(empty($val->endereco)) {
					$endereco = $val->cidade;
				} else {
					$endereco = $val->endereco;
				}

				$temporada .= '<li>
								<div class="temporada_item">
									
									<div class="imagem">
										<a href="'.base_url().'imovel/'.$val->slug.'">
											<img class="imovel" src="'.$imagem.'">
											<img class="status" src="'.base_url().'assets/images/status_temporada.png">
										</a>
									</div>
									
									<div class="dados">
										<a href="'.base_url().'imovel/'.$val->slug.'">
											<h2>#'.$val->id.' - '.$val->titulo.'</h2>
										</a>
										<p><b>Tipo:</b> '.$val->texto_tipo.'</p>
										<p><b>Área útil:</b> '.$val->area_m2.' (m<sup>2</sup>)</p>
										<p><b>Garagem:</b> '.$val->garagem.'</p>
										<p><b>Local:</b> '.$endereco.'</p>
										<p><b>Valor:</b> R$ '.$valor.' </p>
									</div>
								
								</div>
							</li>';

			}

		} 

		return $this->parser->parse( 'templates/temporada', array('base_url' => base_url(), 'lista_temporada' => $temporada), true );

	}

	public function termos(){
		$query = array(
			'campos' => 'titulo, texto',
			'tabela' => 'termos'
		);
		
		$this->select->set($query);
		$t = $this->select->resultado();
						
		$modal_termos = $this->parser->parse( 'templates/modal_termos', array(
			'base_url' => base_url(), 'titulo' => $t[0]->titulo, 'texto' => $t[0]->texto
		), true );

		return $modal_termos;
		
	}
	
	
	public function load( $view, $data = array() ) {

		$this->getTranslate();
		$data = array_merge( $data, $this->translate );
		$data[ "base_url" ] = base_url();
		
		$data[ "head" ] = $this->head();

		/* Carregar o header normal ou dos paineis */
		if($this->uri->segment(1) != 'painel-proprietario' && $this->uri->segment(1) != 'painel-corretor' && $this->uri->segment(1) != 'painel-imobiliaria') {
			$data[ "header" ] = $this->header();
			$data[ "bannerheader" ] = $this->bannerHeader();
		} else {
			$data[ "header" ] = $this->headerPainel();
			$data[ "bannerheader" ] = '';
		}

		$data[ "footer" ] = $this->footer();
		$data[ "termos" ] = $this->termos();

		// $data[ "destaques" ] = $this->Destaques();
		// $data[ "venda" ] = $this->Venda();
		// $data[ "locacao" ] = $this->Locacao();
		// $data[ "temporada" ] = $this->Temporada();

		// $data[ "modalCidade" ] = $this->modalCidade();

		$data[ "anuncios" ] = $this->Anuncios();

		$data[ "noticias" ] = $this->Noticias();

		if(substr($view, 0,9) == 'destaques'){
			$data[ "destaquesExport" ] = $this->DestaquesExport();
			$this->parser->parse( 'templates/metas', $data );
			$this->parser->parse( $view, $data );
			$this->parser->parse( 'templates/footer', $data );
		} else {
			$this->parser->parse( 'templates/metas', $data );


			$this->parser->parse( 'templates/header', $data );

			$this->parser->parse( $view, $data );
			$this->parser->parse( 'templates/footer', $data );
			
		}

	}

}