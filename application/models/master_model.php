<?php
class Master_model extends CI_Model {

	public function cadastrar_form($tabela,$data){
		$this->db->insert($tabela, $data); 
		$id = $this->db->insert_id();
		$this->db->close();
		return $id;
	}
	
	public function update_form($tabela,$data,$id){
		$this->db->where('id', $id);
		$this->db->update($tabela, $data); 
		$this->db->close();
		return true;
	}
	
	public function delete_form($tabela, $id, $campo) {
		$this->db->where($campo, $id);
		$this->db->delete($tabela);
		$this->db->close();
		return true;
	}
	
}
?>