$(document).ready(function(){
	
	$(document).foundation();
	
	$( "#form-cidade_cep" ).mask( "99999-999" );
	
	function changeLink( $this, $button ) {
		
		var href = $button.attr("href").split("=");
		var ids = $button.data("ids").split(",");

		if( Array.isArray( ids ) ) {
			
			ids = ids.filter(function(id){
				if( id != $this.val() ) {
					return id;
				}
			});

			if( $this.is(":checked") ) {
				ids.push( $this.val() );
			}


			ids = ids.join(',');
			console.log(ids);
			$button.data("ids", ids );
			$button.attr("href", $button.data("href")+ids );

		}

	}

	$(".change-anuncio-linha").on( "change", function() {
		changeLink( $( this ), $(".btn-anuncio-linha") );
	});

	$(".change-anuncio-destaque").on( "change", function() {
		changeLink( $( this ), $(".btn-anuncio-destaque") );
	});
	
	//Menu de paorvação
	$(".AbrirModalAprovar").click(function () {
		var id = $(this).attr('data-id');
		var tipo = $(this).attr('data-tipo');
		
		$('#id_imovel_aprovar').val(id);
		$('#tipo_aprovar').val(tipo);
		
		$('#ModalAprovar').foundation('reveal', 'open');
		
	});
	//Menu recusar
	$(".AbrirModalRecusar").click(function () {
		var id = $(this).attr('data-id');
		
		$('#id_veiculo_recusar').val(id);
		
		$('#ModalRecusar').foundation('reveal', 'open');
		
	});
	//Menu Select
	$(".select").on("change",function(){
		var tv = $(this).val();
		if(tv == "") {
			return false;
		} else {
			window.location=tv;
		}
	});

	/* Submenu */
	$('ul.main li.hasmenu').hover(function(){
		$(this).children('a').addClass('active');
		$(this).children('ul.submenu').slideDown('fast');
	}, function(){
		$(this).children('a').removeClass('active');
		$(this).children('ul.submenu').css('display', 'none');
	});

	/* Mask */
	$( ".datepicker" ).mask( "99/99/9999" );
	
	// Tinymce
	tinymce.init({
		selector: "textarea:not(.noeditor)",
		theme: "modern",
		resize: false,
		width: "100%",
		height: 300,
		plugins: [
			 "advlist autolink link lists charmap preview hr pagebreak",
			 "searchreplace wordcount visualblocks visualchars code insertdatetime ",
			 "save contextmenu directionality paste textcolor"
		],
		content_css: base_url +"../assets/css/style.css",
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist", 
		style_formats: [
			{title: 'header 1', block: 'h1', classes: 'header-tinymce'},
			{title: 'header 2', block: 'h2', classes: 'header-tinymce'},
		]
	});

	/* Validate */
	$("#formLogin").validate({
		messages:{
			iptLogin:{
				required: 'Digite o seu login',
			},
			iptPass:{
				required: 'Digite sua senha',
			}
		}
	});

	$("#formRecuperar").validate({
		messages:{
			iptLogin:{
				required: 'Digite o seu login',
			}
		}
	});

	// submit form
	!function() {

		var forms = $( ".form-esquerdo, .form-direito" );
		forms.validate();

		$( "button.submit" ).on( "click", function() {

			$( "textarea:not(.noeditor)" ).each( function() {
				tinyMCE.get( $( this ).attr( "id" ) ).save();
			} );

			if( forms.valid() ) {

				$.post( forms.attr( "action" ), forms.serialize(), function( r ) {
					
					r = $.parseJSON( r );
					
					if( r.sucesso == "true") {

						window.location.href = r.link;

					}

				} );

			}


		} );
	}();

	// slug
	!function() {

		$( '#form-slug' ).slugify( '#form-nome, #form-titulo, #form-Produto' );

	}();

	//upload de imagem
	!function() {

		$( ".form-arquivo" ).each( function() {

			var $this = $( this );
			var progress = $this.find(".progress");
			var bar = $this.find(".meter");
			var percentVal = "1%";
			var content = $this.find(".content-retorno");
			var isImage = $this.hasClass( "image" );
			var isGaleria = $this.hasClass( "galeria" );

			$this.ajaxForm({
				beforeSend: function() {

					if( !isGaleria ) {

						content.fadeOut().empty();
						percentVal = "1%";

					}

					progress.removeClass( "success" ).fadeIn();

					bar.css({
						width: percentVal
					}).html(percentVal);

				},
				uploadProgress: function( event, position, total, percentComplete ) {

					percentVal = percentComplete + "%";
					bar.css({
						width: percentVal
					}).html(percentVal);

				},
				success: function() {

					percentVal = "100%";

					progress.addClass( "success" ).fadeOut(1500);

					bar.css({
						width: percentVal
					}).html(percentVal);

				},
				complete: function(xhr) {

					if( isImage ) {

						content.html( "<img src=\""+xhr.responseText+"\"/>").fadeIn(1500);

					} else
					if( isGaleria ) {

						var json = $.parseJSON( xhr.responseText );
						var li = "";

						li += '<li>';
						li += '<a href="javascript:void(0);" class="deletar" data-id="'+ json.id +'">X</a>';
						li += '<a href="'+json.src_full+'" target="_blank">';
						li += '<img src="'+ json.src +'" alt=""/>';
						li += '</a>';
						li += '<input type="text" placeholder="insira uma legenda" value="" data-id="'+ json.id +'"/>';
						li += '</li>';

						content.prepend( li ).fadeIn(1500);
	
						galeria_deletar();
						galeria_legenda();

					} else {

						if( xhr.responseText == "0" ) {

							content.html( "Ops! erro ao fazer upload, verifique a extensão.").fadeIn(1500);

						} else {

							content.html( "<a href=\""+xhr.responseText+"\" target=\"_blank\">Veja o arquivo</a>").fadeIn(1500);

						}

					}

				}
			});

		} );

		$( ".form-arquivo input[type=\"file\"]" ).on( "change", function() {
			
			if( $( this ).val() != "" ) {

				$( this ).parents( "form" ).submit();

			}

		} );
	}();

	// Galeria
	function galeria_deletar() {

		$( "#galeria .deletar" ).on( "click", function() {

			var $this = $( this );
			var id = $this.data( "id" );
			var data = {
				id: id
			};

			$.post( base_url+$this.data('pagina')+'/galeria_delete', data, function() {

				$this.parent().remove();

			} );

		} );

	}

	function galeria_legenda() {
		$( "#galeria .legenda" ).on( "change", function() {

			var $this = $( this );
			var id = $this.data( "id" );
			var legenda = $this.val();

			var data = {
				id: id,
				legenda: legenda
			};

			$.post( base_url, data, function() {} );

		} );
	}

	galeria_deletar();
	galeria_legenda();

	$('#form-estado').change(function() {
		
		var val = $(this).val();
		
		dados = {
			uf : val
		}
		
		$.post( base_url+"home/selectCidade", dados, function( res ) {
			$('#form-cidade').html(res);
		});
		
	});

	$('.select-tipo-usuario').change(function() {
		
		var val = $(this).val();

		if(val == 3) {
			$('.select-imobiliaria').show();
		} else {
			$('.select-imobiliaria').hide();
		}
		
	});

	/*$('#form-estado2').change(function() {
		
		var val = $(this).val();
		
		dados = {
			uf : val
		}
		
		$.post( base_url+"home/selectCidadeMultiplo", dados, function( res ) {
			$('.lista-cidade').html(res);
		});
		
	});*/

	$(".select-cidades").select2({
		placeholder: 'Selecione a cidade'
	});

	$(".select-estados").select2({
		placeholder: 'Selecione o estado'
	}).on("change", function(){
		
		var val = $(this).val();
		
		dados = {
			uf : val
		};
		
		$.post( base_url+"home/selectCidadeMultiplo", dados, function( res ) {

			var _data = $.parseJSON( res );

			var _cidades = '';

			$.each( _data, function() {
				_cidades += '<option value="'+this.cidade_codigo+'">'+this.cidade_descricao+' - '+ this.uf_sigla +'</option>';
			});

			var _valCidades = $(".select-cidades").val();
			$(".select-cidades").html( _cidades )
			.val( _valCidades )
			.trigger( "change" );

		});

	});

});