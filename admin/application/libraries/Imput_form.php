<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imput_form {

	private $CI = NULL;

	public function __construct() {

		$this->CI =& get_instance();

	}

	public function form( $campos ) {

		$form = array();

		foreach( $campos as $key => $val ) {

			$campo = "";

			$descricao = ( isset( $val['descricao'] ) ) ? $val['descricao'] : '';
			$required = ( isset( $val['required'] ) ) ? $val['required'] : 'required';
			$max_length = ( isset( $val['max_length'] ) ) ? 'maxlength="'.$val['max_length'].'"' : '';
			$disabled = ( isset( $val['disabled'] ) ) ? 'disabled="disabled"' : '';

			
			$function_name = $val['type']; 
			$campo = $this->$function_name( $val['label'], $val['name'], $val['value'], $val['width'], $descricao, $required, $max_length, $disabled );

			$form[] = array(
				"campo" => $campo
			);

		}

		return $form;

	}

	// Upload de arquivo
	public function file( $campos ) {

		$form = array();

		foreach( $campos as $key => $val ) {

			if( isset( $val['value'] ) && $val['value'] ) {

				if( $val[ 'type' ] == 'image' ) {

					$val['value'] = '<img src="'.$val['value'].'">';

				} else {

					$val['value'] = '<a href="'.$val['value'].'" target="_blank">Abrir arquivo</a>';

				}

			}

			$form[] = array(
				"campo" => $this->CI->parser->parse( "templates/form/form_". $val[ 'type' ], $val, true )
			);

		}

		return $form;

	}

	// Upload de imagem
	public function image( $label, $name, $value ) {

		$data = array(
			"label" => $label,
			"id" => "form-". $name,
			"name" => $name,
			"value" => $value
		);

		return $this->CI->parser->parse( "templates/form/input_image", $data, true );

	}

	// Upload de galeria
	public function galeria( $path_galeria, $width, $height, $pagina, $galeria, $elemento = false ) {

		$this->CI->session->set_userdata( "galeria_id", time().uniqid() );

		$elemento = ( $elemento ) ? $elemento : time().uniqid();

		$imagens = $this->CI->crud->read( array( 'galeria' => $galeria, 'elemento'=> $elemento ), 'imagens', array( 'id' => 'DESC' ) );

		$data = array(
			"galeria" => $galeria,
			"elemento" => $elemento,
			"imagens" => $imagens,
			"path_galeria" => base_url().$path_galeria,
			"width" => $width,
			"height" => $height,
			"pagina" => $pagina
		);

		return $this->CI->parser->parse( "templates/form/form_galeria", $data, true );

	}

	public function galeria_legenda( $id, $legenda, $tabela = 'imagens' ) {

		$this->CI->crud->update( array( 'id' => $id ), array( 'legenda' => $legenda ), $tabela );

	}

	public function galeria_insert( $dados, $tabela = 'imagens' ) {

		return $this->CI->crud->create( $dados, $tabela );

	}

	public function galeria_update( $elemento, $galeria_id, $tabela = 'imagens' ) {

		$this->CI->crud->update( array( 'elemento' => $galeria_id ), array( 'elemento' => $elemento ), $tabela );

	}

	public function galeria_delete( $id, $tabela = 'imagens' ) {

		return $this->CI->crud->delete( array( 'id' => $id ), $tabela );

	}

	// Categorias
	public function categorias( $pagina, $tabela, $item ) {

		// Busca as categorias do item
		$id_categorias = array();

		$config = array(
			'campos' => 'p.categorias',
			'tabela' => $tabela .' as p',
			'where' => array( 'p.id' => $item )
		);

		$this->CI->select->set( $config );
		$q_item = $this->CI->select->resultado();

		if( !empty( $q_item ) ) {

			$item = $q_item[0];
			$id_categorias = explode( ",", $item->categorias );

		}
		// #

		$config = array(
			'campos' => 'c.id, c.nome',
			'tabela' => 'areas_admin as a',
			'join' => array(
				array( 'categorias as c', 'c.id_area = a.id', 'left' )
			),
			'where' => array(
				'c.categoria_pai IS NULL' => NULL,
				'c.ativo' => 1,
				'a.pagina' => $pagina
			),
			'orderBy' => array( "c.nome" => "ASC" )
		);

		$this->CI->select->set( $config );

		$q_categorias = $this->CI->select->resultado();

		$categorias = array();

		foreach( $q_categorias as $categoria ) {
			
			// verifica se está selecionada
			$check = '';
			if( in_array( $categoria->id, $id_categorias ) ) {

				$check = 'checked="checked"';

			}
			// #

			// Seleciona a categoria filha
			$config = array(
				'campos' => 'c.id, c.nome',
				'tabela' => 'areas_admin as a',
				'join' => array(
					array( 'categorias as c', 'c.id_area = a.id', 'left' )
				),
				'where' => array(
					'c.categoria_pai' => $categoria->id,
					'c.ativo' => 1,
					'a.pagina' => $pagina
				),
				'orderBy' => array( "c.nome" => "ASC" )
			);

			$this->CI->select->set( $config );
			$q_cat_filho = $this->CI->select->resultado();
			$cat_filho = array();

			foreach( $q_cat_filho as $cat ) {

				// verifica se está selecionada
				$check_filho = '';

				if( in_array( $cat->id, $id_categorias ) ) {

					$check_filho = 'checked="checked"';

				}
				// #

				$cat_filho[] = array(
					"check_filho" => $check_filho,
					"nome" => $cat->nome,
					"id" => $cat->id
				);

			}

			// #

			$categorias[] = array(
				"check" => $check,
				"cat_filho" => $cat_filho,
				"nome" => $categoria->nome,
				"id" => $categoria->id
			);

		}

		$data = array(
			"categorias" => $categorias
		);

		return $this->CI->parser->parse( "templates/form/form_categorias", $data, true );

	}

	public function categorias_update( $tabela, $item, $categorias = array() ) {

		$data = array(
			'categorias' => implode( ",", $categorias )
		);

		$this->CI->crud->update( array( 'id' => $item ), $data, $tabela );

	}

	public function tags( $pagina, $tabela, $item ) {

		// Busca as tags do item
		$id_tags = array();

		$config = array(
			'campos' => 'p.tags',
			'tabela' => $tabela .' as p',
			'where' => array( 'p.id' => $item )
		);

		$this->CI->select->set( $config );
		$q_item = $this->CI->select->resultado();

		if( !empty( $q_item ) ) {

			$item = $q_item[0];
			$id_tags = explode( ",", $item->tags );

		}
		// #

		$config = array(
			'campos' => 't.id, t.nome',
			'tabela' => 'areas_admin as a',
			'join' => array(
				array( 'tags as t', 't.id_area = a.id', 'left' )
			),
			'where' => array(
				't.ativo' => 1,
				'a.pagina' => $pagina
			),
			'orderBy' => array( "t.nome" => "ASC" )
		);

		$this->CI->select->set( $config );

		$q_tags = $this->CI->select->resultado();

		$tags = array();

		foreach( $q_tags as $tag ) {
			
			// verifica se está selecionada
			$check = '';
			if( in_array( $tag->id, $id_tags ) ) {

				$check = 'checked="checked"';

			}
			// #

			$tags[] = array(
				"check" => $check,
				"nome" => $tag->nome,
				"id" => $tag->id
			);

		}

		$data = array(
			"tags" => $tags
		);

		return $this->CI->parser->parse( "templates/form/form_tags", $data, true );

	}

	public function tags_update( $tabela, $item, $categorias = array() ) {

		$data = array(
			'tags' => implode( ",", $categorias )
		);

		$this->CI->crud->update( array( 'id' => $item ), $data, $tabela );

	}

	// Select
	public function select( $label, $name, $itens = array(), $value = 0 ) {

		$options = array();

		foreach( $itens as $item ) {

			if( !is_array( $item ) ) {
				$item = get_object_vars( $item );
			}

			$selected = "";

			if( $item[ 'value' ] == $value ) {

				$selected = 'selected="selected"';

			}

			$options[] = array(
				'label' => $item[ 'label' ],
				'value' => $item[ 'value' ],
				'selected' => $selected
			);

		}

		$data = array(
			'itens' => $options,
			'label' => $label,
			"id" => "form-". $name,
			'name' => $name
		);


		return $this->CI->parser->parse( "templates/form/form_select", $data, true );

	}


	public function select_2( $label, $name, $itens = array(), $value = 0 , $adicional_pars = array()) {

		$options = array();

		foreach( $itens as $item ) {

			if( !is_array( $item ) ) {
				$item = get_object_vars( $item );
			}

			$selected = "";

			if( $item[ 'value' ] == $value ) {

				$selected = 'selected="selected"';

			}

			$options[] = array(
				'label' => $item[ 'label' ],
				'value' => $item[ 'value' ],
				'selected' => $selected
			);

		}

		$data = array(
			'itens' => $options,
			'label' => $label,
			"id" => "form-". $name,
			'name' => $name
		);

		foreach ($adicional_pars as $key => $value) {
			if(empty($data[$key])){
				$data[$key] = $value;
			}
		}
		

		return $this->CI->parser->parse( "templates/form/form_select_2", $data, true );

	}

	// Check
	public function check( $label, $name, $itens = array(), $value = array() ) {

		$inputs = array();

		foreach( $itens as $item ) {

			$checked = "";

			if( in_array( $item[ 'value' ], $value ) ) {

				$checked = 'checked="checked"';

			}

			$inputs[] = array(
				'label' => $item[ 'label' ],
				'value' => $item[ 'value' ],
				'checked' => $checked
			);

		}

		$data = array(
			'itens' => $inputs,
			'label' => $label,
			'name' => $name
		);

		return $this->CI->parser->parse( "templates/form/input_check", $data, true );

	}

	public function text( $label, $name, $value, $width, $descricao, $required, $max_length, $disabled ) {

		$data = array(
			"width" => $width,
			"required" => $required,
			"disabled" => $disabled,
			"max_length" => $max_length,
			"descricao" => $descricao,
			"label" => $label,
			"id" => "form-". $name,
			"name" => $name,
			"value" => $value
		);

		return $this->CI->parser->parse( "templates/form/input_text", $data, true );

	}

	public function data( $label, $name, $value, $width, $descricao, $required, $max_length, $disabled ) {

		$value = ( is_numeric( $value ) ) ? date( "d/m/Y", $value ) : $value;
		$data = array(
			"width" => $width,
			"required" => $required,
			"disabled" => $disabled,
			"max_length" => $max_length,
			"descricao" => $descricao,
			"label" => $label,
			"id" => "form-". $name,
			"name" => $name,
			"value" => $value
		);

		return $this->CI->parser->parse( "templates/form/input_data", $data, true );

	}

	public function color( $label, $name, $value, $width, $descricao, $required, $max_length, $disabled ) {

		$data = array(
			"width" => $width,
			"required" => $required,
			"disabled" => $disabled,
			"max_length" => $max_length,
			"descricao" => $descricao,
			"label" => $label,
			"id" => "form-". $name,
			"name" => $name,
			"value" => $value
		);

		return $this->CI->parser->parse( "templates/form/input_color", $data, true );

	}

	public function password( $label, $name, $value, $width, $descricao, $required, $max_length, $disabled ) {

		$data = array(
			"width" => $width,
			"required" => $required,
			"disabled" => $disabled,
			"max_length" => $max_length,
			"descricao" => $descricao,
			"label" => $label,
			"id" => "form-". $name,
			"name" => $name,
			"value" => $value
		);

		return $this->CI->parser->parse( "templates/form/input_password", $data, true );

	}

	public function hidden( $label, $name, $value, $width, $descricao, $required, $disabled ) {

		$data = array(
			"width" => $width,
			"required" => $required,
			"disabled" => $disabled,
			"descricao" => $descricao,
			"label" => $label,
			"id" => "form-". $name,
			"name" => $name,
			"value" => $value
		);

		return $this->CI->parser->parse( "templates/form/input_hidden", $data, true );

	}

	public function rtext( $label, $name, $value, $width, $descricao, $required, $disabled ) {

		$data = array(
			"width" => $width,
			"required" => $required,
			"disabled" => $disabled,
			"descricao" => $descricao,
			"label" => $label,
			"id" => "form-". $name,
			"name" => $name,
			"value" => $value
		);

		return $this->CI->parser->parse( "templates/form/textarea_rtext", $data, true );

	}

	public function textarea( $label, $name, $value, $width, $descricao, $required, $max_length, $disabled ) {

		$data = array(
			"width" => $width,
			"required" => $required,
			"disabled" => $disabled,
			"max_length" => $max_length,
			"descricao" => $descricao,
			"label" => $label,
			"id" => "form-". $name,
			"name" => $name,
			"value" => $value
		);

		return $this->CI->parser->parse( "templates/form/textarea_text", $data, true );

	}

}