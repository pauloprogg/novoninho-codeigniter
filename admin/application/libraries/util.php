<?php

class Util {

	public function tabelaPerfil($perfil) {

		switch($perfil) {

			case 1: //Prorietário
				$caminho = 'dados_proprietario';
			break;

			case 2: //Corretor
				$caminho = 'dados_corretor';
			break;

			case 3: //Imobiliária
				$caminho = 'dados_imobiliaria';
			break;

		}

		return $caminho;

	}

	public function date2br($data) {

		$data = explode( "-", $data );

		return $data[ 2 ] ."/". $data[ 1 ] ."/". $data[ 0 ];

	}
	public function date2us($data) {

		$data = explode( "/", $data );

		return $data[ 2 ] ."-". $data[ 1 ] ."-". $data[ 0 ];

	}

	public function moeda2br($valor) {
		return number_format($valor, 2, ',', '.');
	}

	public function moeda2us($valor) {
		return number_format($valor, 2, '.', ',');
	}

	public function valor_parcelar($valor, $parcelas) {
		$val = $valor / $parcelas;
		return number_format($val, 2, ',', '.');
	}

	public function valor_desconto($valor, $porc) {
		$val = $valor - ($valor * ($porc / 100));
		return number_format($val, 2, ',', '.');
	}

	public function getIdVideo( $url ) {

		preg_match_all( "/(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=|\/sandalsResorts#\w\/\w\/.*\/))([^\/&]{10,12})/", $url, $matches );

		$idVideo = ( isset( $matches[1][0] ) ) ? $matches[1][0] : "";

		return $idVideo;

	}

	/* usar somente se a função url_title do helper URL não funcionar */
	public function slug( $nom_tag, $slug="-" ) {
  		$string = strtolower($nom_tag);

	    // Código ASCII das vogais
	    $ascii['a'] = range(224, 230);
	    $ascii['e'] = range(232, 235);
	    $ascii['i'] = range(236, 239);
	    $ascii['o'] = array_merge(range(242, 246), array(240, 248));
	    $ascii['u'] = range(249, 252);

	    // Código ASCII dos outros caracteres
	    $ascii['b'] = array(223);
	    $ascii['c'] = array(231);
	    $ascii['d'] = array(208);
	    $ascii['n'] = array(241);
	    $ascii['y'] = array(253, 255);

	    foreach ($ascii as $key=>$item) {
	        $acentos = '';
	        foreach ($item AS $codigo) $acentos .= chr($codigo);
	        $troca[$key] = '/['.$acentos.']/i';
	    }

	    $string = preg_replace(array_values($troca), array_keys($troca), $string);

	    // Slug?
	    if ($slug) {
	        // Troca tudo que não for letra ou número por um caractere ($slug)
	        $string = preg_replace('/[^a-z0-9]/i', $slug, $string);
	        // Tira os caracteres ($slug) repetidos
	        $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
	        $string = trim($string, $slug);
	    }
	    return $string;
	}

	public function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false){
		$lmin = 'abcdefghijklmnopqrstuvwxyz';
		$lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$num = '1234567890';
		$simb = '!@#$%*-';
		$retorno = '';
		$caracteres = '';

		$caracteres .= $lmin;
		if ($maiusculas) $caracteres .= $lmai;
		if ($numeros) $caracteres .= $num;
		if ($simbolos) $caracteres .= $simb;

		$len = strlen($caracteres);
		for ($n = 1; $n <= $tamanho; $n++) {
			$rand = mt_rand(1, $len);
			$retorno .= $caracteres[$rand-1];
		}
		return $retorno;
	}

	public function excel( $table, $destino ) {

		header("Content-type: application/msexcel");
		header("Content-Disposition: attachment; filename=". $destino );

		print $table;

	}

}

?>