<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'third_party/upload/upload.php');

class Image_form {

	public function upload_imagem( $file, $config ) {

		$imagem = $config['image_path'];
		$thumb = $config['image_path'].'thumb/';

		$cortar = (isset($config['cortar']) ? $config['cortar'] : true);
		$converter = (isset($config['converter']) ? $config['converter'] : true);
		$qualidade = (isset($config['qualidade']) ? $config['qualidade'] : 100);
		$greyscale = (isset($config['greyscale']) ? $config['greyscale'] : false);
		$redimensionar = (isset($config['redimensionar']) ? $config['redimensionar'] : true);
		$proporcional = (isset($config['proporcional']) ? $config['proporcional'] : false);
		$thumbnail = ( (isset($config['thumb']) && $config['thumb'] ) ? true : false);
		$marca = (isset($config['marca']) && $config['marca'] ? true : false);

		$handle = new Upload( $file );

		if($handle->uploaded){

			$randomName = md5(uniqid(time()));
			// sleep(1);

			$handle->file_new_name_body = $randomName;
			$handle->jpeg_quality = $qualidade;
			$handle->png_compression = 9;

			if($converter){
				$handle->image_convert = 'jpg';
			}

			if($redimensionar){
				$handle->image_resize = $redimensionar;
				$handle->image_greyscale = $greyscale;

				if($cortar){
					$handle->image_ratio_crop = true;
				} else {
					$handle->image_ratio_fill = true;
				}
				
				if($config['largura'] == '*' && $config['altura'] == '*'){
					list($largura, $altura) = getimagesize($file['tmp_name']);

					$handle->image_x = $largura;
					$handle->image_y = $altura;
				} else {
					if($config['largura'] == '*'){
						$handle->image_ratio_x = true;
					} elseif(is_numeric($config['largura'])){
						$handle->image_x = $config['largura'];
					}

					if($config['altura'] == '*'){
						$handle->image_ratio_y = true;
					} elseif(is_numeric($config['altura'])){
						$handle->image_y = $config['altura'];
					}
				}

			}

			if($proporcional){
				list($width, $height) = getimagesize($file['tmp_name']);

				if($width > $height){
					if($width > $config['largura']){
						$handle->image_x = $config['largura'];
						$handle->image_resize = true;
						$handle->image_ratio_y = true;
						$handle->image_ratio_fill = true;
					}
				} else {
					if($height > $config['altura']){
						$handle->image_y = $config['altura'];
						$handle->image_resize = true;
						$handle->image_ratio_x = true;
						$handle->image_ratio_fill = true;
					}
				}
			}

			if($marca){
				$handle->image_watermark =  $config['marca']['src'];
				$handle->image_watermark_x = $config['marca']['x'];
				$handle->image_watermark_y = $config['marca']['y'];
			}

			$handle->Process($imagem);

			if($thumbnail){
				$greyscale = (isset($config['thumb']['greyscale']) ? $config['thumb']['greyscale'] : false);

				$handle->file_new_name_body = $randomName;
				$handle->jpeg_quality = $qualidade;
				$handle->png_compression = 9;
				$handle->image_greyscale = $greyscale;
				$handle->image_resize = true;
				$handle->image_ratio_crop = true;

				if($converter){
					$handle->image_convert = 'jpg';
				}

				if($config['thumb']['largura'] == '*'){
					$handle->image_ratio_x = true;
				} elseif(is_numeric($config['thumb']['largura'])){
					$handle->image_x = $config['thumb']['largura'];
				}

				if($config['thumb']['altura'] == '*'){
					$handle->image_ratio_y = true;
				} elseif(is_numeric($config['thumb']['altura'])){
					$handle->image_y = $config['thumb']['altura'];
				}

				$handle->Process($thumb);
			}

			return $handle->file_dst_name;

		}
				
	}

}