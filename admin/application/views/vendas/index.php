<!-- Conteudos especificos separados por sessoes -->
{msg_sucesso}
{msg_erro}

<!-- Sessão 1 -->
<section class="row show-for-small-only">
	<div class="small-12 medium-12 large-12 text-center">
		<ul id="actions" class="button-group">
		  <li><a href="{base_url}{pagina}/add/" class="button tiny">Adicionar <br/> Postagem</a></li>
		  <li><a href="{base_url}comentarios/{pagina}" class="button tiny">Gerenciar <br/> Comentários</a></li>
		  <li><a href="{base_url}categorias/{pagina}" class="button tiny">Gerenciar <br/> Categorias</a></li>
		  <li><a href="{base_url}tags/{pagina}" class="button tiny">Gerenciar <br/> Tags</a></li>
		</ul>
	</div>
</section>

<!-- Sessão 2 -->
<section class="row">
	<div class="columns medium-12 large-6">
		<h1>Gerenciar Vendas</h1>
	</div>
	
	<div class="columns medium-12 large-6" style="text-align: right;margin-bottom: 20px;">
		{botao_exportar}
	</div>
	
	<form action="{base_url}{pagina}" method="get">
	<div class="columns medium-3 large-3">
		<select name="ordem" id="slcOrdenacao" />
			<option value="">Ordenação</option>
			<option value="data_desc">Mais Recentes</option>
			<option value="data_asc">Mais Antigas</option>
		</select>
	</div>

	<div class="columns medium-3 large-3">
		<select name="vendedores" id="slcOrdenacao" />
			<option value="">Vendedor</option>
			{vendedores}
				<option value="{id_vendedor}">{nome_vendedor}</option>
			{/vendedores}
		</select>
	</div>

	<div class="columns medium-6 large-6">
		<div class="row collapse">
			<div class="columns large-12"><input type="text" id="iptBusca" name="busca" placeholder="Procure pelo nome do usuário" /></div>
		</div>
	</div>
	
	<div class="columns medium-5 large-5">
		<div class="row">
			<div class="columns large-6"><input type="text" name="data_inicio" placeholder="Data Inicial" class="datepicker"/></div>
			<div class="columns large-6"><input type="text" name="data_fim" placeholder="Data final" class="datepicker"/></div>
		</div>
	</div>
	
	<div class="columns medium-3 large-3">
		<select name="status" id="slcOrdenacao" />
			<option value="">Status</option>
			<option value="aguardando_pagamento">Aguardando Pagamento</option>
			<option value="pago">Pago</option>
			<option value="cancelado">Cancelado</option>
		</select>
	</div>
	
	<div class="columns medium-2 large-2">
		<select name="tipo"/>
			<option value="">Tipo</option>
			<option value="0">Vendas pelo Site</option>
			<option value="1">Vendas pelo Admin</option>
		</select>
	</div>
	
	<div class="columns medium-2 large-2">
		<select name="tipo_usuario"/>
			<option value="1">Proprietário</option>
			<option value="2">Corretor</option>
		</select>
	</div>
	
	<div class="columns medium-2 large-2 right text-right">
		<div class="row">
			<input type="submit" value="buscar" class="button tiny" />
		</div>
	</div>
	
	</form>
</section>

<!-- Sessão 3 -->
<section>
	{lista}

	<div class="row">
		{paginacao}
	</div>
</section>
<!-- Fim dos conteudos especificos -->