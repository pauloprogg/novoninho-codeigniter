{msg_erro}

{msg_aviso}

<!-- Conteudos especificos separados por sessoes -->



<!-- Sessão 1 -->

<section class="row content-form">



	<!-- Coluna Esquerda -->

	<div class="columns medium-8 large-8">



		<!-- Form Campos Esquerdo -->

		<form action="{base_url}{pagina}/update/{id}" method="post" enctype="multipart/form-data" class="row form-esquerdo">
		
			<div class="row">

				<div class="columns small-12 medium-12 large-12">
	
					<h1>Editar Anúncio</h1>
	
				</div>
	
				<div class="columns small-12 medium-12 large-12 text-left">
	
					<label for="form-vendedor"> <strong>Vendedor Vinculado a este Pagamento</strong><small> </small>
	
						{lista_vendedor}
	
					</label>
	
				</div>
	
				{form_esquerdo}
	
				{campo}
	
				{/form_esquerdo}
	
				
	
				<div class="columns small-12 medium-12 large-12">
	
				
	
					<h1 style="margin: 10px 10px 20px;">Anúncios da Venda</h1>
	
					
	
				</div>
			
			</div>

			{lista_anuncios}
			
			<div class="item_anuncio row" style="margin-bottom: 20px; padding: 20px 0px; background-color: rgb(234, 232, 232);">

				<div class="small-12 medium-2 columns">
				
					<label for="form-numero"> <strong>Número</strong><small> </small>

						<input type="text" id="form-id" name="id" value="{id_imovel}" class="input-text required" disabled="disabled">
					
					</label>

				</div>
		
				<div class="small-12 medium-10 columns">
				
					<label for="form-titulo"> <strong>Título</strong><small> </small>

						<input type="text" id="form-titulo" name="titulo" value="{titulo}" class="input-text required" disabled="disabled">
					
					</label>
						
				</div>

				<div class="small-12 medium-2 columns">
				
					<label for="form-valor"> <strong>Valor</strong><small> </small>

						<input type="text" id="form-valor" name="valor" value="{preco}" class="input-text required" disabled="disabled">
					
					</label>

				</div>

				<div class="small-12 medium-3 columns">
				
					<label for="form-destaque"> <strong>Destaque</strong><small> </small>

						<input type="text" id="form-destaque" name="destaque" value="{destaque}" class="input-text required" disabled="disabled">
						
					</label>

				</div>
				
				<div class="small-12 medium-2 columns">
				
					<label for="form-estado"> <strong>Estado</strong><small> </small>

						<input type="text" id="form-estado" name="estado" value="{estado}" class="input-text required" disabled="disabled">
						
					</label>

				</div>
				
				<div class="small-12 medium-5 columns">
				
					<label for="form-cidade"> <strong>Cidade</strong><small> </small>

						<input type="text" id="form-cidade" name="cidade" value="{cidade}" class="input-text required" disabled="disabled">
						
					</label>

				</div>

			</div>

			{/lista_anuncios}

			

		</form>

		<!-- FIM Form Campos Esquerdo -->



	</div>

	<!-- FIM Colona Esquerda -->



	<div class="columns medium-4 large-4">  <!-- Coluna Direita -->





		<!-- Form Campos Esquerdo -->

		<form action="{base_url}{pagina}/update/{id}" method="post" enctype="multipart/form-data" class="form-direito">

			<!-- Painel 1 -->

			<div class="panel">

				<h1>Salvar</h1>



				<!--

				<label for="iptSenha"> <strong>Inserir senha</strong>

					<input type="text" id="iptSenha" name="iptSenha" />

				</label>



				<label for="chkComentarios"><input type="checkbox" id="chkComentarios" name="chkComentarios" />Permitir Comentários</label>

				-->



				<div class="columns small-6 medium-12 large-6 text-right">

					<a href="{base_url}{pagina}" class="button tiny">Voltar</a>

				</div>



				<div class="columns small-6 medium-12 large-6 text-right">

					<button type="button" class="button tiny submit">Salvar</button>

				</div>



				<div class="clearfix"></div>

			</div>

			<!-- FIM Painel 1 -->

			

			<!-- Checks -->

		

			<!-- FIM Checks -->



		</form>

		<!-- FIM Form Campos Esquerdo -->



	</div>

	<!-- FIM Coluna Direita -->



</section>

<!-- FIM dos conteudos especificos -->

