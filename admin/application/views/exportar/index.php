<!-- Conteudos especificos separados por sessoes -->
{msg_sucesso}
{msg_erro}

<!-- Sessão 1 -->
<section class="row show-for-small-only">
	<div class="small-12 medium-12 large-12 text-center">
		<ul id="actions" class="button-group">
		  <li><a href="{base_url}{pagina}/add/" class="button tiny">Adicionar <br/> Postagem</a></li>
		  <li><a href="{base_url}comentarios/{pagina}" class="button tiny">Gerenciar <br/> Comentários</a></li>
		  <li><a href="{base_url}categorias/{pagina}" class="button tiny">Gerenciar <br/> Categorias</a></li>
		  <li><a href="{base_url}tags/{pagina}" class="button tiny">Gerenciar <br/> Tags</a></li>
		</ul>
	</div>
</section>

<!-- Sessão 2 -->
<section class="row">
	<div class="columns medium-12 large-12">
		<h1>Exportação de Anúncios</h1>
	</div>

<!--
	<div class="columns medium-12 large-12" style="text-align: right;margin-bottom: 20px;">
		<a href="{base_url}{pagina}/download{val_get}" class="button tiny"/>Exportar XLS</a>
	</div>
-->

	<form action="{base_url}{pagina}" method="get">

	<div class="row">

		<div class="columns medium-3 large-2">
			<select name="ordem" id="slcOrdenacao" />
				<option value="">Ordenação</option>
				<option value="data_desc" <?php if($this->input->get( "ordem" ) == 'data_desc') echo 'selected="selected"'; ?> >Mais Recentes</option>
				<option value="data_asc" <?php if($this->input->get( "ordem" ) == 'data_asc') echo 'selected="selected"'; ?> >Mais Antigas</option>
				<option value="titulo_asc" <?php if($this->input->get( "ordem" ) == 'titulo_asc') echo 'selected="selected"'; ?> >Titulo A-Z</option>
				<option value="titulo_desc" <?php if($this->input->get( "ordem" ) == 'titulo_desc') echo 'selected="selected"'; ?> >Titulo Z-A</option>
			</select>
		</div>

		<div class="columns medium-3 large-2">
			<select name="filtro" id="slcOrdenacao" />
				<option value="">Filtro</option>
				<option value="ativos" <?php if($this->input->get( "filtro" ) == 'ativos') echo 'selected="selected"'; ?>>Ativos</option>
				<option value="inativos" <?php if($this->input->get( "filtro" ) == 'inativos') echo 'selected="selected"'; ?>>Inativos</option>
				<option value="aprovado" <?php if($this->input->get( "filtro" ) == 'aprovado') echo 'selected="selected"'; ?>>Aprovado</option>
				<option value="nao_aprovado" <?php if($this->input->get( "filtro" ) == 'nao_aprovado') echo 'selected="selected"'; ?>>Não Aprovado</option>
			</select>
		</div>
		
		<div class="columns medium-3 large-2">
			<select name="status" id="slcOrdenacao" />
				<option value="">Status</option>
				<option value="aguardando_pagamento" <?php if($this->input->get( "status" ) == 'aguardando_pagamento') echo 'selected="selected"'; ?> >Aguardando Pagamento</option>
				<option value="aguardando_aprovacao" <?php if($this->input->get( "status" ) == 'aguardando_aprovacao') echo 'selected="selected"'; ?> >Aguardando Aprovação de Pagamento</option>
				<option value="publicado" <?php if($this->input->get( "status" ) == 'publicado') echo 'selected="selected"'; ?> >Pago</option>
				<option value="cancelado" <?php if($this->input->get( "status" ) == 'cancelado') echo 'selected="selected"'; ?> >Cancelado</option>
			</select>
		</div>
		
		<div class="columns medium-12 large-6">
			<div class="columns large-12"><input type="text" id="iptBusca" name="busca" placeholder="Procure pelo usuário ou anuncio" value="<?php echo $this->input->get( "busca" ); ?>" /></div>
		</div>

		<div class="columns medium-2 large-2">
			<select name="destaque"/>
				<option value="">Filtro Destaque</option>
				<option value="1" <?php if($this->input->get( "destaque" ) == '1') echo 'selected="selected"'; ?> >Imóveis Destaque</option>
				<option value="0" <?php if($this->input->get( "destaque" ) == '0') echo 'selected="selected"'; ?> >Não Destaque</option>
			</select>
		</div>
		
		<div class="columns medium-2 large-2">
			<select name="expirado"/>
				<option value="">Filtro Expirado</option>
				<option value="expirado" <?php if($this->input->get( "expirado" ) == 'expirado') echo 'selected="selected"'; ?> >Expirados</option>
				<option value="nao_expirado" <?php if($this->input->get( "expirado" ) == 'nao_expirado') echo 'selected="selected"'; ?> >Não Expirados</option>
			</select>
		</div>
		
		<div class="columns medium-4 large-4">
			<div class="row">
				<div class="columns large-6"><input type="text" name="data_inicio_cadastro" placeholder="Data Inicial Cadastro" class="datepicker" value="<?php echo $this->input->get( "data_inicio_cadastro" ); ?>" /></div>
				<div class="columns large-6"><input type="text" name="data_fim_cadastro" placeholder="Data final Cadastro" class="datepicker" value="<?php echo $this->input->get( "data_fim_cadastro" ); ?>" /></div>
			</div>
		</div>
		
		<div class="columns medium-4 large-4">
			<div class="row">
				<div class="columns large-6"><input type="text" name="data_inicio_expira" placeholder="Data Inicial Expiração" class="datepicker" value="<?php echo $this->input->get( "data_inicio_expira" ); ?>" /></div>
				<div class="columns large-6"><input type="text" name="data_fim_expira" placeholder="Data final Expiração" class="datepicker" value="<?php echo $this->input->get( "data_fim_expira" ); ?>" /></div>
			</div>
		</div>

		<div class="columns medium-3 large-3">
			<select name="finalidade"/>
				<option value="">Finalidade do Imóvel</option>
				{select_finalidade}
			</select>
		</div>

		<div class="columns medium-3 large-3">
			<div class="columns large-12"><input type="text" id="numero_id" name="numero_id" placeholder="Procure pelo número de referencia" value="<?php echo $this->input->get( "numero_id" ); ?>" /></div>
		</div>

		<div class="columns medium-3 large-3">
			<select name="tipo_anuncio"/>
				<option value="">Tipo de Anúncio</option>
				<option value="1" <?php if($this->input->get( "tipo_anuncio" ) == '1') echo 'selected="selected"'; ?> >Venda</option>
				<option value="2" <?php if($this->input->get( "tipo_anuncio" ) == '2') echo 'selected="selected"'; ?> >Locação</option>
				<option value="3" <?php if($this->input->get( "tipo_anuncio" ) == '3') echo 'selected="selected"'; ?> >Temporada</option>
			</select>
		</div>

		<div class="columns medium-3 large-3">
			<select name="tipo_imovel"/>
				<option value="">Tipo de Imovel</option>
				{select_tipo_imoveis}
			</select>
		</div>

		<div class="columns medium-2 large-2">
			<select name="dormitorios"/>
				<option value="">Dormitórios</option>
				<option value="1" <?php if($this->input->get( "dormitorios" ) == '1') echo 'selected="selected"'; ?> >1</option>
				<option value="2" <?php if($this->input->get( "dormitorios" ) == '2') echo 'selected="selected"'; ?> >2</option>
				<option value="3" <?php if($this->input->get( "dormitorios" ) == '3') echo 'selected="selected"'; ?> >3</option>
				<option value="4" <?php if($this->input->get( "dormitorios" ) == '4') echo 'selected="selected"'; ?> >4</option>
				<option value="5" <?php if($this->input->get( "dormitorios" ) == '5') echo 'selected="selected"'; ?> >5</option>
			</select>
		</div>

		<div class="columns medium-2 large-2">
			<select name="suites"/>
				<option value="">Suites</option>
				<option value="1" <?php if($this->input->get( "suites" ) == '1') echo 'selected="selected"'; ?> >1</option>
				<option value="2" <?php if($this->input->get( "suites" ) == '2') echo 'selected="selected"'; ?> >2</option>
				<option value="3" <?php if($this->input->get( "suites" ) == '3') echo 'selected="selected"'; ?> >3</option>
				<option value="4" <?php if($this->input->get( "suites" ) == '4') echo 'selected="selected"'; ?> >4</option>
				<option value="5" <?php if($this->input->get( "suites" ) == '5') echo 'selected="selected"'; ?> >5</option>
			</select>
		</div>

		<div class="columns medium-2 large-2">
			<select name="garagem"/>
				<option value="">Garagem</option>
				<option value="1" <?php if($this->input->get( "garagem" ) == '1') echo 'selected="selected"'; ?> >1</option>
				<option value="2" <?php if($this->input->get( "garagem" ) == '2') echo 'selected="selected"'; ?> >2</option>
				<option value="3" <?php if($this->input->get( "garagem" ) == '3') echo 'selected="selected"'; ?> >3</option>
				<option value="4" <?php if($this->input->get( "garagem" ) == '4') echo 'selected="selected"'; ?> >4</option>
				<option value="5" <?php if($this->input->get( "garagem" ) == '5') echo 'selected="selected"'; ?> >5</option>
			</select>
		</div>

		<div class="columns medium-2 large-2 left">
			<select name="banheiro"/>
				<option value="">Banheiro</option>
				<option value="1" <?php if($this->input->get( "banheiro" ) == '1') echo 'selected="selected"'; ?> >1</option>
				<option value="2" <?php if($this->input->get( "banheiro" ) == '2') echo 'selected="selected"'; ?> >2</option>
				<option value="3" <?php if($this->input->get( "banheiro" ) == '3') echo 'selected="selected"'; ?> >3</option>
				<option value="4" <?php if($this->input->get( "banheiro" ) == '4') echo 'selected="selected"'; ?> >4</option>
				<option value="5" <?php if($this->input->get( "banheiro" ) == '5') echo 'selected="selected"'; ?> >5</option>
			</select>
		</div>

	</div>

	<div class="row">

		<div class="columns medium-3 large-3 left">
			<select name="tipo_usuario" class="select-tipo-usuario" />
				<option value="">Tipo de Usuários</option>
				<option value="1" <?php if($this->input->get( "tipo_usuario" ) == '1') echo 'selected="selected"'; ?> >Proprietário</option>
				<option value="2" <?php if($this->input->get( "tipo_usuario" ) == '2') echo 'selected="selected"'; ?> >Corretor</option>
				<option value="3" <?php if($this->input->get( "tipo_usuario" ) == '3') echo 'selected="selected"'; ?> >Imobiliária</option>
			</select>
		</div>

		<div class="columns medium-3 large-3 left">
			<select class="select-imobiliaria" name="imobiliaria" <?php if($this->input->get( "tipo_usuario" ) != '3') echo 'style="display: none"'; ?> >
				{select_imobiliarias}
			</select>
		</div>

	</div>

	<div class="row">

		<div class="columns medium-3 large-3">
			<select class="select-estados" name="estado[]" multiple="multiple">
				{select_estados}
			</select>
		</div>

		<div class="columns medium-3 large-3 left">
			<select class="select-cidades" name="cidade[]" multiple="multiple">
				{select_cidades}
			</select>
		</div>

	</div>

	<div class="row">

		<div class="columns medium-12 large-12 text-right">
			<div class="row">
				<input type="submit" value="buscar" class="button tiny" />
			</div>
		</div>

	</div>
	
	</form>
</section>

<section class="row">
	<div class="columns medium-12 text-right">
		<a href="javascript:void(0);" data-href="{base_url}{pagina}/linha?ids=" data-ids="" class="button tiny btn-anuncio-linha" data-imo="">Exportar Anúncios de Linha</a>
		<a href="javascript:void(0);" data-href="{base_url}{pagina}/export_destaque?ids=" data-ids="" class="button tiny btn-anuncio-destaque">Exportar Destaques</a>
		<?php /* <a href="javascript:void(0);" data-href="http://www.novoninho.com.br/destaques?ids=" data-ids="" target="_blank" class="button tiny btn-anuncio-destaque">Exportar Destaques</a> */ ?>
	</div>
</section>

<!-- Sessão 3 -->
<section>
	{lista}

	<div class="row">
		{paginacao}
	</div>
</section>
<!-- Fim dos conteudos especificos -->