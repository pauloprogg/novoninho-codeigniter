{msg_erro}
{msg_aviso}
<!-- Conteudos especificos separados por sessoes -->

<!-- Sessão 1 -->
<section class="row content-form">

	<!-- Coluna Esquerda -->
	<div class="columns medium-8 large-8">

		<!-- Form Campos Esquerdo -->
		<form action="{base_url}{pagina}/update/{id}" method="post" enctype="multipart/form-data" class="row form-esquerdo">
			<div class="columns small-12 medium-12 large-12">
				<h1>Editar Dados da Página "Imobiliárias"</h1>
			</div>

			{form_esquerdo}
			{campo}
			{/form_esquerdo}

		</form>
		<!-- FIM Form Campos Esquerdo -->

	</div>
	<!-- FIM Colona Esquerda -->

	<div class="columns medium-4 large-4">  <!-- Coluna Direita -->


		<!-- Form Campos Esquerdo -->
		<form action="{base_url}{pagina}/update/{id}" method="post" enctype="multipart/form-data" class="form-direito">
			<!-- Painel 1 -->
			<div class="panel">
				<h1>Salvar</h1>
				<div class="columns small-6 medium-12 large-6 text-right">
					<button type="button" class="button tiny submit">Salvar</button>
				</div>

				<div class="clearfix"></div>
			</div>
			<!-- FIM Painel 1 -->
			
			<!-- Checks -->
		
			<!-- FIM Checks -->

		</form>
		<!-- FIM Form Campos Esquerdo -->

	</div>
	<!-- FIM Coluna Direita -->

</section>
<!-- FIM dos conteudos especificos -->
