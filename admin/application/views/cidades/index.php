<!-- Conteudos especificos separados por sessoes -->
{msg_sucesso}
{msg_erro}

<!-- Sessão 1 -->
<section class="row show-for-small-only">
	<div class="small-12 medium-12 large-12 text-center">
		<ul id="actions" class="button-group">
		  <li><a href="{base_url}{pagina}/add/" class="button tiny">Adicionar <br/> Postagem</a></li>
		  <li><a href="{base_url}comentarios/{pagina}" class="button tiny">Gerenciar <br/> Comentários</a></li>
		  <li><a href="{base_url}categorias/{pagina}" class="button tiny">Gerenciar <br/> Categorias</a></li>
		  <li><a href="{base_url}tags/{pagina}" class="button tiny">Gerenciar <br/> Tags</a></li>
		</ul>
	</div>
</section>

<!-- Sessão 2 -->
<section class="row">
	<div class="columns medium-12 large-12">
		<h1>Gerenciar Cidades</h1>
	</div>

	
	<form action="{base_url}{pagina}" method="get">
	<div class="columns medium-3 large-3">
		<select name="ordem" id="slcOrdenacao" />
			<option value="">Ordenação</option>
			<option value="nome_asc">Nome A-Z</option>
			<option value="nome_desc">Nome Z-A</option>
		</select>
	</div>
	
	<div class="columns medium-3 large-3">
		<select name="estado" id="slcOrdenacao" />
			<option value="">Estado</option>
			{select_estado}
		</select>
	</div>

	<div class="columns medium-6 large-6">
		<div class="row collapse">
			<div class="columns large-10"><input type="text" id="iptBusca" name="busca" placeholder="Procure pelo nome" /></div>
			<div class="columns large-2"><input type="submit" value="buscar" class="button tiny" /></div>
		</div>
	</div>
	</form>
</section>

<!-- Sessão 3 -->
<section>
	{lista}
	<div class="row table">
		<div class="columns small-12 medium-5 large-5"> <p><strong>Cidade:</strong> {cidade_descricao}</p> </div>
		<div class="columns small-12 medium-2 large-2"> <p><strong>UF:</strong> {estado}</p> </div>
		<div class="columns small-12 medium-3 large-3"> <p><strong>CEP:</strong> {cidade_cep}</p> </div>
		<div class="columns small-12 medium-2 large-2">
			<div class="columns small-6 medium-6 large-6 text-left"><a href="{base_url}{pagina}/read/{cidade_codigo}">Editar</a></div>
		</div>

		<div class="clearfix"></div>
	</div>

	<div class="row"><hr></div>
	{/lista}

	<div class="row">
		{paginacao}
	</div>
</section>
<!-- Fim dos conteudos especificos -->