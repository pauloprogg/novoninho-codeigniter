<!-- Conteudos especificos separados por sessoes -->
{msg_sucesso}
{msg_erro}

<!-- Sessão 1 -->
<section class="row">
	<div class="small-12 medium-12 large-12">
		<ul id="actions" class="button-group">
		  <li><a href="{base_url}{pagina}/add/" class="button tiny">Adicionar <br/> Resposta</a></li>
		</ul>
	</div>
</section>

<!-- Sessão 2 -->
<section class="row">
	<div class="columns medium-12 large-12">
		<h1>Gerenciar respostas Como Soube </h1>
	</div>
</section>

<!-- Sessão 3 -->
<section>
	{lista}
	<div class="row table">
		<div class="columns small-12 medium-1 large-1"> <p><strong>ID:</strong> {id}</p> </div>
		<div class="columns small-12 medium-9 large-9"> <p><strong>Resposta:</strong> {texto}</p> </div>
		<div class="columns small-12 medium-2 large-2">
			<div class="columns small-6 medium-6 large-6 text-left"><a href="{base_url}{pagina}/read/{id}">Editar</a></div>
			<div class="columns small-6 medium-6 large-6 text-right"><a href="{base_url}{pagina}/delete/{id}" class="ac red">Deletar</a></div>
		</div>

		<div class="clearfix"></div>
	</div>

	<div class="row"><hr></div>
	{/lista}

	<div class="row">
		{paginacao}
	</div>
</section>
<!-- Fim dos conteudos especificos -->