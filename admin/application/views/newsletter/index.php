<!-- Conteudos especificos separados por sessoes -->
{msg_sucesso}
{msg_erro}

<!-- Sessão 2 -->
<section class="row">
	<div class="columns medium-12 large-12">
		<h1>Cadastros Newsletter</h1>
	</div>
	<div class="columns medium-12 large-12">
		<div class="button tiny" /><a href="{planilha}" style="color:#fff;">Baixar XLS</a></div>
	</div>

	
</section>

<!-- Sessão 3 -->
<section>
	{lista}
	<div class="row table">
		<div class="columns small-12 medium-1 large-1"> <p><strong>ID:</strong> {id}</p> </div>
		<div class="columns small-12 medium-5 large-9"> <p><strong>Email:</strong> {login}</p> </div>
		<div class="columns small-12 medium-2 large-2">
		</div>

		<div class="clearfix"></div>
	</div>

	<div class="row"><hr></div>
	{/lista}

	<div class="row">
		{paginacao}
	</div>
</section>
<!-- Fim dos conteudos especificos -->