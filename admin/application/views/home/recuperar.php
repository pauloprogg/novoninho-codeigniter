<!-- Conteudos especificos separados por sessoes -->

<!-- Sessão 1 -->
<section class="row loginarea">
	<div class="small-12 medium-12 large-12 columns text-center">
		<a href="{base_url}login"><img src="{base_url}assets/images/logo-fazul.png" alt="Fazul Comunicação | Login" title="Fazul Comunicação | Login" /></a>
	</div>

	<div class="small-12 medium-6 large-4 large-offset-4 medium-offset-3 columns left">
		<div class="panel">
			<form id="formRecuperar" name="formRecuperar" action="{base_url}login/recuperar" method="post">

				<label for="login">Nome de usuário ou email:
					<br/>
					<label for="login" class="error"></label>
					<input type="text" id="login" name="login" required />
				</label>

				<label for="login">
					<span class="erro">{msg_erro}</span>
					<span class="aviso">{msg_aviso}</span>
					<span class="sucesso">{msg_sucesso}</span>
				</label>

				<div class="large-12 text-right">
					<button type="submit" class="tiny">recuperar senha</button>
				</div>
			</form>

		</div>
	</div>
</section>

<!-- Fim dos conteudos especificos -->