<!-- Conteudos especificos separados por sessoes -->

<!-- Sessão 1 -->
<section class="row loginarea">
	<div class="small-12 medium-12 large-12 columns text-center">
		<a href="{base_url}login"><img src="{base_url}assets/images/logo-fazul.png" alt="Fazul Comunicação | Login" title="Fazul Comunicação | Login" /></a>
	</div>

	<div class="small-12 medium-6 large-4 large-offset-4 medium-offset-3 columns left">
		<div class="panel">
			<div class="large-12 text-center">
				<p>Uma nova senha foi enviada para:</p>
				<p class="mail">{email}</p>
			</div>

		</div>
	</div>
</section>

<!-- Fim dos conteudos especificos -->