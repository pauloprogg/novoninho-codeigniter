<!-- Conteudos especificos separados por sessoes -->
{msg_sucesso}
{msg_erro}

<!-- Sessão 1 -->


<!-- Sessão 2 -->
<section class="row">
	<div class="columns medium-12 large-12">
		<h1>Listagem de Contatos</h1>
	</div>

	
	<form action="{base_url}{pagina}" method="get">
	<div class="columns medium-3 large-3">
		<select name="ordem" id="slcOrdenacao" />
			<option value="">Ordenação</option>
			<option value="data_desc">Mais Recentes</option>
			<option value="data_asc">Mais Antigas</option>
			<option value="assunto_asc">Assunto A-Z</option>
			<option value="assunto_desc">Assunto Z-A</option>
			<option value="nome_asc">Nome A-Z</option>
			<option value="nome_desc">Nome Z-A</option>
		</select>
	</div>


	<div class="columns medium-9 large-9">
		<div class="row collapse">
			<div class="columns large-10"><input type="text" id="iptBusca" name="busca" placeholder="Procure pelo título" /></div>
			<div class="columns large-2"><input type="submit" value="buscar" class="button tiny" /></div>
		</div>
	</div>
	</form>
</section>

<!-- Sessão 3 -->
<section>
	{lista}
	<div class="row table">
		<div class="columns small-12 medium-1 large-1"> <p><strong>ID:</strong> {id}</p> </div>
		<div class="columns small-12 medium-3 large-3"> <p><strong>Nome:</strong> {nome}</p> </div>
		<div class="columns small-12 medium-6 large-6"> <p><strong>Assunto:</strong> {assunto}</p> </div>
		<div class="columns small-12 medium-2 large-2">
			<div class="columns small-6 medium-6 large-6 text-left"><a href="{base_url}{pagina}/read/{id}">Visualizar</a></div>
			<div class="columns small-6 medium-6 large-6 text-right"><a href="{base_url}{pagina}/delete/{id}" class="ac red">Deletar</a></div>
		</div>

		<div class="clearfix"></div>
	</div>

	<div class="row"><hr></div>
	{/lista}

	<div class="row">
		{paginacao}
	</div>
</section>
<!-- Fim dos conteudos especificos -->