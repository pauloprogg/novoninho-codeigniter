<!-- Upload Imagem -->
<div class="panel">
	<form action="{base_url}{pagina}/image/{name}" method="post" class="form-arquivo image" enctype="multipart/form-data">
		<h1>{label}</h1>

		<p>
			Selecione uma imagem
			<br/>
			<small>Tamanho recomendado de {width}px X {height}px</small>
		</p>
		<br/>

		<input type="file" name="{name}" class="input-file"/>	

		<div class="progress round" style="display:none;">
			<span class="meter" style="width:1%;"></span>
		</div>

		<span class="content-retorno">{value}</span>

		<div class="clearfix"></div>

	</form>
</div>
<!-- FIM Upload Imagem -->