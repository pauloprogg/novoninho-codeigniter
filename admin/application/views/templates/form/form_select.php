<div class="panel">
	<h1>{label}</h1>

	<select name="{name}" id="{id}" class="required"/>
		<option value="">{label}</option>
		{itens}
		<option value="{value}" {selected}>{label}</option>
		{/itens}
	</select>

	<div class="clearfix"></div>
</div>