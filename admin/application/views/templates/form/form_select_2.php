
<div class="columns small-12 medium-12 large-{width} text-left">

	<label for="{id}"> <strong>{label}</strong><small></small>

		<select id="{id}" name="{name}" class="required"/>
			{itens}
			<option value="{value}" {selected}>{label}</option>
			{/itens}
		</select>

	</label>

</div>