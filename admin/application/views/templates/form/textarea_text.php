<div class="columns small-12 medium-12 large-{width} text-left">

	<label for="{id}"> <strong>{label}</strong><small> {descricao}</small>

	<textarea name="{name}" id="{id}" cols="30" rows="10" class="noeditor {required}" {max_length} {disabled}>{value}</textarea>

	</label>

</div>