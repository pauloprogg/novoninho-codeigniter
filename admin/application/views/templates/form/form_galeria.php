<div class="small-12 medium-12 large-12 text-left">
	<form action="{base_url}{pagina}/galeria" method="post" class="form-arquivo galeria" enctype="multipart/form-data">

		<input type="hidden" name="galeria" value="{galeria}"/>
		<input type="hidden" name="elemento" value="{elemento}"/>

		<label for="iptUpload"> <strong>Galeria de imagens</strong>
			<p>Tamanho recomendado {width}px X {height}px</p>
			<input type="file" name="arquivo" class="input-file"/>
		</label>

		<div class="progress round" style="display:none;">
			<span class="meter" style="width:1%;"></span>
		</div>
		
		<ul id="galeria" class="small-block-grid-2 medium-block-grid-4 large-block-grid-4 content-retorno">
			{imagens}
			<li>
				<a href="javascript:void(0);" class="deletar" data-id="{id}" data-pagina="{pagina}">X</a>
				<a href="{path_galeria}{arquivo}" target="_blank">
					<img src="{path_galeria}thumb/{arquivo}" alt="{legenda}"/>
				</a>
				<input type="text" placeholder="insira uma legenda" value="{legenda}" data-id="{id}" data-pagina="{pagina}" class="legenda"/>
			</li>
			{/imagens}
		</ul>

	</form>
</div>