<div class="panel">
	<h1>Categorias</h1>

	{categorias}
	<label for="chkCategoria_{id}">
		<input type="checkbox" id="chkCategoria_{id}" name="categorias[]" {check} value="{id}"/>
		{nome}
	</label>

	<div class="subcategoria">
		{cat_filho}
		<label for="chkSubCategoria_{id}">
			<input type="checkbox" id="chkSubCategoria_{id}" name="categorias[]" {check_filho} value="{id}"/>
			{nome}
		</label>
		{/cat_filho}
	</div>
	{/categorias}

	<div class="clearfix"></div>
</div>