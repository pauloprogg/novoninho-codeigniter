<div class="panel">
	<h1>{label}</h1>

	{itens}
	<label for="chk_{value}">
		<input type="checkbox" id="chk_{value}" name="{name}[]" {checked} value="{value}"/>
		{label}
	</label>
	{/itens}

	<div class="clearfix"></div>
</div>