<div class="columns small-12 medium-12 large-{width} text-left">

	<label for="{id}"> <strong>{label}</strong><small> {descricao}</small>

		<input type="text" id="{id}" name="{name}" value="{value}" class="input-text datepicker {required}" {max_length} {disabled}/>

	</label>

</div>