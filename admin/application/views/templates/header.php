<body>



	<!-- Conteiner Geral -->

	<div class="wrap">

	{header}

	{dados_usuario}

		<!-- Nav -->

			<nav>

				<div class="row">

					<div class="columns large-12">

						<ul class="main">

							<!-- <li><a href="{base_url}">Dashboard</a></li> -->

							<li class="hasmenu"><a href="javascript:void(0)">Usuários</a>

								<ul class="submenu">

									<li><a href="{base_url}proprietarios/">Gerenciar Proprietários</a></li>

									<li><a href="{base_url}corretores/">Gerenciar Corretores</a></li>

									<li><a href="{base_url}imobiliaria/">Gerenciar Imob. / Empreendimentos</a></li>

								</ul>

							</li>

							<li class="hasmenu"><a href="javascript:void(0)">Imóveis</a>

								<ul class="submenu">

									<li><a href="{base_url}aprovarimoveis/">Aprovar Imóveis</a></li>

                                    

                                    {header_master}

								</ul>

							</li>

							<li class="hasmenu"><a href="javascript:void(0)">Noticias</a>

								<ul class="submenu">

									<li><a href="{base_url}noticias/add/">Adicionar Noticia</a></li>

									<li><a href="{base_url}noticias/">Listar Noticias</a></li>

								</ul>

							</li>

							<li class="hasmenu"><a href="javascript:void(0)">Banners</a>

								<ul class="submenu">

									<li><a href="{base_url}banner-topo/add/">Adicionar Banner Topo</a></li>

									<li><a href="{base_url}banner-empreendimento/add/">Adicionar Banner Emp.</a></li>

									<li><a href="{base_url}anuncios/add/">Adicionar Anuncio</a></li>

									<li><a href="{base_url}banner-topo/">Listar Banners Topo</a></li>

									<li><a href="{base_url}banner-empreendimento/">Listar Banners Emp.</a></li>

									<li><a href="{base_url}anuncios/">Listar Anuncios</a></li>

								</ul>

							</li>

							

							<li class="hasmenu"><a href="javascript:void(0)">Logos</a>

								<ul class="submenu">

									<li><a href="{base_url}multimidia/add/">Adicionar Multimídia</a></li>

									<li><a href="{base_url}imobiliarias/add/">Adicionar Imobiliária</a></li>

									<li><a href="{base_url}multimidia/">Listar Multimídia</a></li>

									<li><a href="{base_url}imobiliarias/">Listar Imobiliárias</a></li>

								</ul>

							</li>

							

							<li class="hasmenu"><a href="javascript:void(0)">Institucional</a>

								<ul class="submenu">

									<li><a href="{base_url}pagina-quemsomos/read/1">Quem Somos</a></li>

									<li><a href="{base_url}pagina-empreendimentos/read/1">Empreendimentos</a></li>

									<li><a href="{base_url}pagina-imobiliarias/read/1">Imobiliárias</a></li>

									<li><a href="{base_url}pagina-anuncieaqui/read/1">Anuncie Aqui</a></li>

									<li><a href="{base_url}pagina-proprietario/read/1">Anuncie - Proprietário</a></li>

									<li><a href="{base_url}pagina-corretor/read/1">Anuncie - Corretor</a></li>

									<li><a href="{base_url}pagina-imobiliaria/read/1">Anuncie - Imobiliária</a></li>

									<li><a href="{base_url}pagina-multimidia/read/1">Multimídia</a></li>

									<li><a href="{base_url}pagina-contato/read/1">Contato</a></li>

								</ul>

							</li>

							

							<li class="hasmenu"><a href="javascript:void(0)">Configurações</a>

								<ul class="submenu">

									<li><a href="{base_url}config">Config. Site</a></li>

									<li><a href="{base_url}usuarios/read/{id_usuario}/">Editar meu perfil</a></li>

									<li><a href="{base_url}usuarios/add/">Adicionar Usuário</a></li>

									<li><a href="{base_url}usuarios/">Gerenciar Usuários</a></li>

									

									<li><a href="{base_url}como-soube/">Resp. Como Soube</a></li>

								</ul>

							</li>



							<li class="hasmenu"><a href="javascript:void(0)">Vendedores</a>

								<ul class="submenu">

									<li><a href="{base_url}vendas">Gerenciar Vendas</a></li>

									<li><a href="{base_url}vendedores">Gerenciar Vendedores</a></li>

									<li><a href="{base_url}vendedores/add">Adicionar Vendedor</a></li>

								</ul>

							</li>

							<li class="hasmenu"><a href="javascript:void(0)">Contatos</a>

								<ul class="submenu">

									<li><a href="{base_url}contato-site">Contatos do Site</a>

									<li><a href="{base_url}newsletter">Newsletter</a>

								</ul>

							</li>

                            

                            <li class="hasmenu"><a href="{base_url}termos">Termos</a></li>

							

							<li class="hasmenu"><a href="{base_url}cidades">Cidades</a></li>

							<li class="hasmenu"><a href="javascript:void(0)">Região</a>

								<ul class="submenu">

									<li><a href="{base_url}regiao/add/">Adicionar Região</a></li>

									<li><a href="{base_url}regiao/">Listar Regiões</a></li>

								</ul>

							</li>

							<li class="hasmenu"><a href="{base_url}exportar">Exportar</a></li>

						</ul>

						<select class="select">

							<option value="">Navegue</option>

							<option value="{base_url}proprietarios/">Gerenciar Proprietários</option>

							<option value="{base_url}corretores/">Gerenciar Corretores</option>

							<option value="{base_url}imobiliaria/">Gerenciar Imob. / Empreendimentos</option>

							<option value="{base_url}aprovarimoveis/">Aprovar Imóveis</option>

							<option value="{base_url}noticias/add/">Adicionar Noticia</option>

							<option value="{base_url}banner-topo/add/">Adicionar Banner Topo</option>

							<option value="{base_url}banner-empreendimento/add/">Adicionar Banner Emp.</option>

							<option value="{base_url}anuncios/add/">Adicionar Anuncio</option>

							<option value="{base_url}banner-topo/">Listar Banners Topo</option>

                            

                            <option value="{base_url}banner-empreendimento/">Listar Banners Emp.</option>

                            

                            <option value="{base_url}anuncios/">Listar Anuncios</option>

                            

                            <option value="{base_url}multimidia/add/">Adicionar Multimidia</option>

                            

                            <option value="{base_url}imobiliarias/add/">Adicionar Imobiliária</option>

                            

                            <option value="{base_url}multimidia/">Listar Multimidia</option>

                            

                            <option value="{base_url}imobiliarias/">Listar Imobiliárias</option>

                            

                            <option value="{base_url}pagina-quemsomos/read/1">Quem Somos</option>

                            

                            <option value="{base_url}pagina-empreendimentos/read/1">Empreendimentos</option>

                            

                            <option value="{base_url}pagina-imobiliarias/read/1">Imobiliárias</option>

                            

                            <option value="{base_url}pagina-anuncieaqui/read/1">Anuncie Aqui</option>

                            

                            <option value="{base_url}pagina-proprietario/read/1">Anuncie - Proprietário</option>

                            

                            <option value="{base_url}pagina-corretor/read/1">Anuncie - Corretor</option>

                            

                            <option value="{base_url}pagina-imobiliaria/read/1">Anuncie - Imobiliária</option>

                            

                            <option value="{base_url}pagina-multimidia/read/1">Multimídia</option>

                            

                            <option value="{base_url}pagina-contato/read/1">Contato</option>

                            

                            <option value="{base_url}config">Config. Site</option>

                            

                            <option value="{base_url}usuarios/read/{id_usuario}/">Editar meu perfil</option>

                            

                            <option value="{base_url}usuarios/add/">Adicionar Usuário</option>

                            

                            <option value="{base_url}usuarios/">Gerenciar Usuários</option>

                            

                            <option value="{base_url}vendas">Gerenciar Vendas</option>

                            

                            <option value="{base_url}vendedores">Gerenciar Vendedores</option>

                            

                            <option value="{base_url}vendedores/add">Adicionar Vendedor</option>

                            

                            <option value="{base_url}contato-site">Contatos do Site</option>

                            <option value="{base_url}exportar">Exportar</option>

						</select>

                        

					</div>

				</div>

			</nav>

		<!-- Fim do Nav -->



		<!-- Header -->

		<header>

			<div class="row">

				<div class="medium-4 large-4 columns show-for-medium-up">

					<a href="{base_url}">

						<img src="{base_url}assets/images/logo.jpg" alt="Novo ninho" title="Novo ninho" />

					</a>

				</div>



				<div class="small-12 medium-8 large-8 columns userarea">

					<div class="columns small-8 medium-10 large-10">

						<p>Bem Vindo</p>

						<p class="user">{nome}</p>

						<a href="{base_url}login/sair/">Sair do Painel</a>

					</div>



					<div class="columns small-4 medium-2 large-2 text-right">

						<img src="{base_url}assets/images/usuario/{image}" alt="{nome}" title="{nome}" class="radius" />

					</div>

				</div>

			</div>

		</header>

		<!-- Fim do Header -->

	{/dados_usuario}

	{/header}