	{footer}
	
		<div id="modalAprovar" class="reveal-modal" data-reveal="">
				
			<h2>Para aprovar o imóvel, selecione se o mesmo será destaque e sua data de expiração(em dias)</h2>
			
			<form id="aprovar_anuncio" action="{base_url}aprovarimoveis/aprovarAnuncio" method="post" novalidate="novalidate">
				<input type="hidden" name="id_imovel_aprovar" id="id_imovel_aprovar">
				<input type="hidden" name="tipo_aprovar" id="tipo_aprovar">
			
				<div class="row">
				
					<div class="small-12 medium-4 columns">
						<label>Normal</label>
						<select name="" id="">
						<option value="1">Sim</option>
					</select>
					</div>

					<div class="small-12 medium-4 columns">
						<label>Destaque?</label>
						<select name="destaque" id="destaque">
							<option value="0">Não</option>
							<option value="1">Sim</option>
						</select>
					</div>

					<div class="small-12 medium-4 columns">
						<label>Super Destaque?</label>
						<select name="super_destaque" id="super_destaque">
							<option value="0">Não</option>
							<option value="1">Sim</option>
						</select>
					</div>

					<div class="small-12 medium-4 columns">
						<label>Feirão?</label>
						<select name="feirao" id="feirao">
							<option value="0">Não</option>
							<option value="1">Sim</option>
						</select>
					</div>

					<div class="small-12 medium-4 columns">
						<label>Destaque Feirão?</label>
						<select name="destaque_feirao" id="destaque_feirao">
							<option value="0">Não</option>
							<option value="1">Sim</option>
						</select>
					</div>

					<div class="small-12 medium-4 columns">
						<label>Super Destaque Feirão?</label>
						<select name="super_destaque_feirao" id="super_destaque_feirao">
							<option value="0">Não</option>
							<option value="1">Sim</option>
						</select>
					</div>
				
					<div class="small-12 medium-3 columns end">
						<label>Data de expiracao</label>
						<input type="text" name="data_expiracao" id="data_expiracao" value="{data_expira}"/>
					</div>
					
					<div class="small-12 columns">
						<input type="submit" class="button tiny" value="Aprovar">
					</div>
					
				</div>
				
			</form>
				
			<a class="close-reveal-modal">×</a>
		</div>

		<div id="modalRecusar" class="reveal-modal" data-reveal="">
				
			<h2>Para recusar o imóvel, adicione uma observação.</h2>
			
			<form id="recusar_anuncio" action="{base_url}aprovarimoveis/recusar" method="post" novalidate="novalidate">
				<input type="hidden" name="id_veiculo" id="id_veiculo_recusar">
			
				<div class="row">
				
					<div class="small-12 columns">
						<label>Observação</label>
						<textarea name="observacao" class="noeditor" rows="8"></textarea>
					</div>
				
					<div class="small-12 columns text-right">
						<input type="submit" class="button tiny" value="Recusar">
					</div>
					
				</div>
				
			</form>
				
			<a class="close-reveal-modal">×</a>
		</div>

		<!-- Footer -->
		<footer class="row">
			<p>{copy_right}</p>
		</footer>
		<!-- Fim do Footer -->

		</div> <!-- .wrap -->
		<!-- Fim do Conteiner Geral -->

		<!-- Scripts -->
		<!--[if lt IE 9]>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
			<script type="text/javascript" src="{base_url}assets/js/lib/rem.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
			<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<!--<![endif]-->
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>

		<script type="text/javascript" src="{base_url}assets/js/lib/foundation/foundation.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/foundation/foundation.equalizer.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/foundation/foundation.reveal.js"></script>

		<script type="text/javascript" src="{base_url}assets/js/lib/jquery.form.min.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/jquery.maskedinput.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/jquery.slugify.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/jquery.validate.js"></script>

		<script type="text/javascript" src="{base_url}assets/js/lib/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="{base_url}assets/js/lib/select2.min.js"></script>
		
		<script type="text/javascript" src="{base_url}assets/js/general.js"></script>

		{js}
		<script type="text/javascript" src="{src}"></script>
		{/js}

		{js_inline}
		<script type="text/javascript">{script}</script>
		{/js_inline}
		<!-- Fim dos Scripts -->

	{/footer}

	</body>
</html>