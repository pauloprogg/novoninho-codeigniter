<html>
<body style="padding: 30px 0">

	<table width="800" border="0" align="center" cellpadding="15" cellspacing="0">
		<tr>
			<td height="100" valign="top"  style="border: 1px solid #ccc;">
				<h2 style="font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #02436B; padding: 0; margin: 0 0 10px 0;">Recuperação de Senha</h2>

				<table width="775" border="0" cellspacing="0" cellpadding="6">
					<tr>
						<td >
							<p style="font-family:Arial, Helvetica, sans-serif; font-size: 12px; color:#02436B; padding:0; margin:0;">
								Olá, {nome}. Sua senha foi recuperada com sucesso, sua nova senha é:
							</p>
						</td>
					</tr>

					<tr>
						<td >
							<p style="font-family:Arial, Helvetica, sans-serif; font-size: 12px; color:#02436B; padding:0; margin:0;">
								<strong>Usuário:</strong>
								{usuario}
							</p>
						</td>
					</tr>

					<tr>
						<td >
							<p style="font-family:Arial, Helvetica, sans-serif; font-size: 12px; color:#02436B; padding:0; margin:0;">
								<strong>Senha:</strong> 
								{senha}
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br />

</body>
</html>