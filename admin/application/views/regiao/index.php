<!-- Conteudos especificos separados por sessoes -->
{msg_sucesso}
{msg_erro}

<!-- Sessão 1 -->
<section class="row show-for-small-only">
	<div class="small-12 medium-12 large-12 text-center">
		<ul id="actions" class="button-group">
		  <li><a href="{base_url}{pagina}/add/" class="button tiny">Adicionar <br/> Postagem</a></li>
		  <li><a href="{base_url}comentarios/{pagina}" class="button tiny">Gerenciar <br/> Comentários</a></li>
		  <li><a href="{base_url}categorias/{pagina}" class="button tiny">Gerenciar <br/> Categorias</a></li>
		  <li><a href="{base_url}tags/{pagina}" class="button tiny">Gerenciar <br/> Tags</a></li>
		</ul>
	</div>
</section>

<!-- Sessão 2 -->
<section class="row">
	<div class="columns medium-12 large-12">
		<h1>Gerenciar Regiões</h1>
	</div>
</section>

<!-- Sessão 3 -->
<section>
	{lista}


	<div class="row">
		{paginacao}
	</div>
</section>
<!-- Fim dos conteudos especificos -->