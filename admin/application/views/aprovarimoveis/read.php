{msg_erro}
{msg_aviso}
<!-- Conteudos especificos separados por sessoes -->

<!-- Sessão 1 -->
<section class="row content-form">

	<!-- Coluna Esquerda -->
	<div class="columns medium-8 large-8">
	
		<!-- Form Campos Esquerdo -->
		<form action="{base_url}{pagina}/update/{id}" method="post" enctype="multipart/form-data" class="row form-esquerdo">
			<div class="columns small-12 medium-12 large-12">
				<h1>Visualizar Dados de Imóvel</h1>
			</div>
			
			<div class="columns small-12 medium-12 large-12">
				<label><strong>Imagens do Imóvel</strong></label>
			
				<ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
					{imagens}
						<li><a href="../../../../assets/uploads/imovel/{id_usuario}/{arquivo}" target="_blank"><img src="../../../../assets/uploads/imovel/{id_usuario}/{arquivo}"></a></li>
					{/imagens}
				</ul>
			</div>
			
			<div class="columns small-12 medium-12 large-12">
				<label class="label_destaque"><strong>{destaque}</strong></label>
			</div>

			{form_esquerdo}
			{campo}
			{/form_esquerdo}

		</form>
		<!-- FIM Form Campos Esquerdo -->

	</div>
	<!-- FIM Colona Esquerda -->

	<div class="columns medium-4 large-4">  <!-- Coluna Direita -->


		<!-- Form Campos Esquerdo -->
		<form action="{base_url}{pagina}/update/{id}" method="post" enctype="multipart/form-data" class="form-direito">
			<!-- Painel 1 -->
			<div class="panel">
				<h1>Salvar</h1>
				{select_ativo}

				<!--
				<label for="iptSenha"> <strong>Inserir senha</strong>
					<input type="text" id="iptSenha" name="iptSenha" />
				</label>

				<label for="chkComentarios"><input type="checkbox" id="chkComentarios" name="chkComentarios" />Permitir Comentários</label>
				-->
				<!--
				<div class="columns small-6 medium-12 large-6">
					<a href="{base_url}{pagina}/delete/{id}" class="ac red">Deletar</a>
				</div>
				-->

				<div class="columns small-6 medium-12 large-6 text-right">
					<button type="submit" class="button tiny submit">Salvar</button>
				</div>

				<div class="clearfix"></div>
			</div>
			<!-- FIM Painel 1 -->
			
			<!-- Checks -->
		
			<!-- FIM Checks -->

		</form>
		<!-- FIM Form Campos Esquerdo -->

	</div>
	<!-- FIM Coluna Direita -->

</section>
<!-- FIM dos conteudos especificos -->
