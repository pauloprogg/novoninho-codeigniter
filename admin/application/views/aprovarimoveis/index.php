<!-- Conteudos especificos separados por sessoes -->
{msg_sucesso}
{msg_erro}

<!-- Sessão 1 -->
<section class="row show-for-small-only">
	<div class="small-12 medium-12 large-12 text-center">
		<ul id="actions" class="button-group">
		  <li><a href="{base_url}{pagina}/add/" class="button tiny">Adicionar <br/> Postagem</a></li>
		  <li><a href="{base_url}comentarios/{pagina}" class="button tiny">Gerenciar <br/> Comentários</a></li>
		  <li><a href="{base_url}categorias/{pagina}" class="button tiny">Gerenciar <br/> Categorias</a></li>
		  <li><a href="{base_url}tags/{pagina}" class="button tiny">Gerenciar <br/> Tags</a></li>
		</ul>
	</div>
</section>

<!-- Sessão 2 -->
<section class="row">
	<div class="columns medium-12 large-12">
		<h1>Aprovar Anúncios</h1>
	</div>

	<div class="columns medium-12 large-12" style="text-align: right;margin-bottom: 20px;">
		{botao_exportar}
	</div>

	
	<form action="{base_url}{pagina}" method="get">
	<div class="columns medium-3 large-2">
		<select name="ordem" id="slcOrdenacao" />
			<option value="">Ordenação</option>
			<option value="data_desc">Mais Recentes</option>
			<option value="data_asc">Mais Antigas</option>
			<option value="titulo_asc">Titulo A-Z</option>
			<option value="titulo_desc">Titulo Z-A</option>
		</select>
	</div>

	<div class="columns medium-3 large-2">
		<select name="filtro" id="slcOrdenacao" />
			<option value="">Filtro</option>
			<option value="ativos">Ativos</option>
			<option value="inativos">Inativos</option>
			<option value="aprovado">Aprovado</option>
			<option value="nao_aprovado">Não Aprovado</option>
		</select>
	</div>
	
	<div class="columns medium-3 large-2">
		<select name="status" id="slcOrdenacao" />
			<option value="">Status</option>
			<option value="aguardando_pagamento">Aguardando Pagamento</option>
			<option value="aguardando_aprovacao">Aguardando Aprovação de Pagamento</option>
			<option value="publicado">Pago</option>
			<option value="cancelado">Cancelado</option>
		</select>
	</div>
	
	<div class="columns medium-12 large-6">
		<div class="columns large-12"><input type="text" id="iptBusca" name="busca" placeholder="Procure pelo usuário ou anuncio" /></div>
	</div>
	
	<div class="columns medium-2 large-2">
		<select name="expirado"/>
			<option value="">Filtro Expirado</option>
			<option value="expirado">Expirados</option>
			<option value="nao_expirado">Não Expirados</option>
		</select>
	</div>
	
	<div class="columns medium-5 large-5">
		<div class="row">
			<div class="columns large-6"><input type="text" name="data_inicio_cadastro" placeholder="Data Inicial Cadastro" class="datepicker"/></div>
			<div class="columns large-6"><input type="text" name="data_fim_cadastro" placeholder="Data final Cadastro" class="datepicker"/></div>
		</div>
	</div>
	
	<div class="columns medium-5 large-5">
		<div class="row">
			<div class="columns large-6"><input type="text" name="data_inicio_expira" placeholder="Data Inicial Expiração" class="datepicker"/></div>
			<div class="columns large-6"><input type="text" name="data_fim_expira" placeholder="Data final Expiração" class="datepicker"/></div>
		</div>
	</div>
	<div class="columns medium-3 large-3">
		<select name="tipo_usuario"/>
			<option value="0">Proprietários e Corretores</option>
			<option value="1">Proprietário</option>
			<option value="2">Corretor</option>
		</select>
	</div>
	
	<div class="columns medium-12 large-12 text-right">
		<div class="row">
			<input type="submit" value="buscar" class="button tiny" />
		</div>
	</div>
	
	</form>
</section>

<!-- Sessão 3 -->
<section>
	{lista}

	<div class="row">
		{paginacao}
	</div>
</section>
<!-- Fim dos conteudos especificos -->