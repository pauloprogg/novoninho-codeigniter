<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastros extends Main_Controller {

	private $pagina = "cadastros";
	private $tabela = "cliente";

	private $config_lista = array(
		'busca' => 'nome',
		'filtro' => array(
			'ativos' => array( 'Apagado' => '1' ),
			'inativos' => array( 'Apagado' => '0' )
		),
		'order' => array(
			'nome_asc' => array( 'Nome' => 'ASC' ),
			'nome_desc' => array( 'Nome' => 'DESC' ),
			'data_asc' => array( 'DataInclusao' => 'ASC' ),
			'data_desc' => array( 'DataInclusao' => 'DESC' )
		),
		'num_link' => 10,
		'qtd_por_pagina' => 30
	);

	private $config_ckecks = array();

	// Configurações de arquivos
	private $config_arquivo = array();

	// Configurações para imagem
	private $config_image = array();

	// Configurações para galeria
	private $config_galeria = array(

		'galeria' => false,
		'config' => array()

	);

	public function __construct() {

		parent::__construct();
		$this->checkLogin();

	}

	public function index() {

		$busca = $this->input->get( "busca" );
		$filtro = $this->input->get( "filtro" );
		$ordem = $this->input->get( "ordem" );

		$where = array();
		$order = array( "IdCliente" => "DESC" );

		if( !empty( $filtro ) || $filtro === '0' ) {

			$where = $this->config_lista[ 'filtro' ][ $filtro ];

		}

		if( !empty( $busca ) ) {

			$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%'" ] = NULL;

		}

		if( !empty( $ordem ) ) {

			if( isset( $this->config_lista[ 'order' ][ $ordem ] ) ) {

				$order = $this->config_lista[ 'order' ][ $ordem ];

			}

		}

		$retorno = $this->crud->read_paginacao( $where, $this->tabela, $order, $this->config_lista[ 'qtd_por_pagina' ], $this->pagina, $this->config_lista[ 'num_link' ] );

		$lista = $retorno[ 'result' ];
		$paginacao = $retorno[ 'paginacao' ];

		$data = array(
			"lista" => $lista,
			"pagina" => $this->pagina,
			"paginacao" => $paginacao
		);

		$this->load( $this->pagina .'/index', $data );

	}

	// Add
	public function add() {

		$campos_form_esquerdo = array(
			"data_cadastro" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "data_cadastro",
				"value" => time(),
				"width" => "12"
			),
			"slug" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "slug",
				"value" => "",
				"width" => "12"
			),
			"nome" => array(
				"type" => "text",
				"label" => "Nome",
				"name" => "nome",
				"value" => "",
				"width" => "12"
			),
			"email" => array(
				"type" => "rtext",
				"label" => "Texto",
				"name" => "texto",
				"value" => "",
				"width" => "12"
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens

		$arquivos = array(
			"imagem" => array(
				"type" => "image",
				"label" => "Imagem de Capa",
				"name" => "imagem",
				"value" => "",
				"width" => $this->config_image[ 'imagem' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'imagem' ][ 'config' ][ 'altura' ]
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );

		// Linhas
		$config = array(
			'campos' => 'nome as label, id as value',
			'tabela' => 'linhas',
			'where' => array( 'ativo' => 1 )
		);

		$this->select->set( $config );
		$itens_select = $this->select->resultado();

		$select = $this->imput_form->select( 'Linha', 'linha', $itens_select, 0 );

		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"select" => $select,
			"pagina" => $this->pagina
		);

		//$this->load( $this->pagina .'/add', $data );

	}

	// Edição
	public function read( $id ) {

		$item = $this->crud->read( array( 'IdCliente'=> $id ), $this->tabela, array( "IdCliente" => "DESC" ) );

		if( empty( $item ) ) {
			redirect( base_url(). $this->pagina );
		}

		$item = $item[0];

		$campos_form_esquerdo = array(
			"DataInclusao" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "DataInclusao",
				"value" => $item->DataInclusao,
				"width" => "12"
			),
			"Nome" => array(
				"type" => "text",
				"label" => "Nome",
				"name" => "Nome",
				"value" => $item->Nome,
				"width" => "12"
			),
			"Email" => array(
				"type" => "text",
				"label" => "E-mail",
				"name" => "Email",
				"value" => $item->Email,
				"width" => "8"
			),
			"Telefone" => array(
				"type" => "text",
				"label" => "Telefone",
				"name" => "Telefone",
				"value" => "( ". $item->Ddd .") ". $item->Telefone,
				"width" => "4"
			),
			"Sexo" => array(
				"type" => "text",
				"label" => "Sexo",
				"name" => "Sexo",
				"value" => $item->Sexo,
				"width" => "2"
			),
			"Nascimento" => array(
				"type" => "data",
				"label" => "Data de Nascimento",
				"name" => "Nascimento",
				"value" => $this->util->date2br( $item->Nascimento ),
				"width" => "5"
			),
			"Cpf" => array(
				"type" => "text",
				"label" => "CPF",
				"name" => "Cpf",
				"value" => $item->Cpf,
				"width" => "5"
			),
			"Endereco" => array(
				"type" => "text",
				"label" => "Endereço",
				"name" => "Endereco",
				"value" => $item->Endereco,
				"width" => "12"
			),
			"Numero" => array(
				"type" => "text",
				"label" => "Numero",
				"name" => "Numero",
				"value" => $item->Numero,
				"width" => "2"
			),
			"Complemento" => array(
				"type" => "text",
				"label" => "Complemento",
				"name" => "Complemento",
				"value" => $item->Complemento,
				"width" => "7"
			),
			"Cep" => array(
				"type" => "text",
				"label" => "Cep",
				"name" => "Cep",
				"value" => $item->Cep,
				"width" => "3"
			),
			"Bairro" => array(
				"type" => "text",
				"label" => "Bairro",
				"name" => "Bairro",
				"value" => $item->Bairro,
				"width" => "5"
			),
			"Cidade" => array(
				"type" => "text",
				"label" => "Cidade",
				"name" => "Cidade",
				"value" => $item->Cidade,
				"width" => "5"
			),
			"Estado" => array(
				"type" => "text",
				"label" => "Estado",
				"name" => "Estado",
				"value" => $item->Estado,
				"width" => "2"
			)

		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );
		
		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"pagina" => $this->pagina,
			"id" => $id
		);

		$this->load( $this->pagina .'/read', $data );

	}

	public function create() {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {
			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}
		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		if( $id = $this->crud->create( $_POST, $this->tabela ) ) {

			// Galeria
			if( $this->config_galeria[ 'galeria' ] ) {

				$this->imput_form->galeria_update( $id );

			}

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item adicionado com sucesso!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao adicionar o item, tente novamente!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => ase_url().$this->pagina."read/".$id
			);

		}

		echo json_encode( $retorno );

	}


	public function update( $id ) {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {

			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}

		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Faz e verifica se fez o update
		if( $this->crud->update( array( "id" => $id ), $_POST, $this->tabela ) ) {

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina."read/".$id
			);

		}

		echo json_encode( $retorno );

	}

	public function delete( $id ) {

		// Faz e verifica se fez o delete
		$this->session->set_flashdata( "msg_sucesso", "Item deletado com sucesso!" );
		redirect( base_url().$this->pagina );

	}

	public function image( $campo ) {

		$src = $this->image_form->upload_imagem( $_FILES[ $campo ], $this->config_image[ $campo ][ 'config' ] );

		$this->session->set_userdata( $campo, $src );

		if( $this->config_image[ $campo ][ 'config' ][ 'thumb' ] ) {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].'thumb/'.$src;

		} else {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].$src;

		}

	}

	public function file( $campo ) {

		$this->load->library( 'upload', $this->config_arquivo[ $campo ][ 'config' ] );

		if( $this->upload->do_upload( $campo ) ) {

			$data = $this->upload->data();
			$name = $data[ 'file_name' ];

			$this->session->set_userdata( $campo, $name );

			echo base_url().$this->config_arquivo[ $campo ][ 'config' ][ 'upload_path' ].$name;

		} else {

			echo '0';

		}

	}

	public function galeria() {

		$src = $this->image_form->upload_imagem( $_FILES[ 'arquivo' ], $this->config_galeria[ 'config' ] );

		$_POST[ 'arquivo' ] = $src;

		$id = $this->imput_form->galeria_insert( $_POST );

		$galeria_id = $this->session->userdata( "galeria_id" );

		$session = $this->session->userdata( $galeria_id );

		$session[] = $id;

		$this->session->set_userdata( $galeria_id, $session );

		$json = array(
			"src" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].'thumb/'.$src,
			"src_full" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].$src,
			"id" => $id,
			"pagina" => $this->pagina,
			"session" => $session,
			"id" => $galeria_id
		);

		echo json_encode( $json );

	}

	public function galeria_legenda() {

		$id = $this->imput_form->galeria_legenda( $_POST[ 'id' ], $_POST[ 'legenda' ] );

	}

	public function galeria_delete() {

		$id = $this->imput_form->galeria_delete( $_POST[ 'id' ] );

	}

}