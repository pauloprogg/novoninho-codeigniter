<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pagina_corretor extends Main_Controller {

	private $pagina = "pagina-corretor";
	private $tabela = "pagina_corretor";

	private $config_lista = array(
		'busca' => false,
		'filtro' => false,
		'order' => false,
		'num_link' => false,
		'qtd_por_pagina' => false
	);

	private $config_ckecks = array();

	// Configurações de arquivos
	private $config_arquivo = array();

	// Configurações para imagem
	private $config_image = array(
		'imagem' => false
	);

	// Configurações para galeria
	private $config_galeria = array(

		'galeria' => 0,
		'config' => array()

	);

	public function __construct() {

		parent::__construct();
		$this->checkLogin();

	}

	// Edição
	public function read( $id ) {

		$item = $this->crud->read( array( 'id'=> $id ), $this->tabela, array( 'id' => 'DESC' ) );

		if( empty( $item ) ) {
			redirect( base_url(). $this->pagina );
		}

		$item = $item[0];

		$campos_form_esquerdo = array(
			"texto1" => array(
				"type" => "rtext",
				"label" => "Texto Padrão",
				"name" => "texto1",
				"value" => $item->texto1,
				"width" => "12"
			),
			"texto2" => array(
				"type" => "rtext",
				"label" => "Texto de Baixo",
				"name" => "texto2",
				"value" => $item->texto2,
				"width" => "12"
			),
			"preco_normal" => array(
				"type" => "text",
				"label" => "Preço do Anuncio",
				"name" => "preco_normal",
				"value" => $item->preco_normal,
				"width" => "12",
				"descricao" => "Ex: 15.50"
			),
			"preco_destaque" => array(
				"type" => "text",
				"label" => "Preço com Destaque",
				"name" => "preco_destaque",
				"value" => $item->preco_destaque,
				"width" => "12",
				"descricao" => "Ex: 100.10"
			),
			"vantagem1" => array(
				"type" => "text",
				"label" => "Vantagem 1",
				"name" => "vantagem1",
				"value" => $item->vantagem1,
				"width" => "12"
			),
			"vantagem2" => array(
				"type" => "text",
				"label" => "Vantagem 2",
				"name" => "vantagem2",
				"value" => $item->vantagem2,
				"width" => "12"
			),
			"vantagem3" => array(
				"type" => "text",
				"label" => "Vantagem 3",
				"name" => "vantagem3",
				"value" => $item->vantagem3,
				"width" => "12"
			),
			"vantagem4" => array(
				"type" => "text",
				"label" => "Vantagem 4",
				"name" => "vantagem4",
				"value" => $item->vantagem4,
				"width" => "12"
			),
			"vantagem5" => array(
				"type" => "text",
				"label" => "Vantagem 5",
				"name" => "vantagem5",
				"value" => $item->vantagem5,
				"width" => "12"
			),
			"vantagem6" => array(
				"type" => "text",
				"label" => "Vantagem 6",
				"name" => "vantagem6",
				"value" => $item->vantagem6,
				"width" => "12"
			),
			"vantagem7" => array(
				"type" => "text",
				"label" => "Vantagem 7",
				"name" => "vantagem7",
				"value" => $item->vantagem7,
				"width" => "12"
			),
			"vantagem8" => array(
				"type" => "text",
				"label" => "Vantagem 8",
				"name" => "vantagem8",
				"value" => $item->vantagem8,
				"width" => "12"
			),
			"vantagem9" => array(
				"type" => "text",
				"label" => "Vantagem 9",
				"name" => "vantagem9",
				"value" => $item->vantagem9,
				"width" => "12"
			),
			"vantagem10" => array(
				"type" => "text",
				"label" => "Vantagem 10",
				"name" => "vantagem10",
				"value" => $item->vantagem10,
				"width" => "12"
			),
			"vantagem11" => array(
				"type" => "text",
				"label" => "Vantagem 11",
				"name" => "vantagem11",
				"value" => $item->vantagem11,
				"width" => "12"
			),
			"vantagem12" => array(
				"type" => "text",
				"label" => "Vantagem 12",
				"name" => "vantagem12",
				"value" => $item->vantagem12,
				"width" => "12"
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens
		$path_image = base_url() ."../assets/uploads/";

		$arquivos = array(
			"imagem" => array(
				"type" => "image",
				"label" => "Imagem",
				"name" => "imagem",
				"value" => ( !empty( $item->imagem ) ) ? $path_image. $item->imagem : "",
				"width" => $this->config_image[ 'imagem' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'imagem' ][ 'config' ][ 'altura' ]
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );

		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"pagina" => $this->pagina,
			"id" => $id
		);

		$this->load( $this->pagina .'/read', $data );

	}

	public function create() {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {
			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}
		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		if( $id = $this->crud->create( $_POST, $this->tabela ) ) {

			// Galeria
			if( $this->config_galeria[ 'galeria' ] ) {

				$galeria_id = $this->session->userdata( 'galeria_id' );
				$this->imput_form->galeria_update( $id, $galeria_id );

			}

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item adicionado com sucesso!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao adicionar o item, tente novamente!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		}

		echo json_encode( $retorno );

	}


	public function update( $id ) {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {

			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}

		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Faz e verifica se fez o update
		if( $this->crud->update( array( "id" => $id ), $_POST, $this->tabela ) ) {

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina."/read/".$id
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina."/read/".$id
			);

		}

		echo json_encode( $retorno );

	}

	public function delete( $id ) {

		// Faz e verifica se fez o delete
		$this->crud->delete( array( "id" => $id ), $this->tabela );
		$this->session->set_flashdata( "msg_sucesso", "Item deletado com sucesso!" );
		redirect( base_url().$this->pagina );

	}

	public function image( $campo ) {

		$src = $this->image_form->upload_imagem( $_FILES[ $campo ], $this->config_image[ $campo ][ 'config' ] );

		$this->session->set_userdata( $campo, $src );

		if( $this->config_image[ $campo ][ 'config' ][ 'thumb' ] ) {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].'thumb/'.$src;

		} else {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].$src;

		}

	}

	public function file( $campo ) {

		$this->load->library( 'upload', $this->config_arquivo[ $campo ][ 'config' ] );

		if( $this->upload->do_upload( $campo ) ) {

			$data = $this->upload->data();
			$name = $data[ 'file_name' ];

			$this->session->set_userdata( $campo, $name );

			echo base_url().$this->config_arquivo[ $campo ][ 'config' ][ 'upload_path' ].$name;

		} else {

			echo '0';

		}

	}

	public function galeria() {

		$src = $this->image_form->upload_imagem( $_FILES[ 'arquivo' ], $this->config_galeria[ 'config' ] );

		$_POST[ 'arquivo' ] = $src;

		$id = $this->imput_form->galeria_insert( $_POST );

		$galeria_id = $_POST[ 'elemento' ];

		$session = $this->session->userdata( $galeria_id );

		$session[] = $id;

		$this->session->set_userdata( $galeria_id, $session );
		$this->session->set_userdata( 'galeria_id', $galeria_id );
// var_dump($galeria_id);
		$json = array(
			"src" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].'thumb/'.$src,
			"src_full" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].$src,
			"id" => $id,
			"pagina" => $this->pagina,
			"session" => $session,
			"id" => $id
		);

		echo json_encode( $json );

	}

	public function galeria_legenda() {

		$id = $this->imput_form->galeria_legenda( $_POST[ 'id' ], $_POST[ 'legenda' ] );

	}

	public function galeria_delete() {

		$id = $this->imput_form->galeria_delete( $_POST[ 'id' ] );

	}

}