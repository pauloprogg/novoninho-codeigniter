<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Regiao extends Main_Controller {


	private $pagina = "regiao";
	private $tabela = "regiao";
	
	public function __construct() {

		parent::__construct();
		$this->checkLogin();

	}

	public function index() {



		


		$config = array(

			'campos' => 'id as "idRegiao", nome as "nomeRegiao"',

			'tabela' => 'regiao'

		);



		$this->select->set($config);

		$paginacao = $this->select->paginacao( "regiao", 5);

		$resultado = $this->select->resultado();

		

		$lista = '';

		

		foreach( $resultado as $val ) {

		

			$id_regiao = $val->idRegiao;

	
			$nome = $val->nomeRegiao;


						

			$lista .= '<div class="row table">

							<div class="columns small-12 medium-1 large-1"> <p><strong>ID:</strong> '.$id_regiao.'</p> </div>

							<div class="columns small-12 medium-9 large-9"> <p><strong>Nome:</strong> '.$nome.'</p> </div>

							<div class="columns small-12 medium-2 large-2">

								<div class="columns small-12 medium-6 large-6 text-left"><a href="'.base_url().$this->pagina.'/read/'.$id_regiao.'">Editar</a></div>

								<div class="columns small-6 medium-6 large-6 text-right"><a href="'.base_url().$this->pagina.'/delete/'.$id_regiao.'" class="ac red">Deletar</a></div>

							</div>



							<div class="clearfix"></div>

						</div>



						<div class="row"><hr></div>';

		

		}



		$data = array(

			"lista" => $lista,

			"pagina" => $this->pagina,

			"paginacao" => $paginacao

		);



		$this->load( $this->pagina .'/index', $data );



	}

	// Add
	public function add() {

		$campos_form_esquerdo = array(
			"nome" => array(
				"type" => "text",
				"label" => "Nome",
				"name" => "nome",
				"value" => "",
				"width" => "12"
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );
		

		//estado
		$config = array(
			'campos' => 'uf_codigo as value, uf_descricao as label',
			'tabela' => 'cep_uf'
		);

		$this->select->set( $config );

		$estados = $this->select->resultado();

		$estadoSelect = '';
		foreach($estados as $val ) {			
			$estadoSelect .= '<option value="'.$val->value.'">'.$val->label.'</option>';			
		}

		$data = array(
			"form_esquerdo" => $form_esquerdo,			
			"estado" => $estadoSelect,
			"cidade" => array(),
			"pagina" => $this->pagina
		);

		$this->load( $this->pagina .'/add', $data );
	}

	// Edição
	public function read( $id ) {

		$item = $this->crud->read( array( 'id'=> $id ), $this->tabela, array( 'id' => 'DESC' ) );

		if( empty( $item ) ) {
			redirect( base_url(). $this->pagina );
		}

		$item = $item[0];

		$campos_form_esquerdo = array(
			"nome" => array(
				"type" => "text",
				"label" => "Nome",
				"name" => "nome",
				"value" => $item->nome,
				"width" => "12"
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );
		

		//estado
		$config = array(
			'campos' => 'uf_codigo as value, uf_descricao as label',
			'tabela' => 'cep_uf'
		);

		$this->select->set( $config );

		$estados = $this->select->resultado();

		$estadoSelect = '';
		foreach($estados as $val ) {			
			$estadoSelect .= '<option value="'.$val->value.'">'.$val->label.'</option>';			
		}

		//cidade
		$config = array(
			'campos' => 'ci.cidade_codigo, ci.cidade_descricao, re.id_cidade',
			'tabela' => 'cep_cidade as ci',
			'join' => array(
				array( 'regiao_cidade as re', 'ci.cidade_codigo = re.id_cidade', 'left' ),
			),
			'where' => 're.id_regiao = "'.$id.'"'
		);

		$this->select->set( $config );
		$cidades = $this->select->resultado();

		
		$cidadeSelect = '';

		
		foreach($cidades as $val ) {
			
			$selected = '';
			
			 if(!empty($val->id_cidade)) {
				$selected = 'selected="selected"';	
			}

			$cidadeSelect .= '<option value="'.$val->id_cidade.'" '.$selected.'>'.$val->cidade_descricao.'</option>';

		}

		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"id" => $id,			
			"estado" => $estadoSelect,
			"cidade" => $cidadeSelect,
			"pagina" => $this->pagina
		);

		$this->load( $this->pagina .'/read', $data );

	}

	public function create() {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);
		
		$arrayCidades = $_POST['cidade'];
				
		//despresando o estado
		if( isset($_POST['estado'] ) ) {

			unset($_POST['estado']);

		}

		if( isset($_POST['cidade'] ) ) {

			unset($_POST['cidade']);

		}

		
		if( $id = $this->crud->create( $_POST, $this->tabela ) ) {

			//Inserindo cidade
			if(!empty($arrayCidades)){
				
				foreach ($arrayCidades as $v) {
					$dataCidades [] = array('id_regiao'=>$id, 'id_cidade' => $v);
				}
				$this->db->insert_batch('regiao_cidade', $dataCidades);

			}
			

			$this->session->set_flashdata( "msg_sucesso", "Item adicionado com sucesso!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao adicionar o item, tente novamente!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		}

		echo json_encode( $retorno );

	}


	public function update( $id ) {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		$arrayCidades = $_POST['cidade'];
				
		//despresando o estado
		if( isset($_POST['estado'] ) ) {

			unset($_POST['estado']);

		}

		if( isset($_POST['cidade'] ) ) {

			unset($_POST['cidade']);

		}

		
		if( $this->crud->update( array( "id" => $id ), $_POST, $this->tabela ) ) {
			$this->crud->delete( array( "id_regiao" => $id ), 'regiao_cidade' );
			//Inserindo cidade

			if(!empty($arrayCidades)){
				$dataCidades = array();
				foreach ($arrayCidades as $v) {
					$dataCidades [] = array('id_regiao'=>$id, 'id_cidade' => $v);
				}
				
				$this->db->insert_batch('regiao_cidade', $dataCidades);

			}
			

			$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina."/read/".$id
			);
		}
		

		echo json_encode( $retorno );

	}

	public function delete( $id ) {

		// Faz e verifica se fez o delete
		$this->crud->delete( array( "id" => $id ), $this->tabela );
		$this->session->set_flashdata( "msg_sucesso", "Item deletado com sucesso!" );
		redirect( base_url().$this->pagina );

	}


}