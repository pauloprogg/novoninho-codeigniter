<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter extends Main_Controller {

	private $pagina = "newsletter";
	private $tabela = "usuarios";

	public function __construct() {

		parent::__construct();
		$this->checkLogin();

	}
	private $config_lista = array(
		'login' => 'login',
		'filtro' => 'ativo',
		'filtro' => array(
			'ativos' => array( 'ativo' => '1' ),
			'inativos' => array( 'ativo' => '0' )
		),
		'order' => array(
			'login_asc' => array( 'login' => 'ASC' ),
			'login_desc' => array( 'login' => 'DESC' )
		),
		'num_link' => 5,
		'qtd_por_pagina' => 15
	);
	// Edição
	public function index() {

		$where = array("newsletter" => "1");
		$order = array( "id" => "DESC" );

		$retorno = $this->crud->read_paginacao( $where, $this->tabela, $order, $this->config_lista[ 'qtd_por_pagina' ], $this->pagina, $this->config_lista[ 'num_link' ] );

		$lista = $retorno[ 'result' ];
		$paginacao = $retorno[ 'paginacao' ];

				$html = '';
		$html .= '<table border="1">';
		$html .= '<tr>';

		$html .= '<td align="center"><b>E-mail</b></td>';
		$html .= '<td align="center"><b>Tipo</b></td>';

		$html .= '</tr>';
		foreach($lista as $item){
			$user = '';
			switch ($item->tipo) {
				case '1':
					$user = 'Proprietário';
					break;
				case '2':
					$user = 'Corretor';
					break;
				case '3':
					$user = 'Imobiliaária';
					break;
			}
			$html .= '<tr>';

			$html .= '<td align="center">'.$item->login.'</td>';
			$html .= '<td align="center">'.$user.'</td>';

			$html .= '</tr>';
		}
		$html .= '</table>';

		$html = utf8_decode($html);
		// Definimos o nome do arquivo que será exportado
		$filename = 'newsletters.xls';
		$arquivo = fopen($filename,'w+');
		if ($arquivo) {
			fwrite($arquivo, $html);
			fclose($arquivo);
		}	




		$data = array(
			"lista" => $lista,
			"pagina" => $this->pagina,
			"paginacao" => $paginacao,
			"planilha" => $filename
		);

		$this->load( $this->pagina .'/index', $data );


	}
}