<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exportar extends Main_Controller {

	private $pagina = "exportar";
	private $tabela = "imoveis";

	private $config_lista = array(
		'busca' => 'imo.titulo',
		'num_link' => 10,
		'filtro' => array(
			'aprovado' => array( 'imo.aprovado' => '1' ),
			'nao_aprovado' => array( 'imo.aprovado' => '0' ),
			'ativos' => array( 'imo.ativo' => '1' ),
			'inativos' => array( 'imo.ativo' => '0' ),
		),
		'order' => array(
			'titulo_asc' => array( 'imo.titulo' => 'ASC' ),
			'titulo_desc' => array( 'imo.titulo' => 'DESC' ),
			'data_asc' => array( 'imo.data_cadastro' => 'ASC' ),
			'data_desc' => array( 'imo.data_cadastro' => 'DESC' )
		),
		'aprovado' => array(
			'aprovado' => '1',
			'nao_aprovado' => '0'
		),
		'status' => array(
			'aguardando_pagamento' => '1',
			'aguardando_aprovacao' => '2',
			'publicado' => '3',
			'cancelado' => '4'
		),
		'qtd_por_pagina' => 25
	);

	private $config_ckecks = array();

	// Configurações de arquivos
	private $config_arquivo = array();

	// Configurações para imagem
	private $config_image = array(
		'imagem' => array(
			'campo' => 'imagem',
			'config' => array(
				'thumb' => array(
					'greyscale' => false, // bolean
					'largura' => 140, // num
					'altura' => 140 // num
				),
				'marca' => false,
				'image_path' => '../assets/uploads/noticias/', // string [ caminho da imagem é necessário ter uma pasta thumb (se tiver)]
				'largura' => 540, // num
				'altura' => 340, // num

				'redimensionar' => true, // boolean [ redimensiona a imagem ]
				'proporcional' => false, // boolean [ redimenciona proporcionalmente ]
				'cortar' => true, // boolean [ corta a imagem sem redimencionar ]
				'converter' => false, // boolean [ converte a image para jpg ]
				'greyscale' => false, // boolean [ coloca efeito greyscale ]
				//'qualidade' => 80 // num [ configura a qualidade da imagem ]
			)
		)

	);

	// Configurações para galeria
	private $config_galeria = array(

		'galeria' => 0,
		'config' => array()

	);

	public function __construct() {

		parent::__construct();
		$this->checkLogin();

	}

	public function index() {
		
		$data_inicio_cadastro = $this->input->get( "data_inicio_cadastro" );
		$data_fim_cadastro = $this->input->get( "data_fim_cadastro" );
		
		$data_inicio_expira = $this->input->get( "data_inicio_expira" );
		$data_fim_expira = $this->input->get( "data_fim_expira" );
		
		$busca = $this->input->get( "busca" );
		$busca_usuario = $this->input->get( "busca_usuario" );
		$filtro = $this->input->get( "filtro" );
		$ordem = $this->input->get( "ordem" );
		$aprovado = $this->input->get( "aprovado" );
		$status = $this->input->get( "status" );
		$tipo_usuario = $this->input->get( "tipo_usuario" );

		$destaque = $this->input->get( "destaque" );

		$numero_id = $this->input->get( "numero_id" );
		$tipo_anuncio = $this->input->get( "tipo_anuncio" );
		$tipo_imovel = $this->input->get( "tipo_imovel" );
		$finalidade = $this->input->get( "finalidade" );

		$dormitorios = $this->input->get( "dormitorios" );
		$suites = $this->input->get( "suites" );
		$garagem = $this->input->get( "garagem" );
		$banheiros = $this->input->get( "banheiro" );
		
		$cidade = $this->input->get( "cidade" );
		$estado = $this->input->get( "estado" );

		$imobiliaria = $this->input->get( "imobiliaria" );

		$expirado = $this->input->get( "expirado" );
		$join = array();
		$order = array();
		$where = array();

		$join[] = array('imoveis_aprovados_vendedor apr','apr.id_imovel = imo.id','left');
		$join[] = array('usuarios us','us.id = imo.id_usuario','left');
		$join[] = array('usuarios_admin usa','usa.id = apr.id_vendedor','left');
		
		if( !empty( $filtro )) {

			$where = $this->config_lista[ 'filtro' ][ $filtro ];

		}

		if( !empty( $busca ) ) {

			if ($tipo_usuario != 0) {
				if($tipo_usuario == 1) { //Proprietario
					$join_tb = 'dados_proprietario';
				} else if($tipo_usuario == 2) { //Corretor
					$join_tb = 'dados_corretor';
				} else if($tipo_usuario == 3) { //Imobiliaria
					$join_tb = 'dados_imobiliaria';
				}

				$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%' OR dp.nome LIKE '%". $busca ."%'" ] = NULL;
				$join[] = array($join_tb.' dp','dp.id = us.perfil','left');				
			}else{
				$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%' OR us.login LIKE '%". $busca ."%'" ] = NULL;
			}
		}

		if( !empty( $data_inicio_expira ) && !empty( $data_fim_expira ) ) {

			$data_inicio = $this->util->date2us( $data_inicio_expira );
			$data_inicio = strtotime( $data_inicio );

			$data_fim = $this->util->date2us( $data_fim_expira );
			$data_fim = strtotime( $data_fim );

			$where[ "imo.data_expira >= '". $data_inicio ."' AND imo.data_expira <= '". $data_fim ."' AND imo.data_expira <> '' "  ] = NULL;

		}
		 if ( !empty($tipo_usuario)) {
		 	$where['us.tipo'] = $tipo_usuario;
		 }
		
		if( !empty( $data_inicio_cadastro ) && !empty( $data_fim_cadastro ) ) {

			$data_inicio = $this->util->date2us( $data_inicio_cadastro );
			$data_inicio = strtotime( $data_inicio );

			$data_fim = $this->util->date2us( $data_fim_cadastro );
			$data_fim = strtotime( $data_fim );

			$where[ "imo.data_cadastro >= '". $data_inicio ."' AND imo.data_cadastro <= '". $data_fim ."'" ] = NULL;

		}

		if( !empty( $ordem ) ) {

			if( isset( $this->config_lista[ 'order' ][ $ordem ] ) ) {

				$order = $this->config_lista[ 'order' ][ $ordem ];

			}

		}
		
		if( !empty( $aprovado )) {

			$where['imo.aprovado'] = $this->config_lista[ 'aprovado' ][ $aprovado ];

		}
		
		if( !empty( $status )) {

			$where['imo.status'] = $this->config_lista[ 'status' ][ $status ];

		}
		
		if( !empty( $expirado )) {
			
			$hoje = time();
			
			if($expirado == 'expirado') {

				$where[ "(imo.data_expira <= '".$hoje."' AND imo.data_expira <> '' )" ] = NULL;
				
			}
			
			if($expirado == 'nao_expirado') {

				$where[ "(imo.data_expira >= '".$hoje."' OR imo.data_expira = '' )" ] = NULL;
				
			}

		}

		if( !empty( $destaque )) {
			$where['imo.destaque'] = $destaque;
		}

		if( !empty( $tipo_anuncio )) {
			$where['imo.tipo'] = $tipo_anuncio;
		}

		if( !empty( $tipo_imovel )) {
			$where['imo.id_tipo_imovel'] = $tipo_imovel;
		}

		if( !empty( $finalidade )) {
			$where['imo.finalidade'] = $finalidade;
		}

		if( !empty( $dormitorios )) {
			$where['imo.dormitorios'] = $dormitorios;
		}

		if( !empty( $suites )) {
			$where['imo.suites'] = $suites;
		}

		if( !empty( $garagem )) {
			$where['imo.garagem'] = $garagem;
		}

		if( !empty( $banheiros )) {
			$where['imo.banheiros'] = $banheiros;
		}

		if( !empty( $cidade )) {

			$d = implode(',', $cidade);
			$where['imo.cidade IN ('.$d.')'] = NULL;

		}

		if( !empty( $estado )) {

			$d = implode(',', $estado);
			$where['imo.estado IN ('.$d.')'] = NULL;

		}

		if( !empty( $imobiliaria ) && $tipo_usuario == 3) {

			$where['imo.id_usuario'] = $imobiliaria;
		}

		if( !empty( $numero_id )) {

			$where = array();
			$where['imo.id'] = $numero_id;

		}
		
		if( !$order ) {

			$order['imo.tipo'] = "ASC";
			$order['imo.id'] = "DESC";
		}

		//$retorno = $this->crud->read_paginacao( $where, $this->tabela, $order, $this->config_lista[ 'qtd_por_pagina' ], $this->pagina, $this->config_lista[ 'num_link' ] );

		// $lista = $retorno[ 'result' ];
		// $paginacao = $retorno[ 'paginacao' ];
		
		$config = array(
			'campos' => 'usa.nome as "nome_usuario", imo.tipo, imo.id as "id_imovel", imo.titulo as "titulo_imovel"',
			'tabela' => 'imoveis imo',
			'where' => $where,
			'join' => $join,
			'orderBy' => $order
		);

		$this->select->set($config);

		$paginacao = $this->select->paginacao( "imoveisaprovar", 5);
		$total_imoveis = $this->select->total();
		$imo = $this->select->resultado();

		// echo $this->db->last_query();
		
		$lista = '';
		$ids = array();
		if($total_imoveis > 0) {
		
			foreach( $imo as $i ) {
				
				$id_imovel = $i->id_imovel;
				$ids[] = $id_imovel;
				$titulo_imovel = $i->titulo_imovel;
				$nome_usuario = $i->nome_usuario;

				$tipos = array(
					'1' => 'Venda',
					'2' => 'Locação',
					'3' => 'Temporada',
				);
			
				$lista .= '<div class="row table">
								<div class="columns small-12 medium-1 large-1"> <p><strong>ID:</strong> '.$id_imovel.'</p> </div>
								<div class="columns small-12 medium-2"> <p><strong>Título:</strong> '.$titulo_imovel.'</p> </div>
								<div class="columns small-12 medium-2"> <p><strong>Tipo:</strong> '.$tipos[$i->tipo].'</p> </div>
								<div class="columns small-12 medium-2">
									<label>
										<input type="checkbox" value="'.$id_imovel.'" class="change-anuncio-linha"/> Anúncio linha
									</label>
								</div>
								<div class="columns small-12 medium-2">
									<label>
										<input type="checkbox" value="'.$id_imovel.'" class="change-anuncio-destaque"/> Anúncio destaque
									</label>
								</div>
								<div class="columns small-12 medium-2">
									<div class="columns small-6 medium-6 large-6 text-right"><a href="'.base_url().$this->pagina.'/read/'.$id_imovel.'" class="ac red">Visualizar</a></div>
								</div>

								<div class="clearfix"></div>
							</div>

							<div class="row"><hr></div>';
				
			}
			
		}
		$ids = implode(',', $ids);
		$q = ( isset( $_SERVER[ 'QUERY_STRING' ] ) && !empty( $_SERVER[ 'QUERY_STRING' ] ) ) ? "?".$_SERVER[ 'QUERY_STRING' ] : '';

		/* Dados dos filtros */

		//Tipo imovel
		$config = array(
			'campos' => 'id as "value", tipo as "label"',
			'tabela' => 'tipo_imovel',
			'orderBy' => array('tipo' => 'ASC')
		);

		$this->select->set($config);
		$tipo_imoveis = $this->select->resultado();

		$select_tipo_imoveis = '';

		foreach($tipo_imoveis as $val ) {			

			$selected = '';
			if($val->value == $tipo_imovel) {
				$selected = 'selected="selected"';
			}

			$select_tipo_imoveis .= '<option value="'.$val->value.'" '.$selected.' >'.$val->label.'</option>';			
		}

		//Imobiliarias

		$config = array(
			'campos' => 'u.id as "value", d.nome as "label"',
			'tabela' => 'usuarios u',
			'where' => 'u.tipo = 3 AND u.ativo=1',
			'join' => array(
				array('dados_imobiliaria d','d.id = u.perfil','left')
			),
			'orderBy' => array('d.nome' => 'ASC')
		);

		$this->select->set($config);
		$imobiliarias = $this->select->resultado();

		$select_imobiliarias = '<option value="">Selecione a Imobiliária</option>';

		foreach($imobiliarias as $val ) {			

			$selected = '';
			if($val->value == $imobiliaria) {
				$selected = 'selected="selected"';
			}

			$select_imobiliarias .= '<option value="'.$val->value.'" '.$selected.' >'.$val->label.'</option>';			
		}

		//Finalidade
		$config = array(
			'campos' => 'id as "value", tipo as "label"',
			'tabela' => 'finalidade',
			'orderBy' => array('tipo' => 'ASC')
		);

		$this->select->set($config);
		$finalidades = $this->select->resultado();

		$select_finalidade = '';

		foreach($finalidades as $val ) {			

			$selected = '';
			if($val->value == $finalidade) {
				$selected = 'selected="selected"';
			}

			$select_finalidade .= '<option value="'.$val->value.'" '.$selected.' >'.$val->label.'</option>';			
		}

		//Estados
		$config = array(
			'campos' => 'uf_codigo as value, uf_descricao as label',
			'tabela' => 'cep_uf'
		);

		$this->select->set( $config );
		$estados = $this->select->resultado();

		$estadoSelect = '';

		foreach($estados as $val ) {

			$selected = '';

			if(!empty($estado)) {
				if(in_array($val->value, $estado)) {
					$selected = 'selected="selected"';
				}
			}

			$estadoSelect .= '<option value="'.$val->value.'" '.$selected.' >'.$val->label.'</option>';			
		}

		$select_cidades = '';
		if(!empty($estado)) {
			$est = implode(',', $estado);

			$config = array(
				'campos' => 'cidade_codigo as value, cidade_descricao as label',
				'tabela' => 'cep_cidade',
				'where' => array('uf_codigo IN ('.$est.') ' => NULL)
			);

			$this->select->set( $config );
			$cidades = $this->select->resultado();

			foreach($cidades as $val ) {

				$selected = '';

				if(!empty($cidade)) {
					if(in_array($val->value, $cidade)) {
						$selected = 'selected="selected"';
					}
				}

				$select_cidades .= '<option value="'.$val->value.'" '.$selected.' >'.$val->label.'</option>';			
			}

		} 

		$data = array(

			"select_tipo_imoveis" => $select_tipo_imoveis,
			"select_finalidade" => $select_finalidade,
			"select_estados" => $estadoSelect,
			"select_cidades" => $select_cidades,
			"select_imobiliarias" => $select_imobiliarias,

			"lista" => $lista,
			"pagina" => $this->pagina,
			"paginacao" => $paginacao,
			"val_get" => $q,
			"ids" => $ids,
		);

		$this->load( $this->pagina .'/index', $data );

	}

	// Edição
	public function read( $id ) {

		$item = $this->crud->read( array( 'id'=> $id ), $this->tabela, array( 'id' => 'DESC' ) );

		$config = array(
			'campos' => 'dp.nome as "nome_usuario", us.id as "id_usuario", imo.id as "id_imovel", imo.titulo as "titulo_imovel", imo.data_expira, imo.ativo, imo.descricao1, imo.descricao2 , imo.descricao3, imo.aprovado as "aprovado"',
			'tabela' => 'imoveis imo',
			'where' => array( "imo.id" => $id ),
			'join' => array(
						array('usuarios us','us.id = imo.id_usuario','left'),
						array('dados_proprietario dp','dp.id = us.perfil','left')
					),
			'limit' => 1
		);

		$this->select->set($config);
		$item = $this->select->resultado();

		if( empty( $item ) ) {
			redirect( base_url(). $this->pagina );
		}

		$item = $item[0];

		$campos_form_esquerdo = array(
			"titulo" => array(
				"type" => "text",
				"label" => "Imóvel",
				"name" => "titulo",
				"value" => $item->titulo_imovel,
				"width" => "12",
				"disabled" => true
			),
			"usuario" => array(
				"type" => "text",
				"label" => "Usuário",
				"name" => "usuario",
				"value" => $item->nome_usuario,
				"width" => "6",
				"disabled" => true
			),
			"data_expira" => array(
				"type" => "text",
				"label" => "Data de Expiração",
				"name" => "data_expira",
				"value" => ( !empty( $item->data_expira ) ? date( "d/m/Y", $item->data_expira ) : "N/D" ),
				"width" => "6",
				"disabled" => true
			),
			"descricao1" => array(
				"type" => "textarea",
				"label" => "Descrição 1",
				"name" => "descricao1",
				"value" => $item->descricao1,
				"width" => "12"
			),
			"descricao2" => array(
				"type" => "textarea",
				"label" => "Descrição 2",
				"name" => "descricao2",
				"value" => $item->descricao2,
				"width" => "12"
			),
			"descricao3" => array(
				"type" => "textarea",
				"label" => "Descrição 3",
				"name" => "descricao3",
				"value" => $item->descricao3,
				"width" => "12"
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens
		$path_image = base_url() ."../assets/uploads/noticias/thumb/";

		$arquivos = array(
			"imagem" => array(
				"type" => "image",
				"label" => "Imagem",
				"name" => "imagem",
				"value" => ( !empty( $item->imagem ) ) ? $path_image. $item->imagem : "",
				"width" => $this->config_image[ 'imagem' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'imagem' ][ 'config' ][ 'altura' ]
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );
		
		if($item->ativo == 1) {
			$ativo = 'selected="selected"';
			$inativo = '';
		} else {
			$ativo = '';
			$inativo = 'selected="selected"';
		}
		
		$select_ativo = '<select name="ativo" id="slcPublicar" class="required"/>
							<option value="1" '.$ativo.'>Ativo</option>
							<option value="0" '.$inativo.'>Inativo</option>
						</select>';
						
		// Imagens
		
		$config = array(
			'campos' => 'arquivo',
			'tabela' => 'imagens',
			'where' => array('id_imovel' => $id)
		);

		$this->select->set($config);
		$imagens = $this->select->resultado();

		/* Verifica se a imagem existe */
		foreach($imagens as $x => $val) {

			$arquivo = trim($val->arquivo);
			$path = '/assets/uploads/imovel/'.$item->id_usuario.'/';

			if(file_exists( $_SERVER['DOCUMENT_ROOT'].$path.$arquivo )) {
				$imagens[$x]->arquivo = '/../../..'.$path.$arquivo;
			} else {
				$imagens[$x]->arquivo = $arquivo;
			}

		}

		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"pagina" => $this->pagina,
			"id_usuario" => $item->id_usuario,
			"id" => $id,
			"select_ativo" => $select_ativo,
			"imagens" => $imagens
		);

		$this->load( $this->pagina .'/read', $data );

	}

	public function create() {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {
			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}
		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		if( $id = $this->crud->create( $_POST, $this->tabela ) ) {

			// Galeria
			if( $this->config_galeria[ 'galeria' ] ) {

				$galeria_id = $this->session->userdata( 'galeria_id' );
				$this->imput_form->galeria_update( $id, $galeria_id );

			}

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item adicionado com sucesso!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao adicionar o item, tente novamente!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		}

		echo json_encode( $retorno );

	}


	public function update( $id ) {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {

			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}

		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Faz e verifica se fez o update
		if( $this->crud->update( array( "id" => $id ), $_POST, $this->tabela ) ) {

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina."/read/".$id
			);

		}

		echo json_encode( $retorno );

	}

	public function delete( $id ) {

		// Faz e verifica se fez o delete
		$this->crud->delete( array( "id" => $id ), $this->tabela );
		$this->session->set_flashdata( "msg_sucesso", "Item deletado com sucesso!" );
		redirect( base_url().$this->pagina );

	}

	public function image( $campo ) {

		$src = $this->image_form->upload_imagem( $_FILES[ $campo ], $this->config_image[ $campo ][ 'config' ] );

		$this->session->set_userdata( $campo, $src );

		if( $this->config_image[ $campo ][ 'config' ][ 'thumb' ] ) {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].'thumb/'.$src;

		} else {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].$src;

		}

	}

	public function file( $campo ) {

		$this->load->library( 'upload', $this->config_arquivo[ $campo ][ 'config' ] );

		if( $this->upload->do_upload( $campo ) ) {

			$data = $this->upload->data();
			$name = $data[ 'file_name' ];

			$this->session->set_userdata( $campo, $name );

			echo base_url().$this->config_arquivo[ $campo ][ 'config' ][ 'upload_path' ].$name;

		} else {

			echo '0';

		}

	}

	public function galeria() {

		$src = $this->image_form->upload_imagem( $_FILES[ 'arquivo' ], $this->config_galeria[ 'config' ] );

		$_POST[ 'arquivo' ] = $src;

		$id = $this->imput_form->galeria_insert( $_POST );

		$galeria_id = $_POST[ 'elemento' ];

		$session = $this->session->userdata( $galeria_id );

		$session[] = $id;

		$this->session->set_userdata( $galeria_id, $session );
		$this->session->set_userdata( 'galeria_id', $galeria_id );
// var_dump($galeria_id);
		$json = array(
			"src" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].'thumb/'.$src,
			"src_full" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].$src,
			"id" => $id,
			"pagina" => $this->pagina,
			"session" => $session,
			"id" => $id
		);

		echo json_encode( $json );

	}

	public function galeria_legenda() {

		$id = $this->imput_form->galeria_legenda( $_POST[ 'id' ], $_POST[ 'legenda' ] );

	}

	public function galeria_delete() {

		$id = $this->imput_form->galeria_delete( $_POST[ 'id' ] );

	}
	
	public function aprovarVendedor( $id ) {
	
		$hoje = time();
		
		$login = $this->session->userdata( "login_admin" );
	
		$dados = array(
			'data_cadastro' => $hoje,
			'aprovado' => 0,
			'id_imovel' => $id,
			'id_vendedor' => $login['id_usuario']
		);

		$this->crud->create( $dados, 'imoveis_aprovados_vendedor' );
		
		$dados = array(
			'aprovado_vendedor' => 1
		);
		
		$this->crud->update( array( "id" => $id ), $dados, 'imoveis' );

		redirect( base_url(). $this->pagina );

	}
	
	public function recusar( $id ) {
	
		$dados = array(
			'status' => 1,
			'aprovado' => 0,
			'data_expira' => 0
		);

		$this->crud->update( array( "id" => $id ), $dados, 'imoveis' );
		
		redirect( base_url(). $this->pagina );

	}

	public function download() {

		$busca = $this->input->get( "busca" );
		$busca_usuario = $this->input->get( "busca_usuario" );
		$filtro = $this->input->get( "filtro" );
		$ordem = $this->input->get( "ordem" );
		$aprovado = $this->input->get( "aprovado" );
		$status = $this->input->get( "status" );

		if( !empty( $filtro )) {

			$where = $this->config_lista[ 'filtro' ][ $filtro ];

		}

		if( !empty( $busca ) ) {

			$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%'" ] = NULL;

		}

		if( !empty( $busca_usuario ) ) {

			$where[ $this->config_lista[ 'busca_usuario' ] ." LIKE '%".$busca_usuario."%'" ] = NULL;

		}

		if( !empty( $ordem ) ) {

			if( isset( $this->config_lista[ 'order' ][ $ordem ] ) ) {

				$order = $this->config_lista[ 'order' ][ $ordem ];

			}

		}

		if( !empty( $aprovado )) {

			$where['imo.aprovado'] = $this->config_lista[ 'aprovado' ][ $aprovado ];

		}

		if( !empty( $status )) {

			$where['imo.status'] = $this->config_lista[ 'status' ][ $status ];

		}

		// $where['imo.status'] = 3;
		// $where['imo.aprovado'] = 0;
		$where['us.tipo'] = 1;
		$order['imo.id'] = "DESC";

		//$retorno = $this->crud->read_paginacao( $where, $this->tabela, $order, $this->config_lista[ 'qtd_por_pagina' ], $this->pagina, $this->config_lista[ 'num_link' ] );

		// $lista = $retorno[ 'result' ];
		// $paginacao = $retorno[ 'paginacao' ];

		$config = array(
			'campos' => 'dp.nome as "nome_usuario", imo.id as "id_imovel", imo.titulo as "titulo_imovel", imo.status as "status", imo.aprovado as "aprovado", imo.data_expira, imo.email',
			'tabela' => 'imoveis imo',
			'where' => $where,
			'join' => array(
						array('usuarios us','us.id = imo.id_usuario','left'),
						array('dados_proprietario dp','dp.id = us.perfil','left')
					),
			'orderBy' => $order
		);

		$this->select->set($config);
		$total_imoveis = $this->select->total();
		$imo = $this->select->resultado();

		$lista = '';

		if($total_imoveis > 0) {

			foreach( $imo as $i ) {

				$id_imovel = $i->id_imovel;
				$titulo_imovel = $i->titulo_imovel;
				$nome_usuario = $i->nome_usuario;

				if($i->aprovado == 1) {
					$aprovado = 'Aprovado';
				} else {
					$aprovado = 'Nao Aprovado';
				}

				if($i->status == 1) { $status = 'Aguar. Pagamento'; }
				if($i->status == 2) { $status = 'Aguar. Aprovacao'; }
				if($i->status == 3) { $status = 'Publicado'; }
				if($i->status == 4) { $status = 'Cancelado'; }

				$lista .= '
					<tr>
						<td>'.$titulo_imovel.'</td>
						<td>'.$nome_usuario.'</td>
						<td>'.$i->email.'</td>
						<td>'.$aprovado.'</td>
						<td>'.$status.'</td>
						<td>'.( ( !empty( $i->data_expira ) ) ? date( "d/m/Y", $i->data_expira ) : "N/D") .'</td>

					</tr>
				';

			}

		}

		$data = array(
			'lista_pagamentos' => $lista
		);

		$table = $this->parser->parse( $this->pagina.'/table', $data, true );
		$this->util->excel( $table, "anuncios_". date( "d-m-Y" ) .".xls" );

	}

	public function linha(){

		$ids = $this->input->get('ids');

		if(!empty($ids)){
			
			$where['imo.id in ('.$ids.')'] = NULL;
		
			$order['imo.tipo'] = "ASC";
			$order['imo.id'] = "DESC";
			
			$config = array(
				'campos' => 'us.tipo as "tipo_usuario", us.perfil as "usuario_perfil", imo.tipo, ti.tipo as "tipo_imovel", imo.id as "id_imovel", 
				imo.titulo as "titulo_imovel", imo.valor as "valor_imovel", 
				imo.dormitorios, imo.suites, imo.garagem, imo.banheiros, imo.area_m2, imo.cep, imo.numero, imo.complemento, imo.descricao1, imo.data_cadastro, 
				fi.tipo as "finalidade", 
				cep_rua.endereco_logradouro as "rua_texto", cep_bairro.bairro_descricao as "bairro_texto", cep_cidade.cidade_descricao as "cidade_texto", cep_uf.uf_descricao as "uf_texto"',
				'tabela' => 'imoveis imo',
				'where' => $where,
				'join' => array(
							array('imoveis_aprovados_vendedor apr','apr.id_imovel = imo.id','left'),
							array('usuarios_admin usa','usa.id = apr.id_vendedor','left'),
							array('usuarios us','us.id = imo.id_usuario','left'),
							array('tipo_imovel ti','ti.id = imo.id_tipo_imovel','left'),
							array('finalidade fi','fi.id = imo.finalidade','left'),
							array('cep_endereco cep_rua','cep_rua.endereco_codigo = imo.rua','left'),
							array('cep_bairro cep_bairro','cep_bairro.bairro_codigo = imo.bairro','left'),
							array('cep_cidade cep_cidade','cep_cidade.cidade_codigo = imo.cidade','left'),
							array('cep_uf cep_uf','cep_uf.uf_codigo = imo.estado','left'),
						),
				'orderBy' => $order
			);

			$this->select->set($config);
			$paginacao = $this->select->paginacao( "imoveisaprovar", 5);
			$total_imoveis = $this->select->total();
			$imo = $this->select->resultado();

			$imoveis_export = array();
			foreach($imo as $i){
				$imoveis_export[$i->tipo][] = $i;
			}
			$table = '
			<table>
				<thead>
					<tr>
						<th style="border:1px solid #ccc;">ID</th>

						<th style="border:1px solid #ccc;">TIPO USUÁRIO</th>

						<th style="border:1px solid #ccc;">EMAIL CONTATO</th>
						<th style="border:1px solid #ccc;">TELEFONE CONTATO</th>

						<th style="border:1px solid #ccc;">TITULO</th>

						<th style="border:1px solid #ccc;">TIPO ANUNCIO</th>
						<th style="border:1px solid #ccc;">TIPO IMOVÉL</th>
						<th style="border:1px solid #ccc;">FINALIDADE</th>

						<th style="border:1px solid #ccc;">DORMITÓRIOS</th>
						<th style="border:1px solid #ccc;">SUITES</th>
						<th style="border:1px solid #ccc;">GARAGEM</th>
						<th style="border:1px solid #ccc;">BANHEIROS</th>

						<th style="border:1px solid #ccc;">AREA M2</th>

						<th style="border:1px solid #ccc;">CEP</th>
						<th style="border:1px solid #ccc;">RUA</th>
						<th style="border:1px solid #ccc;">NUMERO</th>
						<th style="border:1px solid #ccc;">COMPLEMENTO</th>
						<th style="border:1px solid #ccc;">BAIRRO</th>
						<th style="border:1px solid #ccc;">CIDADE</th>
						<th style="border:1px solid #ccc;">ESTADO</th>

						<th style="border:1px solid #ccc;">DESCRIÇÃO</th>
						<th style="border:1px solid #ccc;">VALOR</th>

						<th style="border:1px solid #ccc;">DATA CADASTRO</th>
					</tr>
				</thead>
				<tbody>
			';

			$tipos = array(
				'1' => 'Venda',
				'2' => 'Locação',
				'3' => 'Temporada',
			);

			$tipos_usuario = array(
				'1' => 'Proprietário',
				'2' => 'Corretor',
				'3' => 'Imobiliária',
			);

			foreach($imoveis_export as $k => $tipo){

				$table .= '
					<tr>
						<td colspan="4"><strong>'.$tipos[$k].'</strong></td>
					</tr>
				';
				foreach($tipo as $t){

					switch($t->tipo_usuario) {

						case 1: //Proprietario
							$tab_dados = 'dados_proprietario';
						break;

						case 2: //Corretor
							$tab_dados = 'dados_corretor';
						break;

						case 3: //Imobiliaria
							$tab_dados = 'dados_imobiliaria';
						break;

					}

					$config = array(
						'campos' => 'd.telefone, d.email',
						'tabela' => $tab_dados.' d',
						'where' => array('d.id' => $t->usuario_perfil)
					);

					$this->select->set($config);
					$user = $this->select->resultado();

					$table .='
						<tr>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->id_imovel.'</td>

							<td style="border:1px solid #ccc; padding: 0 10px;">'.$tipos_usuario[$t->tipo_usuario].'</td>

							<td style="border:1px solid #ccc; padding: 0 10px;">'.$user[0]->email.'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$user[0]->telefone.'</td>

							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->titulo_imovel.'</td>

							<td style="border:1px solid #ccc; padding: 0 10px;">'.$tipos[$t->tipo].'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->tipo_imovel.'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->finalidade.'</td>

							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->dormitorios.'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->suites.'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->garagem.'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->banheiros.'</td>

							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->area_m2.'</td>

							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->cep.'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->rua_texto.'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->numero.'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->complemento.'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->bairro_texto.'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->cidade_texto.'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->uf_texto.'</td>

							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->descricao1.'</td>
							<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->valor_imovel.'</td>

							<td style="border:1px solid #ccc; padding: 0 10px;">'.date('d/m/Y', $t->data_cadastro).'</td>
						</tr>
					';
				}
			}
			$table .= '
				</tbody>
			</table>
			';

			$destino = 'Anuncios_linha_'.date('d-m-Y_h:i').'.xls';

			header("Content-type: application/msexcel; charset=utf-8");
			header("Content-Disposition: attachment; filename=". $destino );

			print utf8_decode($table);
		} else {
			echo 'Consulta inválida';
		}
	}

	public function export_destaque(){

		$ids = $this->input->get('ids');

		if(!empty($ids)){
			
			$where['imo.id in ('.$ids.')'] = NULL;
			$where['img.destaque'] = 1;
		
			$order['imo.tipo'] = "ASC";
			$order['imo.id'] = "DESC";
			
			$config = array(
				'campos' => 'us.tipo as "tipo_usuario", us.perfil as "usuario_perfil", us.id as "id_usuario", imo.tipo, ti.tipo as "tipo_imovel", imo.id as "id_imovel", 
				imo.titulo as "titulo_imovel", imo.valor as "valor_imovel", 
				imo.dormitorios, imo.suites, imo.garagem, imo.banheiros, imo.area_m2, imo.cep, imo.numero, imo.complemento, imo.descricao1, imo.data_cadastro, 
				fi.tipo as "finalidade", 
				cep_rua.endereco_logradouro as "rua_texto", cep_bairro.bairro_descricao as "bairro_texto", cep_cidade.cidade_descricao as "cidade_texto", cep_uf.uf_descricao as "uf_texto",
				img.arquivo as "imagem"',
				'tabela' => 'imoveis imo',
				'where' => $where,
				'join' => array(
							array('imoveis_aprovados_vendedor apr','apr.id_imovel = imo.id','left'),
							array('usuarios_admin usa','usa.id = apr.id_vendedor','left'),
							array('usuarios us','us.id = imo.id_usuario','left'),
							array('tipo_imovel ti','ti.id = imo.id_tipo_imovel','left'),
							array('finalidade fi','fi.id = imo.finalidade','left'),
							array('cep_endereco cep_rua','cep_rua.endereco_codigo = imo.rua','left'),
							array('cep_bairro cep_bairro','cep_bairro.bairro_codigo = imo.bairro','left'),
							array('cep_cidade cep_cidade','cep_cidade.cidade_codigo = imo.cidade','left'),
							array('cep_uf cep_uf','cep_uf.uf_codigo = imo.estado','left'),
							array('imagens img','img.id_imovel = imo.id','left'),
						),
				'orderBy' => $order
			);

			$this->select->set($config);
			$paginacao = $this->select->paginacao( "imoveisaprovar", 5);
			$total_imoveis = $this->select->total();
			$imo = $this->select->resultado();

			$imoveis_export = array();
			foreach($imo as $i){
				$imoveis_export[$i->tipo][] = $i;
			}
			$table = '
			<table>
				<thead>
					<tr>
						<th style="border:1px solid #ccc;">ID</th>

						<th style="border:1px solid #ccc;">TIPO USUÁRIO</th>

						<th style="border:1px solid #ccc;">EMAIL CONTATO</th>
						<th style="border:1px solid #ccc;">TELEFONE CONTATO</th>

						<th style="border:1px solid #ccc;">TITULO</th>

						<th style="border:1px solid #ccc;">TIPO ANUNCIO</th>
						<th style="border:1px solid #ccc;">TIPO IMOVÉL</th>
						<th style="border:1px solid #ccc;">FINALIDADE</th>

						<th style="border:1px solid #ccc;">DORMITÓRIOS</th>
						<th style="border:1px solid #ccc;">SUITES</th>
						<th style="border:1px solid #ccc;">GARAGEM</th>
						<th style="border:1px solid #ccc;">BANHEIROS</th>

						<th style="border:1px solid #ccc;">AREA M2</th>

						<th style="border:1px solid #ccc;">CEP</th>
						<th style="border:1px solid #ccc;">RUA</th>
						<th style="border:1px solid #ccc;">NUMERO</th>
						<th style="border:1px solid #ccc;">COMPLEMENTO</th>
						<th style="border:1px solid #ccc;">BAIRRO</th>
						<th style="border:1px solid #ccc;">CIDADE</th>
						<th style="border:1px solid #ccc;">ESTADO</th>

						<th style="border:1px solid #ccc;">DESCRIÇÃO</th>
						<th style="border:1px solid #ccc;">VALOR</th>

						<th style="border:1px solid #ccc;">DATA CADASTRO</th>
						<th style="border:1px solid #ccc;">IMAGEM</th>
					</tr>
				</thead>
				<tbody>
			';

			$tipos = array(
				'1' => 'Venda',
				'2' => 'Locação',
				'3' => 'Temporada',
			);

			$tipos_usuario = array(
				'1' => 'Proprietário',
				'2' => 'Corretor',
				'3' => 'Imobiliária',
			);

			/* Configuração do Arquivo ZIP */
			// Instancia a Classe Zip
			$zip = new ZipArchive();

			$caminho_zip = 'assets/uploads/zip_imagens/';
			$arquivo_zip = date('d-m-Y').'-exportacao_destaque.zip';

			// Cria o Arquivo Zip, caso não consiga exibe mensagem de erro e finaliza script
			if($zip->open( $caminho_zip.$arquivo_zip , ZIPARCHIVE::CREATE) == TRUE) {

				foreach($imoveis_export as $k => $tipo){

					$table .= '
						<tr>
							<td colspan="4"><strong>'.$tipos[$k].'</strong></td>
						</tr>
					';

					foreach($tipo as $t){

						/* Variaveis das imagens em zip */

						$diretorio = '../assets/uploads/imovel/'.$t->id_usuario.'/';

						$nome_arquivo = 'imagem_destaque_'.$t->id_imovel.'.jpg';

						$imagem = trim($t->imagem);

						if(file_exists( $diretorio.$imagem )){
							$zip->addFile( $diretorio.$imagem, $nome_arquivo );
						}

						switch($t->tipo_usuario) {

							case 1: //Proprietario
								$tab_dados = 'dados_proprietario';
							break;

							case 2: //Corretor
								$tab_dados = 'dados_corretor';
							break;

							case 3: //Imobiliaria
								$tab_dados = 'dados_imobiliaria';
							break;

						}

						$config = array(
							'campos' => 'd.telefone, d.email',
							'tabela' => $tab_dados.' d',
							'where' => array('d.id' => $t->usuario_perfil)
						);

						$this->select->set($config);
						$user = $this->select->resultado();

						$table .='
							<tr>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->id_imovel.'</td>

								<td style="border:1px solid #ccc; padding: 0 10px;">'.utf8_decode($tipos_usuario[$t->tipo_usuario]).'</td>

								<td style="border:1px solid #ccc; padding: 0 10px;">'.$user[0]->email.'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.$user[0]->telefone.'</td>

								<td style="border:1px solid #ccc; padding: 0 10px;">'.utf8_decode($t->titulo_imovel).'</td>

								<td style="border:1px solid #ccc; padding: 0 10px;">'.utf8_decode($tipos[$t->tipo]).'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.utf8_decode($t->tipo_imovel).'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.utf8_decode($t->finalidade).'</td>

								<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->dormitorios.'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->suites.'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->garagem.'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->banheiros.'</td>

								<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->area_m2.'</td>

								<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->cep.'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.utf8_decode($t->rua_texto).'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->numero.'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.utf8_decode($t->complemento).'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.utf8_decode($t->bairro_texto).'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.utf8_decode($t->cidade_texto).'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.utf8_decode($t->uf_texto).'</td>

								<td style="border:1px solid #ccc; padding: 0 10px;">'.utf8_decode($t->descricao1).'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.$t->valor_imovel.'</td>

								<td style="border:1px solid #ccc; padding: 0 10px;">'.date('d/m/Y', $t->data_cadastro).'</td>
								<td style="border:1px solid #ccc; padding: 0 10px;">'.$nome_arquivo.'</td>
							</tr>
						';
					}
				}

				$table .= '
					</tbody>
				</table>
				';

				$destino = 'Anuncios_linha_'.date('d-m-Y').'.xls';
				$fp = fopen( $caminho_zip. "/".$destino, "wb");
				fwrite($fp,$table);
				fclose($fp);

				$zip->addFile( $caminho_zip."/".$destino, $destino );

				$zip->close();

				header( 'Location: '.base_url().$caminho_zip.$arquivo_zip );
					
			} else {
    			var_dump($zip);
			}

		} else {
			echo 'Consulta inválida';
		}
	}

}