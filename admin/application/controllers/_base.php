<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends Main_Controller {

	private $pagina = "usuarios";
	private $tabela = "usuarios_admin";

	private $config_lista = array(
		'busca' => 'nome',
		'filtro' => 'ativo',
		'order' => array(
			'email' => array( 'id' => 'ASC' ),
			'email2' => array( 'id' => 'DESC' )
		),
		'num_link' => 1
	);

	// Configurações de arquivos
	private $config_arquivo = array(
		'arquivo-1' => array(
			'campo' => 'arquivo-1',
			'config' => array(
				'upload_path' => 'assets/images/',
				'allowed_types' => 'pdf|zip|rar|txt|docx',
				'max_size' => '1000',
				'file_name' => 'titulo-do-arquivo',
				'remove_spaces' => true,
				// 'encrypt_name' => true //nome do arquivo vira hash,
			),
		)
	);

	// Configurações para imagem
	private $config_image = array(
		'image' => array(
			'campo' => 'image',
			'config' => array(
				'thumb' => array(
					'greyscale' => true, // bolean
					'largura' => 100, // num
					'altura' => 100 // num
				),
				'marca' => array(
					'src' => "assets/images/apple-touch-icon.png", // strig [ caminho para marca d'agua ]
					'x' => 260, // num [ posição X da marca ]
					'y' => 260 // num [ posição Y da marca ]
				),
				// 'thumb' => false,
				// 'marca' => false,
				'image_path' => 'assets/images/', // string [ caminho da imagem é necessário ter uma pasta thumb (se tiver)]
				'largura' => 300, // num
				'altura' => 300, // num

				'redimensionar' => true, // boolean [ redimensiona a imagem ]
				'proporcional' => false, // boolean [ redimenciona proporcionalmente ]
				'cortar' => true, // boolean [ corta a imagem sem redimencionar ]
				'converter' => false, // boolean [ converte a image para jpg ]
				'greyscale' => false, // boolean [ coloca efeito greyscale ]
				// 'qualidade' => 80 // num [ configura a qualidade da imagem ]
			)
		),
		'image-2' => array(
			'campo' => 'image-2',
			'config' => array(
				'thumb' => false,
				'marca' => false,
				'image_path' => 'assets/images/', // string [ caminho da imagem é necessário ter uma pasta thumb (se tiver)]
				'largura' => 200, // num
				'altura' => 300, // num
				'redimensionar' => true, // boolean [ redimensiona a imagem ]
				'proporcional' => false, // boolean [ redimenciona proporcionalmente ]
				'cortar' => true, // boolean [ corta a imagem sem redimencionar ]
				'converter' => false, // boolean [ converte a image para jpg ]
				'greyscale' => false, // boolean [ coloca efeito greyscale ]
				// 'qualidade' => 80 // num [ configura a qualidade da imagem ]
			)
		)

	);

	// Configurações para galeria
	private $config_galeria = array(

		'galeria' => '1',
		'config' => array(
			'thumb' => array(
				'greyscale' => false, // bolean
				'largura' => 100, // num
				'altura' => 100 // num
			),
			'marca' => false,
			'image_path' => 'assets/images/', // string [ caminho da imagem é necessário ter uma pasta thumb (se tiver)]
			'largura' => 200, // num
			'altura' => 300, // num
			'redimensionar' => true, // boolean [ redimensiona a imagem ]
			'proporcional' => false, // boolean [ redimenciona proporcionalmente ]
			'cortar' => true, // boolean [ corta a imagem sem redimencionar ]
			'converter' => false, // boolean [ converte a image para jpg ]
			'greyscale' => false, // boolean [ coloca efeito greyscale ]
			// 'qualidade' => 80 // num [ configura a qualidade da imagem ]
		)

	);

	public function __construct() {

		parent::__construct();
		$this->checkLogin();

	}

	public function index() {

		$busca = $this->input->get( "busca" );
		$filtro = $this->input->get( "filtro" );
		$ordem = $this->input->get( "ordem" );

		$where = array();
		$order = array( "id" => "ASC" );

		if( !empty( $filtro ) || $filtro === '0' ) {

			$where[ $this->config_lista[ 'filtro' ] ] = $filtro;

		}

		if( !empty( $busca ) ) {

			$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%'" ] = NULL;

		}

		if( !empty( $ordem ) ) {

			if( isset( $this->config_lista[ 'order' ][ $ordem ] ) ) {

				$order = $this->config_lista[ 'order' ][ $ordem ];

			}

		}

		$retorno = $this->crud->read_paginacao( $where, 'usuarios_admin', $order, 1, $this->pagina, $this->config_lista[ 'num_link' ] );

		$lista = $retorno[ 'result' ];
		$paginacao = $retorno[ 'paginacao' ];


		$data = array(
			"lista" => $lista,
			"pagina" => $this->pagina,
			"paginacao" => $paginacao
		);

		$this->load( 'usuarios/index', $data );

	}

	// Add
	public function add() {

		$campos_form_esquerdo = array(
			"nome" => array(
				"type" => "text",
				"label" => "Nome",
				"name" => "nome",
				"value" => "",
				"width" => "12"
			),
			"data_cadastro" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "data_cadastro",
				"value" => time(),
				"width" => "12"
			),
			"ativo" => array(
				"type" => "text",
				"label" => "Ativo",
				"name" => "ativo",
				"value" => "1",
				"width" => "12"
			),
			"email" => array(
				"type" => "text",
				"label" => "E-mail",
				"name" => "email",
				"value" => "",
				"width" => "12"
			),
			"senha" => array(
				"type" => "password",
				"label" => "Senha",
				"name" => "senha",
				"value" => "",
				"width" => "12"
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens
		$path_image = base_url() ."assets/images/thumb/";

		$arquivos = array(
			"image" => array(
				"type" => "image",
				"label" => "Imagem Principal",
				"name" => "image",
				"value" => "",
				"width" => $this->config_image[ 'image' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'image' ][ 'config' ][ 'altura' ]
			),
			"image-2" => array(
				"type" => "image",
				"label" => "Imagem Segundaria",
				"name" => "image-2",
				"value" => "",
				"width" => $this->config_image[ 'image-2' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'image-2' ][ 'config' ][ 'altura' ]
			),
			"arquivo-1" => array(
				"type" => "arquivo",
				"label" => "Arquivo de Teste",
				"name" => "arquivo-1",
				"value" => "",
				"ext" => ".pdf | .zip | .rar | .txt | .docx"
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );

		$image_path = $this->config_galeria[ 'config' ][ 'image_path' ];
		$image_width = $this->config_galeria[ 'config' ][ 'largura' ];
		$image_height = $this->config_galeria[ 'config' ][ 'altura' ];

		$form_galeria = $this->imput_form->galeria( $image_path, $image_width, $image_height, $this->pagina, $this->config_galeria['galeria'], false );

		// Categorias
		$categorias = $this->imput_form->categorias( $this->pagina, $this->tabela, false);

		// Tags
		$tags = $this->imput_form->tags( $this->pagina, $this->tabela, false);


		// Filtros categorias
		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"form_galeria" => $form_galeria,
			"categorias" => $categorias,
			"tags" => $tags,
			"pagina" => $this->pagina
		);

		// $this->imput_form->categorias_update( $this->tabela, 1, array( "1", "4", "7" ) );
		// $this->imput_form->tags_update( $this->tabela, 1, array( "1", "4", "7" ) );

		$this->load( 'usuarios/read', $data );

	}

	// Edição
	public function read( $id ) {

		$item = $this->crud->read( array( 'id'=> $id ), 'usuarios_admin' );

		if( empty( $item ) ) {
			redirect( base_url(). $this->pagina );
		}

		$item = $item[0];

		$campos_form_esquerdo = array(
			"nome" => array(
				"type" => "text",
				"label" => "Nome",
				"name" => "nome",
				"value" => $item->nome,
				"width" => "12"
			),
			"data_cadastro" => array(
				"type" => "rtext",
				"label" => "Dada de Cadastro",
				"name" => "data_cadastro",
				"value" => $item->data_cadastro,
				"width" => "12"
			),
			"ativo" => array(
				"type" => "hidden",
				"label" => "Ativo",
				"name" => "ativo",
				"value" => $item->ativo,
				"width" => "12"
			),
			"email" => array(
				"type" => "text",
				"label" => "E-mail",
				"name" => "email",
				"value" => $item->email,
				"width" => "12"
			),
			"senha" => array(
				"type" => "password",
				"label" => "Senha",
				"name" => "senha",
				"value" => "",
				"width" => "12"
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens
		$path_image = base_url() ."assets/images/thumb/";

		$arquivos = array(
			"image" => array(
				"type" => "image",
				"label" => "Imagem Principal",
				"name" => "image",
				"value" => ( !empty( $item->image ) ) ? $path_image. $item->image : "",
				"width" => $this->config_image[ 'image' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'image' ][ 'config' ][ 'altura' ]
			),
			"image-2" => array(
				"type" => "image",
				"label" => "Imagem Segundaria",
				"name" => "image-2",
				"value" => "",
				"width" => $this->config_image[ 'image-2' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'image-2' ][ 'config' ][ 'altura' ]
			),
			"arquivo-1" => array(
				"type" => "arquivo",
				"label" => "Arquivo de Teste",
				"name" => "arquivo-1",
				"value" => "",
				"ext" => ".pdf | .zip | .rar | .txt | .docx"
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );

		$image_path = $this->config_galeria[ 'config' ][ 'image_path' ];
		$image_width = $this->config_galeria[ 'config' ][ 'largura' ];
		$image_height = $this->config_galeria[ 'config' ][ 'altura' ];

		$form_galeria = $this->imput_form->galeria( $image_path, $image_width, $image_height, $this->pagina, $item->id, 2 );

		// Categorias
		$categorias = $this->imput_form->categorias( $this->pagina, $this->tabela, 1);

		// Tags
		$tags = $this->imput_form->tags( $this->pagina, $this->tabela, 1);

		// Checks
		$itens_check = array(
			array(
				'label' => 'Teste 1',
				'value' => '1'
			),
			array(
				'label' => 'Teste 2',
				'value' => '2'
			),
			array(
				'label' => 'Teste 3',
				'value' => '3'
			),
		);
		$checks = $this->imput_form->check( 'Teste', 'check_teste', $itens_check, array( 2, 3 ) );

		// Select
		$itens_select = array(
			array(
				'label' => 'Teste 1',
				'value' => '1'
			),
			array(
				'label' => 'Teste 2',
				'value' => '2'
			),
			array(
				'label' => 'Teste 3',
				'value' => '3'
			),
		);
		$select = $this->imput_form->select( 'Teste', 'select_teste', $itens_select, 1 );

		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"form_galeria" => $form_galeria,
			"categorias" => $categorias,
			"tags" => $tags,
			"checks" => $checks,
			"select" => $select,
			"pagina" => $this->pagina,
			"id" => $id
		);

		// $this->imput_form->categorias_update( $this->tabela, 1, array( "1", "4", "7" ) );
		// $this->imput_form->tags_update( $this->tabela, 1, array( "1", "4", "7" ) );

		$this->load( 'usuarios/read', $data );

	}

	public function create() {

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) && empty( $_POST[ 'senha' ] ) ) {

			unset( $_POST[ 'senha' ] );

		} else {

			$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];

		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		if( $id = $this->crud->create( $_POST, $this->tabela ) ) {

			// Galeria
			$this->imput_form->galeria_update( $id );

			// Categorias
			$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			// Tags
			$this->imput_form->tags_update( $this->tabela, $id, $tags );

			$this->session->set_flashdata( "msg_sucesso", "Item adicionado com sucesso!" );
			redirect( base_url().$this->pagina );

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao adicionar o item, tente novamente!" );
			redirect( base_url().$this->pagina."read/".$id );

		}

	}


	public function update( $id ) {

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {

			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}

		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];

		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Faz e verifica se fez o update
		if( $this->crud->update( array( "id" => $id ), $_POST, $this->tabela ) ) {

			// Categorias
			if( isset( $_POST[ 'categorias' ] ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}

			// Tags
			if( isset( $_POST[ 'tags' ] ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );
			redirect( base_url().$this->pagina );

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );
			redirect( base_url().$this->pagina."read/".$id );

		}

	}

	public function delete( $id ) {

		// Faz e verifica se fez o delete
		if( $this->crud->delete( array( 'id' => $id ), $this->tabela ) ) {

			$this->session->set_flashdata( "msg_sucesso", "Item deletado com sucesso!" );
			redirect( base_url().$this->pagina );

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao deletar tente novamente!" );
			redirect( base_url().$this->pagina );

		}

	}

	public function image( $campo ) {

		$src = $this->image_form->upload_imagem( $_FILES[ $campo ], $this->config_image[ $campo ][ 'config' ] );

		$this->session->set_userdata( $campo, $src );

		if( $this->config_image[ $campo ][ 'config' ][ 'thumb' ] ) {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].'thumb/'.$src;

		} else {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].$src;

		}

	}

	public function file( $campo ) {

		$this->load->library( 'upload', $this->config_arquivo[ $campo ][ 'config' ] );

		if( $this->upload->do_upload( $campo ) ) {

			$data = $this->upload->data();
			$name = $data[ 'file_name' ];

			$this->session->set_userdata( $campo, $name );

			echo base_url().$this->config_arquivo[ $campo ][ 'config' ][ 'upload_path' ].$name;

		} else {

			echo '0';

		}

	}

	public function galeria() {

		$src = $this->image_form->upload_imagem( $_FILES[ 'arquivo' ], $this->config_galeria[ 'config' ] );

		$_POST[ 'arquivo' ] = $src;

		$id = $this->imput_form->galeria_insert( $_POST );

		$galeria_id = $this->session->userdata( "galeria_id" );

		$session = $this->session->userdata( $galeria_id );

		$session[] = $id;

		$this->session->set_userdata( $galeria_id, $session );

		$json = array(
			"src" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].'thumb/'.$src,
			"src_full" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].$src,
			"id" => $id,
			"pagina" => $this->pagina,
			"session" => $session,
			"id" => $galeria_id
		);

		echo json_encode( $json );

	}

	public function galeria_legenda() {

		$id = $this->imput_form->galeria_legenda( $_POST[ 'id' ], $_POST[ 'legenda' ] );

	}

	public function galeria_delete() {

		$id = $this->imput_form->galeria_delete( $_POST[ 'id' ] );

	}

}