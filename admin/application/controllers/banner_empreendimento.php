<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_empreendimento extends Main_Controller {

	private $pagina = "banner-empreendimento";
	private $tabela = "banner_empreendimento";

	private $config_lista = array(
		'busca' => 'titulo',
		'filtro' => array(
			'ativos' => array( 'ativo' => '1' ),
			'inativos' => array( 'ativo' => '0' )
		),
		'order' => array(
			'titulo_asc' => array( 'titulo' => 'ASC' ),
			'titulo_desc' => array( 'titulo' => 'DESC' ),
			'data_asc' => array( 'data_cadastro' => 'ASC' ),
			'data_desc' => array( 'data_cadastro' => 'DESC' )
		),
		'num_link' => 10,
		'qtd_por_pagina' => 25
	);

	private $config_ckecks = array();

	// Configurações de arquivos
	private $config_arquivo = array();

	// Configurações para imagem
	private $config_image = array(
		'imagem' => array(
			'campo' => 'imagem',
			'config' => array(
				'thumb' => false,
				'marca' => false,
				'image_path' => '../assets/uploads/banner_empreendimento/', // string [ caminho da imagem é necessário ter uma pasta thumb (se tiver)]
				'largura' => 630, // num
				'altura' => 250, // num

				'redimensionar' => true, // boolean [ redimensiona a imagem ]
				'proporcional' => false, // boolean [ redimenciona proporcionalmente ]
				'cortar' => true, // boolean [ corta a imagem sem redimencionar ]
				'converter' => false, // boolean [ converte a image para jpg ]
				'greyscale' => false, // boolean [ coloca efeito greyscale ]
				//'qualidade' => 80 // num [ configura a qualidade da imagem ]
			)
		)

	);

	// Configurações para galeria
	private $config_galeria = array(

		'galeria' => 0,
		'config' => array()

	);

	public function __construct() {

		parent::__construct();
		$this->checkLogin();

	}

	public function index() {

		$busca = $this->input->get( "busca" );
		$filtro = $this->input->get( "filtro" );
		$ordem = $this->input->get( "ordem" );

		$where = array();
		$order = array( "id" => "DESC" );

		if( !empty( $filtro ) || $filtro === '0' ) {

			$where = $this->config_lista[ 'filtro' ][ $filtro ];

		}

		if( !empty( $busca ) ) {

			$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%'" ] = NULL;

		}

		if( !empty( $ordem ) ) {

			if( isset( $this->config_lista[ 'order' ][ $ordem ] ) ) {

				$order = $this->config_lista[ 'order' ][ $ordem ];

			}

		}

		$retorno = $this->crud->read_paginacao( $where, $this->tabela, $order, $this->config_lista[ 'qtd_por_pagina' ], $this->pagina, $this->config_lista[ 'num_link' ] );

		$lista = $retorno[ 'result' ];
		$paginacao = $retorno[ 'paginacao' ];
		
		foreach($lista as $x => $val) {
			if($val->ativo == 1) {
				$lista[$x]->status = '<p><strong>Ativo</strong></p>';
			} else {
				$lista[$x]->status = '<p style="color: red"><strong>Inativo</strong></p>';
			}
		}

		$data = array(
			"lista" => $lista,
			"pagina" => $this->pagina,
			"paginacao" => $paginacao
		);

		$this->load( $this->pagina .'/index', $data );

	}

	public function duplicate( $id ){

		$item = $this->crud->read( array( 'id'=> $id ), $this->tabela, array( 'id' => 'DESC' ) );

		if( empty( $item ) ) {
			redirect( base_url(). $this->pagina );
		}

		$item = $item[0];

		$campos_form_esquerdo = array(
			"data_cadastro" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "data_cadastro",
				"value" => $item->data_cadastro,
				"width" => "12"
			),
			"titulo" => array(
				"type" => "text",
				"label" => "Titulo",
				"name" => "titulo",
				"value" => $item->titulo,
				"width" => "12"
			),
			"link" => array(
				"type" => "text",
				"label" => "Link",
				"name" => "link",
				"value" => $item->link,
				"width" => "12"
			),
			"duplicar" => array(
				"type" => "text",
				"label" => "duplicar",
				"name" => "duplicar",
				"value" => $item->id,
				"width" => "12"
			),
			
			// "legenda" => array(
			// 	"type" => "textarea",
			// 	"label" => "Legenda",
			// 	"name" => "legenda",
			// 	"value" => $item->legenda,
			// 	"width" => "12",
			// 	"descricao" => "240 Caracteres",
			// 	"max_length" => "240"
			// )
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens
		$path_image = base_url() ."../assets/uploads/banner_empreendimento/";

		$arquivos = array(
			"imagem" => array(
				"type" => "image",
				"label" => "Imagem",
				"name" => "imagem",
				"value" => ( !empty( $item->imagem ) ) ? $path_image. $item->imagem : "",
				"width" => $this->config_image[ 'imagem' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'imagem' ][ 'config' ][ 'altura' ]
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );
		
		if($item->ativo == 1) {
			$ativo = 'selected="selected"';
			$inativo = '';
		} else {
			$ativo = '';
			$inativo = 'selected="selected"';
		}
		
		$select_ativo = '<select name="ativo" id="slcPublicar" class="required"/>
							<option value="1" '.$ativo.'>Ativo</option>
							<option value="0" '.$inativo.'>Inativo</option>
						</select>';

		//estado
		$config = array(
			'campos' => 'uf_codigo as value, uf_descricao as label',
			'tabela' => 'cep_uf'
		);

		$this->select->set( $config );

		$estados = $this->select->resultado();
		$estados_id = explode(',', $item->estado);
		$estadoSelect = '';
		foreach($estados as $val ) {
			
			$selected = '';
			
			if(in_array($val->value, $estados_id)) {
				$selected = 'selected="selected"';	
			}

			$estadoSelect .= '<option value="'.$val->value.'" '.$selected.'>'.$val->label.'</option>';	

		}

		//cidade
		$config = array(
			'campos' => 'c.cidade_codigo, c.cidade_descricao, e.uf_sigla',
			'tabela' => 'cep_cidade as c',
			'join' => array(
				array( 'cep_uf as e', 'e.uf_codigo = c.uf_codigo', 'inner' ),
			),
			'where' => array('c.uf_codigo IN ('.$item->estado.')' => NULL)
		);

		$this->select->set( $config );
		$cidades = $this->select->resultado();

		$cidades_id = explode(',', $item->cidade);

		$cidadeSelect = '';
		foreach($cidades as $val ) {
			
			$selected = '';
			
			if(in_array($val->cidade_codigo, $cidades_id)) {
				$selected = 'selected="selected"';	
			}

			$cidadeSelect .= '<option value="'.$val->cidade_codigo.'" '.$selected.'>'.$val->cidade_descricao.' - '.$val->uf_sigla.'</option>';

		}

		$arrayFeirao = array(array("value" => 1, "label" => "Sim"), array("value" => 0, "label" => "Não"));	
		$selectFeirao = $this->imput_form->select_2('Feirão' , "feirao", $arrayFeirao, $item->feirao, array('descricao' =>""));

		
		
		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"estado" => $estadoSelect,
			"cidade" => $cidadeSelect,
			"pagina" => $this->pagina,
			"selectFeirao" => $selectFeirao,
			
			// "id" => $id,
			"select_ativo" => $select_ativo
		);

		$this->load( $this->pagina .'/add', $data );

	}

	// Add
	public function add() {

		$campos_form_esquerdo = array(
			"data_cadastro" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "data_cadastro",
				"value" => time(),
				"width" => "12"
			),
			"titulo" => array(
				"type" => "text",
				"label" => "Titulo",
				"name" => "titulo",
				"value" => "",
				"width" => "12"
			),
			"link" => array(
				"type" => "text",
				"label" => "Link",
				"name" => "link",
				"value" => "",
				"width" => "12"
			)
			// "legenda" => array(
			// 	"type" => "textarea",
			// 	"label" => "Legenda",
			// 	"name" => "legenda",
			// 	"value" => "",
			// 	"width" => "12",
			// 	"descricao" => "240 Caracteres",
			// 	"max_length" => "240"
			// )
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens
		$path_image = base_url() ."../assets/uploads/banner_empreendimento/";

		$arquivos = array(
			"imagem" => array(
				"type" => "image",
				"label" => "Imagem",
				"name" => "imagem",
				"value" => "",
				"width" => $this->config_image[ 'imagem' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'imagem' ][ 'config' ][ 'altura' ]
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );

		//estado
		$config = array(
			'campos' => 'uf_codigo as value, uf_descricao as label',
			'tabela' => 'cep_uf'
		);

		$this->select->set( $config );

		$estados = $this->select->resultado();

		$estadoSelect = '';
		foreach($estados as $val ) {			
			$estadoSelect .= '<option value="'.$val->value.'">'.$val->label.'</option>';			
		}

		$arrayFeirao = array(array("value" => 1, "label" => "Sim"), array("value" => 0, "label" => "Não"));	
		$selectFeirao = $this->imput_form->select_2('Feirão' , "feirao",  $arrayFeirao);

		

		//regiao
		$config = array(
			'campos' => 'id as value, nome as label',
			'tabela' => 'regiao'
		);

		$this->select->set( $config );

		$regiao = $this->select->resultado();

		$regiaoSelect = '';
		foreach($regiao as $val ) {			
			$regiaoSelect .= '<option value="'.$val->value.'">'.$val->label.'</option>';			
		}

		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"selectFeirao" => $selectFeirao,
			
			"regiao" => $regiaoSelect,
			"estado" => $estadoSelect,
			"cidade" => array(),
			"pagina" => $this->pagina
		);

		$this->load( $this->pagina .'/add', $data );

	}

	// Edição
	public function read( $id ) {

		$item = $this->crud->read( array( 'id'=> $id ), $this->tabela, array( 'id' => 'DESC' ) );

		if( empty( $item ) ) {
			redirect( base_url(). $this->pagina );
		}

		$item = $item[0];

		$campos_form_esquerdo = array(
			"data_cadastro" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "data_cadastro",
				"value" => $item->data_cadastro,
				"width" => "12"
			),
			"titulo" => array(
				"type" => "text",
				"label" => "Titulo",
				"name" => "titulo",
				"value" => $item->titulo,
				"width" => "12"
			),
			"link" => array(
				"type" => "text",
				"label" => "Link",
				"name" => "link",
				"value" => $item->link,
				"width" => "12"
			)
			// "legenda" => array(
			// 	"type" => "textarea",
			// 	"label" => "Legenda",
			// 	"name" => "legenda",
			// 	"value" => $item->legenda,
			// 	"width" => "12",
			// 	"descricao" => "240 Caracteres",
			// 	"max_length" => "240"
			// )
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens
		$path_image = base_url() ."../assets/uploads/banner_empreendimento/";
		
		$arquivos = array(
			"imagem" => array(
				"type" => "image",
				"label" => "Imagem",
				"name" => "imagem",
				"value" => ( !empty( $item->imagem ) ) ? $path_image. $item->imagem : "",
				"width" => $this->config_image[ 'imagem' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'imagem' ][ 'config' ][ 'altura' ]
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );
		
		if($item->ativo == 1) {
			$ativo = 'selected="selected"';
			$inativo = '';
		} else {
			$ativo = '';
			$inativo = 'selected="selected"';
		}
		
		$select_ativo = '<select name="ativo" id="slcPublicar" class="required"/>
							<option value="1" '.$ativo.'>Ativo</option>
							<option value="0" '.$inativo.'>Inativo</option>
						</select>';

		//estado
		$config = array(
			'campos' => 'uf_codigo as value, uf_descricao as label',
			'tabela' => 'cep_uf'
		);

		$this->select->set( $config );

		$estados = $this->select->resultado();
		$estados_id = explode(',', $item->estado);
		$estadoSelect = '';
		foreach($estados as $val ) {
			
			$selected = '';
			
			if(in_array($val->value, $estados_id)) {
				$selected = 'selected="selected"';	
			}

			$estadoSelect .= '<option value="'.$val->value.'" '.$selected.'>'.$val->label.'</option>';	

		}

		//cidade
		$config = array(
			'campos' => 'c.cidade_codigo, c.cidade_descricao, e.uf_sigla',
			'tabela' => 'cep_cidade as c',
			'join' => array(
				array( 'cep_uf as e', 'e.uf_codigo = c.uf_codigo', 'inner' ),
			),
			'where' => array('c.uf_codigo IN ('.$item->estado.')' => NULL)
		);

		$this->select->set( $config );
		$cidades = $this->select->resultado();

		$cidades_id = explode(',', $item->cidade);

		$cidadeSelect = '';
		foreach($cidades as $val ) {
			
			$selected = '';
			
			if(in_array($val->cidade_codigo, $cidades_id)) {
				$selected = 'selected="selected"';	
			}

			$cidadeSelect .= '<option value="'.$val->cidade_codigo.'" '.$selected.'>'.$val->cidade_descricao.' - '.$val->uf_sigla.'</option>';

		}

		$arrayFeirao = array(array("value" => 1, "label" => "Sim"), array("value" => 0, "label" => "Não"));	
		$selectFeirao = $this->imput_form->select_2('Feirão' , "feirao", $arrayFeirao, $item->feirao, array('descricao' =>""));

		
		

		//regiao
		$config = array(
			'campos' => 'id as value, nome as label',
			'tabela' => 'regiao'
		);

		$this->select->set( $config );

		$regiao = $this->select->resultado();

		$regiaoSelect = '';
		foreach($regiao as $val ) {			
			$regiaoSelect .= '<option value="'.$val->value.'">'.$val->label.'</option>';			
		}


		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"regiao" => $regiaoSelect,
			"estado" => $estadoSelect,
			"cidade" => $cidadeSelect,
			"pagina" => $this->pagina,
			"selectFeirao" => $selectFeirao,
			
			"id" => $id,
			"select_ativo" => $select_ativo
		);

		$this->load( $this->pagina .'/read', $data );

	}

	public function create() {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {
			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}
		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {
				
				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}
		

		//Confere se o banner foi duplicado ou não 
		if(empty($_POST[ 'imagem' ]) && !empty($_POST[ 'duplicar' ])){

			$id = array(

				'campos' => 'imagem',

				'tabela' => 'banner_empreendimento',

				'where' => array('id' => $_POST[ 'duplicar' ])

			);

			$this->select->set($id);

			$idImagem = $this->select->resultado();
			
			
			$folder = "../assets/uploads/banner_empreendimento/";

			$imgOriginal = $idImagem[0]->imagem;

			$imgCopia = time().$imgOriginal;

			
			if(copy($folder.$imgOriginal, $folder.$imgCopia)){
			
				$_POST[ 'imagem' ] = $imgCopia;

			}
				

			


		}
		
		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}
		
		//Configuração da cidade
		if( isset( $_POST[ 'cidade' ] ) ) {

			$_POST['cidade'] = implode(',', $_POST['cidade']);

		}
		
		//Configuração de estado
		if( isset( $_POST[ 'estado' ] ) ) {

			$_POST['estado'] = implode(',', $_POST['estado']);

		}

		if( isset( $_POST[ 'duplicar' ] ) ) {

			unset($_POST[ 'duplicar' ]);

		}

		if( $id = $this->crud->create( $_POST, $this->tabela ) ) {

			// Galeria
			if( $this->config_galeria[ 'galeria' ] ) {

				$galeria_id = $this->session->userdata( 'galeria_id' );
				$this->imput_form->galeria_update( $id, $galeria_id );

			}

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item adicionado com sucesso!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao adicionar o item, tente novamente!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		}

		echo json_encode( $retorno );

	}


	public function update( $id ) {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {

			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}

		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}
		
		//Configuração da cidade
		if( isset( $_POST[ 'cidade' ] ) ) {

			$_POST['cidade'] = implode(',', $_POST['cidade']);

		}
		
		//Configuração de estado
		if( isset( $_POST[ 'estado' ] ) ) {

			$_POST['estado'] = implode(',', $_POST['estado']);

		}

		// Faz e verifica se fez o update
		if( $this->crud->update( array( "id" => $id ), $_POST, $this->tabela ) ) {

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina."/read/".$id
			);

		}

		echo json_encode( $retorno );

	}

	public function delete( $id ) {

		// Faz e verifica se fez o delete
		$this->crud->delete( array( "id" => $id ), $this->tabela );
		$this->session->set_flashdata( "msg_sucesso", "Item deletado com sucesso!" );
		redirect( base_url().$this->pagina );

	}

	public function image( $campo ) {

		$src = $this->image_form->upload_imagem( $_FILES[ $campo ], $this->config_image[ $campo ][ 'config' ] );

		$this->session->set_userdata( $campo, $src );

		if( $this->config_image[ $campo ][ 'config' ][ 'thumb' ] ) {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].'thumb/'.$src;

		} else {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].$src;

		}

	}

	public function file( $campo ) {

		$this->load->library( 'upload', $this->config_arquivo[ $campo ][ 'config' ] );

		if( $this->upload->do_upload( $campo ) ) {

			$data = $this->upload->data();
			$name = $data[ 'file_name' ];

			$this->session->set_userdata( $campo, $name );

			echo base_url().$this->config_arquivo[ $campo ][ 'config' ][ 'upload_path' ].$name;

		} else {

			echo '0';

		}

	}

	public function galeria() {

		$src = $this->image_form->upload_imagem( $_FILES[ 'arquivo' ], $this->config_galeria[ 'config' ] );

		$_POST[ 'arquivo' ] = $src;

		$id = $this->imput_form->galeria_insert( $_POST );

		$galeria_id = $_POST[ 'elemento' ];

		$session = $this->session->userdata( $galeria_id );

		$session[] = $id;

		$this->session->set_userdata( $galeria_id, $session );
		$this->session->set_userdata( 'galeria_id', $galeria_id );
// var_dump($galeria_id);
		$json = array(
			"src" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].'thumb/'.$src,
			"src_full" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].$src,
			"id" => $id,
			"pagina" => $this->pagina,
			"session" => $session,
			"id" => $id
		);

		echo json_encode( $json );

	}

	public function galeria_legenda() {

		$id = $this->imput_form->galeria_legenda( $_POST[ 'id' ], $_POST[ 'legenda' ] );

	}

	public function galeria_delete() {

		$id = $this->imput_form->galeria_delete( $_POST[ 'id' ] );

	}

}