<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Login extends Main_Controller {



	public function index() {



		$usuario = $this->input->post('login', TRUE);

		$senha = $this->input->post('senha', TRUE);



		/* Usuario */

		$config = array(

			'campos' => 'senha',

			'tabela' => 'usuarios_admin',

			'where' => array( 'ativo' => '1', 'usuario' => $usuario )

		);



		$this->select->set($config);

		$q_usuario = $this->select->resultado();



		if( !empty( $q_usuario ) ) {



			$config = array(

				'campos' => 'id, nome, image, usuario, senha, permissoes, master',

				'tabela' => 'usuarios_admin',

				'where' => array( 'ativo' => '1', 'usuario' => $usuario, 'senha' => md5( $senha ) )

			);



			$this->select->set($config);

			$q_senha = $this->select->resultado();



			if( !empty( $q_senha ) ) {



				$this->session->unset_userdata( "login_admin" );



				$login = array(

					"login_admin" => array(

						"logado" => true,

						"id_usuario" => $q_senha[0]->id,

						"nome" => $q_senha[0]->nome,

						"image" => ( !empty( $q_senha[0]->image ) ) ? $q_senha[0]->image : 'img-profile.png',

						"usuario" => $q_senha[0]->usuario,

						"senha" => $q_senha[0]->senha,

						"permissoes" => $q_senha[0]->permissoes,
						
						"admin_master" => $q_senha[0]->master

					)

				);



				$this->session->set_userdata( $login );





				$hash = md5( time().uniqid() );



				$data = array(

					'hash' => $hash

				);



				$this->db->where( array( 'usuario' => $q_senha[0]->usuario, 'senha' => $q_senha[0]->senha ) );

				$this->db->update( 'usuarios_admin', $data );



				$data_acesso = array(

					'data_cadastro' => time(),

					'id_usuario' => $q_senha[0]->id,

					'ip' => $_SERVER['REMOTE_ADDR']

				);

				$this->db->insert( 'acessos_admin', $data_acesso );



				// deu certo



			} else {



				$data_acesso_negado = array(

					'data_cadastro' => time(),

					'usuario' => $usuario,

					'senha' => $senha,

					'ip' => $_SERVER['REMOTE_ADDR']

				);

				$this->db->insert( 'acesso_negado_admin', $data_acesso_negado );



				$this->session->set_flashdata( "msg_erro", "Usuário ou senha inválido!" );



			}



		} else {



			$data_acesso_negado = array(

				'data_cadastro' => time(),

				'usuario' => $usuario,

				'senha' => $senha,

				'ip' => $_SERVER['REMOTE_ADDR']

			);

			$this->db->insert( 'acesso_negado_admin', $data_acesso_negado );





			$this->session->set_flashdata( "msg_erro", "Usuário ou senha inválido!" );



		}



		redirect( base_url() );



	}



	public function recuperar() {



		$usuario = $this->input->post('login', TRUE);



		/* Usuario */

		$config = array(

			'campos' => 'nome, email',

			'tabela' => 'usuarios_admin',

			'where' => "ativo = 1 AND usuario = '". $usuario ."' OR email = '". $usuario ."'"

		);



		$this->select->set($config);

		$q_usuario = $this->select->resultado();



		if( !empty( $q_usuario ) ) {



			$hash = md5( time().uniqid() );

			$nova_senha = $this->util->geraSenha(8);



			$email = $q_usuario[0]->email;

			$nome = $q_usuario[0]->nome;



			$data = array(

				"nome" => $nome,

				"usuario" => $usuario,

				"senha" => $nova_senha

			);



			$msg = $this->parser->parse( "templates/email/recuperar_senha", $data, true );



			$this->load->library('email');



			$config['protocol'] = 'mail';

			$config['charset'] = 'utf-8';

			$config['wordwrap'] = TRUE;

			$config['mailtype'] = "html";



			$this->email->initialize($config);



			$data = array();



			$to = $email;

			$ca = $nome;



			$this->email->from( $email, $nome);

			$this->email->to($to);



			$this->email->subject( 'Recuperação de Senha' );

			$this->email->message($msg);



			if( $this->email->send() ) {



				$data = array(

					'hash' => $hash,

					'senha' => md5( $nova_senha )

				);



				$this->db->where( array( 'usuario' => $usuario ) );

				$this->db->update( 'usuarios_admin', $data );



				$this->session->set_flashdata( "status_sucesso", "1" );

				$this->session->set_flashdata( "email", $email );

				redirect( base_url() ."recuperar/" );



			} else {



				$this->session->set_flashdata( "msg_erro", "Erro ao recuperar!" );

				redirect( base_url() ."recuperar/" );



			}



		} else {



			$this->session->set_flashdata( "msg_erro", "Usuário inválido ou não cadastrado!" );

			redirect( base_url() ."recuperar/" );



		}



	}



	public function sair() {



		$this->session->unset_userdata( "login_admin" );



		redirect( base_url() );



	}



}