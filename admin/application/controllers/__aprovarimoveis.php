<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aprovarimoveis extends Main_Controller {

	private $pagina = "aprovarimoveis";
	private $tabela = "imoveis";

	private $config_lista = array(
		'busca' => 'imo.titulo',
		'busca_usuario' => 'dp.nome',
		'filtro' => array(
			'aprovado' => array( 'imo.aprovado' => '1' ),
			'nao_aprovado' => array( 'imo.aprovado' => '0' ),
			'ativos' => array( 'imo.ativo' => '1' ),
			'inativos' => array( 'imo.ativo' => '0' ),
		),
		'order' => array(
			'titulo_asc' => array( 'imo.titulo' => 'ASC' ),
			'titulo_desc' => array( 'imo.titulo' => 'DESC' ),
			'data_asc' => array( 'imo.data_cadastro' => 'ASC' ),
			'data_desc' => array( 'imo.data_cadastro' => 'DESC' )
		),
		'aprovado' => array(
			'aprovado' => '1',
			'nao_aprovado' => '0'
		),
		'status' => array(
			'aguardando_pagamento' => '1',
			'aguardando_aprovacao' => '2',
			'publicado' => '3',
			'cancelado' => '4'
		),
		'num_link' => 10,
		'qtd_por_pagina' => 25
	);

	private $config_ckecks = array();

	// Configurações de arquivos
	private $config_arquivo = array();

	// Configurações para imagem
	private $config_image = array(
		'imagem' => array(
			'campo' => 'imagem',
			'config' => array(
				'thumb' => array(
					'greyscale' => false, // bolean
					'largura' => 140, // num
					'altura' => 140 // num
				),
				'marca' => false,
				'image_path' => '../assets/uploads/noticias/', // string [ caminho da imagem é necessário ter uma pasta thumb (se tiver)]
				'largura' => 540, // num
				'altura' => 340, // num

				'redimensionar' => true, // boolean [ redimensiona a imagem ]
				'proporcional' => false, // boolean [ redimenciona proporcionalmente ]
				'cortar' => true, // boolean [ corta a imagem sem redimencionar ]
				'converter' => false, // boolean [ converte a image para jpg ]
				'greyscale' => false, // boolean [ coloca efeito greyscale ]
				//'qualidade' => 80 // num [ configura a qualidade da imagem ]
			)
		)

	);

	// Configurações para galeria
	private $config_galeria = array(

		'galeria' => 0,
		'config' => array()

	);

	public function __construct() {

		parent::__construct();
		$this->checkLogin();

	}

	public function index() {

		$data_inicio_cadastro = $this->input->get( "data_inicio_cadastro" );
		$data_fim_cadastro = $this->input->get( "data_fim_cadastro" );
		
		$data_inicio_expira = $this->input->get( "data_inicio_expira" );
		$data_fim_expira = $this->input->get( "data_fim_expira" );
		
		$busca = $this->input->get( "busca" );
		$busca_usuario = $this->input->get( "busca_usuario" );
		$filtro = $this->input->get( "filtro" );
		$ordem = $this->input->get( "ordem" );
		$aprovado = $this->input->get( "aprovado" );
		$status = $this->input->get( "status" );
		$tipo_usuario = $this->input->get( "tipo_usuario" );
		
		$expirado = $this->input->get( "expirado" );
		$join = array();
		$join[] = array('usuarios us','us.id = imo.id_usuario','left');
		
		if( !empty( $filtro )) {

			$where = $this->config_lista[ 'filtro' ][ $filtro ];

		}

		if( !empty( $busca ) ) {

			if ($tipo_usuario != 0) {
				if($tipo_usuario == 1) { //Proprietario
					$join_tb = 'dados_proprietario';
				} else if($tipo_usuario == 2) { //Corretor
					$join_tb = 'dados_corretor';
				}

				$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%' OR dp.nome LIKE '%". $busca ."%'" ] = NULL;
				$join[] = array($join_tb.' dp','dp.id = us.perfil','left');				
			}else{
				$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%' OR us.login LIKE '%". $busca ."%'" ] = NULL;
			}
		}

		if( !empty( $data_inicio_expira ) && !empty( $data_fim_expira ) ) {

			$data_inicio = $this->util->date2us( $data_inicio_expira );
			$data_inicio = strtotime( $data_inicio );

			$data_fim = $this->util->date2us( $data_fim_expira );
			$data_fim = strtotime( $data_fim );

			$where[ "imo.data_expira >= '". $data_inicio ."' AND imo.data_expira <= '". $data_fim ."' AND imo.data_expira <> '' "  ] = NULL;

		}
		 if ( empty($tipo_usuario)) {
		 	$tipo_usuario = 0; // todos usuarios
		 }
		
		if( !empty( $data_inicio_cadastro ) && !empty( $data_fim_cadastro ) ) {

			$data_inicio = $this->util->date2us( $data_inicio_cadastro );
			$data_inicio = strtotime( $data_inicio );

			$data_fim = $this->util->date2us( $data_fim_cadastro );
			$data_fim = strtotime( $data_fim );

			$where[ "imo.data_cadastro >= '". $data_inicio ."' AND imo.data_cadastro <= '". $data_fim ."'" ] = NULL;

		}

		if( !empty( $ordem ) ) {

			if( isset( $this->config_lista[ 'order' ][ $ordem ] ) ) {

				$order = $this->config_lista[ 'order' ][ $ordem ];

			}

		}
		
		if( !empty( $aprovado )) {

			$where['imo.aprovado'] = $this->config_lista[ 'aprovado' ][ $aprovado ];

		}
		
		if( !empty( $status )) {

			$where['imo.status'] = $this->config_lista[ 'status' ][ $status ];

		}
		
		if( !empty( $expirado )) {
			
			$hoje = time();
			
			if($expirado == 'expirado') {

				$where[ "(imo.data_expira <= '".$hoje."' AND imo.data_expira <> '' )" ] = NULL;
				
			}
			
			if($expirado == 'nao_expirado') {

				$where[ "(imo.data_expira >= '".$hoje."' OR imo.data_expira = '' )" ] = NULL;
				
			}

		}





		// $where['imo.status'] = 3;
		// $where['imo.aprovado'] = 0;
		$where['us.tipo <> 3'] = NULL;
		$order['imo.revisado_prop'] = "DESC";
		$order['imo.aprovado'] = "asc";
		$order['imo.id'] = "DESC";

		//$retorno = $this->crud->read_paginacao( $where, $this->tabela, $order, $this->config_lista[ 'qtd_por_pagina' ], $this->pagina, $this->config_lista[ 'num_link' ] );

		// $lista = $retorno[ 'result' ];
		// $paginacao = $retorno[ 'paginacao' ];
		
		$config = array(
			'campos' => 'us.tipo as tipoUser, us.perfil as perfilUser,imo.id as "id_imovel", imo.titulo as "titulo_imovel", imo.status as "status", imo.aprovado as "aprovado", imo.aprovado_vendedor, imo.data_expira as "expira"',
			'tabela' => 'imoveis imo',
			'where' => $where,
			'join' => $join,
			'orderBy' => $order
		);

		$this->select->set($config);
		$paginacao = $this->select->paginacao( "aprovarimoveis", 5);
		$total_imoveis = $this->select->total();
		$imo = $this->select->resultado();
		//var_dump($imo);
		
		$lista = '';
		
		if($total_imoveis > 0) {
		
			foreach( $imo as $i ) {
		
				$id_imovel = $i->id_imovel;
				$titulo_imovel = $i->titulo_imovel;
				//$nome_usuario = $i->nome_usuario;

				$id_user = $i->perfilUser;
				$id_tipoUser = $i->tipoUser;
				
				//verifica se o tipo do imovel é igual ao do tipo  usario do filtro ou se o imovel é de proprietario e corretor
				if (($tipo_usuario == $id_tipoUser) || ($tipo_usuario == 0 && $id_tipoUser != 3)) {
				
				//verifica quem é prorpietario
					$labelTipo = '';
					if ($id_tipoUser == 1) {
						$labelTipo = 'Proprietario';
						$config = array(
							'campos' => 'prop.id, prop.nome',
							'tabela' => 'dados_proprietario prop',
							'where' => 'prop.id = '.$id_user
						);
					}else{
						$labelTipo = 'Corretor';
						$config = array(
							'campos' => 'corr.id, corr.nome',
							'tabela' => 'dados_corretor corr',
							'where' => 'corr.id = '.$id_user
						);
					}

					$this->select->set($config);
					$usuarioImovel = $this->select->resultado();
					
					$nome_usuario = $usuarioImovel[0]->nome;



					
					if($i->aprovado == 1) { //Imóvel aprovado e pago
						
						$aprovado = '<p style="color: green">Aprovado</p>';
					
						$btn_aprovar = '<div class="columns small-6 medium-6 large-6 text-left">
						<a href="#" class="AbrirModalRecusar" data-reveal-id="modalRecusar" data-id="'.$id_imovel.'">Recusar</a>
						</div>';
						$status = '';
						
						$status_bar = '<div class="columns small-6 medium-6 large-6 text-left">'.$aprovado.'</div>
										<div class="columns small-6 medium-6 large-6 text-right">'.$status.'</div>';
						
					} elseif ($i->aprovado_vendedor == 1 && $i->aprovado == 0) {
						
						$aprovado = '<p style="color: green">Aprovado por Vendedor</p>';
						$btn_aprovar = '';
						$status = '';
						
						$status_bar = '<div class="columns small-6 medium-6 large-6 text-left">'.$aprovado.'</div>
										<div class="columns small-6 medium-6 large-6 text-right">'.$status.'</div>';
						
					} else {
					
						if($i->status == 1) { 
							$status = '<p style="color: gray">Aguar. Pagamento</p>'; 
							
							//Checagem de admin master
							$login = $this->session->userdata( "login_admin" );
							
							if($login['admin_master'] == 1) {
								$tipo = 'master';
							} else {
								$tipo = 'vendedor';	
							}
							
							$btn_aprovar = '<div class="columns small-6 medium-6 large-6 text-left">
								<a href="#" class="AbrirModalAprovar" data-reveal-id="modalAprovar" data-id="'.$id_imovel.'" data-tipo="'.$tipo.'">Aprovar</a>
							</div>';
							
							$status_bar = '<div class="columns small-12 medium-12 large-12 text-right">'.$status.'</div>';
							
						}
					
						if($i->status == 2) { 
						
							$status = '<p style="color: rgb(182, 182, 65)">Aguar. Pagamento</p>'; 
							$btn_aprovar = '';
							
							$status_bar = '<div class="columns small-12 medium-12 large-12 text-right">'.$status.'</div>';
							
						}
						
						if($i->status == 3) { //Pago pelo site
						
							$aprovado = '<p style="color: red">Não Aprovado</p>';
							$status = '<p style="color: green">Pago</p>'; 
							
							//Checagem de admin master
							$login = $this->session->userdata( "login_admin" );
							
							if($login['admin_master'] == 1) {
								$btn_aprovar = 'aprovarAnuncioPago';
							} else {
								$btn_aprovar = 'aprovarVendedor';	
							}
							
							$btn_aprovar = '<div class="columns small-126 medium-6 large-6 text-left"><a href="'.base_url().$this->pagina.'/'.$btn_aprovar.'/'.$id_imovel.'">Aprovar</a></div>';
							
							$status_bar = '<div class="columns small-6 medium-6 large-6 text-left">'.$aprovado.'</div>
										<div class="columns small-6 medium-6 large-6 text-right">'.$status.'</div>';
							
						}
						
						if($i->status == 4) { 
						
							$aprovado = '<p style="color: red">Não Aprovado</p>';
						
							$status = '<p style="color: red">Cancelado</p>'; 
							$btn_aprovar = '<div class="columns small-6 medium-6 large-6 text-left"><a href="'.base_url().$this->pagina.'/'.$btn_aprovar.'/'.$id_imovel.'">Aprovar</a></div>';
							
							$status_bar = '<div class="columns small-6 medium-6 large-6 text-left">'.$aprovado.'</div>
										<div class="columns small-6 medium-6 large-6 text-right">'.$status.'</div>';
							
						}
						
					}
					
					if(empty($i->expira)) {
						$expira = 'Não definido';	
					} else {
						$hoje = time();
						$expira = date('d/m/Y', $i->expira);
						
						if($hoje >=  $i->expira) { 
							$expira = '<span style="color: red">EXPIRADO</span>'; 
						} else {
							$expira = date('d/m/Y', $i->expira);	
						}
						
					}
						
					$lista .= '<div class="row table">
									<!-- <div class="columns small-12 medium-1 large-1"> <p><strong>ID:</strong> '.$id_imovel.'</p> </div> -->
									<div class="columns small-12 medium-3 large-3"> <p><strong>Título:</strong> '.$titulo_imovel.'</p> </div>
									<div class="columns small-12 medium-3 large-3"> <p><strong>Usuário:</strong> '.$labelTipo.' - '.$nome_usuario.'</p> </div>
									<div class="columns small-12 medium-2 large-2"> <p><strong>Expira:</strong> '.$expira.'</p> </div>
									<div class="columns small-12 medium-2 large-2">
										'.$status_bar.'
									</div>
									<div class="columns small-12 medium-2 large-2">
										'.$btn_aprovar.'
										<div class="columns small-6 medium-6 large-6 text-right"><a href="'.base_url().$this->pagina.'/read/'.$id_imovel.'/'.$id_tipoUser.'" class="ac red">Visualizar</a></div>
									</div>

									<div class="clearfix"></div>
								</div>

								<div class="row"><hr></div>';
				}
				
			}
			
		}

		$q = ( isset( $_SERVER[ 'QUERY_STRING' ] ) && !empty( $_SERVER[ 'QUERY_STRING' ] ) ) ? "?".$_SERVER[ 'QUERY_STRING' ] : '';
		
		//Permissao para poder Baixar o XML
		$dados = $this->session->userdata('login_admin');
		
		$permissoes = explode(',', $dados['permissoes']);
		
		if(in_array('25', $permissoes)) { //ID da permissao do botao
			
			$botao_exportar = '<a href="'.base_url().$this->pagina.'/download'.$q.'" class="button tiny"/>Exportar XLS</a>';
								
		} else {
			
			$botao_exportar = '&nbsp;';
			
		}

		$data = array(
			"lista" => $lista,
			"pagina" => $this->pagina,
			"paginacao" => $paginacao,
			"botao_exportar" => $botao_exportar,
			"val_get" => $q
		);

		$this->load( $this->pagina .'/index', $data );

	}

	// Edição
	public function read( $id, $tipo ) {

		$item = $this->crud->read( array( 'id'=> $id ), $this->tabela, array( 'id' => 'DESC' ) );

		if ($tipo == 1) {
			$join = array('dados_proprietario dp','dp.id = us.perfil','left');
		}else{
			$join = array('dados_corretor dp','dp.id = us.perfil','left');
		}

		$config = array(
			'campos' => 'imo.id_usuario as "usuario", dp.nome as "nome_usuario", imo.destaque, imo.id as "id_imovel", imo.titulo as "titulo_imovel", imo.data_expira, imo.data_cadastro, imo.tipo, imo.valor, imo.ativo, imo.descricao1, imo.descricao2 , imo.descricao3, imo.aprovado as "aprovado"',
			'tabela' => 'imoveis imo',
			'where' => array( "imo.id" => $id ),
			'join' => array(
						array('usuarios us','us.id = imo.id_usuario','left'),
						$join
					),
			'limit' => 1
		);

		$this->select->set($config);
		$item = $this->select->resultado();

		if( empty( $item ) ) {
			redirect( base_url(). $this->pagina );
		}

		$item = $item[0];

		$campos_form_esquerdo = array(
			"titulo" => array(
				"type" => "text",
				"label" => "Imóvel",
				"name" => "titulo",
				"value" => $item->titulo_imovel,
				"width" => "12",
				"disabled" => true
			),
			"usuario" => array(
				"type" => "text",
				"label" => "Usuário",
				"name" => "usuario",
				"value" => $item->nome_usuario,
				"width" => "6",
				"disabled" => true
			),
			"valor" => array(
				"type" => "text",
				"label" => "Valor",
				"name" => "valor",
				"value" => $item->valor,
				"width" => "6",
				"disabled" => true
			),
			"data_cadastro" => array(
				"type" => "text",
				"label" => "Data de Cadastro",
				"name" => "data_cadastro",
				"value" => ( !empty( $item->data_cadastro ) ? date( "d/m/Y", $item->data_cadastro ) : "N/D" ),
				"width" => "6",
				"disabled" => true
			),
			"data_expira" => array(
				"type" => "text",
				"label" => "Data de Expiração",
				"name" => "data_expira",
				"value" => ( !empty( $item->data_expira ) ? date( "d/m/Y", $item->data_expira ) : "N/D" ),
				"width" => "6",
				"disabled" => true
			),
			"descricao1" => array(
				"type" => "textarea",
				"label" => "Descrição 1",
				"name" => "descricao1",
				"value" => $item->descricao1,
				"width" => "12"
			),
			"descricao2" => array(
				"type" => "textarea",
				"label" => "Descrição 2",
				"name" => "descricao2",
				"value" => $item->descricao2,
				"width" => "12"
			),
			"descricao3" => array(
				"type" => "textarea",
				"label" => "Descrição 3",
				"name" => "descricao3",
				"value" => $item->descricao3,
				"width" => "12"
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens
		$path_image = base_url() ."../../assets/uploads/noticias/thumb/";

		$arquivos = array(
			"imagem" => array(
				"type" => "image",
				"label" => "Imagem",
				"name" => "imagem",
				"value" => ( !empty( $item->imagem ) ) ? $path_image. $item->imagem : "",
				"width" => $this->config_image[ 'imagem' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'imagem' ][ 'config' ][ 'altura' ]
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );
		
		if($item->ativo == 1) {
			$ativo = 'selected="selected"';
			$inativo = '';
		} else {
			$ativo = '';
			$inativo = 'selected="selected"';
		}
		
		$select_ativo = '<select name="ativo" id="slcPublicar" class="required"/>
							<option value="1" '.$ativo.'>Ativo</option>
							<option value="0" '.$inativo.'>Inativo</option>
						</select>';
						
		// Imagens
		
		$config = array(
			'campos' => 'arquivo',
			'tabela' => 'imagens',
			'where' => array('id_imovel' => $id)
		);

		$this->select->set($config);
		$imagens = $this->select->resultado();
		
		//Verificação de Destaque
		if($item->destaque == 1) {
			$destaque = 'Anúncio Destaque';	
		} else {
			$destaque = 'Anúncio Comum';		
		}

		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"pagina" => $this->pagina,
			"id" => $id,
			"select_ativo" => $select_ativo,
			"imagens" => $imagens,
			"destaque" => $destaque,
			"id_usuario" => $item->usuario
		);

		$this->load( $this->pagina .'/read', $data );

	}

	public function create() {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {
			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}
		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		if( $id = $this->crud->create( $_POST, $this->tabela ) ) {

			// Galeria
			if( $this->config_galeria[ 'galeria' ] ) {

				$galeria_id = $this->session->userdata( 'galeria_id' );
				$this->imput_form->galeria_update( $id, $galeria_id );

			}

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item adicionado com sucesso!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao adicionar o item, tente novamente!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		}

		echo json_encode( $retorno );

	}


	public function update( $id ) {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {

			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}

		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Faz e verifica se fez o update
		if( $this->crud->update( array( "id" => $id ), $_POST, $this->tabela ) ) {

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina."/read/".$id
			);

		}

		echo json_encode( $retorno );

	}

	public function delete( $id ) {

		// Faz e verifica se fez o delete
		$this->crud->delete( array( "id" => $id ), $this->tabela );
		$this->session->set_flashdata( "msg_sucesso", "Item deletado com sucesso!" );
		redirect( base_url().$this->pagina );

	}

	public function image( $campo ) {

		$src = $this->image_form->upload_imagem( $_FILES[ $campo ], $this->config_image[ $campo ][ 'config' ] );

		$this->session->set_userdata( $campo, $src );

		if( $this->config_image[ $campo ][ 'config' ][ 'thumb' ] ) {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].'thumb/'.$src;

		} else {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].$src;

		}

	}

	public function file( $campo ) {

		$this->load->library( 'upload', $this->config_arquivo[ $campo ][ 'config' ] );

		if( $this->upload->do_upload( $campo ) ) {

			$data = $this->upload->data();
			$name = $data[ 'file_name' ];

			$this->session->set_userdata( $campo, $name );

			echo base_url().$this->config_arquivo[ $campo ][ 'config' ][ 'upload_path' ].$name;

		} else {

			echo '0';

		}

	}

	public function galeria() {

		$src = $this->image_form->upload_imagem( $_FILES[ 'arquivo' ], $this->config_galeria[ 'config' ] );

		$_POST[ 'arquivo' ] = $src;

		$id = $this->imput_form->galeria_insert( $_POST );

		$galeria_id = $_POST[ 'elemento' ];

		$session = $this->session->userdata( $galeria_id );

		$session[] = $id;

		$this->session->set_userdata( $galeria_id, $session );
		$this->session->set_userdata( 'galeria_id', $galeria_id );
// var_dump($galeria_id);
		$json = array(
			"src" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].'thumb/'.$src,
			"src_full" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].$src,
			"id" => $id,
			"pagina" => $this->pagina,
			"session" => $session,
			"id" => $id
		);

		echo json_encode( $json );

	}

	public function galeria_legenda() {

		$id = $this->imput_form->galeria_legenda( $_POST[ 'id' ], $_POST[ 'legenda' ] );

	}

	public function galeria_delete() {

		$id = $this->imput_form->galeria_delete( $_POST[ 'id' ] );

	}
	
	public function aprovarAnuncioVendedor( $id ) {
		
		$dados = array(
			'aprovado_vendedor' => 0,
			'aprovado' => 1
		);
		
		$this->crud->update( array( "id" => $id ), $dados, 'imoveis' );
		
		$this->crud->delete( array( "id_imovel" => $id ), 'imoveis_aprovados_vendedor' );
		$this->session->set_flashdata( "msg_sucesso", "Anúncio aprovado com sucesso!" );
		
		//Log de alteração
		
		$login = $this->session->userdata( "login_admin" );
		
		$dados = array(
			'id_usuario' => $login['id_usuario'],
			'id_imovel' => $id,
			'acao' => 'Aprovar Anúncio Vendedor',
			'data_cadastro' => time()
		);
		
		$id = $this->crud->create( $dados, 'log_anuncios' );

		redirect( base_url(). $this->pagina );

	}
	
	//Se o anuncio ja estiver pago
	public function aprovarAnuncioPago( $id ) {
		
		
		$hoje = time();
		$expira = mktime(0,0,0,date('m',$hoje), date('d', $hoje)+$this->dt_ex, date('Y', $hoje));
		
		$dados = array(
			'aprovado' => 1,
			'data_expira' => $expira,
			'obs_recusado' => NULL
		);

		$this->crud->update( array( "id" => $id ), $dados, 'imoveis' );
		
		$this->session->set_flashdata( "msg_sucesso", "Anúncio aprovado com sucesso!" );
		
		//Log de alteração
		
		$login = $this->session->userdata( "login_admin" );
		
		$dados = array(
			'id_usuario' => $login['id_usuario'],
			'id_imovel' => $id,
			'acao' => 'Aprovar Anúncio Site',
			'data_cadastro' => time()
		);
		
		$id = $this->crud->create( $dados, 'log_anuncios' );
		
		redirect( base_url(). $this->pagina );
		
	}
	
	public function aprovarAnuncio() {
		
		$login = $this->session->userdata( "login_admin" );
		
		extract($_POST);
	
		$hoje = time();
		$expira = mktime(0,0,0,date('m',$hoje), date('d', $hoje)+$data_expiracao, date('Y', $hoje));
		
		//Id do usuario
		$config = array(
			'campos' => 'id_usuario',
			'tabela' => 'imoveis',
			'where' => array('id' => $id_imovel_aprovar)
		);
		
		$this->select->set($config);
		$u = $this->select->resultado();
		
		$id_usuario = $u[0]->id_usuario;
		
		//Preço dos anuncios
		$config = array(
			'campos' => 'preco_normal, preco_destaque',
			'tabela' => 'config'
		);
		
		$this->select->set($config);
		$preco = $this->select->resultado();
		
		$preco_normal = $preco[0]->preco_normal;
		$preco_destaque = $preco[0]->preco_destaque;
		
		$id_usuario = $u[0]->id_usuario;
		
		if($destaque == 1) {
			$preco = $preco_destaque;
		} else {
			$preco = $preco_normal;
		}
		
		//Simulação de Pagamento
		$dados = array(
			'id_usuario' => $id_usuario,
			'pago' => 1,
			'valor' => $preco,
			'data_cadastro' => time(),
			'tipo' => 1
		);
		
		$id_pagamento = $this->crud->create( $dados, 'pagamentos' );
		
		//Simulação de Imóvel de Pagamento
		$dados = array(
			'id_pagamento' => $id_pagamento,
			'id_imovel' => $id_imovel_aprovar,
			'preco' => $preco,
			'destaque' => $destaque,
			'data_cadastro' => time(),
			'data_expira' => $expira
		);
		
		$id = $this->crud->create( $dados, 'pagamentos_imoveis' );
		
		//Checa se o usuario é master
		if($login['admin_master'] == 1) {
			$aprovado = 1;
			$aprovado_vendedor = 0;	
			
		} else { //Se for um vendedor
			
			$dados = array(
				'data_cadastro' => time(),
				'aprovado' => 0,
				'id_imovel' => $id_imovel_aprovar,
				'id_vendedor' => $login['id_usuario']
			);
			
			$id_pagamento = $this->crud->create( $dados, 'imoveis_aprovados_vendedor' );
		
			$aprovado = 0;
			$aprovado_vendedor = 1;	
			
		}
		
		//Alteração do status do imóvel
		$dados = array(
			'status' => 3,
			'aprovado' => $aprovado,
			'aprovado_vendedor' => $aprovado_vendedor,
			'destaque' => $destaque,
			'data_expira' => $expira
		);

		$this->crud->update( array( "id" => $id_imovel_aprovar ), $dados, 'imoveis' );
		
		$this->crud->delete( array( "id_imovel" => $id ), 'imoveis_aprovados_vendedor' );
		$this->session->set_flashdata( "msg_sucesso", "Anúncio aprovado com sucesso!" );
		
		//Log de alteração
		$dados = array(
			'id_usuario' => $login['id_usuario'],
			'id_imovel' => $id_imovel_aprovar,
			'acao' => 'Aprovar Anúncio Admin',
			'data_cadastro' => time()
		);
		
		$id = $this->crud->create( $dados, 'log_anuncios' );
		
		redirect( base_url(). $this->pagina );

	}
	
	public function recusar() {
		
		$id = $this->input->post( 'id_veiculo', true );
		$obs = $this->input->post( 'observacao', true );
	
		$dados = array(
			'revisado_prop' => 0,
			'obs_recusado' => $obs,
			'aprovado' => 0,
			'aprovado_vendedor' => 0,
			'data_expira' => 0
		);

		/*$dados = array(
			'status' => 1,
			'aprovado' => 0,
			'aprovado_vendedor' => 0,
			'data_expira' => 0
		);*/

		$this->crud->update( array( "id" => $id ), $dados, 'imoveis' );
		
		$this->crud->delete( array( "id_imovel" => $id ), 'imoveis_aprovados_vendedor' );
		$this->session->set_flashdata( "msg_sucesso", "Anúncio recusado com sucesso!" );
		
		//Log de alteração
		
		$login = $this->session->userdata( "login_admin" );
		
		$dados = array(
			'id_usuario' => $login['id_usuario'],
			'id_imovel' => $id,
			'acao' => 'Recusar Anúncio',
			'data_cadastro' => time()
		);
		
		$id = $this->crud->create( $dados, 'log_anuncios' );
		
		redirect( base_url(). $this->pagina );

	}

	public function download() {

		$data_inicio_cadastro = $this->input->get( "data_inicio_cadastro" );
		$data_fim_cadastro = $this->input->get( "data_fim_cadastro" );
		
		$data_inicio_expira = $this->input->get( "data_inicio_expira" );
		$data_fim_expira = $this->input->get( "data_fim_expira" );
		
		$busca = $this->input->get( "busca" );
		$busca_usuario = $this->input->get( "busca_usuario" );
		$filtro = $this->input->get( "filtro" );
		$ordem = $this->input->get( "ordem" );
		$aprovado = $this->input->get( "aprovado" );
		$status = $this->input->get( "status" );
		
		$expirado = $this->input->get( "expirado" );

		if( !empty( $filtro )) {

			$where = $this->config_lista[ 'filtro' ][ $filtro ];

		}

		if( !empty( $busca ) ) {

			$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%' OR dp.nome LIKE '%". $busca ."%'" ] = NULL;

		}

		if( !empty( $data_inicio_expira ) && !empty( $data_fim_expira ) ) {

			$data_inicio = $this->util->date2us( $data_inicio_expira );
			$data_inicio = strtotime( $data_inicio );

			$data_fim = $this->util->date2us( $data_fim_expira );
			$data_fim = strtotime( $data_fim );

			$where[ "imo.data_expira >= '". $data_inicio ."' AND imo.data_expira <= '". $data_fim ."'" ] = NULL;

		}
		
		if( !empty( $data_inicio_cadastro ) && !empty( $data_fim_cadastro ) ) {

			$data_inicio = $this->util->date2us( $data_inicio_cadastro );
			$data_inicio = strtotime( $data_inicio );

			$data_fim = $this->util->date2us( $data_fim_cadastro );
			$data_fim = strtotime( $data_fim );

			$where[ "imo.data_cadastro >= '". $data_inicio ."' AND imo.data_cadastro <= '". $data_fim ."'" ] = NULL;

		}

		if( !empty( $ordem ) ) {

			if( isset( $this->config_lista[ 'order' ][ $ordem ] ) ) {

				$order = $this->config_lista[ 'order' ][ $ordem ];

			}

		}
		
		if( !empty( $aprovado )) {

			$where['imo.aprovado'] = $this->config_lista[ 'aprovado' ][ $aprovado ];

		}
		
		if( !empty( $status )) {

			$where['imo.status'] = $this->config_lista[ 'status' ][ $status ];

		}
		
		if( !empty( $expirado )) {
			
			$hoje = time();
			
			if($expirado == 'expirado') {

				$where[ "(imo.data_expira <= '".$hoje."' AND imo.data_expira <> '' )" ] = NULL;
				
			}
			
			if($expirado == 'nao_expirado') {

				$where[ "(imo.data_expira >= '".$hoje."' OR imo.data_expira = '' )" ] = NULL;
				
			}

		}


		// $where['imo.status'] = 3;
		// $where['imo.aprovado'] = 0;
		$where['us.tipo'] = 1;
		$order['imo.id'] = "DESC";

		//$retorno = $this->crud->read_paginacao( $where, $this->tabela, $order, $this->config_lista[ 'qtd_por_pagina' ], $this->pagina, $this->config_lista[ 'num_link' ] );

		// $lista = $retorno[ 'result' ];
		// $paginacao = $retorno[ 'paginacao' ];

		$config = array(
			'campos' => 'dp.nome as "nome_usuario", imo.id as "id_imovel", imo.titulo as "titulo_imovel", imo.status as "status", imo.aprovado as "aprovado", imo.data_expira, imo.email',
			'tabela' => 'imoveis imo',
			'where' => $where,
			'join' => array(
						array('usuarios us','us.id = imo.id_usuario','left'),
						array('dados_proprietario dp','dp.id = us.perfil','left')
					),
			'orderBy' => $order
		);

		$this->select->set($config);
		$total_imoveis = $this->select->total();
		$imo = $this->select->resultado();

		$lista = '';

		if($total_imoveis > 0) {

			foreach( $imo as $i ) {

				$id_imovel = $i->id_imovel;
				$titulo_imovel = $i->titulo_imovel;
				$nome_usuario = $i->nome_usuario;

				if($i->aprovado == 1) {
					$aprovado = 'Aprovado';
				} else {
					$aprovado = 'Nao Aprovado';
				}

				if($i->status == 1) { $status = 'Aguar. Pagamento'; }
				if($i->status == 2) { $status = 'Aguar. Aprovacao'; }
				if($i->status == 3) { $status = 'Publicado'; }
				if($i->status == 4) { $status = 'Cancelado'; }

				$lista .= '
					<tr>
						<td>'.$titulo_imovel.'</td>
						<td>'.$nome_usuario.'</td>
						<td>'.$i->email.'</td>
						<td>'.$aprovado.'</td>
						<td>'.$status.'</td>
						<td>'.( ( !empty( $i->data_expira ) ) ? date( "d/m/Y", $i->data_expira ) : "N/D") .'</td>

					</tr>
				';

			}

		}

		$data = array(
			'lista_pagamentos' => $lista
		);

		$table = $this->parser->parse( $this->pagina.'/table', $data, true );
		$this->util->excel( $table, "anuncios_". date( "d-m-Y" ) .".xls" );

	}

}