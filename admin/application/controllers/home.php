<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Main_Controller {

	public function index() {

		$data = array();

		$login_session = $this->session->userdata( "login_admin" );

		if( isset( $login_session ) && !empty( $login_session ) ) {

			// Acessos
			$config = array(
				'campos' => 'a.*, u.nome',
				'tabela' => 'acessos_admin as a',
				'join' => array(
					array( 'usuarios_admin as u', 'u.id = a.id_usuario', 'inner' )
				),
				'orderBy' => array( 'a.data_cadastro' => 'DESC' ),
				'limit' => 12
			);
			$this->select->set( $config );
			$q_acessos = $this->select->resultado();
			$acessos = array();

			foreach( $q_acessos as $a ) {

				$acessos[] = array(
					'data' => date( 'd/m/Y', $a->data_cadastro ),
					'hora' => date( "H\hi", $a->data_cadastro ),
					'nome' => $a->nome
				);

			}


			$data = array(
				"acessos" => $acessos
			);

			$this->load( 'home/index', $data );

		} else {

			$this->load( 'home/login', $data, false );

		}

	}

	public function selectCidade(){
	
		$uf = $this->input->post('uf', TRUE);
		
		$config = array(
			'campos' => 'cidade_codigo, cidade_descricao',
			'tabela' => 'cep_cidade',
			'where' => array('uf_codigo' => $uf)
		);
		
		$this->select->set($config);
		$cidade = $this->select->resultado();
		
		$html = '';
		
		foreach($cidade as $val) {
		
			$html .= '<option value="'.$val->cidade_codigo.'">'.$val->cidade_descricao.'</option>';
		
		}
		
		echo $html;
	}
	
	public function selectCidadeMultiplo() {
	
		$uf = $this->input->post('uf', TRUE);
		
		$config = array(
			'campos' => 'c.cidade_codigo, c.cidade_descricao, e.uf_sigla',
			'tabela' => 'cep_cidade as c',
			'join' => array(
				array( 'cep_uf as e', 'e.uf_codigo = c.uf_codigo', 'inner' ),
			),
			'orderBy' => array('c.cidade_descricao' => 'ASC')
		);

		if( is_array( $uf ) ) {
			$config['where'] = array( 'c.uf_codigo IN ('. implode(',', $uf) .')' => NULL );
		} else {
			$config['where'] = array('c.uf_codigo' => $uf);
		}
		
		$this->select->set($config);
		$cidade = $this->select->resultado();
		
		/*$html = '<ul style="list-style: none">';		
		foreach($cidade as $val ) {			
			$html .= '<li><label><input type="checkbox" class="check check_cidade" id="cidade" name="cidade[]" value="'.$val->cidade_codigo.'">'.$val->cidade_descricao.'</label></li>';			
		}		
		$html .= '</ul>';		
		echo $html;*/

		echo json_encode($cidade);
	
	}

	public function recuperar() {

		$data = array();

		if( $this->session->flashdata( "status_sucesso" ) == 1 ) {

			$data[ "email" ] = $this->session->flashdata( "email" );

			$this->load( 'home/recuperar_sucesso', $data, false );

		} else {

			$this->load( 'home/recuperar', $data, false );

		}

	}

}