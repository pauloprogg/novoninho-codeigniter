<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendas extends Main_Controller {

	private $pagina = "vendas";
	private $tabela = "pagamentos";

	private $config_lista = array(
		'busca' => 'dp.nome',
		'filtro' => array(
			'ativos' => array( 'pg.ativo' => '1' ),
			'inativos' => array( 'pg.ativo' => '0' )
		),
		'order' => array(
			'data_asc' => array( 'pg.data_cadastro' => 'ASC' ),
			'data_desc' => array( 'pg.data_cadastro' => 'DESC' )
		),
		'status' => array(
			'aguardando_pagamento' => '0',
			'pago' => '1',
			'cancelado' => '2'
		),
		'num_link' => 10,
		'qtd_por_pagina' => 25
	);

	private $config_ckecks = array();

	// Configurações de arquivos
	private $config_arquivo = array();

	// Configurações para imagem
	private $config_image = array(
		'campo' => 'imagem',
			'config' => array(
				'thumb' => false,
				'marca' => false,
				'image_path' => '../assets/uploads/anuncios/', // string [ caminho da imagem é necessário ter uma pasta thumb (se tiver)]
				'largura' => 220, // num
				'altura' => 150, // num

				'redimensionar' => true, // boolean [ redimensiona a imagem ]
				'proporcional' => false, // boolean [ redimenciona proporcionalmente ]
				'cortar' => true, // boolean [ corta a imagem sem redimencionar ]
				'converter' => false, // boolean [ converte a image para jpg ]
				'greyscale' => false, // boolean [ coloca efeito greyscale ]
				//'qualidade' => 80 // num [ configura a qualidade da imagem ]
			)

	);

	// Configurações para galeria
	private $config_galeria = array(

		'galeria' => 0,
		'config' => array()

	);

	public function __construct() {

		parent::__construct();
		$this->checkLogin();

	}

	public function index() {
		$data_inicio = $this->input->get( "data_inicio" );
		
		$data_fim = $this->input->get( "data_fim" );
		$busca = $this->input->get( "busca" );
		$filtro = $this->input->get( "filtro" );
		$ordem = $this->input->get( "ordem" );
		$status = $this->input->get( "status" );
		$vendedores = $this->input->get( "vendedores" );
		$tipo = $this->input->get( "tipo" );
		
		$tipo_usuario = $this->input->get( "tipo_usuario" );
		$where = array();
		
		$join = array();
		$join[] = array('usuarios us','us.id = pg.id_usuario','left');
		$order = array( "pg.id" => "DESC" );

		if( !empty( $filtro ) || $filtro === '0' ) {

			$where = $this->config_lista[ 'filtro' ][ $filtro ];

		}
		
		if( !empty( $data_inicio ) && !empty( $data_fim ) ) {
			$data_inicio = $this->util->date2us( $data_inicio );
			$data_inicio = strtotime( $data_inicio );
			$data_fim = $this->util->date2us( $data_fim );
			$data_fim = strtotime( $data_fim );
			$where[ "pg.data_cadastro >= '". $data_inicio ."' AND pg.data_cadastro <= '". $data_fim ."'" ] = NULL;
		}
		if( !empty( $busca ) ) {
			
			if($tipo_usuario == 1) { //Proprietario
				$join_tb = 'dados_proprietario';
			} else if($tipo_usuario == 2) { //Corretor
				$join_tb = 'dados_corretor';
			}
			$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%'" ] = NULL;
			$join[] = array($join_tb.' dp','dp.id = us.perfil','left');
		}
		if($tipo != '') {
			$where[ "pg.tipo = ".$tipo ] = NULL;
		}
		if( !empty( $ordem ) ) {

			if( isset( $this->config_lista[ 'order' ][ $ordem ] ) ) {

				$order = $this->config_lista[ 'order' ][ $ordem ];

			}

		}
		if( !empty( $status )) {

			$where['pg.pago'] = $this->config_lista[ 'status' ][ $status ];

		}
		
		if( !empty( $tipo_usuario )) {

			$where['us.tipo'] = $tipo_usuario;

		}
		/* Select de Vendedores */
		
		$config = array(
			'campos' => 'nome as "nome_vendedor", id as "id_vendedor"',
			'tabela' => 'vendedores',
			'orderBy' => array('nome' => 'DESC')
		);
		
		$this->select->set($config);
		$vendedores_lista = $this->select->resultado();

		// $retorno = $this->crud->read_paginacao( $where, $this->tabela, $order, $this->config_lista[ 'qtd_por_pagina' ], $this->pagina, $this->config_lista[ 'num_link' ] );

		// $lista = $retorno[ 'result' ];
		// $paginacao = $retorno[ 'paginacao' ];
		if( !empty( $vendedores )) {
			$where['vp.id_vendedor'] = $vendedores;
			$join[] = array('vendedores_pagamentos vp','vp.id_pagamento = pg.id','left');
		}
		
		$config = array(
			'campos' => 'pg.id as "id_venda", pg.data_cadastro as "data", pg.pago as "status", pg.tipo, us.tipo as "tipo_usuario", us.perfil as "perfil_usuario"',
			'tabela' => 'pagamentos pg',
			'where' => $where,
			'join' => $join,
			'orderBy' => $order
		);
		$this->select->set($config);
		$paginacao = $this->select->paginacao( "vendas", 5);
		$total_vendas = $this->select->total();
		$ven = $this->select->resultado();
		$lista = '';
		if($total_vendas > 0) {
			foreach( $ven as $v ) {
				
				$id_venda = $v->id_venda;
				$data = $v->data;
				//$nome_usuario = $v->nome_usuario;
				
				if($v->tipo_usuario == 1) { //Proprietario
					$tab_user = 'dados_proprietario';
				} else if($v->tipo_usuario == 2) { //Corretor
					$tab_user = 'dados_corretor';
				}
				$config = array(
					'campos' => 'dp.nome as "nome_usuario"',
					'tabela' => $tab_user.' dp',
					'where' => array('id' => $v->perfil_usuario)
		
				);
				
				$this->select->set($config);
				$dados_user = $this->select->resultado();
				
				if(count($dados_user) > 0){

					$nome_usuario = $dados_user[0]->nome_usuario;

					
					$config = array(
						'campos' => 'v.nome as "nome_vendedor"',
						'tabela' => 'vendedores_pagamentos vp',
						'where' => array('vp.id_pagamento' => $id_venda),
						'join' => array(
									array('vendedores v','v.id = vp.id_vendedor','left')
								)
					);
					
					$this->select->set($config);
					$vendedor = $this->select->total();
					
					if($vendedor > 0) {
						$vendedor = $this->select->resultado();
						$nome_vendedor = $vendedor[0]->nome_vendedor;
					} else {
						$nome_vendedor = '';
					}
					
					if($v->status == 0) { $status = '<p style="color: gray">Aguar. Pagamento</p>'; }
					if($v->status == 1) { $status = '<p style="color: green">Pago</p>'; }
					if($v->status == 2) { $status = '<p style="color: red">Cancelado. Pagamento</p>'; }
					if($v->tipo == 1) { //Vendas realizadas no admin
						
						$tipo = 'Venda pelo <strong>Admin</strong>';
						
					} else { //Vendas realizadas no site
					
						$tipo = 'Venda pelo <strong>Site</strong>';
						
					}
					$lista .= '<div class="row table">
								<div class="columns small-12 medium-1 large-1"> <p><strong>Num:</strong> '.$id_venda.'</p> </div>
								<div class="columns small-12 medium-3 large-3"> <p><strong>Usuário:</strong> '.$nome_usuario.'</p> </div>
								<div class="columns small-12 medium-2 large-2"> <p><strong>Vendedor:</strong> '.$nome_vendedor.'</p> </div>
								<div class="columns small-12 medium-2 large-2"> <p><strong>Data:</strong> '.date('d/m/Y', $data).'</p> </div>
								
								<div class="columns small-12 medium-2 large-2"> <p>'.$tipo.'</p> </div>
								<div class="columns small-12 medium-1 large-1"> <p> '.$status.'</p> </div>
								<div class="columns small-12 medium-1 large-1">
									<div class="columns small-6 medium-6 large-6 text-left"><a href="{base_url}{pagina}/read/'.$id_venda.'">Editar</a></div>
								</div>

								<div class="clearfix"></div>
							</div>

							<div class="row"><hr></div>';
				}
			}
			
		}
		
		$q = ( isset( $_SERVER[ 'QUERY_STRING' ] ) && !empty( $_SERVER[ 'QUERY_STRING' ] ) ) ? "?".$_SERVER[ 'QUERY_STRING' ] : '';
		//Permissao para poder Baixar o XML
		$dados = $this->session->userdata('login_admin');
		
		$permissoes = explode(',', $dados['permissoes']);
		
		if(in_array('26', $permissoes)) { //ID da permissao do botao
			
			$botao_exportar = '<a href="'.base_url().$this->pagina.'/download'.$q.'" class="button tiny"/>Exportar XLS</a>';
								
		} else {
			
			$botao_exportar = '&nbsp;';
			
		}
		$data = array(
			"lista" => $lista,
			"pagina" => $this->pagina,
			"paginacao" => $paginacao,
			"vendedores" => $vendedores_lista,
			
			"botao_exportar" => $botao_exportar,
			"val_get" => $q
		);

		$this->load( $this->pagina .'/index', $data );

	}

	// Add
	public function add() {

		$campos_form_esquerdo = array(
			"data_cadastro" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "data_cadastro",
				"value" => time(),
				"width" => "12"
			),
			"nome" => array(
				"type" => "text",
				"label" => "Nome",
				"name" => "nome",
				"value" => "",
				"width" => "12"
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens
		$path_image = base_url() ."../assets/uploads/banner_empreendimento/";

		$arquivos = array(
			"imagem" => array(
				"type" => "image",
				"label" => "Imagem",
				"name" => "imagem",
				"value" => "",
				"width" => '',
				"height" => ''
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );

		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"pagina" => $this->pagina
		);

		$this->load( $this->pagina .'/add', $data );

	}

	// Edição
	public function read( $id ) {

		$item = $this->crud->read( array( 'id'=> $id ), $this->tabela, array( 'id' => 'DESC' ) );

		if( empty( $item ) ) {
			redirect( base_url(). $this->pagina );
		}

		$item = $item[0];

		$pagamentos_imoveis = $this->crud->read( array( 'id_pagamento'=> $item->id ), 'pagamentos_imoveis', array( 'id' => 'DESC' ) );

		if( empty( $pagamentos_imoveis ) ) {
			redirect( base_url(). $this->pagina );
		}
		$pagamentos_imoveis = $pagamentos_imoveis[0];
		
		$config = array(
			'campos' => 'us.id as "id_usuario", pg.id as "id_venda", pg.data_cadastro as "data", pg.pago as "status", pg.valor as "valor", pg.tipo as "tipo", pg.tipoPagamento as "tipo_pagamento", pg.pago as "pago"',
			'tabela' => 'pagamentos pg',
			'where' => array( "pg.id" => $id ),
			'join' => array(
						array('usuarios us','us.id = pg.id_usuario','left')
					),
			'limit' => 1
		);
		$this->select->set($config);
		$total_vendas = $this->select->total();
		$ven = $this->select->resultado();
		if( empty( $ven ) ) {
			redirect( base_url(). $this->pagina );
		}
		$ven = $ven[0];
		
		$valor = $this->util->moeda2br($ven->valor);
		
		//Nome do usuário
		$config = array(
			'campos' => 'tipo, perfil',
			'tabela' => 'usuarios',
			'where' => array( "id" => $ven->id_usuario )
		);
		
		$this->select->set($config);
		$us = $this->select->resultado();
		
		if($us[0]->tipo == 1) {
			$perfil_tb = 'dados_proprietario';
		} else if($us[0]->tipo == 2) {
			$perfil_tb = 'dados_corretor';
		}
		
		$config = array(
			'campos' => 'nome',
			'tabela' => $perfil_tb,
			'where' => array( "id" => $us[0]->perfil )
		);
		
		$this->select->set($config);
		$us_nome = $this->select->resultado();
		
		if($ven->tipo == 1) {
			$tipo = 'Pelo Admin';
		} else {
			$tipo = 'Pelo Site';	
		}
		if($ven->tipo_pagamento == 1) {
			$tipo_pagamento = 'PagSeguro';
		} else {
			$tipo_pagamento = 'Outro';	
		}
		
		if($ven->pago == 0) {
			$status = 'Aguardando Pagamento';
		} else if($ven->pago == 1) {
			$status = 'Pago';
		} else {
			$status = 'Cancelado';	
		}
		
		//Total de Anuncios
		$config = array(
			'campos' => 'pg.*, imo.titulo as "titulo", imo.id as "id_imovel", cid.cidade_descricao as "cidade", est.uf_sigla as "estado"',
			'tabela' => 'pagamentos_imoveis pg',
			'where' => array('pg.id_pagamento' => $id),
			
			'join' => array(
				array('imoveis imo','imo.id = pg.id_imovel','left'),
				array('cep_cidade cid','cid.cidade_codigo = imo.cidade','left'),
				array('cep_uf est','est.uf_codigo = imo.estado','left')
			)
		);
		$this->select->set($config);
		$total_anuncio = $this->select->total();
		$anuncios = $this->select->resultado();
		
		//Total de Destaque
		$config = array(
			'campos' => '*',
			'tabela' => 'pagamentos_imoveis',
			'where' => array('id_pagamento' => $id, 'destaque' => 1)
		);
		$this->select->set($config);
		$total_destaque = $this->select->total();
		$campos_form_esquerdo = array(
			"usuario" => array(
				"type" => "text",
				"label" => "Usuário",
				"name" => "usuario",
				"value" => $us_nome[0]->nome,
				"width" => "12",
				"disabled" => true
			),
			"data_cadastro" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "data_cadastro",
				"value" => $item->data_cadastro,
				"width" => "12"
			),
			"data_cadastro" => array(
				"type" => "text",
				"label" => "Data do Pagamento",
				"name" => "data_cadastro",
				"value" => ( !empty( $pagamentos_imoveis->data_cadastro ) ? date( "d/m/Y", $pagamentos_imoveis->data_cadastro ) : "N/D" ),
				"width" => "6",
				"disabled" => true
			),
			"data_expira" => array(
				"type" => "text",
				"label" => "Data de Expiração",
				"name" => "data_expira",
				"value" => ( !empty( $pagamentos_imoveis->data_expira ) ? date( "d/m/Y", $pagamentos_imoveis->data_expira ) : "N/D" ),
				"width" => "6",
				"disabled" => true
			),
			
			"valor" => array(
				"type" => "text",
				"label" => "Valor Total (R$)",
				"name" => "valor",
				"value" => $valor,
				"width" => "6",
				"disabled" => true
			),
			
			"tipo" => array(
				"type" => "text",
				"label" => "Tipo de Venda",
				"name" => "tipo_venda",
				"value" => $tipo,
				"width" => "6",
				"disabled" => true
			),
			
			"status" => array(
				"type" => "text",
				"label" => "Status da Venda",
				"name" => "status",
				"value" => $status,
				"width" => "12",
				"disabled" => true
			),
			"tipo_pagamento" => array(
				"type" => "text",
				"label" => "Tipo de Pagamento",
				"name" => "tipo_pagamento",
				"value" => $tipo_pagamento,
				"width" => "12",
				"disabled" => true
			),
			
			"total_anuncio" => array(
				"type" => "text",
				"label" => "Total de Anúncios",
				"name" => "total_anuncio",
				"value" => $total_anuncio,
				"width" => "6",
				"disabled" => true
			),
			
			"total_destaque" => array(
				"type" => "text",
				"label" => "Total de Destaques",
				"name" => "total_destaque",
				"value" => $total_destaque,
				"width" => "6",
				"disabled" => true
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens
		$path_image = base_url() ."../assets/uploads/banner_empreendimento/";

		$arquivos = array(
			"imagem" => array(
				"type" => "image",
				"label" => "Imagem",
				"name" => "imagem",
				"value" => ( !empty( $item->imagem ) ) ? $path_image. $item->imagem : "",
				"width" => '',
				"height" => ''
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );
		
		if($item->ativo == 1) {
			$ativo = 'selected="selected"';
			$inativo = '';
		} else {
			$ativo = '';
			$inativo = 'selected="selected"';
		}
		
		$select_ativo = '<select name="ativo" id="slcPublicar" class="required"/>
							<option value="1" '.$ativo.'>Ativo</option>
							<option value="0" '.$inativo.'>Inativo</option>
						</select>';
						
		//Select de Vendedores
		$config = array(
			'campos' => 'nome as "nome_vendedor", id as "id_vendedor"',
			'tabela' => 'vendedores',
			'orderBy' => array('nome' => 'DESC')
		);
		
		$this->select->set($config);
		$vendedores_lista = $this->select->resultado();
		
		$l_ven = '<select name="vendedor" id="slcPublicar" class="required"/>
					<option value="">Nenhum Vendedor Selecionado</option>';
		
		foreach($vendedores_lista as $v) {
		
			$config = array(
				'campos' => 'id',
				'tabela' => 'vendedores_pagamentos',
				'where' => array('id_vendedor' => $v->id_vendedor, 'id_pagamento' => $id)
			);
			
			$this->select->set($config);
			$a = $this->select->total();
			
			if($a > 0) {
				$selected = 'selected="selected"';
			} else {
				$selected = '';
			}
		
			$l_ven .= '<option value="'.$v->id_vendedor.'" '.$selected.'>'.$v->nome_vendedor.'</option>';
		
		}
		foreach($anuncios as $val) {
			
			if($val->destaque == 1) {
				$val->destaque = 'Anúncio Destaque';
			} else {
				$val->destaque = 'Anúncio Normal';
			}
			
			$val->preco = $this->util->moeda2br($val->preco);
			
		}
		$l_ven .= '</select>';
		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"pagina" => $this->pagina,
			"id" => $id,
			"select_ativo" => $select_ativo,
			"lista_vendedor" => $l_ven ,
			
			"lista_anuncios" => $anuncios
		);

		$this->load( $this->pagina .'/read', $data );

	}

	public function create() {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {
			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}
		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {
			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);
		}
		// Checks
		foreach( $this->config_ckecks as $check ) {
			$_POST[ $check ] = implode( ",", $_POST[ $check ] );
		}

		// // Seta o indice das imagens no $_POST
		// foreach( $this->config_image as $image ) {

			// $campo = $image[ 'campo' ];

			// if( $src = $this->session->userdata( $campo ) ) {

				// $_POST[ $campo ] = $src;
				// $this->session->unset_userdata( $campo );

			// }

		// }

		// // Seta o indice dos arquivos no $_POST
		// foreach( $this->config_arquivo as $arquivo ) {

			// $campo = $arquivo[ 'campo' ];

			// if( $src = $this->session->userdata( $campo ) ) {

				// $_POST[ $campo ] = $src;
				// $this->session->unset_userdata( $campo );

			// }

		// }

		if( $id = $this->crud->create( $_POST, $this->tabela ) ) {

			// Galeria
			if( $this->config_galeria[ 'galeria' ] ) {

				$galeria_id = $this->session->userdata( 'galeria_id' );
				$this->imput_form->galeria_update( $id, $galeria_id );

			}

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item adicionado com sucesso!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao adicionar o item, tente novamente!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		}

		echo json_encode( $retorno );

	}


	public function update( $id ) {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {

			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}

		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}
		
		// Vincular Vendedor
		
		if(!empty($_POST['vendedor'])) {
		
			$delete = $this->crud->delete( array( "id_pagamento" => $id ), 'vendedores_pagamentos' );
		
			$data = array(
				'id_vendedor' => $_POST['vendedor'],
				'id_pagamento' => $id
			);
			
			$this->crud->create( $data, 'vendedores_pagamentos' );
			
		}
		
		$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );
		
		$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		// // Seta o indice das imagens no $_POST
		// foreach( $this->config_image as $image ) {

			// $campo = $image[ 'campo' ];

			// if( $src = $this->session->userdata( $campo ) ) {

				// $_POST[ $campo ] = $src;
				// $this->session->unset_userdata( $campo );

			// }

		// }

		// // Seta o indice dos arquivos no $_POST
		// foreach( $this->config_arquivo as $arquivo ) {

			// $campo = $arquivo[ 'campo' ];

			// if( $src = $this->session->userdata( $campo ) ) {

				// $_POST[ $campo ] = $src;
				// $this->session->unset_userdata( $campo );

			// }

		// }

		// Faz e verifica se fez o update
		// if( $this->crud->update( array( "id" => $id ), $_POST, $this->tabela ) ) {

			// // Categorias
			// if( !empty( $categorias ) ) {
				// $this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			// }
			// // Tags
			// if( !empty( $tags ) ) {
				// $this->imput_form->tags_update( $this->tabela, $id, $tags );
			// }

			// $this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );

			// $retorno = array(
				// "sucesso" => "true",
				// "link" => base_url().$this->pagina
			// );

		// } else {

			// $this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );

			// $retorno = array(
				// "sucesso" => "true",
				// "link" => base_url().$this->pagina."/read/".$id
			// );

		// }

		echo json_encode( $retorno );

	}

	public function delete( $id ) {

		// Faz e verifica se fez o delete
		$this->crud->delete( array( "id" => $id ), $this->tabela );
		$this->session->set_flashdata( "msg_sucesso", "Item deletado com sucesso!" );
		redirect( base_url().$this->pagina );

	}

	public function image( $campo ) {

		$src = $this->image_form->upload_imagem( $_FILES[ $campo ], $this->config_image[ $campo ][ 'config' ] );

		$this->session->set_userdata( $campo, $src );

		if( $this->config_image[ $campo ][ 'config' ][ 'thumb' ] ) {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].'thumb/'.$src;

		} else {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].$src;

		}

	}

	public function file( $campo ) {

		$this->load->library( 'upload', $this->config_arquivo[ $campo ][ 'config' ] );

		if( $this->upload->do_upload( $campo ) ) {

			$data = $this->upload->data();
			$name = $data[ 'file_name' ];

			$this->session->set_userdata( $campo, $name );

			echo base_url().$this->config_arquivo[ $campo ][ 'config' ][ 'upload_path' ].$name;

		} else {

			echo '0';

		}

	}

	public function galeria() {

		$src = $this->image_form->upload_imagem( $_FILES[ 'arquivo' ], $this->config_galeria[ 'config' ] );

		$_POST[ 'arquivo' ] = $src;

		$id = $this->imput_form->galeria_insert( $_POST );

		$galeria_id = $_POST[ 'elemento' ];

		$session = $this->session->userdata( $galeria_id );

		$session[] = $id;

		$this->session->set_userdata( $galeria_id, $session );
		$this->session->set_userdata( 'galeria_id', $galeria_id );
// var_dump($galeria_id);
		$json = array(
			"src" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].'thumb/'.$src,
			"src_full" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].$src,
			"id" => $id,
			"pagina" => $this->pagina,
			"session" => $session,
			"id" => $id
		);

		echo json_encode( $json );

	}

	public function galeria_legenda() {

		$id = $this->imput_form->galeria_legenda( $_POST[ 'id' ], $_POST[ 'legenda' ] );

	}

	public function galeria_delete() {

		$id = $this->imput_form->galeria_delete( $_POST[ 'id' ] );

	}
	
	public function download() {
		$data_inicio = $this->input->get( "data_inicio" );
		
		$data_fim = $this->input->get( "data_fim" );
		$busca = $this->input->get( "busca" );
		$filtro = $this->input->get( "filtro" );
		$ordem = $this->input->get( "ordem" );
		$status = $this->input->get( "status" );
		$vendedores = $this->input->get( "vendedores" );
		$tipo = $this->input->get( "tipo" );
		
		$tipo_usuario = $this->input->get( "tipo_usuario" );
		$where = array();
		
		$join = array();
		$join[] = array('usuarios us','us.id = pg.id_usuario','left');
		$order = array( "pg.id" => "DESC" );
		if( !empty( $filtro ) || $filtro === '0' ) {

			$where = $this->config_lista[ 'filtro' ][ $filtro ];

		}

		if( !empty( $data_inicio ) && !empty( $data_fim ) ) {
			$data_inicio = $this->util->date2us( $data_inicio );
			$data_inicio = strtotime( $data_inicio );
			$data_fim = $this->util->date2us( $data_fim );
			$data_fim = strtotime( $data_fim );
			$where[ "pg.data_cadastro >= '". $data_inicio ."' AND pg.data_cadastro <= '". $data_fim ."'" ] = NULL;
		}
		if( !empty( $busca ) ) {
			
			if($tipo_usuario == 1) { //Proprietario
				$join_tb = 'dados_proprietario';
			} else if($tipo_usuario == 2) { //Corretor
				$join_tb = 'dados_corretor';
			}
			$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%'" ] = NULL;
			$join[] = array($join_tb.' dp','dp.id = us.perfil','left');
		}
		
		if($tipo != '') {
			$where[ "pg.tipo = ".$tipo ] = NULL;
		}
		if( !empty( $ordem ) ) {
			if( isset( $this->config_lista[ 'order' ][ $ordem ] ) ) {
				$order = $this->config_lista[ 'order' ][ $ordem ];
			}
		}
		if( !empty( $status )) {
			$where['pg.pago'] = $this->config_lista[ 'status' ][ $status ];
		}
		
		if( !empty( $tipo_usuario )) {
			$where['us.tipo'] = $tipo_usuario;
		}
		
		/* Select de Vendedores */
		
		$config = array(
			'campos' => 'nome as "nome_vendedor", id as "id_vendedor"',
			'tabela' => 'vendedores',
			'orderBy' => array('nome' => 'DESC')
		);
		
		$this->select->set($config);
		$vendedores_lista = $this->select->resultado();
		
		if( !empty( $vendedores )) {
			$where['vp.id_vendedor'] = $vendedores;
			$join[] = array('vendedores_pagamentos vp','vp.id_pagamento = pg.id','left');
		}
		$config = array(
			'campos' => 'pg.id as "id_venda", pg.data_cadastro as "data", pg.pago as "status", pg.tipo, pg.tipoPagamento, us.tipo as "tipo_usuario", us.perfil as "perfil_usuario", pg.valor',
			'tabela' => 'pagamentos pg',
			'where' => $where,
			'join' => $join,
			'orderBy' => $order
		);
		
		$this->select->set($config);
		$total_vendas = $this->select->total();
		$ven = $this->select->resultado();
		$lista = '';
		$t_anuncio = 0;
		$t_destaque = 0;		
		foreach( $ven as $v ) {
			$id_venda = $v->id_venda;
			$data = $v->data;
			if($v->tipo_usuario == 1) { //Proprietario
				$tab_user = 'dados_proprietario';
			} else if($v->tipo_usuario == 2) { //Corretor
				$tab_user = 'dados_corretor';
			}
			$config = array(
				'campos' => 'dp.nome as "nome_usuario"',
				'tabela' => $tab_user.' dp',
				'where' => array('id' => $v->perfil_usuario)
	
			);
			
			$this->select->set($config);
			$dados_user = $this->select->resultado();
			
			$nome_usuario = utf8_decode($dados_user[0]->nome_usuario);
			$valor = $this->util->moeda2br($v->valor);
			$config = array(
				'campos' => 'v.nome as "nome_vendedor"',
				'tabela' => 'vendedores_pagamentos vp',
				'where' => array('vp.id_pagamento' => $id_venda),
				'join' => array(
							array('vendedores v','v.id = vp.id_vendedor','left')
						)
			);
			
			$this->select->set($config);
			$vendedor = $this->select->total();
			
			if($vendedor > 0) {
				$vendedor = $this->select->resultado();
				$nome_vendedor = $vendedor[0]->nome_vendedor;
			} else {
				$nome_vendedor = '';
			}
			
			if($v->status == 0) { $status = '<p style="color: gray">Aguar. Pagamento</p>'; }
			if($v->status == 1) { $status = '<p style="color: green">Pago</p>'; }
			if($v->status == 2) { $status = '<p style="color: red">Cancelado. Pagamento</p>'; }
			if($v->tipo == 0) { $tipo = '<p>Venda pelo <strong>Site</strong></p>'; }
			
			if($v->tipo == 1) { $tipo = '<p>Venda pelo <strong>Admin</strong></p>'; }
			if($v->tipoPagamento == 1) { $tipoPagamento = '<p>Pago via <strong>PagSeguro</strong></p>'; }else{$tipoPagamento = '<p>Pago via <strong>Outros</strong></p>';}
			
			//Total de Anuncios
			$config = array(
	
				'campos' => 'pg.id',
	
				'tabela' => 'pagamentos_imoveis pg',
	
				'where' => array('id_pagamento' => $id_venda)
	
			);
	
			$this->select->set($config);
			$total_anuncio = $this->select->total();
			
			//Total de Destaque
			$config = array(
	
				'campos' => 'id',
	
				'tabela' => 'pagamentos_imoveis',
	
				'where' => array('id_pagamento' => $id_venda, 'destaque' => 1)
	
			);
	
			$this->select->set($config);
			$total_destaque = $this->select->total();
			
			$t_anuncio += $total_anuncio;
			$t_destaque += $total_destaque;
			$lista .= '<tr style="background:#efefef;color:#000;">
				<td>'.$id_venda.'</td>
				<td>'.$status.'</td>
				<td>'.date('d/m/Y', $data).'</td>
				<td>R$ '.$valor.'</td>
				<td>'.$nome_usuario.'</td>
				<td>'.$nome_vendedor.'</td>
				
				<td>'.$tipo.'</td>
				
				<td>'.$total_anuncio.'</td>
				
				<td>'.$total_destaque.'</td>
				<td>'.$tipoPagamento.'</td>
			</tr>';
		}

		$data = array(
			'lista_pagamentos' => $lista,
			'total_anuncios' => $t_anuncio,
			'total_destaques' => $t_destaque
		);
		
		$table = $this->parser->parse( $this->pagina.'/table', $data, true );
		$this->util->excel( $table, "pedidos_". date( "d-m-Y" ) .".xls" );
		
	}

}