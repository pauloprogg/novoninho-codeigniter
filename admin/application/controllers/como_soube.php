<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Como_soube extends Main_Controller {

	private $pagina = "como-soube";
	private $tabela = "como_soube";

	public function __construct() {

		parent::__construct();
		$this->checkLogin();

	}
	private $config_lista = array(
		'busca' => 'nome',
		'filtro' => 'ativo',
		'filtro' => array(
			'ativos' => array( 'ativo' => '1' ),
			'inativos' => array( 'ativo' => '0' )
		),
		'order' => array(
			'nome_asc' => array( 'nome' => 'ASC' ),
			'nome_desc' => array( 'nome' => 'DESC' ),
			'data_asc' => array( 'data_cadastro' => 'ASC' ),
			'data_desc' => array( 'data_cadastro' => 'DESC' )
		),
		'num_link' => 5,
		'qtd_por_pagina' => 15
	);
	// Edição
	public function index() {

		$where = array();
		$order = array( "id" => "DESC" );

		$retorno = $this->crud->read_paginacao( $where, $this->tabela, $order, $this->config_lista[ 'qtd_por_pagina' ], $this->pagina, $this->config_lista[ 'num_link' ] );

		$lista = $retorno[ 'result' ];
		$paginacao = $retorno[ 'paginacao' ];

		$data = array(
			"lista" => $lista,
			"pagina" => $this->pagina,
			"paginacao" => $paginacao
		);

		$this->load( $this->pagina .'/index', $data );


	}

	public function update( $id ) {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);
		


		// Faz e verifica se fez o update
		if( $this->crud->update( array( "id" => $id ), $_POST, $this->tabela ) ) {

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina."read/".$id
			);

		}

		echo json_encode( $retorno );

	}

	public function add() {

		$campos_form_esquerdo = array(
			"texto" => array(
				"type" => "text",
				"label" => "Resposta",
				"name" => "texto",
				"value" => "",
				"width" => "255"
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens


		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"pagina" => $this->pagina
		);

		$this->load( $this->pagina .'/add', $data );

	}

	public function create() {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		if( $id = $this->crud->create( $_POST, $this->tabela ) ) {


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao adicionar o item, tente novamente!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		}

		echo json_encode( $retorno );

	}

	public function delete( $id ) {

		// Faz e verifica se fez o delete
		$this->crud->delete( array( "id" => $id ), $this->tabela );
		$this->session->set_flashdata( "msg_sucesso", "Item deletado com sucesso!" );
		redirect( base_url().$this->pagina );
	}

	public function read( $id ) {

		$item = $this->crud->read( array( 'id'=> $id ), $this->tabela );

		if( empty( $item ) ) {
			redirect( base_url(). $this->pagina );
		}

		$item = $item[0];

		$campos_form_esquerdo = array(
			"texto" => array(
				"type" => "text",
				"label" => "Resposta",
				"name" => "texto",
				"value" => $item->texto,
				"width" => "255"
			)
		);


		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );


		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"id" => $id,
			"pagina" => $this->pagina
		);

		$this->load( $this->pagina .'/read', $data );

	}

}