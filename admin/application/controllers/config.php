<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Config extends Main_Controller {

	private $pagina = "config";
	private $tabela = "config";

	private $config_lista = array();

	private $config_ckecks = array();

	// Configurações de arquivos
	private $config_arquivo = array();

	// Configurações para imagem
	private $config_image = array();

	// Configurações para galeria
	private $config_galeria = array(

		'galeria' => false,
		'config' => array()

	);

	public function __construct() {

		parent::__construct();
		$this->checkLogin();

	}

	// Edição
	public function index() {

		$id = "1";

		$item = $this->crud->read( array( 'id'=> $id ), $this->tabela );

		if( empty( $item ) ) {
			redirect( base_url(). $this->pagina );
		}

		$item = $item[0];

		$campos_form_esquerdo = array(
			// "facebook" => array(
				// "type" => "text",
				// "label" => "Facebook",
				// "name" => "facebook",
				// "value" => $item->facebook,
				// "width" => "12"
			// ),
			// "twitter" => array(
				// "type" => "text",
				// "label" => "Twitter",
				// "name" => "twitter",
				// "value" => $item->twitter,
				// "width" => "12"
			// ),
			"preco_normal" => array(
				"type" => "text",
				"label" => "Preço Padrão Normal",
				"descricao" => "(Ex: 100.00)",
				"name" => "preco_normal",
				"value" => $item->preco_normal,
				"width" => "12"
			),
			"preco_destaque" => array(
				"type" => "text",
				"label" => "Preço Padrão Destaque",
				"descricao" => "(Ex: 100.00)",
				"name" => "preco_destaque",
				"value" => $item->preco_destaque,
				"width" => "12"
			),
			"preco_super_destaque" => array(
				"type" => "text",
				"label" => "Preço Padrão Super Destaque",
				"descricao" => "(Ex: 100.00)",
				"name" => "preco_super_destaque",
				"value" => $item->preco_super_destaque,
				"width" => "12"
			),

			"preco_feirao" => array(
				"type" => "text",
				"label" => "Preço Padrão Feirão",
				"descricao" => "(Ex: 100.00)",
				"name" => "preco_feirao",
				"value" => $item->preco_feirao,
				"width" => "12"
			),
			"preco_destaque_feirao" => array(
				"type" => "text",
				"label" => "Preço Padrão Destaque Feirão",
				"descricao" => "(Ex: 100.00)",
				"name" => "preco_destaque_feirao",
				"value" => $item->preco_destaque_feirao,
				"width" => "12"
			),
			"preco_super_destaque_feirao" => array(
				"type" => "text",
				"label" => "Preço Padrão Super Destaque Feirão",
				"descricao" => "(Ex: 100.00)",
				"name" => "preco_super_destaque_feirao",
				"value" => $item->preco_super_destaque_feirao,
				"width" => "12"
			),
			
			"preco_web" => array(
				"type" => "text",
				"label" => "Preço Padrão Web",
				"descricao" => "(Ex: 100.00)",
				"name" => "preco_web",
				"value" => $item->preco_web,
				"width" => "12"
			),

			"expiracao" => array(
				"type" => "text",
				"label" => "Expiração",
				"name" => "expiracao",
				"descricao" => "Em Dias",
				"value" => $item->expiracao,
				"width" => "12"
			),
			// "expiracao" => array(
			// 	"type" => "select",
			// 	"label" => "Feirão",
			// 	"name" => "feirao",
			// 	"descricao" => "",
			// 	"value" => $item->feirao,
			// 	"itens" => array(
			// 		'1' => "Sim",
			// 		'2' => "Não"
			// 	),
			// 	"width" => "12"
			// ),
			// "tipo_pagamento" => array(
			// 	"type" => "select",
			// 	"label" => "Tipo de Pagamento",
			// 	"name" => "tipo_pagamento",
			// 	"descricao" => "  ",
			// 	"value" => $item->tipo_pagamento,
			// 	"itens" => array(
			// 		'1' => "Pagseguro",
			// 		'2' => "Cielo"
			// 	),
			// 	"width" => "12"
			// )
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );
					
		$arrayPars = array( array("value" => 1, "label" => "Pagseguro"), array("value" => 2, "label" => "Cielo") );
		$selectPag = $this->imput_form->select_2('Tipo de Pagamento' , "tipo_pagamento", $arrayPars, $item->tipo_pagamento, array('descricao' =>""));
		$arrayFeirao = array(array("value" => 1, "label" => "Sim"), array("value" => 0, "label" => "Não"));	
		$selectFeirao = $this->imput_form->select_2('Feirão' , "feirao", $arrayFeirao, $item->feirao, array('descricao' =>""));
		//estado
		$config = array(
			'campos' => 'uf_codigo as value, uf_descricao as label',
			'tabela' => 'cep_uf'
		);
		$this->select->set( $config );
		$estados = $this->select->resultado();
		$estadoSelect = '';
		foreach($estados as $val ) {			
			$estadoSelect .= '<option value="'.$val->value.'">'.$val->label.'</option>';			
		}
		//cidade
		$config = array(
			'campos' => 'ci.cidade_codigo, ci.cidade_descricao, fe.id_cidade',
			'tabela' => 'cep_cidade as ci',
			'join' => array(
				array( 'feirao_cidade as fe', 'ci.cidade_codigo = fe.id_cidade', 'left' ),
			),
			'where' => 'fe.feirao = 1'
		);
		$this->select->set( $config );
		$cidades = $this->select->resultado();
		
		$cidadeSelect = '';
		
		foreach($cidades as $val ) {
			
			$selected = '';
			
			 if(!empty($val->id_cidade)) {
				$selected = 'selected="selected"';	
			}
			$cidadeSelect .= '<option value="'.$val->id_cidade.'" '.$selected.'>'.$val->cidade_descricao.'</option>';
		}
		//regiao
		$config = array(
			'campos' => 'id as value, nome as label',
			'tabela' => 'regiao'
		);
		$this->select->set( $config );
		$regiao = $this->select->resultado();
		$regiaoSelect = '';
		foreach($regiao as $val ) {			
			$regiaoSelect .= '<option value="'.$val->value.'">'.$val->label.'</option>';			
		}

		$data = array(
			"estado" => $estadoSelect,
			"cidade" => $cidadeSelect,
			"regiao" => $regiaoSelect,
			"form_esquerdo" => $form_esquerdo,
			"selectPag" => $selectPag,
			"selectFeirao" => $selectFeirao,
			"pagina" => $this->pagina,
			"id" => $id
		);

		$this->load( $this->pagina .'/read', $data );

	}

	public function update( $id ) {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {

			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}

		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}
		
				
		//despresando o estado
		if( isset($_POST['estado'] ) ) {
			unset($_POST['estado']);
		}
		$arrayCidades = array();
		if( isset($_POST['cidade'] ) ) {
			$arrayCidades = $_POST['cidade'];
			unset($_POST['cidade']);
		}
		$arrayCidadesRegiao = array();
		if( isset($_POST['regiao'] ) ) {
			if(!empty($_POST['regiao'])){
				$config = array(
					'campos' => 'id_regiao, id_cidade',
					'tabela' => 'regiao_cidade',
					'where' => array('id_regiao' => $_POST['regiao'])
				);
				$this->select->set( $config );
				$arrayCidadesRegiao = $this->select->resultado();
			}
			
			unset($_POST['regiao']);
		}
		
		

		// Faz e verifica se fez o update
		if( $this->crud->update( array( "id" => $id ), $_POST, $this->tabela ) ) {
			
			//Query para mudar o preço de todos os usuario
			$this->db->query("UPDATE usuarios SET preco_normal = '".$_POST['preco_normal']."', preco_destaque = '".$_POST['preco_destaque']."', preco_super_destaque = '".$_POST['preco_super_destaque']."', preco_feirao = '".$_POST['preco_feirao']."', preco_destaque_feirao = '".$_POST['preco_destaque_feirao']."', preco_super_destaque_feirao = '".$_POST['preco_super_destaque_feirao']."', preco_web = '".$_POST['preco_web']."', expiracao = '".$_POST['expiracao']."'");
			//=============================//
			$this->crud->delete( array( "feirao" => "1" ), 'feirao_cidade' );
			
			if(!empty($arrayCidades)){
				$dataCidades = array();
				
				foreach ($arrayCidades as $v) {
					$dataCidades [] = array('id_cidade' => $v, 'feirao' => 1);
				}
				
				$this->db->insert_batch('feirao_cidade', $dataCidades);
			}
			if(!empty($arrayCidadesRegiao)){
				
				foreach ($arrayCidadesRegiao as $v) {
					$dataCidadesRegiao [] = array('id_cidade'=>$v->id_cidade, 'feirao' => 1);
				}
				$this->db->insert_batch('feirao_cidade', $dataCidadesRegiao);
			}
		
			$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );
			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina."read/".$id
			);

		}

		echo json_encode( $retorno );

	}

	public function image( $campo ) {

		$src = $this->image_form->upload_imagem( $_FILES[ $campo ], $this->config_image[ $campo ][ 'config' ] );

		$this->session->set_userdata( $campo, $src );

		if( $this->config_image[ $campo ][ 'config' ][ 'thumb' ] ) {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].'thumb/'.$src;

		} else {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].$src;

		}

	}

	public function file( $campo ) {

		$this->load->library( 'upload', $this->config_arquivo[ $campo ][ 'config' ] );

		if( $this->upload->do_upload( $campo ) ) {

			$data = $this->upload->data();
			$name = $data[ 'file_name' ];

			$this->session->set_userdata( $campo, $name );

			echo base_url().$this->config_arquivo[ $campo ][ 'config' ][ 'upload_path' ].$name;

		} else {

			echo '0';

		}

	}

	public function galeria() {

		$src = $this->image_form->upload_imagem( $_FILES[ 'arquivo' ], $this->config_galeria[ 'config' ] );

		$_POST[ 'arquivo' ] = $src;

		$id = $this->imput_form->galeria_insert( $_POST );

		$galeria_id = $this->session->userdata( "galeria_id" );

		$session = $this->session->userdata( $galeria_id );

		$session[] = $id;

		$this->session->set_userdata( $galeria_id, $session );

		$json = array(
			"src" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].'thumb/'.$src,
			"src_full" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].$src,
			"id" => $id,
			"pagina" => $this->pagina,
			"session" => $session,
			"id" => $galeria_id
		);

		echo json_encode( $json );

	}

	public function galeria_legenda() {

		$id = $this->imput_form->galeria_legenda( $_POST[ 'id' ], $_POST[ 'legenda' ] );

	}

	public function galeria_delete() {

		$id = $this->imput_form->galeria_delete( $_POST[ 'id' ] );

	}

}