<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Corretores extends Main_Controller {



	private $pagina = "corretores";

	private $tabela = "usuarios";



	private $config_lista = array(

		'busca' => 'dp.nome',

		'filtro' => array(

			'ativos' => array( 'us.ativo' => '1' ),

			'inativos' => array( 'us.ativo' => '0' )

		),

		'order' => array(

			'nome_asc' => array( 'dp.nome' => 'ASC' ),

			'nome_desc' => array( 'dp.nome' => 'DESC' ),

			'data_asc' => array( 'us.data_cadastro' => 'ASC' ),

			'data_desc' => array( 'us.data_cadastro' => 'DESC' )

		),

		'num_link' => 10,

		'qtd_por_pagina' => 25

	);



	private $config_ckecks = array();



	// Configurações de arquivos

	private $config_arquivo = array();



	// Configurações para imagem

	private $config_image = array(

		'imagem' => array(

			'campo' => 'imagem',

			'config' => array(

				'thumb' => array(

					'greyscale' => false, // bolean

					'largura' => 140, // num

					'altura' => 140 // num

				),

				'marca' => false,

				'image_path' => '../assets/uploads/noticias/', // string [ caminho da imagem é necessário ter uma pasta thumb (se tiver)]

				'largura' => 540, // num

				'altura' => 340, // num



				'redimensionar' => true, // boolean [ redimensiona a imagem ]

				'proporcional' => false, // boolean [ redimenciona proporcionalmente ]

				'cortar' => true, // boolean [ corta a imagem sem redimencionar ]

				'converter' => false, // boolean [ converte a image para jpg ]

				'greyscale' => false, // boolean [ coloca efeito greyscale ]

				//'qualidade' => 80 // num [ configura a qualidade da imagem ]

			)

		)



	);



	// Configurações para galeria

	private $config_galeria = array(



		'galeria' => 0,

		'config' => array()



	);



	public function __construct() {



		parent::__construct();

		$this->checkLogin();



	}



	public function index() {



		$busca = $this->input->get( "busca" );

		$filtro = $this->input->get( "filtro" );

		$ordem = $this->input->get( "ordem" );



		if( !empty( $filtro )) {



			$where = $this->config_lista[ 'filtro' ][ $filtro ];



		}



		if( !empty( $busca ) ) {



			$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%'" ] = NULL;



		}



		if( !empty( $ordem ) ) {



			if( isset( $this->config_lista[ 'order' ][ $ordem ] ) ) {



				$order = $this->config_lista[ 'order' ][ $ordem ];



			}



		}

		

		$where['us.tipo'] = 2;

		$order['us.id'] = "DESC";



		//$retorno = $this->crud->read_paginacao( $where, $this->tabela, $order, $this->config_lista[ 'qtd_por_pagina' ], $this->pagina, $this->config_lista[ 'num_link' ] );



		// $lista = $retorno[ 'result' ];

		// $paginacao = $retorno[ 'paginacao' ];

		

		/*  */

		

		$config = array(

			'campos' => 'us.tipo as "tipoUsuario", us.id as "idUsuario", us.perfil as "perfilUsuario", dp.nome as "nomeUsuario"',

			'tabela' => 'usuarios us',

			'where' => $where,

			'join' => array(

						array('dados_corretor dp','dp.id = us.perfil','left')

					),

			'orderBy' => $order

		);



		$this->select->set($config);

		$paginacao = $this->select->paginacao( "corretores", 5);

		$resultado = $this->select->resultado();

		

		$lista = '';

		

		foreach( $resultado as $val ) {

		

			$id_usuario = $val->idUsuario;

			

			$tipo = $val->tipoUsuario;

			$id_perfil = $val->perfilUsuario;

			

			$nome = $val->nomeUsuario;

			

			/* Numero de Anuncios */

			

			$config = array(

				'campos' => 'id',

				'tabela' => 'imoveis',

				'where' => array('id_usuario' => $id_usuario)

			);



			$this->select->set($config);

			$num_anuncio = $this->select->total();

			

			/* Número de Publicados */

			

			$config = array(

				'campos' => 'id',

				'tabela' => 'imoveis',

				'where' => array('id_usuario' => $id_usuario, 'status' => 3)

			);



			$this->select->set($config);

			$num_publicados = $this->select->total();

			

			/* Número de Destaques */

			

			$config = array(

				'campos' => 'id',

				'tabela' => 'imoveis',

				'where' => array('id_usuario' => $id_usuario, 'destaque' => 1, 'status' => 3)

			);



			$this->select->set($config);

			$num_destaque = $this->select->total();



			/* Número de Super Destaques */

			

			$config = array(

				'campos' => 'id',

				'tabela' => 'imoveis',

				'where' => array('id_usuario' => $id_usuario, 'super_destaque' => 1, 'status' => 3)

			);



			$this->select->set($config);

			$num_super_destaque = $this->select->total();

			/* Número de Feirão */

			$config = array(

				'campos' => 'id',

				'tabela' => 'imoveis',

				'where' => array('id_usuario' => $id_usuario, 'feirao' => 1, 'status' => 3)

			);



			$this->select->set($config);

			$num_feirao = $this->select->total();



			

			$lista .= '<div class="row table">

							<!-- <div class="columns small-12 medium-1 large-1"> <p><strong>ID:</strong> '.$id_usuario.'</p> </div> -->

							<div class="columns small-12 medium-1 large-1"> <p><strong>Nome:</strong> '.$nome.'</p> </div>

							<div class="columns small-12 medium-2 large-2"> <p><strong>Anuncios:</strong> '.$num_anuncio.'</p> </div>

							<div class="columns small-12 medium-2 large-2"> <p><strong>Publicados:</strong> '.$num_publicados.'</p> </div>

							<div class="columns small-12 medium-2 large-2"> <p><strong>Destaque:</strong> '.$num_destaque.'</p> </div>

							<div class="columns small-12 medium-2 large-2 text-center"> <p><strong>Super Destaque:</strong> '.$num_super_destaque.'</p> </div>

							<div class="columns small-12 medium-2 large-2 text-center"> <p><strong>Feirão:</strong> '.$num_feirao.'</p> </div>



							<div class="columns small-12 medium-1 large-1">

								<div class="columns small-12 medium-12 large-12 text-left"><a href="'.base_url().$this->pagina.'/read/'.$id_usuario.'">Editar</a></div>

								<!-- <div class="columns small-6 medium-6 large-6 text-right"><a href="{base_url}{pagina}/delete/{id}" class="ac red">Deletar</a></div> -->

							</div>



							<div class="clearfix"></div>

						</div>



						<div class="row"><hr></div>';

		

		}



		$data = array(

			"lista" => $lista,

			"pagina" => $this->pagina,

			"paginacao" => $paginacao

		);



		$this->load( $this->pagina .'/index', $data );



	}



	// Edição

	public function read( $id ) {



		$item = $this->crud->read( array( 'id'=> $id ), $this->tabela, array( 'id' => 'DESC' ) );



		if( empty( $item ) ) {

			redirect( base_url(). $this->pagina );

		}



		$item = $item[0];



		$dados_item = $this->crud->read( array( 'id'=> $item->perfil ), $this->util->tabelaPerfil( $item->tipo ), array( 'id' => 'DESC' ) );
		//print_r($dados_item);exit();


		if( empty( $dados_item ) ) {

			redirect( base_url(). $this->pagina );

		}



		$dados_item = $dados_item[0];



		$campos_form_esquerdo = array(

			"nome" => array(

				"type" => "text",

				"label" => "Usuário",

				"name" => "nome",

				"value" => $dados_item->nome,

				"width" => "12",

				"disabled" => true

			),
			"email" => array(

				"type" => "text",

				"label" => "E-mail",

				"name" => "email",

				"value" => $dados_item->email,

				"width" => "12",

				"disabled" => true

			),

			"telefone" => array(

				"type" => "text",

				"label" => "Telefone",

				"name" => "telefone",

				"value" => $dados_item->telefone,

				"width" => "12",

				"disabled" => true

			),

			"melhor_horario" => array(

				"type" => "text",

				"label" => "Melhor horário",

				"name" => "melhor_horario",

				"value" => $dados_item->melhor_horario,

				"width" => "12",

				"disabled" => true

			),
			"creci" => array(

				"type" => "text",

				"label" => "Creci",

				"name" => "creci",

				"value" => $dados_item->creci,

				"width" => "12",

				"disabled" => true

			),
			"como_soube" => array(

				"type" => "text",

				"label" => "Como Soube",

				"name" => "como_soube",

				"value" => $dados_item->como_soube,

				"width" => "12",

				"disabled" => true
			),
			"vendedor" => array(

				"type" => "text",

				"label" => "Vendedor",

				"name" => "vendedor",

				"value" => $dados_item->vendedor,

				"width" => "12",

				"disabled" => true
			),

			"preco_normal" => array(

				"type" => "text",

				"label" => "Preço do Anúncio Comum",

				"descricao" => "(Ex: 100.00)",

				"name" => "preco_normal",

				"value" => $item->preco_normal,

				"width" => "6"

			),

			"preco_destaque" => array(

				"type" => "text",

				"label" => "Preço do Anúncio Destaque",

				"descricao" => "(Ex: 100.00)",

				"name" => "preco_destaque",

				"value" => $item->preco_destaque,

				"width" => "6"

			),

			"preco_super_destaque" => array(

				"type" => "text",

				"label" => "Preço do Anúncio Super Destaque",

				"descricao" => "(Ex: 100.00)",

				"name" => "preco_super_destaque",

				"value" => $item->preco_super_destaque,

				"width" => "6"

			),

			"preco_feiraoe" => array(

				"type" => "text",

				"label" => "Preço do Anúncio Feirão",

				"descricao" => "(Ex: 100.00)",

				"name" => "preco_feirao",

				"value" => $item->preco_feirao,

				"width" => "6"

			),

			"preco_web" => array(

				"type" => "text",

				"label" => "Preço do Anúncio Web",

				"descricao" => "(Ex: 100.00)",

				"name" => "preco_web",

				"value" => $item->preco_web,

				"width" => "6"

			),

			"expiracao" => array(

				"type" => "text",

				"label" => "Expiração",

				"name" => "expiracao",

				"descricao" => "Em Dias",

				"value" => $item->expiracao,

				"width" => "6",

				"required" => false

			)

		);



		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );



		// seta os campos de aquivos e imagens

		$path_image = base_url() ."../assets/uploads/noticias/thumb/";



		$arquivos = array(

			"imagem" => array(

				"type" => "image",

				"label" => "Imagem",

				"name" => "imagem",

				"value" => ( !empty( $item->imagem ) ) ? $path_image. $item->imagem : "",

				"width" => $this->config_image[ 'imagem' ][ 'config' ][ 'largura' ],

				"height" => $this->config_image[ 'imagem' ][ 'config' ][ 'altura' ]

			)

		);



		$form_arquivos = $this->imput_form->file( $arquivos );



		if($item->ativo == 1) {

			$ativo = 'selected="selected"';

			$inativo = '';

		} else {

			$ativo = '';

			$inativo = 'selected="selected"';

		}

		

		$select_ativo = '<select name="ativo" id="slcPublicar" class="required"/>

							<option value="1" '.$ativo.'>Ativo</option>

							<option value="0" '.$inativo.'>Inativo</option>

						</select>';

		

		$data = array(

			"form_esquerdo" => $form_esquerdo,

			"form_arquivos" => $form_arquivos,

			"pagina" => $this->pagina,

			"id" => $id,

			"select_ativo" => $select_ativo

		);



		$this->load( $this->pagina .'/read', $data );



	}



	public function create() {



		$retorno = array(

			"sucesso" => "false",

			"link" => base_url()

		);



		// encrypt da senha se existir o campo

		if( isset( $_POST[ 'senha' ] ) ) {

			if( empty( $_POST[ 'senha' ] ) ) {



				unset( $_POST[ 'senha' ] );



			} else {



				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );



			}

		}



		// Categorias

		$categorias = "";

		if( isset( $_POST[ 'categorias' ] ) ) {



			$categorias = $_POST[ 'categorias' ];

			unset($_POST[ 'categorias' ]);

		}



		// Tags

		$tags = "";

		if( isset( $_POST[ 'tags' ] ) ) {



			$tags = $_POST[ 'tags' ];

			unset($_POST[ 'tags' ]);



		}



		// Checks

		foreach( $this->config_ckecks as $check ) {



			$_POST[ $check ] = implode( ",", $_POST[ $check ] );



		}



		// Seta o indice das imagens no $_POST

		foreach( $this->config_image as $image ) {



			$campo = $image[ 'campo' ];



			if( $src = $this->session->userdata( $campo ) ) {



				$_POST[ $campo ] = $src;

				$this->session->unset_userdata( $campo );



			}



		}



		// Seta o indice dos arquivos no $_POST

		foreach( $this->config_arquivo as $arquivo ) {



			$campo = $arquivo[ 'campo' ];



			if( $src = $this->session->userdata( $campo ) ) {



				$_POST[ $campo ] = $src;

				$this->session->unset_userdata( $campo );



			}



		}



		if( $id = $this->crud->create( $_POST, $this->tabela ) ) {



			// Galeria

			if( $this->config_galeria[ 'galeria' ] ) {



				$galeria_id = $this->session->userdata( 'galeria_id' );

				$this->imput_form->galeria_update( $id, $galeria_id );



			}



			// Categorias

			if( !empty( $categorias ) ) {

				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );

			}

			// Tags

			if( !empty( $tags ) ) {

				$this->imput_form->tags_update( $this->tabela, $id, $tags );

			}



			$this->session->set_flashdata( "msg_sucesso", "Item adicionado com sucesso!" );





			$retorno = array(

				"sucesso" => "true",

				"link" => base_url().$this->pagina

			);



		} else {



			$this->session->set_flashdata( "msg_erro", "Erro ao adicionar o item, tente novamente!" );





			$retorno = array(

				"sucesso" => "true",

				"link" => base_url().$this->pagina

			);



		}



		echo json_encode( $retorno );



	}





	public function update( $id ) {



		$retorno = array(

			"sucesso" => "false",

			"link" => base_url()

		);



		// encrypt da senha se existir o campo

		if( isset( $_POST[ 'senha' ] ) ) {



			if( empty( $_POST[ 'senha' ] ) ) {



				unset( $_POST[ 'senha' ] );



			} else {



				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );



			}



		}



		// Categorias

		$categorias = "";

		if( isset( $_POST[ 'categorias' ] ) ) {



			$categorias = $_POST[ 'categorias' ];

			unset($_POST[ 'categorias' ]);

		}



		// Tags

		$tags = "";

		if( isset( $_POST[ 'tags' ] ) ) {



			$tags = $_POST[ 'tags' ];

			unset($_POST[ 'tags' ]);



		}



		// Checks

		foreach( $this->config_ckecks as $check ) {



			$_POST[ $check ] = implode( ",", $_POST[ $check ] );



		}



		// Seta o indice das imagens no $_POST

		foreach( $this->config_image as $image ) {



			$campo = $image[ 'campo' ];



			if( $src = $this->session->userdata( $campo ) ) {



				$_POST[ $campo ] = $src;

				$this->session->unset_userdata( $campo );



			}



		}



		// Seta o indice dos arquivos no $_POST

		foreach( $this->config_arquivo as $arquivo ) {



			$campo = $arquivo[ 'campo' ];



			if( $src = $this->session->userdata( $campo ) ) {



				$_POST[ $campo ] = $src;

				$this->session->unset_userdata( $campo );



			}



		}



		// Faz e verifica se fez o update

		if( $this->crud->update( array( "id" => $id ), $_POST, $this->tabela ) ) {



			// Categorias

			if( !empty( $categorias ) ) {

				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );

			}

			// Tags

			if( !empty( $tags ) ) {

				$this->imput_form->tags_update( $this->tabela, $id, $tags );

			}



			$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );



			$retorno = array(

				"sucesso" => "true",

				"link" => base_url().$this->pagina

			);



		} else {



			$this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );



			$retorno = array(

				"sucesso" => "true",

				"link" => base_url().$this->pagina."/read/".$id

			);



		}



		echo json_encode( $retorno );



	}



	public function delete( $id ) {



		// Faz e verifica se fez o delete

		$this->crud->delete( array( "id" => $id ), $this->tabela );

		$this->session->set_flashdata( "msg_sucesso", "Item deletado com sucesso!" );

		redirect( base_url().$this->pagina );



	}



	public function image( $campo ) {



		$src = $this->image_form->upload_imagem( $_FILES[ $campo ], $this->config_image[ $campo ][ 'config' ] );



		$this->session->set_userdata( $campo, $src );



		if( $this->config_image[ $campo ][ 'config' ][ 'thumb' ] ) {



			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].'thumb/'.$src;



		} else {



			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].$src;



		}



	}



	public function file( $campo ) {



		$this->load->library( 'upload', $this->config_arquivo[ $campo ][ 'config' ] );



		if( $this->upload->do_upload( $campo ) ) {



			$data = $this->upload->data();

			$name = $data[ 'file_name' ];



			$this->session->set_userdata( $campo, $name );



			echo base_url().$this->config_arquivo[ $campo ][ 'config' ][ 'upload_path' ].$name;



		} else {



			echo '0';



		}



	}



	public function galeria() {



		$src = $this->image_form->upload_imagem( $_FILES[ 'arquivo' ], $this->config_galeria[ 'config' ] );



		$_POST[ 'arquivo' ] = $src;



		$id = $this->imput_form->galeria_insert( $_POST );



		$galeria_id = $_POST[ 'elemento' ];



		$session = $this->session->userdata( $galeria_id );



		$session[] = $id;



		$this->session->set_userdata( $galeria_id, $session );

		$this->session->set_userdata( 'galeria_id', $galeria_id );

// var_dump($galeria_id);

		$json = array(

			"src" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].'thumb/'.$src,

			"src_full" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].$src,

			"id" => $id,

			"pagina" => $this->pagina,

			"session" => $session,

			"id" => $id

		);



		echo json_encode( $json );



	}



	public function galeria_legenda() {



		$id = $this->imput_form->galeria_legenda( $_POST[ 'id' ], $_POST[ 'legenda' ] );



	}



	public function galeria_delete() {



		$id = $this->imput_form->galeria_delete( $_POST[ 'id' ] );



	}



}