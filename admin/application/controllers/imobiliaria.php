<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Imobiliaria extends Main_Controller {


	private $pagina = "imobiliaria";

	private $tabela = "usuarios";



	private $config_lista = array(

		'busca' => 'dp.nome',

		'filtro' => array(

			'ativos' => array( 'us.ativo' => '1' ),

			'inativos' => array( 'us.ativo' => '0' )

		),

		'order' => array(

			'nome_asc' => array( 'dp.nome' => 'ASC' ),

			'nome_desc' => array( 'dp.nome' => 'DESC' ),

			'data_asc' => array( 'us.data_cadastro' => 'ASC' ),

			'data_desc' => array( 'us.data_cadastro' => 'DESC' )

		),

		'num_link' => 10,

		'qtd_por_pagina' => 25

	);



	private $config_ckecks = array();



	// Configurações de arquivos

	private $config_arquivo = array();



	// Configurações para imagem

	private $config_image = array(

		'imagem' => array(

			'campo' => 'imagem',

			'config' => array(

				'thumb' => array(

					'greyscale' => false, // bolean

					'largura' => 140, // num

					'altura' => 140 // num

				),

				'marca' => false,

				'image_path' => '../assets/uploads/noticias/', // string [ caminho da imagem é necessário ter uma pasta thumb (se tiver)]

				'largura' => 540, // num

				'altura' => 340, // num



				'redimensionar' => true, // boolean [ redimensiona a imagem ]

				'proporcional' => false, // boolean [ redimenciona proporcionalmente ]

				'cortar' => true, // boolean [ corta a imagem sem redimencionar ]

				'converter' => false, // boolean [ converte a image para jpg ]

				'greyscale' => false, // boolean [ coloca efeito greyscale ]

				//'qualidade' => 80 // num [ configura a qualidade da imagem ]

			)

		)



	);



	// Configurações para galeria

	private $config_galeria = array(



		'galeria' => 0,

		'config' => array()



	);



	public function __construct() {



		parent::__construct();

		$this->checkLogin();



	}



	public function index() {



		$busca = $this->input->get( "busca" );

		$filtro = $this->input->get( "filtro" );

		$ordem = $this->input->get( "ordem" );



		if( !empty( $filtro ) || $filtro === '0' ) {



			$where = $this->config_lista[ 'filtro' ][ $filtro ];



		}



		if( !empty( $busca ) ) {



			$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%'" ] = NULL;



		}



		if( !empty( $ordem ) ) {



			if( isset( $this->config_lista[ 'order' ][ $ordem ] ) ) {



				$order = $this->config_lista[ 'order' ][ $ordem ];



			}



		}



		$where['us.tipo'] = 3;

		$order['us.id'] = "DESC";



		//$retorno = $this->crud->read_paginacao( $where, $this->tabela, $order, $this->config_lista[ 'qtd_por_pagina' ], $this->pagina, $this->config_lista[ 'num_link' ] );



		// $lista = $retorno[ 'result' ];

		// $paginacao = $retorno[ 'paginacao' ];



		/*  */



		$config = array(

			'campos' => 'us.tipo as "tipoUsuario", us.id as "idUsuario", us.perfil as "perfilUsuario", dp.nome as "nomeUsuario", us.ativo as "ativado"',

			'tabela' => 'usuarios us',

			'where' => $where,

			'join' => array(

						array('dados_imobiliaria dp','dp.id = us.perfil','left')

					),

			'orderBy' => $order

		);



		$this->select->set($config);

		$paginacao = $this->select->paginacao( "imobiliaria", 5);

		$resultado = $this->select->resultado();



		$lista = '';



		foreach( $resultado as $val ) {



			$id_usuario = $val->idUsuario;



			/* Nome */



			$tipo = $val->tipoUsuario;

			$id_perfil = $val->perfilUsuario;



			$nome = $val->nomeUsuario;



			/* Numero de Anuncios */



			$config = array(

				'campos' => 'id',

				'tabela' => 'imoveis',

				'where' => array('id_usuario' => $id_usuario)

			);



			$this->select->set($config);

			$num_anuncio = $this->select->total();



			/* Número de Publicados */



			$config = array(

				'campos' => 'id',

				'tabela' => 'imoveis',

				'where' => array('id_usuario' => $id_usuario, 'status' => 3)

			);



			$this->select->set($config);

			$num_publicados = $this->select->total();



			/* Número de Destaques */



			$config = array(

				'campos' => 'id',

				'tabela' => 'imoveis',

				'where' => array('id_usuario' => $id_usuario, 'destaque' => 1, 'status' => 3)

			);



			$this->select->set($config);

			$num_destaque = $this->select->total();



			if($val->ativado == 1) {

				$status = '<p><strong>Ativa</strong></p>';

			} else {

				$status = '<p style="color: red"><strong>Inativa</strong></p>';

			}

			/* Número de Feirão */

			

			$config = array(

				'campos' => 'id',

				'tabela' => 'imoveis',

				'where' => array('id_usuario' => $id_usuario, 'feirao' => 1, 'status' => 3)

			);



			$this->select->set($config);

			$num_feirao = $this->select->total();


			/* Super destaque */

			

			$config = array(

				'campos' => 'id',

				'tabela' => 'imoveis',

				'where' => array('id_usuario' => $id_usuario, 'super_destaque' => 1, 'status' => 3)

			);



			$this->select->set($config);

			$num_super_destaque = $this->select->total();


			$lista .= '<div class="row table">

							<!-- <div class="columns small-12 medium-1 large-1"> <p><strong>ID:</strong> '.$id_usuario.'</p> </div> -->

							<div class="columns small-12 medium-1 large-1"> <p><strong>Nome:</strong> '.$nome.'</p> </div>

							<div class="columns small-12 medium-2 large-2"> '.$status.' </div>

							<div class="columns small-12 medium-2 large-2"> <p><strong>Anuncios:</strong> '.$num_anuncio.'</p> </div>

							<div class="columns small-12 medium-2 large-2"> <p><strong>Destaques:</strong> '.$num_destaque.'</p> </div>

							<div class="columns small-12 medium-2 large-2"> <p><strong>Super Destaque:</strong> '.$num_super_destaque.'</p> </div>

							<div class="columns small-12 medium-2 large-2"> <p><strong>Feirão:</strong> '.$num_feirao.'</p> </div>

							<div class="columns small-12 medium-1 large-1">

								<div class="columns small-12 medium-12 large-12 text-left"><a href="'.base_url().$this->pagina.'/read/'.$id_usuario.'">Editar</a></div>

								<!-- <div class="columns small-6 medium-6 large-6 text-right"><a href="{base_url}{pagina}/delete/{id}" class="ac red">Deletar</a></div> -->

							</div>



							<div class="clearfix"></div>

						</div>



						<div class="row"><hr></div>';



		}



		$data = array(

			"lista" => $lista,

			"pagina" => $this->pagina,

			"paginacao" => $paginacao

		);



		$this->load( $this->pagina .'/index', $data );



	}



	// Edição

	public function read( $id ) {



		$item = $this->crud->read( array( 'id'=> $id ), $this->tabela, array( 'id' => 'DESC' ) );



		if( empty( $item ) ) {

			redirect( base_url(). $this->pagina );

		}



		$item = $item[0];



		$dados_item = $this->crud->read( array( 'id'=> $item->perfil ), $this->util->tabelaPerfil( $item->tipo ), array( 'id' => 'DESC' ) );
		//print_r($dados_item);exit();


		if( empty( $dados_item ) ) {

			redirect( base_url(). $this->pagina );

		}



		$dados_item = $dados_item[0];

		$dataBr = date("d/m/Y",$dados_item->expira_em);

		$campos_form_esquerdo = array(

			"nome" => array(

				"type" => "text",

				"label" => "Usuário",

				"name" => "nome",

				"value" => $dados_item->nome,

				"width" => "6",

				"disabled" => true

			),
			"email" => array(

				"type" => "text",

				"label" => "E-mail",

				"name" => "email",

				"value" => $dados_item->email,

				"width" => "12",

				"disabled" => true

			),

			"telefone" => array(

				"type" => "text",

				"label" => "Telefone",

				"name" => "telefone",

				"value" => $dados_item->telefone,

				"width" => "12",

				"disabled" => true

			),

			"melhor_horario" => array(

				"type" => "text",

				"label" => "Melhor horário",

				"name" => "melhor_horario",

				"value" => $dados_item->melhor_horario,

				"width" => "12",

				"disabled" => true

			),
			"creci" => array(

				"type" => "text",

				"label" => "Creci",

				"name" => "creci",

				"value" => $dados_item->creci,

				"width" => "12",

				"disabled" => true

			),
			"como_soube" => array(

				"type" => "text",

				"label" => "Como Soube",

				"name" => "como_soube",

				"value" => $dados_item->como_soube,

				"width" => "12",

				"disabled" => true
			),
			"vendedor" => array(

				"type" => "text",

				"label" => "Vendedor",

				"name" => "vendedor",

				"value" => $dados_item->vendedor,

				"width" => "12",

				"disabled" => true
			),
			"responsavel" => array(

				"type" => "text",

				"label" => "Responsável",

				"name" => "responsavel",

				"value" => $dados_item->responsavel,

				"width" => "12",

				"disabled" => true

			),
			"cidade" => array(

				"type" => "text",

				"label" => "Cidade",

				"name" => "cidade",

				"value" => $dados_item->cidade."/".$dados_item->estado,

				"width" => "12",

				"disabled" => true

			),

			"num_anuncios" => array(

				"type" => "text",

				"label" => "Número de Anúncios Permitidos",

				"descricao" => "(Ex: 50)",

				"name" => "num_anuncios",

				"value" => $item->num_anuncios,

				"width" => "12"

			),


			"num_feirao" => array(

				"type" => "text",

				"label" => "Número de Feirões Permitidos",

				"descricao" => "(Ex: 50)",

				"name" => "num_feirao",

				"value" => $item->num_feirao,

				"width" => "12"

			),

			"num_destaque" => array(

				"type" => "text",

				"label" => "Número de Destaques Permitidos",

				"descricao" => "(Ex: 50)",

				"name" => "num_destaque",

				"value" => $item->num_destaque,

				"width" => "12"

			),


			"num_super_destaque" => array(

				"type" => "text",

				"label" => "Número de Super Destaques Permitidos",

				"descricao" => "(Ex: 50)",

				"name" => "num_super_destaque",

				"value" => $item->num_super_destaque,

				"width" => "12"

			),

			"num_destaque_feirao" => array(

				"type" => "text",

				"label" => "Número de Destaques do Feirão Permitidos ",

				"descricao" => "(Ex: 50)",

				"name" => "num_destaque_feirao",

				"value" => $item->num_destaque_feirao,

				"width" => "12"

			),


			"num_super_destaque_feirao" => array(

				"type" => "text",

				"label" => "Número de Super Destaques do Feirão Permitidos",

				"descricao" => "(Ex: 50)",

				"name" => "num_super_destaque_feirao",

				"value" => $item->num_super_destaque_feirao,

				"width" => "12"

			),

			"expiracao" => array(

				"type" => "text",

				"label" => "Expiração",

				"name" => "expiracao",

				"descricao" => "Em Dias",

				"value" => $item->expiracao,

				"width" => "12",

				"required" => false

			)

		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		//Tipos de XML
		$config = array(
			'campos' => 'id, nome',
			'tabela' => 'tipo_xml'
		);

		$this->select->set($config);
		$xml = $this->select->resultado();
		
		$checked = '';
		
		if(is_null($item->tipo_xml) || empty($item->tipo_xml)) {
			$checked = 'checked="checked"';	
		}
		
		$lista_xml = '<div class="small-6 medium-3 columns left tipo_xml">
					<label>
						<input type="radio" name="tipo_xml" id="tipo_xml" value="" '.$checked.'><strong>Nenhum</strong>
					</label>
				</div>';
		
		foreach($xml as $val) {
			
			$checked = '';
			
			if($val->id == $item->tipo_xml) {
				$checked = 'checked="checked"';	
			}
			
			$lista_xml .= '<div class="small-6 medium-3 columns left tipo_xml">
					<label>
						<input type="radio" name="tipo_xml" id="tipo_xml" value="'.$val->id.'" '.$checked.'>'.$val->nome.'
					</label>
				</div>';
			
		}

		// seta os campos de aquivos e imagens

		$path_image = base_url() ."../assets/uploads/noticias/thumb/";



		$arquivos = array(

			"imagem" => array(

				"type" => "image",

				"label" => "Imagem",

				"name" => "imagem",

				"value" => ( !empty( $item->imagem ) ) ? $path_image. $item->imagem : "",

				"width" => $this->config_image[ 'imagem' ][ 'config' ][ 'largura' ],

				"height" => $this->config_image[ 'imagem' ][ 'config' ][ 'altura' ]

			)

		);



		$form_arquivos = $this->imput_form->file( $arquivos );



		if($item->ativo == 1) {

			$ativo = 'selected="selected"';

			$inativo = '';

		} else {

			$ativo = '';

			$inativo = 'selected="selected"';

		}



		$select_ativo = '<select name="ativo" id="slcPublicar" class="required"/>

							<option value="1" '.$ativo.'>Ativo</option>

							<option value="0" '.$inativo.'>Inativo</option>

						</select>';


		// $arrayFeirao = array(array("value" => 1, "label" => "Sim"), array("value" => 0, "label" => "Não"));	
		// $selectFeirao = $this->imput_form->select_2('Feirão' , "num_feirao", $arrayFeirao);



		$data = array(

			"form_esquerdo" => $form_esquerdo,

			"form_arquivos" => $form_arquivos,

			// "selectFeirao" => $selectFeirao,

			"pagina" => $this->pagina,

			"id" => $id,

			"select_ativo" => $select_ativo,
			
			"lista_xml" => $lista_xml

		);



		$this->load( $this->pagina .'/read', $data );



	}



	public function create() {



		$retorno = array(

			"sucesso" => "false",

			"link" => base_url()

		);



		// encrypt da senha se existir o campo

		if( isset( $_POST[ 'senha' ] ) ) {

			if( empty( $_POST[ 'senha' ] ) ) {



				unset( $_POST[ 'senha' ] );



			} else {



				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );



			}

		}



		// Categorias

		$categorias = "";

		if( isset( $_POST[ 'categorias' ] ) ) {



			$categorias = $_POST[ 'categorias' ];

			unset($_POST[ 'categorias' ]);

		}



		// Tags

		$tags = "";

		if( isset( $_POST[ 'tags' ] ) ) {



			$tags = $_POST[ 'tags' ];

			unset($_POST[ 'tags' ]);



		}



		// Checks

		foreach( $this->config_ckecks as $check ) {



			$_POST[ $check ] = implode( ",", $_POST[ $check ] );



		}



		// Seta o indice das imagens no $_POST

		foreach( $this->config_image as $image ) {



			$campo = $image[ 'campo' ];



			if( $src = $this->session->userdata( $campo ) ) {



				$_POST[ $campo ] = $src;

				$this->session->unset_userdata( $campo );



			}



		}



		// Seta o indice dos arquivos no $_POST

		foreach( $this->config_arquivo as $arquivo ) {



			$campo = $arquivo[ 'campo' ];



			if( $src = $this->session->userdata( $campo ) ) {



				$_POST[ $campo ] = $src;

				$this->session->unset_userdata( $campo );



			}



		}



		if( $id = $this->crud->create( $_POST, $this->tabela ) ) {



			// Galeria

			if( $this->config_galeria[ 'galeria' ] ) {



				$galeria_id = $this->session->userdata( 'galeria_id' );

				$this->imput_form->galeria_update( $id, $galeria_id );



			}



			// Categorias

			if( !empty( $categorias ) ) {

				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );

			}

			// Tags

			if( !empty( $tags ) ) {

				$this->imput_form->tags_update( $this->tabela, $id, $tags );

			}



			$this->session->set_flashdata( "msg_sucesso", "Item adicionado com sucesso!" );





			$retorno = array(

				"sucesso" => "true",

				"link" => base_url().$this->pagina

			);



		} else {



			$this->session->set_flashdata( "msg_erro", "Erro ao adicionar o item, tente novamente!" );





			$retorno = array(

				"sucesso" => "true",

				"link" => base_url().$this->pagina

			);



		}



		echo json_encode( $retorno );



	}





	public function update( $id ) {



		$retorno = array(

			"sucesso" => "false",

			"link" => base_url()

		);
		
				//print_r($_POST);exit();

		// encrypt da senha se existir o campo

		if( isset( $_POST[ 'senha' ] ) ) {



			if( empty( $_POST[ 'senha' ] ) ) {



				unset( $_POST[ 'senha' ] );



			} else {



				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );



			}



		}



		// Categorias

		$categorias = "";

		if( isset( $_POST[ 'categorias' ] ) ) {



			$categorias = $_POST[ 'categorias' ];

			unset($_POST[ 'categorias' ]);

		}



		// Tags

		$tags = "";

		if( isset( $_POST[ 'tags' ] ) ) {



			$tags = $_POST[ 'tags' ];

			unset($_POST[ 'tags' ]);



		}



		// Checks

		foreach( $this->config_ckecks as $check ) {



			$_POST[ $check ] = implode( ",", $_POST[ $check ] );



		}



		// Seta o indice das imagens no $_POST

		foreach( $this->config_image as $image ) {



			$campo = $image[ 'campo' ];



			if( $src = $this->session->userdata( $campo ) ) {



				$_POST[ $campo ] = $src;

				$this->session->unset_userdata( $campo );



			}



		}



		// Seta o indice dos arquivos no $_POST

		foreach( $this->config_arquivo as $arquivo ) {



			$campo = $arquivo[ 'campo' ];



			if( $src = $this->session->userdata( $campo ) ) {



				$_POST[ $campo ] = $src;

				$this->session->unset_userdata( $campo );



			}



		}

		$item = $this->crud->read( array( 'id'=> $id ), $this->tabela, array( 'id' => 'DESC' ) );
		$item = $item[0];
		//print_r($item);exit();
		//adiciona a data de expiração para o trigger gerenciar o momento de inativar 
		$expira_em['expira_em'] = strtotime(date('Y-m-d', strtotime("+".$_POST['expiracao']." days")));


		// Faz e verifica se fez o update

		if( $this->crud->update( array( "id" => $id ), $_POST, $this->tabela ) ) {

		    
		    $this->crud->update( array( "id" => $item->perfil ), array( 'expira_em' => $expira_em['expira_em'] ), 'dados_imobiliaria' );


			if( $_POST['ativo'] == '0' ) {

				$this->crud->update( array( "id_usuario" => $id ), array( 'ativo' => $_POST['ativo'] ), 'imoveis' );

			}else{

				$this->crud->update( array( "id_usuario" => $id ), array( 'ativo' => $_POST['ativo'], 'status' => 3 , 'aprovado' => 1 ), 'imoveis' );
			}



			// Categorias

			if( !empty( $categorias ) ) {

				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );

			}

			// Tags

			if( !empty( $tags ) ) {

				$this->imput_form->tags_update( $this->tabela, $id, $tags );

			}



			$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );



			$retorno = array(

				"sucesso" => "true",

				"link" => base_url().$this->pagina

			);



		} else {



			$this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );



			$retorno = array(

				"sucesso" => "true",

				"link" => base_url().$this->pagina."/read/".$id

			);



		}



		echo json_encode( $retorno );



	}



	public function delete( $id ) {



		// Faz e verifica se fez o delete

		$this->crud->delete( array( "id" => $id ), $this->tabela );

		$this->session->set_flashdata( "msg_sucesso", "Item deletado com sucesso!" );

		redirect( base_url().$this->pagina );



	}



	public function image( $campo ) {



		$src = $this->image_form->upload_imagem( $_FILES[ $campo ], $this->config_image[ $campo ][ 'config' ] );



		$this->session->set_userdata( $campo, $src );



		if( $this->config_image[ $campo ][ 'config' ][ 'thumb' ] ) {



			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].'thumb/'.$src;



		} else {



			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].$src;



		}



	}



	public function file( $campo ) {



		$this->load->library( 'upload', $this->config_arquivo[ $campo ][ 'config' ] );



		if( $this->upload->do_upload( $campo ) ) {



			$data = $this->upload->data();

			$name = $data[ 'file_name' ];



			$this->session->set_userdata( $campo, $name );



			echo base_url().$this->config_arquivo[ $campo ][ 'config' ][ 'upload_path' ].$name;



		} else {



			echo '0';



		}



	}



	public function galeria() {



		$src = $this->image_form->upload_imagem( $_FILES[ 'arquivo' ], $this->config_galeria[ 'config' ] );



		$_POST[ 'arquivo' ] = $src;



		$id = $this->imput_form->galeria_insert( $_POST );



		$galeria_id = $_POST[ 'elemento' ];



		$session = $this->session->userdata( $galeria_id );



		$session[] = $id;



		$this->session->set_userdata( $galeria_id, $session );

		$this->session->set_userdata( 'galeria_id', $galeria_id );

// var_dump($galeria_id);

		$json = array(

			"src" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].'thumb/'.$src,

			"src_full" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].$src,

			"id" => $id,

			"pagina" => $this->pagina,

			"session" => $session,

			"id" => $id

		);



		echo json_encode( $json );



	}



	public function galeria_legenda() {



		$id = $this->imput_form->galeria_legenda( $_POST[ 'id' ], $_POST[ 'legenda' ] );



	}



	public function galeria_delete() {



		$id = $this->imput_form->galeria_delete( $_POST[ 'id' ] );



	}



}