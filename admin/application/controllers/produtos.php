<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos extends Main_Controller {

	private $pagina = "produtos";
	private $tabela = "produtos";

	private $config_lista = array(
		'busca' => 'Produto',
		'filtro' => array(
			'ativos' => array( 'ativo' => 'true' ),
			'inativos' => array( 'ativo' => 'false' ),
			'feminino' => array( 'Genero' => 'F' ),
			'masculino' => array( 'Genero' => 'M' ),
			'sem_promocao' => array( 'Por IS NULL' => NULL ),
			'com_promocao' => array( 'Por != 0' => NULL )
		),
		'order' => array(
			'nome_asc' => array( 'nome' => 'ASC' ),
			'nome_desc' => array( 'nome' => 'DESC' ),
			'baseado_asc' => array( 'Baseado' => 'ASC' ),
			'baseado_desc' => array( 'Baseado' => 'DESC' ),
			'data_asc' => array( 'DataInclusao' => 'ASC' ),
			'data_desc' => array( 'DataInclusao' => 'DESC' )
		),
		'num_link' => 10,
		'qtd_por_pagina' => 25
	);

	private $config_ckecks = array();

	// Configurações de arquivos
	private $config_arquivo = array();

	// Configurações para imagem
	private $config_image = array(
		'Imagem' => array(
			'campo' => 'Imagem',
			'config' => array(
				'thumb' => array(
					'greyscale' => false, // bolean
					'largura' => 250, // num
					'altura' => 245 // num
				),
				'marca' => false,
				'image_path' => '../assets/uploads/produtos/', // string [ caminho da imagem é necessário ter uma pasta thumb (se tiver)]
				'largura' => 500, // num
				'altura' => 490, // num

				'redimensionar' => true, // boolean [ redimensiona a imagem ]
				'proporcional' => false, // boolean [ redimenciona proporcionalmente ]
				'cortar' => true, // boolean [ corta a imagem sem redimencionar ]
				'converter' => false, // boolean [ converte a image para jpg ]
				'greyscale' => false, // boolean [ coloca efeito greyscale ]
				//'qualidade' => 80 // num [ configura a qualidade da imagem ]
			)
		)

	);

	// Configurações para galeria
	private $config_galeria = array(

		'galeria' => 1,
		'config' => array(
			'thumb' => array(
				'greyscale' => false, // bolean
				'largura' => 250, // num
				'altura' => 245 // num
			),
			'marca' => false,
			'image_path' => '../assets/uploads/produtos/', // string [ caminho da imagem é necessário ter uma pasta thumb (se tiver)]
			'largura' => 500, // num
			'altura' => 490, // num
			'redimensionar' => true, // boolean [ redimensiona a imagem ]
			'proporcional' => false, // boolean [ redimenciona proporcionalmente ]
			'cortar' => true, // boolean [ corta a imagem sem redimencionar ]
			'converter' => false, // boolean [ converte a image para jpg ]
			'greyscale' => false, // boolean [ coloca efeito greyscale ]
			// 'qualidade' => 80 // num [ configura a qualidade da imagem ]
		)

	);

	public function __construct() {

		parent::__construct();
		$this->checkLogin();

	}

	public function index() {

		$busca = $this->input->get( "busca" );
		$filtro = $this->input->get( "filtro" );
		$ordem = $this->input->get( "ordem" );

		$where = array();
		$order = array( "IdProduto" => "DESC" );

		if( !empty( $filtro ) || $filtro === '0' ) {

			$where = $this->config_lista[ 'filtro' ][ $filtro ];

		}

		if( !empty( $busca ) ) {

			$where[ $this->config_lista[ 'busca' ] ." LIKE '%". $busca ."%'" ] = NULL;

		}

		if( !empty( $ordem ) ) {

			if( isset( $this->config_lista[ 'order' ][ $ordem ] ) ) {

				$order = $this->config_lista[ 'order' ][ $ordem ];

			}

		}

		$retorno = $this->crud->read_paginacao( $where, $this->tabela, $order, $this->config_lista[ 'qtd_por_pagina' ], $this->pagina, $this->config_lista[ 'num_link' ] );

		$lista = $retorno[ 'result' ];
		$paginacao = $retorno[ 'paginacao' ];

		$data = array(
			"lista" => $lista,
			"pagina" => $this->pagina,
			"paginacao" => $paginacao
		);

		$this->load( $this->pagina .'/index', $data );

	}

	// Add
	public function add() {

		$campos_form_esquerdo = array(
			"DataInclusao" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "DataInclusao",
				"value" => time(),
				"width" => "12"
			),
			"slug" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "slug",
				"value" => "",
				"width" => "12"
			),
			"Produto" => array(
				"type" => "text",
				"label" => "Nome",
				"name" => "Produto",
				"value" => "",
				"width" => "12"
			),
			"Baseado" => array(
				"type" => "text",
				"label" => "Baseado",
				"name" => "Baseado",
				"value" => "",
				"width" => "6",
				"required" => "unrequired"
			),
			"IDProdutoSistema" => array(
				"type" => "text",
				"label" => "ID do Produto no Sistema",
				"name" => "IDProdutoSistema",
				"value" => "",
				"width" => "6"
			),
			"Numero" => array(
				"type" => "text",
				"label" => "Nº",
				"name" => "Numero",
				"value" => "",
				"width" => "3",
				"required" => "unrequired"
			),
			"Valor" => array(
				"type" => "text",
				"label" => "Valor",
				"name" => "Valor",
				"value" => "",
				"width" => "3",
				"descricao" => "Ex.: 100.00"
			),
			"Por" => array(
				"type" => "text",
				"label" => "Desconto",
				"name" => "Por",
				"value" => "0",
				"width" => "3",
				"descricao" => "(%)"
			),
			// "ML" => array(
			// 	"type" => "text",
			// 	"label" => "ML",
			// 	"name" => "ML",
			// 	"value" => "",
			// 	"width" => "2",
			// ),
			// "Tamanho" => array(
			// 	"type" => "text",
			// 	"label" => "Tamanho",
			// 	"name" => "Tamanho",
			// 	"value" => "",
			// 	"width" => "2"
			// ),
			"Peso" => array(
				"type" =>"text",
				"label" => "Peso",
				"name" => "Peso",
				"value" => "",
				"width" => "3",
				"descricao" => "(g)"				
			),
			"Descricao" => array(
				"type" => "rtext",
				"label" => "Descrição",
				"name" => "Descricao",
				"value" => "",
				"width" => "12"
			),
			"Composicao" => array(
				"type" => "rtext",
				"label" => "Notas",
				"name" => "Composicao",
				"value" => "",
				"width" => "12"
			),
			// "Caracteristica" => array(
			// 	"type" => "rtext",
			// 	"label" => "Caracteristica",
			// 	"name" => "Caracteristica",
			// 	"value" => "",
			// 	"width" => "12"
			// ),
			"Mododeusar" => array(
				"type" => "rtext",
				"label" => "Dicas",
				"name" => "Mododeusar",
				"value" => "",
				"width" => "12"
			),
			"description" => array(
				"type" => "textarea",
				"label" => "Meta Description",
				"name" => "description",
				"value" => "",
				"width" => "12",
				"required" => "unrequired"
			),
			"keywords" => array(
				"type" => "textarea",
				"label" => "Meta Keywords",
				"name" => "keywords",
				"value" => "",
				"width" => "12",
				"required" => "unrequired"
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens
		$path_image = base_url() ."../assets/uploads/produtos/";

		$arquivos = array(
			"Imagem" => array(
				"type" => "image",
				"label" => "Imagem Principal",
				"name" => "Imagem",
				"value" => "",
				"width" => $this->config_image[ 'Imagem' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'Imagem' ][ 'config' ][ 'altura' ]
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );

		$image_path = $this->config_galeria[ 'config' ][ 'image_path' ];
		$image_width = $this->config_galeria[ 'config' ][ 'largura' ];
		$image_height = $this->config_galeria[ 'config' ][ 'altura' ];

		$form_galeria = $this->imput_form->galeria( $image_path, $image_width, $image_height, $this->pagina, $this->config_galeria['galeria'], false );

		

		// Categorias
		$config = array(
			'campos' => 'nome as label, id as value',
			'tabela' => 'categorias',
			'where' => array( 'ativo' => 1 )
		);

		$this->select->set( $config );
		$itens_categorias = $this->select->resultado();

		$select_categorias = $this->imput_form->select( 'Categoria', 'IdCategoria', $itens_categorias, 0 );

		// Produtos
		$config = array(
			'campos' => 'Produto as label, IdProduto as value',
			'tabela' => 'produtos',
			'where' => array( 'Ativo' => 'true' )
		);

		$this->select->set( $config );
		$itens_produtos = $this->select->resultado();

		$compre_junto = $this->imput_form->select( 'Compre Junto', 'CompreJunto', $itens_produtos, 0 );

		$viu_tambem1 = $this->imput_form->select( 'Viu Também 1', 'ViuTambem1', $itens_produtos, 0 );
		$viu_tambem2 = $this->imput_form->select( 'Viu Também 2', 'ViuTambem2', $itens_produtos, 0 );
		$viu_tambem3 = $this->imput_form->select( 'Viu Também 3', 'ViuTambem3', $itens_produtos, 0 );
		$viu_tambem4 = $this->imput_form->select( 'Viu Também 4', 'ViuTambem4', $itens_produtos, 0 );

		$itens_destaque = array(
			array(
				'label' => 'Sim',
				'value' => 'True'
			),
			array(
				'label' => 'Não',
				'value' => 'False'
			),
		);
		$destaque = $this->imput_form->select( 'Destaque?', 'Destaque', $itens_destaque, 'False' );

		$itens_lancamento = array(
			array(
				'label' => 'Sim',
				'value' => 'True'
			),
			array(
				'label' => 'Não',
				'value' => 'False'
			),
		);
		$lancamento = $this->imput_form->select( 'Lançamento?', 'Lancamento', $itens_lancamento, 'False' );

		$itens_indisponivel= array(
			array(
				'label' => 'Sim',
				'value' => 1
			),
			array(
				'label' => 'Não',
				'value' => 0
			),
		);
		$indisponivel = $this->imput_form->select( 'Indisponivel?', 'Indisponivel', $itens_indisponivel, 0 );

		$itens_mais_vendido= array(
			array(
				'label' => 'Sim',
				'value' => 1
			),
			array(
				'label' => 'Não',
				'value' => 0
			),
		);
		$mais_vendido = $this->imput_form->select( 'Mais Vendido?', 'MaisVendido', $itens_mais_vendido, 0 );

		$itens_fixacao= array(
			array(
				'label' => 'Suave',
				'value' => 1
			),
			array(
				'label' => 'Marcante',
				'value' => 2
			),
			array(
				'label' => 'Intenso',
				'value' => 3
			),
			array(
				'label' => 'Forte',
				'value' => 4
			),
		);
		$fixacao = $this->imput_form->select( 'Nivel de Fixação', 'Fixacao', $itens_fixacao, 1 );

		$itens_genero = array(
			array(
				'label' => 'Masculino',
				'value' => 'M'
			),
			array(
				'label' => 'Feminino',
				'value' => 'F'
			),
			array(
				'label' => 'Unisex',
				'value' => 'U'
			)
		);
		$genero = $this->imput_form->select( 'Genero', 'Genero', $itens_genero, '' );

		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"form_galeria" => $form_galeria,
			"select_categorias" => $select_categorias,
			"compre_junto" => $compre_junto,
			"viu_tambem1" => $viu_tambem1,
			"viu_tambem2" => $viu_tambem2,
			"viu_tambem3" => $viu_tambem3,
			"viu_tambem4" => $viu_tambem4,
			"destaque" => $destaque,
			"mais_vendido" => $mais_vendido,
			"lancamento" => $lancamento,
			"indisponivel" => $indisponivel,
			"fixacao" => $fixacao,
			"genero" => $genero,
			"pagina" => $this->pagina
		);

		$this->load( $this->pagina .'/add', $data );

	}

	// Edição
	public function read( $id ) {

		$item = $this->crud->read( array( 'IdProduto'=> $id ), $this->tabela, array( 'IdProduto' => 'DESC' ) );

		if( empty( $item ) ) {
			redirect( base_url(). $this->pagina );
		}

		$item = $item[0];

		$campos_form_esquerdo = array(
			"DataInclusao" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "DataInclusao",
				"value" => $item->DataInclusao,
				"width" => "12"
			),
			"slug" => array(
				"type" => "hidden",
				"label" => "",
				"name" => "slug",
				"value" => $item->slug,
				"width" => "12"
			),
			"Produto" => array(
				"type" => "text",
				"label" => "Nome",
				"name" => "Produto",
				"value" => $item->Produto,
				"width" => "12"
			),
			"Baseado" => array(
				"type" => "text",
				"label" => "Baseado",
				"name" => "Baseado",
				"value" => $item->Baseado,
				"width" => "6",
				"required" => "unrequired"
			),
			"IDProdutoSistema" => array(
				"type" => "text",
				"label" => "ID do Produto no Sistema",
				"name" => "IDProdutoSistema",
				"value" => $item->IDProdutoSistema,
				"width" => "6"
			),
			"Numero" => array(
				"type" => "text",
				"label" => "Nº",
				"name" => "Numero",
				"value" => $item->Numero,
				"width" => "3",
				"required" => "unrequired"
			),
			"Valor" => array(
				"type" => "text",
				"label" => "Valor",
				"name" => "Valor",
				"value" => $item->Valor,
				"width" => "3",
				"descricao" => "Ex.: 100.00",
			),
			"Por" => array(
				"type" => "text",
				"label" => "Desconto",
				"name" => "Por",
				"value" => $item->Por,
				"width" => "3",
				"descricao" => "(%)",
			),
			// "ML" => array(
			// 	"type" => "text",
			// 	"label" => "ML",
			// 	"name" => "ML",
			// 	"value" => $item->ML,
			// 	"width" => "2"
			// ),
			// "Tamanho" => array(
			// 	"type" => "text",
			// 	"label" => "Tamanho",
			// 	"name" => "Tamanho",
			// 	"value" => $item->Tamanho,
			// 	"width" => "2"
			// ),
			"Peso" => array(
				"type" =>"text",
				"label" => "Peso",
				"name" => "Peso",
				"value" => $item->Peso,
				"width" => "3",
				"descricao" => "(g)"
			),
			"Descricao" => array(
				"type" => "rtext",
				"label" => "Descrição",
				"name" => "Descricao",
				"value" => $item->Descricao,
				"width" => "12"
			),
			"Composicao" => array(
				"type" => "rtext",
				"label" => "Notas",
				"name" => "Composicao",
				"value" => $item->Composicao,
				"width" => "12"
			),
			// "Caracteristica" => array(
			// 	"type" => "rtext",
			// 	"label" => "Caracteristica",
			// 	"name" => "Caracteristica",
			// 	"value" => $item->Caracteristica,
			// 	"width" => "12"
			// ),
			"Mododeusar" => array(
				"type" => "rtext",
				"label" => "Dicas",
				"name" => "Mododeusar",
				"value" => $item->Mododeusar,
				"width" => "12"
			),
			"description" => array(
				"type" => "textarea",
				"label" => "Meta Description",
				"name" => "description",
				"value" => $item->description,
				"width" => "12",
				"required" => "unrequired"
			),
			"keywords" => array(
				"type" => "textarea",
				"label" => "Meta Keywords",
				"name" => "keywords",
				"value" => $item->keywords,
				"width" => "12",
				"required" => "unrequired"
			)
		);

		$form_esquerdo = $this->imput_form->form( $campos_form_esquerdo );

		// seta os campos de aquivos e imagens
		$path_image = base_url() ."../assets/uploads/produtos/thumb/";

		$arquivos = array(
			"Imagem" => array(
				"type" => "image",
				"label" => "Imagem de Capa",
				"name" => "Imagem",
				"value" => ( !empty( $item->Imagem ) ) ? $path_image. $item->Imagem : "",
				"width" => $this->config_image[ 'Imagem' ][ 'config' ][ 'largura' ],
				"height" => $this->config_image[ 'Imagem' ][ 'config' ][ 'altura' ]
			)
		);

		$form_arquivos = $this->imput_form->file( $arquivos );


		$image_path = $this->config_galeria[ 'config' ][ 'image_path' ];
		$image_width = $this->config_galeria[ 'config' ][ 'largura' ];
		$image_height = $this->config_galeria[ 'config' ][ 'altura' ];

		$form_galeria = $this->imput_form->galeria( $image_path, $image_width, $image_height, $this->pagina, $this->config_galeria['galeria'], $item->IdProduto );

		// Categorias
		$config = array(
			'campos' => 'nome as label, id as value',
			'tabela' => 'categorias',
			'where' => array( 'ativo' => 1 )
		);

		$this->select->set( $config );
		$itens_categorias = $this->select->resultado();

		$select_categorias = $this->imput_form->select( 'Categoria', 'IdCategoria', $itens_categorias, $item->IdCategoria );

		// Produtos
		$config = array(
			'campos' => 'Produto as label, IdProduto as value',
			'tabela' => 'produtos',
			'where' => array( 'Ativo' => 'true', 'IdProduto != '.$id => NULL )
		);

		$this->select->set( $config );
		$itens_produtos = $this->select->resultado();

		$compre_junto = $this->imput_form->select( 'Compre Junto', 'CompreJunto', $itens_produtos, $item->CompreJunto );

		$viu_tambem1 = $this->imput_form->select( 'Viu Também 1', 'ViuTambem1', $itens_produtos, $item->ViuTambem1 );
		$viu_tambem2 = $this->imput_form->select( 'Viu Também 2', 'ViuTambem2', $itens_produtos, $item->ViuTambem2 );
		$viu_tambem3 = $this->imput_form->select( 'Viu Também 3', 'ViuTambem3', $itens_produtos, $item->ViuTambem3 );
		$viu_tambem4 = $this->imput_form->select( 'Viu Também 4', 'ViuTambem4', $itens_produtos, $item->ViuTambem4 );

		$itens_destaque = array(
			array(
				'label' => 'Sim',
				'value' => 'True'
			),
			array(
				'label' => 'Não',
				'value' => 'False'
			),
		);
		$destaque = $this->imput_form->select( 'Destaque?', 'Destaque', $itens_destaque, $item->Destaque );

		$itens_lancamento = array(
			array(
				'label' => 'Sim',
				'value' => 'True'
			),
			array(
				'label' => 'Não',
				'value' => 'False'
			),
		);
		$lancamento = $this->imput_form->select( 'Lançamento?', 'Lancamento', $itens_lancamento, $item->Lancamento );

		$itens_indisponivel= array(
			array(
				'label' => 'Sim',
				'value' => 1
			),
			array(
				'label' => 'Não',
				'value' => 0
			),
		);
		$indisponivel = $this->imput_form->select( 'Indisponivel?', 'Indisponivel', $itens_indisponivel, $item->Indisponivel );

		$itens_mais_vendido= array(
			array(
				'label' => 'Sim',
				'value' => 'True'
			),
			array(
				'label' => 'Não',
				'value' => 'False'
			),
		);
		$mais_vendido = $this->imput_form->select( 'Mais Vendido?', 'MaisVendido', $itens_mais_vendido, $item->MaisVendido );


		$itens_fixacao= array(
			array(
				'label' => 'Suave',
				'value' => 1
			),
			array(
				'label' => 'Marcante',
				'value' => 2
			),
			array(
				'label' => 'Intenso',
				'value' => 3
			),
			array(
				'label' => 'Forte',
				'value' => 4
			),
		);
		$fixacao = $this->imput_form->select( 'Nivel de Fixação', 'Fixacao', $itens_fixacao, $item->Fixacao );

		$itens_genero = array(
			array(
				'label' => 'Masculino',
				'value' => 'M'
			),
			array(
				'label' => 'Feminino',
				'value' => 'F'
			),
			array(
				'label' => 'Unisex',
				'value' => 'U'
			),
		);
		$genero = $this->imput_form->select( 'Genero', 'Genero', $itens_genero, $item->Genero );

		$data = array(
			"form_esquerdo" => $form_esquerdo,
			"form_arquivos" => $form_arquivos,
			"form_galeria" => $form_galeria,
			"select_categorias" => $select_categorias,
			"compre_junto" => $compre_junto,
			"viu_tambem1" => $viu_tambem1,
			"viu_tambem2" => $viu_tambem2,
			"viu_tambem3" => $viu_tambem3,
			"viu_tambem4" => $viu_tambem4,
			"destaque" => $destaque,
			"lancamento" => $lancamento,
			"indisponivel" => $indisponivel,
			"mais_vendido" => $mais_vendido,
			"fixacao" => $fixacao,
			"genero" => $genero,
			"pagina" => $this->pagina,
			"id" => $id
		);

		$this->load( $this->pagina .'/read', $data );

	}

	public function create() {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {
			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}
		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		if( $id = $this->crud->create( $_POST, $this->tabela ) ) {

			// Galeria
			if( $this->config_galeria[ 'galeria' ] ) {

				$galeria_id = $this->session->userdata( 'galeria_id' );
				$this->imput_form->galeria_update( $id, $galeria_id );

			}

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item adicionado com sucesso!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao adicionar o item, tente novamente!" );


			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		}

		echo json_encode( $retorno );

	}


	public function update( $id ) {

		$retorno = array(
			"sucesso" => "false",
			"link" => base_url()
		);

		// encrypt da senha se existir o campo
		if( isset( $_POST[ 'senha' ] ) ) {

			if( empty( $_POST[ 'senha' ] ) ) {

				unset( $_POST[ 'senha' ] );

			} else {

				$_POST[ 'senha' ] = md5( $_POST[ 'senha' ] );

			}

		}

		// Categorias
		$categorias = "";
		if( isset( $_POST[ 'categorias' ] ) ) {

			$categorias = $_POST[ 'categorias' ];
			unset($_POST[ 'categorias' ]);
		}

		// Tags
		$tags = "";
		if( isset( $_POST[ 'tags' ] ) ) {

			$tags = $_POST[ 'tags' ];
			unset($_POST[ 'tags' ]);

		}

		// Checks
		foreach( $this->config_ckecks as $check ) {

			$_POST[ $check ] = implode( ",", $_POST[ $check ] );

		}

		// Seta o indice das imagens no $_POST
		foreach( $this->config_image as $image ) {

			$campo = $image[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Seta o indice dos arquivos no $_POST
		foreach( $this->config_arquivo as $arquivo ) {

			$campo = $arquivo[ 'campo' ];

			if( $src = $this->session->userdata( $campo ) ) {

				$_POST[ $campo ] = $src;
				$this->session->unset_userdata( $campo );

			}

		}

		// Faz e verifica se fez o update
		if( $this->crud->update( array( "IdProduto" => $id ), $_POST, $this->tabela ) ) {

			// Categorias
			if( !empty( $categorias ) ) {
				$this->imput_form->categorias_update( $this->tabela, $id, $categorias );
			}
			// Tags
			if( !empty( $tags ) ) {
				$this->imput_form->tags_update( $this->tabela, $id, $tags );
			}

			$this->session->set_flashdata( "msg_sucesso", "Item atualizado com sucesso!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina
			);

		} else {

			$this->session->set_flashdata( "msg_erro", "Erro ao atualizar tente novamente!" );

			$retorno = array(
				"sucesso" => "true",
				"link" => base_url().$this->pagina."/read/".$id
			);

		}

		echo json_encode( $retorno );

	}

	public function delete( $id ) {

		// Faz e verifica se fez o delete
		$this->crud->delete( array( "IdProduto" => $id ), $this->tabela );
		$this->session->set_flashdata( "msg_sucesso", "Item deletado com sucesso!" );
		redirect( base_url().$this->pagina );

	}

	public function image( $campo ) {

		$src = $this->image_form->upload_imagem( $_FILES[ $campo ], $this->config_image[ $campo ][ 'config' ] );

		$this->session->set_userdata( $campo, $src );

		if( $this->config_image[ $campo ][ 'config' ][ 'thumb' ] ) {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].'thumb/'.$src;

		} else {

			echo base_url().$this->config_image[ $campo ][ 'config' ][ 'image_path' ].$src;

		}

	}

	public function file( $campo ) {

		$this->load->library( 'upload', $this->config_arquivo[ $campo ][ 'config' ] );

		if( $this->upload->do_upload( $campo ) ) {

			$data = $this->upload->data();
			$name = $data[ 'file_name' ];

			$this->session->set_userdata( $campo, $name );

			echo base_url().$this->config_arquivo[ $campo ][ 'config' ][ 'upload_path' ].$name;

		} else {

			echo '0';

		}

	}

	public function galeria() {

		$src = $this->image_form->upload_imagem( $_FILES[ 'arquivo' ], $this->config_galeria[ 'config' ] );

		$_POST[ 'arquivo' ] = $src;

		$id = $this->imput_form->galeria_insert( $_POST );

		$galeria_id = $_POST[ 'elemento' ];

		$session = $this->session->userdata( $galeria_id );

		$session[] = $id;

		$this->session->set_userdata( $galeria_id, $session );
		$this->session->set_userdata( 'galeria_id', $galeria_id );
// var_dump($galeria_id);
		$json = array(
			"src" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].'thumb/'.$src,
			"src_full" => base_url().$this->config_galeria[ 'config' ][ 'image_path' ].$src,
			"id" => $id,
			"pagina" => $this->pagina,
			"session" => $session,
			"id" => $id
		);

		echo json_encode( $json );

	}

	public function galeria_legenda() {

		$id = $this->imput_form->galeria_legenda( $_POST[ 'id' ], $_POST[ 'legenda' ] );

	}

	public function galeria_delete() {

		$id = $this->imput_form->galeria_delete( $_POST[ 'id' ] );

	}

}