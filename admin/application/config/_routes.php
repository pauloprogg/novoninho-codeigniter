<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';
$route['recuperar'] = 'home/recuperar';

$route['proprietarios/(:num)'] = 'proprietarios/index';
$route['corretores/(:num)'] = 'corretores/index';
$route['imobiliaria/(:num)'] = 'imobiliaria/index';
$route['aprovarimoveis/(:num)'] = 'aprovarimoveis/index';
$route['imoveisaprovados/(:num)'] = 'imoveisaprovados/index';
$route['noticias/(:num)'] = 'noticias/index';
$route['banner-topo/(:num)'] = 'banner-topo/index';
$route['banner-empreendimento/(:num)'] = 'banner-empreendimento/index';
$route['anuncios/(:num)'] = 'anuncios/index';
$route['revista/(:num)'] = 'revista/index';
$route['multimidia/(:num)'] = 'multimidia/index';
$route['imobiliarias/(:num)'] = 'imobiliarias/index';
$route['vendas/(:num)'] = 'vendas/index';
$route['vendedores/(:num)'] = 'vendedores/index';
$route['cidades/(:num)'] = 'cidades/index';


/* Banners */

$route['banner-empreendimento'] = 'banner_empreendimento';
$route['banner-empreendimento/add'] = 'banner_empreendimento/add';
$route['banner-empreendimento/create'] = 'banner_empreendimento/create';
$route['banner-empreendimento/read/(:num)'] = 'banner_empreendimento/read/$1';
$route['banner-empreendimento/update/(:num)'] = 'banner_empreendimento/update/$1';
$route['banner-empreendimento/delete/(:num)'] = 'banner_empreendimento/delete/$1';
$route['banner-empreendimento/image/(:any)'] = 'banner_empreendimento/image/$1';
$route['banner-empreendimento/file/(:any)'] = 'banner_empreendimento/file/$1';

$route['banner-topo'] = 'banner_topo';
$route['banner-topo/add'] = 'banner_topo/add';
$route['banner-topo/create'] = 'banner_topo/create';
$route['banner-topo/read/(:num)'] = 'banner_topo/read/$1';
$route['banner-topo/update/(:num)'] = 'banner_topo/update/$1';
$route['banner-topo/delete/(:num)'] = 'banner_topo/delete/$1';
$route['banner-topo/image/(:any)'] = 'banner_topo/image/$1';
$route['banner-topo/file/(:any)'] = 'banner_topo/file/$1';

/* Páginas */

$route['contato-site'] = 'contato_site';
$route['contato-site/delete/(:num)'] = 'contato_site/delete/$1';

$route['pagina-quemsomos/read/(:num)'] = 'pagina_quemsomos/read/$1';
$route['pagina-quemsomos/update/(:num)'] = 'pagina_quemsomos/update/$1';
$route['pagina-empreendimentos/read/(:num)'] = 'pagina_empreendimentos/read/$1';
$route['pagina-empreendimentos/update/(:num)'] = 'pagina_empreendimentos/update/$1';
$route['pagina-imobiliarias/read/(:num)'] = 'pagina_imobiliarias/read/$1';
$route['pagina-imobiliarias/update/(:num)'] = 'pagina_imobiliarias/update/$1';
$route['pagina-anuncieaqui/read/(:num)'] = 'pagina_anuncieaqui/read/$1';
$route['pagina-anuncieaqui/update/(:num)'] = 'pagina_anuncieaqui/update/$1';
$route['pagina-proprietario/read/(:num)'] = 'pagina_proprietario/read/$1';
$route['pagina-proprietario/update/(:num)'] = 'pagina_proprietario/update/$1';
$route['pagina-corretor/read/(:num)'] = 'pagina_corretor/read/$1';
$route['pagina-corretor/update/(:num)'] = 'pagina_corretor/update/$1';
$route['pagina-imobiliaria/read/(:num)'] = 'pagina_imobiliaria/read/$1';
$route['pagina-imobiliaria/update/(:num)'] = 'pagina_imobiliaria/update/$1';
$route['pagina-revistas/read/(:num)'] = 'pagina_revistas/read/$1';
$route['pagina-revistas/update/(:num)'] = 'pagina_revistas/update/$1';
$route['pagina-multimidia/read/(:num)'] = 'pagina_multimidia/read/$1';
$route['pagina-multimidia/update/(:num)'] = 'pagina_multimidia/update/$1';
$route['pagina-contato/read/(:num)'] = 'pagina_contato/read/$1';
$route['pagina-contato/update/(:num)'] = 'pagina_contato/update/$1';

$route['newsletter'] = 'newsletter';
$route['newsletter/index/'] = 'newsletter/index';


$route['como-soube'] = 'como_soube';
$route['como-soube/create'] = 'como_soube/create';
$route['como-soube/read/(:num)'] = 'como_soube/read/$1';
$route['como-soube/update/(:num)'] = 'como_soube/update/$1';
$route['como-soube/delete/(:num)'] = 'como_soube/delete/$1';
$route['como-soube/add'] = 'como_soube/add';
$route['como-soube/index/'] = 'como_soube/index';

$route['contato-site/read/(:num)'] = 'contato_site/read/$1';

$route['config-site'] = 'config_site';
$route['config-site/update/(:num)'] = 'config_site/update/$1';

$route['config-seo'] = 'config_seo';
$route['config-seo/read/(:num)'] = 'config_seo/read/$1';
$route['config-seo/update/(:num)'] = 'config_seo/update/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */