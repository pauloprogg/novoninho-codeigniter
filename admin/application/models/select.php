<?php

class Select extends CI_Model {

	private $campos,$tabela,$join,$where,$orderBy,$limit,$offset,$group;

	public function setCampos($p){
		$this->campos = $p;
	}
	public function getCampos(){
		return $this->campos;
	}

	public function setTabela($p){
		$this->tabela = $p;
	}
	public function getTabela(){
		return $this->tabela;
	}

	public function setJoin($p){
		$this->join = $p;
	}
	public function getJoin(){
		return $this->join;
	}

	public function setWhere($p){
		$this->where = $p;
	}
	public function getWhere(){
		return $this->where;
	}

	public function setOrderBy($p){
		$this->orderBy = $p;
	}
	public function getOrderBy(){
		return $this->orderBy;
	}

	public function setLimit($p){
		$this->limit = $p;
	}
	public function getLimit(){
		return $this->limit;
	}

	public function setOffset($p){
		$this->offset = $p;
	}
	public function getOffset(){
		return $this->offset;
	}

	public function setGroup($p){
		$this->group = $p;
	}
	public function getGroup(){
		return $this->group;
	}

	public function set($config){

		$this->setCampos($config['campos']);		
		$this->setTabela($config['tabela']);
		$this->setJoin(((isset($config['join'])) ? $config['join'] : array()));
		$this->setWhere(((isset($config['where'])) ? $config['where'] : array()));
		$this->setOrderBy(((isset($config['orderBy'])) ? $config['orderBy'] : array()));
		$this->setLimit(((isset($config['limit'])) ? $config['limit'] : 0));
		$this->setOffset(((isset($config['offset'])) ? $config['offset'] : NULL));
		$this->setGroup(((isset($config['group'])) ? $config['group'] : array()));

	}

	private function calculoOffset(){

		$pagina = (int)$this->uri->segment($this->uri->total_segments());
		$pagina = (($pagina > 0) ? $pagina-1 : 0);		

		$numreg = $this->getLimit();

		$quant_pg = ceil($pagina*$numreg);

		$this->setOffset( $quant_pg );

	}

	public function resultado(){

		$this->db->select($this->getCampos());
		$this->db->from($this->getTabela());

		//join
		$getJoin = $this->getJoin();
		if(!empty($getJoin)){
			/*array(
				array('comments c','c.id = blogs.id','left'),
				array('comments','comments.id = blogs.id','left')
			);*/

			$totaljoin = count($getJoin);
			for( $i = 0; $i < $totaljoin; $i++ ){

				if( !empty( $getJoin[$i] ) ){
					
					$this->db->join($getJoin[$i][0], $getJoin[$i][1], $getJoin[$i][2] );
					
				}
			}
		}

		$group = $this->getGroup();
		if(!empty($group)){
			$this->db->group_by($group);
		}

		//where
		$where = $this->getWhere();
		if(!empty($where)){
			//Você pode escrever suas próprias cláusulas manualmente
			//ou usar arrays = $array = array('name' => $name, 'title' => $title, 'status' => $status);
			//$array = array('name !=' => $name, 'id <' => $id, 'date >' => $date);
			$this->db->where($where);
		}

		//Ordem
		$ordem = $this->getOrderBy();
		if(!empty($ordem)){
			/*$orderBy = array("d.id"=>"desc");*/

			foreach($ordem as $key => $valor){
				$this->db->order_by($key,$valor);
			}
						

		}

		//LIMIT		
		if(!!$this->getLimit()){
                    
			if(is_numeric($this->getOffset())){
                            
				$this->db->limit($this->getLimit(),$this->getOffset());
			}else{
                            
				$this->db->limit($this->getLimit());
			}
		}

		$query = $this->db->get();
		$this->db->close();

		return $query->result();
	}


	public function total(){
		$this->db->select($this->getCampos());
		$this->db->from($this->getTabela());

		//join
		$getJoin = $this->getJoin();
		if(!empty($getJoin)){
			/*array(
				array('comments','comments.id = blogs.id','left'),
				array('comments','comments.id = blogs.id','left')
			);*/

			$totaljoin = count($getJoin);

			for($i = 0; $i < $totaljoin; $i++){

				if( !empty( $getJoin[$i] ) ){
					
					$this->db->join( $getJoin[$i][0], $getJoin[$i][1], $getJoin[$i][2] );
					
				}
			}
		}

		$group = $this->getGroup();
		if(!empty($group)){
			$this->db->group_by($group);
		}

		//where
		$where = $this->getWhere();
		if(!empty($where)){
			//Você pode escrever suas próprias cláusulas manualmente do gosto
			//ou usar arrays = $array = array('name' => $name, 'title' => $title, 'status' => $status);
			//$array = array('name !=' => $name, 'id <' => $id, 'date >' => $date);
			$this->db->where($where);
		}

		//Ordem
		$ordem = $this->getOrderBy();
		if(!empty($ordem)){
			/*$orderBy = array("d.id"=>"desc");*/

			foreach($ordem as $key => $valor){
				$this->db->order_by($key,$valor);
			}
						

		}

		$querytotal = $this->db->get();
        $this->db->close();
        return $querytotal->num_rows();
	}



	public function paginacao($url, $numLinks, $suffix = false ){

		$this->calculoOffset();
		$this->load->library('pagination');
		$config['base_url'] = base_url().$url;
		$config['suffix'] = ( $suffix ) ? $suffix : "?".$_SERVER[ 'QUERY_STRING' ];

		$config['total_rows'] = $this->total();

		$config['per_page'] = $this->getLimit();

		$config['first_tag_open'] = '<li class="arrow">';

		$config['first_tag_close'] = '</li>';

		$config['first_link'] = '&laquo;';

		$config['last_tag_open'] = '<li class="arrow">';

		$config['last_tag_close'] = '</li>';

		$config['last_link'] = '&raquo;';

		$config['num_links'] = $numLinks;

		$config['full_tag_open'] = '<ul class="pagination">';

		$config['full_tag_close'] = '</ul>';

		$config['cur_tag_open'] = '<li>';

		$config['cur_tag_close'] = '</li>';

		$config['next_link'] = "&raquo;";

		$config['next_tag_open'] = '<spam class="hide">';

		$config['next_tag_close'] = '</spam>';

		$config['prev_link'] = "&laquo;";

		$config['prev_tag_open'] = '<span class="hide">';

		$config['prev_tag_close'] = '</span>';

		$config['num_tag_open'] = '<li>';

		$config['num_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="current"><a href="javascript:void(0);">';

		$config['cur_tag_close'] = '</a></li>';

		$config['use_page_numbers'] = TRUE;

		$config['uri_segment'] = $this->uri->total_segments();

		$this->pagination->initialize($config);

		$paginacao = $this->pagination->create_links();

		return ((!empty($paginacao)) ? $paginacao : NULL);

	}

}

/*
	# chama o model select no controller
	$this->load->model("select");

	#campos = campos da tabela - obrigatorio

	#tabela = nome da tabela - obrigatorio

	#where = pode ser array ex array('campo' => 'valor') ou uma string personalizada

	#join = array ex 	array(
							array('comments c','c.id = blogs.id','left'),
							array('comments','comments.id = blogs.id','left')
						); 
						
	#orderBy = array ex array('campo' => 'ASC')

	#limit = inteiro - é obrigatorio caso use paginação

	#group = pode ser array ex array("title", "date") ou uma string 


	$this->load->model("select");

	$config = array('campos' => 'url_amigavel, texto1, texto2, arquivo0', 
							
				'tabela' => 'sa_servicos',

				'limit' => 1

				);
							
	$this->select->set($config);

	# nome da pagina
	# numero de link por pagina
	# usando a paginação tem que chamar ela antes de resultados
	$paginacao = $this->select->paginacao("teste", 3);

	#retorna o resultado
	$resultado = $this->select->resultado();

	//echo $this->db->last_query();

	#retorna o total de registro 
	$total = $this->select->total();


	echo "<pre>";
	print_r($resultado);

	echo $paginacao;

*/
?>