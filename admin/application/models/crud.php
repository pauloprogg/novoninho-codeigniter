<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud extends CI_Model {

	public function create( $data, $tabela, $insert_batch = false ) {

		if( $insert_batch ) {

			$this->db->insert_batch( $tabela, $data );
			return $this->db->insert_id();

		} else {

			$this->db->insert( $tabela, $data );
		
			return $this->db->insert_id();

		}

	}

	public function read( $where, $tabela, $order = array( 'id' => 'DESC' ), $limit = NULL ) {
		
		$config = array(
			'campos' => '*',
			'tabela' => $tabela,
			'where' => $where,
			'orderBy' => $order,
			'limit' => $limit
		);

		$this->select->set($config);

		return $this->select->resultado();

	}

	public function read_paginacao( $where, $tabela, $order = array( 'id' => 'DESC' ), $limit = NULL, $path, $n_link = 3 ) {

		$config = array(
			'campos' => '*',
			'tabela' => $tabela,
			'where' => $where,
			'orderBy' => $order,
			'limit' => $limit
		);

		$this->select->set($config);

		$paginacao = $this->select->paginacao( $path, $n_link );

		return array(
			'result' => $this->select->resultado(),
			'paginacao' => $paginacao
		);

	}

	public function update( $where, $data, $tabela ) {
		
		$this->db->where( $where );
		return $this->db->update( $tabela, $data );

	}

	public function delete( $where, $tabela ) {
		
		$this->db->where( $where );
		$this->db->delete( $tabela ); 

	}

}