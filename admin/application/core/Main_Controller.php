<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_Controller extends CI_Controller {

	public $title = "Admin";
	public $keywords = "";
	public $description = "";
	public $image = "assets/images/apple-touch-icon-114x114.png";
	public $site_name = "Padão Codeigniter";
	public $author = "Fazul Agência Web";

	public $css = array();
	public $js = array();
	public $js_inline = array();

	public $translate = array();
	public $sufixo = "";
	public $sufixo_sql = "";

	public $menu_header = array();
	public $menu_footer = array();
	public $menu_sidebar = array();

	public $usuarioLimitado = false;
	
	public $dt_ex = 0;

	public function __construct() {

		global $CFG;
		parent::__construct();

		if( $CFG->config['language_active'] ) {
			$this->lang->load('main', $this->config->item('language'));
			$this->sufixo = SUFIXO;
			$this->sufixo_sql = SUFIXO_SQL;
		}

		$login_session = $this->session->userdata( "login_admin" );

		$permissoes = $login_session['permissoes'];
		$permissoes = explode(',', $permissoes);

		//Checagem para liberar o acesso ao "editar meu perfil"
		$pag = $this->uri->segment(1);
		$subPag = $this->uri->segment(2);
		$idUserPag = $this->uri->segment(3);
		$idUserSession = $login_session['id_usuario'];

		$painelUserPag = $pag."/".$subPag."/".$idUserPag;
		$painelUserSession = $pag."/".$subPag."/".$idUserSession;


		if (!empty($pag) && $pag != "sair"){
			$config = array(
				'campos' => 'id, pagina',
				'tabela' => 'areas_admin',
				'where' => array('pagina'=>$pag)
			);

			$this->select->set($config);
			$areas = $this->select->resultado();
			if(!empty($areas)){
				if (!in_array($areas[0]->id ,$permissoes )){
					if ($painelUserPag != $painelUserSession){
						$this->session->set_flashdata( "msg_erro", "Você não tem permissão para acessar esta página!" );
						redirect( base_url() );
					} else {
						$this->usuarioLimitado = true;
					}
				}
			}

		}
		//Pegando os dias de expiração das configurações
		$config = array(
			'campos' => 'expiracao',
			'tabela' => 'config'
		);
		
		$this->select->set($config);
		$dias_expira = $this->select->resultado();
		
		$this->dt_ex = $dias_expira[0]->expiracao;

	}

	public function head() {

		$data = array(
			array(
				"title" => $this->title,
				"keywords" => $this->keywords,
				"description" => $this->description,
				"image" => $this->image,
				"site_name" => $this->site_name,
				"author" => $this->author,
				"css" => $this->css
			)
		);

		return $data;

	}

	public function header() {

		if( $this->session->userdata( "login_admin" ) == "" ) {
			$this->session->userdata( "login_admin", array() );
		}

		$data = array(
			array(

			)
		);

		return $data;

	}


	public function footer() {
		
		//Pegando os dias de expiração das configurações
		$config = array(
			'campos' => 'expiracao',
			'tabela' => 'config'
		);
		
		$this->select->set($config);
		$dias_expira = $this->select->resultado();
		
		$dt_ex = $dias_expira[0]->expiracao;

		$data = array(
			array(
				"copy_right" => date("Y") ." &copy;",
				"js" => $this->js,
				"js_inline" => $this->js_inline,
				"data_expira" => $this->dt_ex
			)
		);

		return $data;

	}

	public function getTranslate() {

		global $LANG;
		$this->translate = $LANG->language;

	}

	public function checkLogin() {

		$login_session = $this->session->userdata( "login_admin" );

		if( !isset( $login_session ) || empty( $login_session ) ) {

			redirect( base_url() );

		}

	}

	public function checkAcesso() {
		// $login_session = $this->session->userdata( "login_admin" );

		// $permissoes = explode(",",$login_session['permissoes']);


		// $config = array(
		// 	'campos' => 'id, pagina',
		// 	'tabela' => 'areas_admin',
		// 	'where' => array('pagina'=>$this->uri->segment(1))
		// );

		// $this->select->set($config);
		// $areas = $this->select->resultado();

		// if(!empty($areas)){
		// 	if (!in_array( $permissoes, $areas[0]->id)){
		// 		redirect( base_url() );
		// 	}
		// }
	}
	

	public function load( $view, $data = array(), $include = true ) {

		$this->getTranslate();
		$data = array_merge( $data, $this->translate );

		$this->checkAcesso();

		$msg_erro = $this->session->flashdata( "msg_erro" );
		$msg_aviso = $this->session->flashdata( "msg_aviso" );
		$msg_sucesso = $this->session->flashdata( "msg_sucesso" );

		$data[ "msg_erro" ] = '';
		$data[ "msg_aviso" ] = '';
		$data[ "msg_sucesso" ] = '';

		if( !empty( $msg_erro ) ) {
			$data[ "msg_erro" ] = '<span class="alert-box text-center error">'.$msg_erro.'</span>';
		}
		if( !empty( $msg_aviso ) ) {
			$data[ "msg_aviso" ] = '<span class="alert-box text-center warning">'.$msg_aviso.'</span>';
		}
		if( !empty( $msg_sucesso ) ) {
			$data[ "msg_sucesso" ] = '<span class="alert-box text-center success">'.$msg_sucesso.'</span>';
		}

		$login = $this->session->userdata( "login_admin" );

		if( $login ) {

			$data[ "dados_usuario" ] = array( $login );

		}
		
		//Verifica se vai exibir o "Aprovar Imóveis de Vendedores" ou não
		
		$data['header_master'] = '';
		
		if($login['admin_master'] == 1) {
			$data['header_master'] = '<li><a href="{base_url}imoveisaprovados/">Imóveis Aprovados</a></li>';
		}

		$data[ "base_url" ] = base_url();

		$data[ "head" ] = $this->head();

		if( $include ) {
			$data[ "header" ] = $this->header();

		}

		$data[ "footer" ] = $this->footer();

		$this->parser->parse( 'templates/metas', $data );

		if( $include ) {
			$this->parser->parse( 'templates/header', $data );
		}

		$this->parser->parse( $view, $data );
		$this->parser->parse( 'templates/footer', $data );

	}

}