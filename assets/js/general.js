//Carousel

/*
jQuery.validator.addMethod("cpf", function (value, element) {
    value = jQuery.trim(value);

    value = value.replace('.', '');
    value = value.replace('.', '');
    cpf = value.replace('-', '');
    while (cpf.length < 11) cpf = "0" + cpf;
    var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
    var a = [];
    var b = new Number;
    var c = 11;
    for (i = 0; i < 11; i++) {
        a[i] = cpf.charAt(i);
        if (i < 9) b += (a[i] * --c);
    }
    if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11 - x }
    b = 0;
    c = 11;
    for (y = 0; y < 10; y++) b += (a[y] * c--);
    if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11 - x; }
    if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) return this.optional(element) || false;
    return this.optional(element) || true;
}, "Informe um CPF válido."); // Mensagem padrão
*/

function carouselDestaque(pause){

	$('#carrousel_destaque').carouFredSel({
		responsive: true,
		direction: "left",
		items: {
			visible: {
                        min: 1,
                        max: 2
                    },
			width: 460
		},
		scroll : {
			items           : 1,
			duration        : 500,
			pauseOnHover    : true
		},
		prev: '#prev_destaque',
		next: '#next_destaque',
		auto: pause,
		swipe: {
			onMouse: true,
			onTouch: false
		}
	});
}

function carouselVenda(pause){
	$('#carrousel_venda').carouFredSel({
		responsive: true,
		direction: "left",
		items: {
			visible:
                    {
                        min: 1,
                        max: 3
                    },
			width: 220
		},
		scroll : {
			items           : 1,
			duration        : 500,
			pauseOnHover    : true
		},
		prev: '#prev_venda',
		next: '#next_venda',
		auto: pause,
		swipe: {
			onMouse: true,
			onTouch: false
		}
	});
}

function carouselLocacao(pause){
	$('#carrousel_locacao').carouFredSel({
		responsive: true,
		direction: "left",
		items: {
			visible:
                    {
                        min: 1,
                        max: 3
                    },
			width: 220
		},
		scroll : {
			items           : 1,
			duration        : 500,
			pauseOnHover    : true
		},
		prev: '#prev_locacao',
		next: '#next_locacao',
		auto: pause,
		swipe: {
			onMouse: true,
			onTouch: false
		}
	});
}

function carouselTemporada(pause){
	$('#carrousel_temporada').carouFredSel({
		responsive: true,
		direction: "left",
		items: {
			visible:
                    {
                        min: 1,
                        max: 3
                    },
			width: 220
		},
		scroll : {
			items           : 1,
			duration        : 500,
			pauseOnHover    : true
		},
		prev: '#prev_temporada',
		next: '#next_temporada',
		auto: pause,
		swipe: {
			onMouse: true,
			onTouch: false
		}
	});
}

function carouselBannerTopo(){
	$('#carrousel_banner_topo').carouFredSel({
		responsive: true,
		direction: "left",
		items: {
			visible: 1
		},
		scroll : {
			items           : 1,
			duration        : 1000,
			pauseOnHover    : true
		},
		swipe: {
			onMouse: true,
			onTouch: false
		},
		pagination: {
			anchorBuilder: function(nr) {
				return '<li></li>';
			},
			container: "#paginacao_topo"
		}
	});
}

function carouselFotosImovel(pause){
	$('#carrousel_fotos_imovel').carouFredSel({
		responsive: true,
		direction: "left",
		items: {
			visible: 7,
			// width: '86px',
			// height: '86px'
		},
		scroll : {
			items           : 1,
			duration        : 1000,
			pauseOnHover    : true
		},
		prev: '#prev_galeria',
		next: '#next_galeria',
		auto: pause,
		swipe: {
			onMouse: true,
			onTouch: false
		}
	});
}

function carouselBannerLogo(){
	$('#carrousel_banner_logo').carouFredSel({
		responsive: true,
		direction: "left",
		items: {
			visible: 1
		},
		scroll : {
			items           : 1,
			duration        : 1000,
			pauseOnHover    : true
		}
	});
}

function cep() {

	carregando(true);

	var endereco = $('.endereco');
	var bairro 	= $('.bairro');
	var cidade 	= $('.cidade');
	var uf 		= $('.estado');

	var endereco_id = $('#id_rua');
	var bairro_id 	= $('#id_bairro');
	var cidade_id 	= $('#id_cidade');
	var uf_id 		= $('#id_estado');

	var cep_val = $('.cep');

	if(cep_val.val() == ""){
		carregando(false);
		cep_val.focus();

		endereco.val('');
		bairro.val('');

		endereco_id.val('');
		bairro_id.val('');

		return false;
	}

	var ccep = consultaCEP(cep_val.val(), PATH);
	//carregando(false);

	if(ccep.result == true){

		carregando(false);

		if(ccep.resultado == 0){

			alert('CEP não encontrado!');

			endereco.val('');
			bairro.val('');
			cidade.val('');
			uf.val('');

			endereco_id.val('');
			bairro_id.val('');
			cidade_id.val('');
			uf_id.val('');

			return false;

		} else if(ccep.resultado == 2){

			//var h = '<div data-alert class="alert-box warning radius">CEP único<a href="javascript:void(0)" class="close">&times;</a></div>';
			//$(this).parents('form').find('.form_result').html(h).show();

			endereco.val('');
			bairro.val('');
			cidade.val(ccep.cidade);
			uf.val(ccep.estado);

			endereco_id.val('');
			bairro_id.val('');
			cidade_id.val(ccep.cidade_id);
			uf_id.val(ccep.estado_id);

		} else {

			endereco.val(ccep.rua);
			bairro.val(ccep.bairro);
			cidade.val(ccep.cidade);
			uf.val(ccep.estado);

			endereco_id.val(ccep.rua_id);
			bairro_id.val(ccep.bairro_id);
			cidade_id.val(ccep.cidade_id);
			uf_id.val(ccep.estado_id);

		}

	}

};

function consultaCEP(cep, PATH){

	var str = "";
	$.ajax({
		'type'	: 'POST',
		'url'	: PATH + 'painel/cep',
		'async' : false,
		'dataType': 'json',
		'data'	: {
			'cep' : cep
		},
		'success': function(data) {
			str = data
		}
	});

	return (str);
}

function excluir( id ) {

	var per = confirm("Deseja excluir esta foto?");

	if(per){

		var arquivo = $("#" + id).attr('src');

		dados = {
			method: 3,
			id: id
		}

		$.post(PATH+"painel/imagem", dados, function(data){});
		$("#l" + id).remove();

	}

}

function fotoDestaque() {

	var v = $('.btn_foto_destaque:checked').val();
	var id_imovel = $('#id_imovel').val();

	if(!id_imovel) {
		id_imovel = 0;
	}

	dados = {
			id_foto : v,
			id_imovel : id_imovel
		}
		

	$.post( PATH+"painel/imagemDestaque", dados, function( res ) {
		// location.href = PATH+caminho;
	});

}

//Abrir modal de cidades no primeiro acesso do cliente
function abrirModalCidade() {
	$('#abrirModalCidades').trigger('click');
}

//Div de Carregamento
function carregando(status){

	if(status){

		$('#overlay').html('').append('<div id="overlay_load"></div>').fadeIn(); //Carregando

	} else {

		$('#overlay').fadeOut();

		$('#overlay_load').remove();

	}

}

$(document).ready(function(){

	//atualizar altura dos carrocels na home
	//destaque*********************
	maxHeight = 0;
	// $('.destaque').each(function() {
 //     	maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
 //   	});

 //   	$('.destaque').each(function() {
 //     	$(this).height(maxHeight);
 //   	});
 //    $('.destaque .caroufredsel_wrapper').height(maxHeight);

	//venda*********************
    maxHeight = 0;
	$('.venda_item').each(function() {
     	maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
   	});

   	$('.venda_item').each(function() {
     	$(this).height(maxHeight);
   	});
    $('.lista_venda .caroufredsel_wrapper').height(maxHeight);


    //locacao*********************
    maxHeight = 0;
	$('.locacao_item').each(function() {
     	maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
   	});

   	$('.locacao_item').each(function() {
     	$(this).height(maxHeight);
   	});
    $('.lista_venda .caroufredsel_wrapper').height(maxHeight);


    //temporada*********************
    maxHeight = 0;
	$('.temporada_item').each(function() {
     	maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
   	});

   	$('.temporada_item').each(function() {
     	$(this).height(maxHeight);
   	});
    $('.lista_venda .caroufredsel_wrapper').height(maxHeight);


	$(document).foundation();

	//Select 2
	$('select.classificados_cidade').select2({
  		placeholder: "Selecione a Cidade"
	});

	$("body").on('change', '#bairro', function(){
		$(".check-bairros").prop('checked', $(this).prop('checked') );
	});

	//Se Tornar um anunciante
	$('#ativar_anunciante').on('click', function(){
		$.ajax({
			type: "POST",
			url: PATH+'painel_proprietario/ativarAnunciante',
			dataType: "html",
			data: [],
			beforeSend: function(){
				carregando(true);
			},
			success: function(retorno){

				carregando(false);
				location.href = PATH+'painel_proprietario';

			}
		});
		return false;
	});


	//Botão de Favoritar
	$('.wish_imovel').on('click', function(){

		var id = $(this).attr('data-id');
		var act = $(this).attr('data-act');

		var logado = $(this).attr('data-logado');

		if(logado == 0) { //Não está logado

			$('#modalCadastroUsuario').foundation('reveal', 'open');

		} else {

			dados = {
				id_imovel : id
			}

			$.ajax({
				type: "POST",
				url: PATH+'imovel/'+act,
				dataType: "html",
				data: dados,
				beforeSend: function(){
					carregando(true);
				},
				success: function(retorno){

					carregando(false);

					$('.wish_remove').toggle(300);
					$('.wish_add').toggle(300);

				}
			});

		}
		return false;

	});

	$('.check_mascara').click(function(){

		$('#cpf_cnpj').removeClass('cpf').removeClass('cnpj').val('');

		if($(this).val() == 1) {

			$('#cpf_cnpj').addClass('cpf');

		} else {

			$('#cpf_cnpj').addClass('cnpj');

		}

		$( ".cpf" ).mask( "999.999.999-99" );
		$( ".cnpj" ).mask( "99.999.999/9999-99" );

	});

	//Revelar formulário de usuario comum
	$('.viewFormUsuario').on('click', function(){
		$('.cadastroUsuario').slideToggle(300);
	});

	$( "#publicar-imovel" ).on( "click", function() {
		var conf = confirm( "Depois do seu pagamento aprovado o novo ninho irá publicar seu imóvel em até 48 horas." );
		if( conf ) {

			carregando(true);
			return true;
		} else {
			return false;
		}
	} );
	//Botão de Visualizar
	$('#visualizar_anuncio').click(function(){
		var temDisabled=0;
		$('select').each(function(){
			if ($(this).is(':disabled') ) {
				//console.log($(this));
				temDisabled=1;
				$(this).removeAttr( "disabled" )
			}
		});
		$('input').each(function(){
			if ($(this).is(':disabled') ) {
				//console.log($(this));
				temDisabled=1;
				$(this).removeAttr( "disabled" )
			}
		});
		$('textarea').each(function(){
			if ($(this).is(':disabled') ) {
				//console.log($(this));
				temDisabled=1;
				$(this).removeAttr( "disabled" )
			}
		});

		var $this = $( this );
		var dados = $('.formulario_imovel').serialize();
		$.ajax({
			type: "POST",
			url: PATH+'painel/visualizarImovel',
			dataType: "html",
			data: dados,
			beforeSend: function(){
				carregando(true);
			},
			success: function(retorno){

				carregando(false);
				//$this.css( "display", "none" );
				$('.visualizarimovel').slideDown(300);
				$('#dados_imovel').html(retorno);

				$('body,html').animate({
					"scrollTop": $('.visualizarimovel').offset().top
				},1000);

			}
		});

		if (temDisabled == 1) {
			$('select').each(function(){
				$(this).attr( 'disabled', 'disabled' )
			});
		};
		if (temDisabled == 1) {
			$('input').each(function(){
				$(this).attr( 'disabled', 'disabled' )
			});
		};
		if (temDisabled == 1) {
			$('textarea').each(function(){
				$(this).attr( 'disabled', 'disabled' )
			});
		};
		return false;

	});

	//Botao para ativar a pesquisa por cep do classificados
	$('.ativar_cep').click(function(){

		if($('#usar_cep').val() == 1) {
			$('#usar_cep').val(0);
			$('.semcep').slideDown(300);
			$('.comcep').slideUp(300);

			var tot = $('.classificados_cidade option:selected').length;

			if(tot == 1) {
				$('.lista_bairros').slideDown(300);
			} else {
				$('.lista_bairros').slideUp(300);
			}

			//$('.cidade_classificados .check_cidade').trigger('change');
		} else if ($('#usar_cep').val() == 0) {
			$('#usar_cep').val(1);
			$('.semcep').slideUp(300);
			$('.comcep').slideDown(300);
		}

		if($('.cep_texto').text() == 'Pesquisa por Cidade/Estado') {
			$('.cep_texto').text('Pesquisa por Endereço');
		} else if($('.cep_texto').text() == 'Pesquisa por Endereço') {
			$('.cep_texto').text('Pesquisa por Cidade/Estado');
		}

		$('#cidade').prop('disabled', function(i, v) { return !v; });
		$('#estado').prop('disabled', function(i, v) { return !v; });

	});

	//Botao de visualizacao mapa
	$('body').delegate('.ativar_mapa', 'click', function(){

		if($('.mapa_texto').text() == 'Visualizar') {
			$('.mapa_texto').text('Esconder');
		} else if($('.mapa_texto').text() == 'Esconder') {
			$('.mapa_texto').text('Visualizar');
		}

		if($('.box_mapa').hasClass('box_mapa_hidden')) {
			$('.box_mapa').css('display', 'none').toggleClass('box_mapa_hidden').slideDown();
		} else {
			$('.box_mapa').slideToggle();
		}

	});

	//Botao do menu mobile
	$( ".open_menu" ).click(function(){
		$('.menu_mobile li').toggle(300);
	});

	abrirModalCidade();

	//cep();

	$( ".cep" ).on( "change", cep );
	$( ".go-cep" ).on( "click", cep );

	$(".moeda").maskMoney({thousands:'.', decimal:',', affixesStay: false});

	$( ".data" ).mask( "99/99/9999" );

	$( ".horario" ).mask( "99:99" );

	/* mask */
// jQuery Masked Input
$('.telefone').focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
        element.mask("(99) 99999-999?9");
    } else {
        element.mask("(99) 9999-9999?9");
    }
}).trigger('focusout');

$('.fone').focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
        element.mask("(99) 99999-999?9");
    } else {
        element.mask("(99) 9999-9999?9");
    }
}).trigger('focusout');

$('.celular').focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
        element.mask("(99) 99999-999?9");
    } else {
        element.mask("(99) 9999-9999?9");
    }
}).trigger('focusout');

	/*
$('.telefone').keydown(function(){
var phone, element;
element = $(this);
phone = element.val().replace(/\D/g, '');

if(phone.length >= 10) {
element.mask("(99) 99999-999?9");
}
}).trigger('keydown');
$('.telefone').focusout(function(){
var phone, element;
element = $(this);
element.unmask();
phone = element.val().replace(/\D/g, '');
if(phone.length > 10) {
element.mask("(99) 99999-999?9");
} else {
element.mask("(99) 9999-9999?9");
}
}).trigger('focusout');

$('.fone').keydown(function(){
var phone, element;
element = $(this);
phone = element.val().replace(/\D/g, '');

if(phone.length >= 9) {
element.mask("99999-999?9");
}
}).trigger('keydown');
$('.fone').focusout(function(){
var phone, element;
element = $(this);
element.unmask();
phone = element.val().replace(/\D/g, '');
if(phone.length > 9) {
element.mask("99999-999?9");
} else {
element.mask("9999-9999?9");
}
}).trigger('focusout');
*/

//**********************

	//$( ".telefone" ).mask( "(99) 99999-9999" );

	//$( ".celular" ).mask( "(99) 99999-9999" );

	$( ".cpf" ).mask( "999.999.999-99" );

	$( ".cep" ).mask( "99999-999" );

	$( ".cnpj" ).mask( "99.999.999/9999-99" );

	carouselDestaque(true);
	carouselVenda(true);
	carouselLocacao(true);
	carouselTemporada(true);
	carouselBannerTopo();
	carouselFotosImovel(true);
	carouselBannerLogo();

	$(window).bind({load:carouselDestaque});
	$(window).bind({load:carouselVenda});
	$(window).bind({load:carouselLocacao});
	$(window).bind({load:carouselTemporada});
	$(window).bind({load:carouselBannerTopo});
	$(window).bind({load:carouselFotosImovel});
	$(window).bind({load:carouselBannerLogo});

	$(window).resize(function(){
		carouselDestaque(true);
		carouselVenda(true);
		carouselLocacao(true);
		carouselTemporada(true);
		carouselBannerTopo();
		carouselFotosImovel(true);
		carouselBannerLogo();
	});

	$('#pause_destaque').click(function(){
        carouselDestaque(false);
	});

	$('#pause_venda').click(function(){
        carouselVenda(false);
	});

	$('#pause_locacao').click(function(){
        carouselLocacao(false);
	});

	$('#pause_temporada').click(function(){
        carouselTemporada(false);
	});

	$('#pause_galeria').click(function(){
        carouselFotosImovel(false);
	});

	// //Configurações da Modal de Cidades
	// $('#modalCidades').foundation({
		// close_on_background_click: false
	// });

	//Galeria de imagem dos imoveis e dos empreendimentos
	$('.galeria_foto').click(function(){

		var imagem = $(this).find('img').attr('src');

		$('.imagem_principal').find('img').attr('src', imagem);

		return false;

	});

	$( ".div_titulo" ).click(function(){

		var div = $(this).parent().parent().parent().attr('data-id');

		$(this).parent().parent().parent().toggleClass('ativo');

		$(this).parent().parent().parent().find('.seta1').toggle();
		$(this).parent().parent().parent().find('.seta2').toggle();

		$('#'+div).toggle(300);

	});

	/* VALIDATE DE FORMULARIOS */

	$( "#formulario_contato" ).validate({

		rules: {
			nome: { required: true },
			email: { required: true, email: true },
			assunto: { required: true },
			mensagem: { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				dataType: "html",
				data: dados,
				beforeSend: function(){
					carregando(true);
				},
				success: function(retorno){

					carregando(false);
					form.reset();
					alert("Email Enviado com sucesso!");

				}
			});
			return false;
		}

	});

	$( "#cadastro_usuario_comum" ).validate({

		rules: {
			nome: { required: true },
			email: { required: true, email: true },
			senha: { required: true },
			confirmar_senha: { required: true, equalTo: "#senha_usuario" },
			contrato : { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				beforeSend: function(){
					carregando(true);
				},
				success: function(r){

					carregando(false);

					if(r.status == 1) { //Erro
						alert(r.msg);
					} else {
						location.reload();
					}

				}
			},'json');
			return false;
		}

	});

	$( "#cadastro_proprietario" ).validate({

		rules: {
			nome: { required: true },
			email: { required: true, email: true },
			senha: { required: true },
			confirmar_senha: { required: true, equalTo: "#senha" },
			contrato : { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				beforeSend: function(){
					carregando(true);
				},
				success: function(r){

					carregando(false);

					if(r.status == 1) { //Erro
						alert(r.msg);
					} else {
						$('#link_modal').trigger('click');

						setTimeout(function(){
							location.href = PATH+'painel_proprietario';
						}, 5000);

					}

				}
			},'json');
			return false;
		}

	});

	$( "#cadastro_corretor" ).validate({

		rules: {
			creci: { required: true },
			nome: { required: true },
			email: { required: true, email: true },
			senha: { required: true },
			confirmar_senha: { required: true, equalTo: "#senha" },
			contrato : { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				beforeSend: function(){
					carregando(true);
				},
				success: function(r){

					carregando(false);

					if(r.status == 1) { //Erro
						alert(r.msg);
					} else {
						$('#link_modal').trigger('click');

						setTimeout(function(){
							location.href = PATH+'painel_corretor';
						}, 5000);

					}

				}
			},'json');
			return false;
		}

	});

	$( "#cadastro_imobiliaria" ).validate({

		rules: {
			creci: { required: true },
			nome: { required: true },
			responsavel: { required: true },
			endereco: { required: true },
			numero: { required: true },
			cidade: { required: true },
			estado: { required: true },
			email: { required: true, email: true },
			telefone: { required: true },
			senha: { required: true },
			confirmar_senha: { required: true, equalTo: "#senha" },
			contrato : { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				beforeSend: function(){
					carregando(true);
				},
				success: function(r){

					carregando(false);

					if(r.status == 1) { //Erro
						alert(r.msg);
					} else {
						$('#link_modal').trigger('click');

						setTimeout(function(){
							location.href = PATH+'painel_imobiliaria';
						}, 5000);

					}

				}
			},'json');
			return false;
		}

	});

	$( "#formulario_login" ).validate({

		rules: {
			login_email: { required: true },
			login_senha: { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				beforeSend: function(){
					carregando(true);
				},
				success: function(r){

					carregando(false);

					if(r.status == 1) { //Erro
						alert(r.msg);
					} else {
						location.href = PATH+r.caminho;
					}

				}
			},'json');
			return false;
		}

	});

	$( "#formulario_login_favorito" ).validate({

		rules: {
			login_email: { required: true },
			login_senha: { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				beforeSend: function(){
					carregando(true);
				},
				success: function(r){

					carregando(false);

					if(r.status == 1) { //Erro
						alert(r.msg);
					} else {
						location.reload();
					}

				}
			},'json');
			return false;
		}

	});

	$( "#proprietario_editarperfil" ).validate({

		rules: {
			nome: { required: true },
			email: { required: true, email: true },
			//cpf: { required: true, cpf: true },
			confirmar_senha: { equalTo: "#senha" }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				success: function(r){
					carregando(false);

					if(r.status == 1) { //Erro
						alert(r.msg);
					} else {
						alert(r.msg);
						location.href = PATH+'painel_proprietario';
					}
				}
			},'json');
			return false;
		}

	});

	$( "#corretor_editarperfil" ).validate({

		rules: {
			creci: { required: true },
			nome: { required: true },
			email: { required: true, email: true },
			confirmar_senha: { equalTo: "#senha" }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				success: function(r){
					carregando(false);

					if(r.status == 1) { //Erro
						alert(r.msg);
					} else {
						alert(r.msg);
						location.href = PATH+'painel_corretor';
					}
				}
			},'json');
			return false;
		}

	});

	$( "#imobiliaria_editarperfil" ).validate({

		rules: {
			creci: { required: true },
			imobiliaria: { required: true },
			responsavel: { required: true },
			endereco: { required: true },
			numero: { required: true },
			cidade: { required: true },
			estado: { required: true },
			email: { required: true, email: true },
			confirmar_senha: { equalTo: "#senha" }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				success: function(r){
					if(r.status == 1) { //Erro
						alert(r.msg);
					} else {
						alert(r.msg);
						location.href = PATH+'painel_imobiliaria';
					}
				}
			},'json');
			return false;
		}

	});

	$( "#cadastrar_imovel" ).validate({

		rules: {
			titulo: { required: true },
			descricao1: { required: true },
			cep: { required: true },
			numero: { required: true },
			cidade: { required: true },
			estado: { required: true },
			valor: { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			//$('.btn_azul').attr('disabled', 'disabled');

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				beforeSend: function(){
					carregando(true);
				},
				success: function(r){

					carregando(false);

					if(r.status == 1) {

						$('.form_result').html('<div data-alert="" class="alert-box alert radius">'+r.msg+'<a href="javascript:void(0)" class="close">×</a></div>');

					} else {
						location.href = PATH+$this.attr("caminho");
					}
				}
			},'json');
			return false;
		}

	});

	$( "#cadastrar_imovel_imobiliaria" ).validate({

		rules: {
			titulo: { required: true },
			descricao1: { required: true },
			numero: { required: true },
			id_cidade: { required: true },
			id_estado: { required: true },
			valor: { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			//$('.btn_azul').attr('disabled', 'disabled');

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				beforeSend: function(){
					carregando(true);
				},
				success: function(r){

					carregando(false);

					if(r.status == 1) {

						$('.form_result').html('<div data-alert="" class="alert-box alert radius">'+r.msg+'<a href="javascript:void(0)" class="close">×</a></div>');

					} else {
						location.href = PATH+$this.attr("caminho");
					}
				}
			},'json');
			return false;
		}

	});

	$( "#editar_imovel" ).validate({

		rules: {
			titulo: { required: true },
			descricao1: { required: true },
			cep: { required: true },
			cidade: { required: true },
			estado: { required: true },
			valor: { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				success: function(r){
					location.href = PATH+$this.attr("caminho");
				}
			},'json');
			return false;
		}

	});

	$( "#editar_imovel_imobiliaria" ).validate({

		rules: {
			titulo: { required: true },
			descricao1: { required: true },
			id_cidade: { required: true },
			id_estado: { required: true },
			valor: { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				success: function(r){
					location.href = PATH+$this.attr("caminho");
				}
			},'json');
			return false;
		}

	});

	$( "#editar_imovel_bloqueado" ).validate({

		rules: {
			descricao1: { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				success: function(r){
					location.href = PATH+$this.attr("caminho");
				}
			},'json');
			return false;
		}

	});

	$( "#contato_imovel" ).validate({

		rules: {
			nome: { required: true },
			email: { required: true, email: true },
			mensagem: { required: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				success: function(r){
					form.reset();
					$('.contato_realizado').show(300);
				}
			},'json');
			return false;
		}

	});

	$('.usar_contato').click(function() {

			var v = $('.usar_contato:checked').val();

			if(v == 2) {
				$('.dados_contato').show();
			} else {
				$('.dados_contato').hide();
			}

	});

	/* Botões de Ocultar e Destaque das Imobiliarias */
	$('.imobiliaria_ocultar').click(function() {

		var id = $(this).attr('id');

		dados = {
			id : id
		}

		if(confirm('Deseja Ocultar/Mostar este anuncio?')) {
			$.post( PATH+"painel-imobiliaria/doOcultar", dados, function( res ) {
				location.href = PATH+'painel-imobiliaria';
			});
		}

		return false;

	});

	$('.imobiliaria_destaque').click(function() {

		var id = $(this).attr('id');

		dados = {
			id : id
		}

		$.post( PATH+"painel-imobiliaria/doDestaque", dados, function( res ) {

			if(res.status == 1) {
				alert(res.msg);
			} else {
				location.href = PATH+'painel-imobiliaria';
			}

		},'json');

		return false;

	});


	$('.imobiliaria_super_destaque').click(function() {

		var id = $(this).attr('id');

		dados = {
			id : id
		}

		$.post( PATH+"painel-imobiliaria/doSuperDestaque", dados, function( res ) {

			if(res.status == 1) {
				alert(res.msg);
			} else {
				location.href = PATH+'painel-imobiliaria';
			}

		},'json');

		return false;

	});


	$('.imobiliaria_feirao').click(function() {

		var id = $(this).attr('id');

		dados = {
			id : id
		}

		$.post( PATH+"painel-imobiliaria/doFeirao", dados, function( res ) {

			if(res.status == 1) {
				alert(res.msg);
			} else {
				location.href = PATH+'painel-imobiliaria';
			}

		},'json');

		return false;

	});


	$('.imobiliaria_destaque_feirao').click(function() {

		var id = $(this).attr('id');

		dados = {
			id : id
		}

		$.post( PATH+"painel-imobiliaria/doDestaqueFeirao", dados, function( res ) {

			if(res.status == 1) {
				alert(res.msg);
			} else {
				location.href = PATH+'painel-imobiliaria';
			}

		},'json');

		return false;

	});


	$('.imobiliaria_super_destaque_feirao').click(function() {

		var id = $(this).attr('id');

		dados = {
			id : id
		}

		$.post( PATH+"painel-imobiliaria/doSuperDestaqueFeirao", dados, function( res ) {

			if(res.status == 1) {
				alert(res.msg);
			} else {
				location.href = PATH+'painel-imobiliaria';
			}

		},'json');

		return false;

	});

	$('body').on('click', '#feirao', function(){
 		var disabled = true;
 		

 		if( $(this).is(':checked') ){
 			disabled = false; 
 		}

 		$('#destaque_feirao, #super_destaque_feirao').prop('disabled', disabled);

 	});


	/* Envio de Imagens no Cadastro de Imóveis */
	$('#file_uploaddisabled').remove();
	$('#file_upload').uploadifive({

		'fileSizeLimit' : '100MB',
        'fileType' : '*.jpg; *.png',
		'uploadScript' : PATH+'assets/js/lib/uploadify/uploadifyImoveis.php',
		'buttonText' : 'Enviar Imagens',
		'buttonClass': 'btn_azul',
		'multi'      : true,
		'queueSizeLimit' : 10,
		'width': 200,
		'formData'      : {
			'slug': "imovel",
			'w': 600,
			'h': 600,
			'id_usuario': $('#id_usuario').val()
		},
		'onUploadComplete' : function(file, data) {

			var usuario = $('#id_usuario').val();
			var imagemExibe = $.trim(data);

			dados = {
				method : 1,
				arquivo : data,
				elemento : window.item,
				galeria: window.galeria
			};

			$.post( PATH+"painel/imagem", dados, function( res ) {

				var retorno = "";

				var imagem = PATH+'assets/uploads/imovel/'+usuario+'/'+imagemExibe;

				retorno += "<div class='links_fotos' id='l"+ res +"'>";
					retorno += "<a  href='javascript:void(0)' rel='foto_galeria' style='cursor:move'>";
						retorno += "<img id='"+res+"' data-id='"+res+"' src='"+imagem+"' alt='"+$.trim(data)+"' />";
					retorno += "</a>";
					retorno += "<a href='javascript:void(0)' onclick=\"excluir('" + res + "')\" class='excluir'></a>";
					retorno += "<input type='radio' class='btn_foto_destaque' id='btn_foto_destaque' name='btn_foto_destaque' onclick='fotoDestaque()' value='"+res+"' title='Imagem Destaque' />";
				retorno += "</div>";

				$('#resultado_f').append( retorno );

			});

			$('#resultado_f').slideDown("fast");
        }

	});
	 $('#1file_upload').uploadify({
	 	'fileSizeLimit' : '100MB',
	 	'fileTypeDesc' : 'Image Files',
        'fileTypeExts' : '*.jpg; *.png',
        'swf'      : PATH+'assets/js/lib/uploadify/uploadify.swf',
        'uploader' : PATH+'assets/js/lib/uploadify/uploadifyImoveis.php',
		'buttonText' : 'Enviar Imagens',
		'multi'      : true,
		'queueSizeLimit' : 10,
		'formData'      : {
			'slug': "imovel",
			'w': 600,
			'h': 600,
			'id_usuario': $('#id_usuario').val()
		},
		'onQueueFull': function (event, queueSizeLimit) {
                alert("I'm stuffed, please don't put anymore files in me!");
                return false;
        },
		'onUploadSuccess' : function(file, data, response) {

			var usuario = $('#id_usuario').val();
			var imagemExibe = $.trim(data);

			dados = {
				method : 1,
				arquivo : data,
				elemento : window.item,
				galeria: window.galeria
			};

			$.post( PATH+"painel/imagem", dados, function( res ) {

				var retorno = "";

				var imagem = PATH+'assets/uploads/imovel/'+usuario+'/'+imagemExibe;

				retorno += "<div class='links_fotos' id='l"+ res +"'>";
					retorno += "<a  href='javascript:void(0)' rel='foto_galeria' style='cursor:move'>";
						retorno += "<img id='"+res+"' data-id='"+res+"' src='"+imagem+"' alt='"+$.trim(data)+"' />";
					retorno += "</a>";
					retorno += "<a href='javascript:void(0)' onclick=\"excluir('" + res + "')\" class='excluir'></a>";
					retorno += "<input type='radio' class='btn_foto_destaque' id='btn_foto_destaque' name='btn_foto_destaque' onclick='fotoDestaque()' value='"+res+"' title='Imagem Destaque' />";
				retorno += "</div>";

				$('#resultado_f').append( retorno );

			});

			$('#resultado_f').slideDown("fast");

		}

	});


	/* Botão de Pagar */
	$('#enviar_publicar').click(function() {

		
		var data = new Array();
		var c = 0;
		var caminho = $(this).attr('caminho');

		$('.selecao').each(function(){
		var preco = new Object();

			if($(this).is(':checked')) {

				var id_imovel = $(this).val();


				preco.id = id_imovel;
				preco.feirao = 0;
				preco.destaque = 0;
				preco.super_destaque = 0;
				preco.destaque_feirao = 0;
				preco.super_destaque_feirao = 0;
				data[c] = preco;

				if($(this).parent().parent().find('.feirao').is(':checked')){
					preco.id = id_imovel;
					preco.feirao = 1;
					data[c] = preco;
				}
				
				if($(this).parent().parent().find('.destaque').is(':checked')){
					preco.id = id_imovel;					
					preco.destaque = 1;
				 	data[c] = preco;
				}

				if($(this).parent().parent().find('.super_destaque').is(':checked')){
					preco.super_destaque = 1;
				 	data[c] = preco;
				}
				
				if($(this).parent().parent().find('.destaque_feirao').is(':checked')){
					preco.id = id_imovel;
					preco.destaque_feirao = 1;
				 	data[c] = preco;
				}
				
				if($(this).parent().parent().find('.super_destaque_feirao').is(':checked')){
					preco.super_destaque_feirao = 1;
				 	data[c] = preco;
				}
				
				c++;

			}


		});
		
		if( data != "" ) {

			dados = {
				itens: data
			};

			$.post( $('#enviar_publicar').attr("action"), dados, function( res ) {
				location.href = PATH+caminho+'/publicar';
			});

		} else {

			alert('Selecione algum anuncio para publicar!');

		}

	});


	/* Excluir Imovel */
	$('.deletar_imovel').click(function() {

		var id = $(this).attr('id');
		var caminho = $(this).attr('caminho');

		dados = {
			id : id,
			caminho : caminho
		}

		if(confirm("Deseja Deletar este Imóvel?")) {
			$.post( PATH+"painel/deletar", dados, function( res ) {
				location.href = PATH+caminho;
			});
		}

		return false;

	});

	/* Estado */
	$('.estado, .select_estado').change(function() {

		var val = $(this).val();

		dados = {
			uf : val
		}

		$.post( PATH+"classificados/montarCidades", dados, function( res ) {
			$('.cidade, .select_cidade').html(res);
			//$('.cidade, .select_cidade').trigger('change');
			$('.modal_cidade').show(300);
		});

	});

	/* Feirao */ 

	$('.cidade, .select_cidade').change(function() {
		
		
		var val = $(this).val();

		dados = {
			id_cidade : val
		}

		$.post( PATH+"classificados/montarFeirao", dados, function( res ) {
			if(res != ''){
				$('.feirao, .select_feirao').html(res).attr('disabled', false);
				$('.feirao, .select_feirao').trigger('change');
				$('.modal_feirao').show(300);
			} else {
				$('.feirao, .select_feirao').html('').attr('disabled', true);
				$('.feirao, .select_feirao').trigger('change');
				$('.modal_feirao').hide(300);
			}
		});
		

	});

	$('.estado_sessao').change(function() {

		var val = $(this).val();

		dados = {
			uf : val
		}

		$.post( PATH+"classificados/montarCidadesSessao", dados, function( res ) {
			// console.log(res);
			$('.cidade').html(res);
			$('.cidade').trigger('change');
			$('.modal_cidade').show(300);
		});

	});

	$('.estado_xml').change(function() {

		var val = $(this).val();

		dados = {
			uf : val
		}

		$.post( PATH+"classificados/montarCidadesXml", dados, function( res ) {
			// console.log(res);
			$('.cidade_xml').html(res);
		});

	});

	$('.estado_classificados').change(function() {

		var val = $(this).val();

		dados = {
			uf : val
		}

		$.ajax({
			type: "POST",
			url: PATH+"classificados/montarCidadesClassificados",
			data: dados,
			beforeSend: function(){
				carregando(true);
			},
			success: function(res){
				carregando(false);
				$('.cidade_classificados').html(res);
				$('div.lista_bairros').hide(200);
			}
		},'json');
		return false;

	});

	/* Bairros */

	$('body').on('change', '.classificados_cidade', function() {

		var tot = $('.classificados_cidade option:selected').length;

		if(tot > 0) {

			if(tot == 1) {

				var val = $('.classificados_cidade option:selected').val();

				dados = {
					cidade : val
				}

				$.ajax({
					type: "POST",
					url: PATH+"classificados/montarBairros",
					data: dados,
					beforeSend: function(){
						carregando(true);
					},
					success: function(res){
						carregando(false);
						$('div.lista_bairros').show(200);
						$('div.bairros ul').html(res);
					}
				},'json');
				return false;

			} else {

				$('div.lista_bairros').hide(200);

			}

		} else {

			$('div.lista_bairros').hide(200);

		}

	});

	/* Enviar Arquivo XML */
	$('#enviar_xml').click(function() {
		$('#modalEscolherXML').foundation('reveal', 'open');
	});

	$('#carrega_xml').click(function() {
		$('.file_xml').trigger('click');
		return false;
	});

	$('.file_xml').on('change', function(){

		var val = $(this).val();

		if(val != '') {

			var arquivo = $(this).val().split("\\");
			var nome_arquivo = arquivo[2];

			$('.nome_arquivo').text(nome_arquivo);

			$('.submit_xml').show(200);

		}

		return false;

	});

	$('#form_xml').ajaxForm({
		dataType:  'json',
		beforeSend: function() {
			carregando(true);
		},
		success: function(data) {
			carregando(false);

			if(data.status == 0) { //Erro

				$('.form_result').html('<div data-alert="" class="alert-box alert radius">'+data.msg+'</div>').show(200);

			} else { //Sucesso

				$('.form_result').html('<div data-alert="" class="alert-box success radius">'+data.msg+'</div>').show(200);
				setTimeout(function(){
					location.href = PATH+'painel-imobiliaria'; },
				2000);

			}

		},
		complete: function() {
			carregando(false);
		}
	});

	/* Esqueci minha senha */

	$('.esquecisenha').click(function() {
		$('#formulario_login').hide(300);
		$('#esqueci_senha').show(300);
	});

	$( "#esqueci_senha" ).validate({

		rules: {
			email: { required: true, email: true }
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				success: function(r){

					if(r == 0) { //Erro
						alert('A Nova Senha foi enviado ao seu email');
						$('#formulario_login').show(300);
						$('#esqueci_senha').hide(300);
						form.reset();
					} else {
						alert('Email não cadastrado no sistema.');
					}

				}
			},'json');
			return false;
		}

	});

	$( "#trocarCidade" ).validate({

		rules: {
			estado: { required: true},
			cidade: { required: true}
		},

		submitHandler: function(form){

			var $this = $( form );
			var dados = $this.serialize();

			$.ajax({
				type: "POST",
				url: $this.attr("action"),
				data: dados,
				dataType : "json",
				success: function(r){

					if(!!r.success){
						if(!!r.feirao){
							window.location.href = PATH+'classificados';
						} else {
							window.location.reload();
						}
					}

				}
			},'json');
			return false;
		}

	});

	//Selecionar todos os imóveis do painel
	$('#select_todos').click(function(){

		if( $(this).is(':checked') ){
            $('input.selecao').prop('checked', true);
	    } else {
			$('input.selecao').prop('checked', false);
	    }

	});

	//Selecionar todas as cidades
	$('.limpar_select').click(function(){

		$('.select2-selection__choice__remove').trigger('click');

	});

});