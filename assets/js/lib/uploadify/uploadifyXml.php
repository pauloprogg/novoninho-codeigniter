<?php
/*

Uploadify v3.1.0

Copyright (c) 2012 Reactive Apps, Ronnie Garcia

Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 

*/



// Define a destination



$targetFolder = '../../../../assets/uploads/xml'; // Relative to the root

if (!empty($_FILES)) {

	$tempFile = $_FILES['Filedata']['tmp_name'];

	$targetPath = $targetFolder;

	$ext = pathinfo($_FILES['Filedata']['name'],PATHINFO_EXTENSION);

	$newname = explode(".",$_FILES['Filedata']['name']);
	$newname = $newname[0];

	$fileName = $newname."-".md5(uniqid(rand(),true)).".".$ext;

	$targetFile = rtrim($targetPath,'/') . '/' . $fileName;

	$targetFilex = "/".$fileName;

	   // Validate the file type

		$fileTypes = array('jpg', 'xml', 'jpeg','gif','png', 'pdf', 'rar', 'zip', 'txt', 'JPG', 'JPEG', 'GIF', 'PNG', 'PDF', 'RAR', 'ZIP', 'TXT'); // File extensions
		$soImg = array('pdf', 'rar', 'zip', 'txt','PDF', 'RAR', 'ZIP', 'TXT');
		
		
		$fileParts = pathinfo($_FILES['Filedata']['name']);



		if (in_array($fileParts['extension'],$fileTypes)) {

			if(move_uploaded_file($tempFile,$targetFile)){

				
				if (!in_array($fileParts['extension'],$soImg)) {
				#foto

					$imgsize = getimagesize($targetFile);

					switch(strtolower($ext)){

						case "jpg":

						$image = imagecreatefromjpeg($targetFile);

						break;

						case "png":

						$image = imagecreatefrompng($targetFile);

						break;

						case "gif":

						$image = imagecreatefromgif($targetFile);

						break;

						default:

						exit;

						break;

					}

					$max_w = $_POST['w'];

					$max_h = $_POST['h'];

					$src_w = $imgsize[0];

					$src_h = $imgsize[1];

					$new_w = $src_w;

					$new_h = $src_h;

					$ratio = 1 * ($src_w / $src_h);

					$c = false;

					if($src_w > $max_w && $src_h < $max_h){

						$new_w = $max_w;

						$c = true;

					} else if($src_h > $max_h && $src_w < $max_w){

						$new_h = $max_h;

						$c = true;

					} else if($src_w > $max_w && $src_h > $max_h){

						$new_w = $max_h * $ratio;

						$new_h = $max_h;

						$c = true;

					}

			   if($ratio > 1){ //horizontal

			   	$thumb_w = $new_w;

			   	$thumb_h = $new_w / $ratio;

				}else if($ratio < 1){	//vertical

					$thumb_w = $new_h * $ratio;

					$thumb_h = $new_h;

				} else {	//quadrada

						$thumb_w = $new_h * $ratio; //$new_w;

						$thumb_h = $new_h;

					}

					$picture     = imagecreatetruecolor($thumb_w, $thumb_h);

					imagealphablending($picture, false);

					imagesavealpha($picture, true);

					$bool = false;

					if($c){

						$bool = imagecopyresampled($picture, $image, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h);

						if($bool){

							switch(strtolower($ext))	{

								case "jpg":

								header("Content-Type: image/jpeg");

								imagejpeg($picture,$targetFile,80);

								break;

								case "png":

								header("Content-Type: image/png");

								imagepng($picture,$targetFile);

								break;

								case "gif":

								header("Content-Type: image/gif");

								imagegif($picture,$targetFile);

								break;

							}

						}

					}

					imagedestroy($picture);
					imagedestroy($image);

			  # mini

					switch(strtolower($ext))	{

						case "jpg":

						$image = imagecreatefromjpeg($targetFile);

						break;

						case "png":

						$image = imagecreatefrompng($targetFile);

						break;

						case "gif":

						$image = imagecreatefromgif($targetFile);

						break;

						default:

						exit;

						break;

					}

				$src_w = imagesx($image);
				$src_h = imagesy($image);
				$ratio = 1 * ($src_w / $src_h);

			   //$new_h = $_POST['h'];

				$thumb_h= $_POST['h'];
				$thumb_w = $_POST['w'];//$new_h * $ratio;

				$picture = imagecreatetruecolor($thumb_w, $thumb_h);

				imagealphablending($picture, false);
				imagesavealpha($picture, true);

				$bool = imagecopyresampled($picture, $image, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h);

				if($bool){

					switch(strtolower($ext))	{

						case "jpg":

						header("Content-Type: image/jpeg");
						imagejpeg($picture,$targetPath.$targetFilex,80);

						break;

						case "png":

						header("Content-Type: image/png");
						imagepng($picture,$targetPath.$targetFilex);

						break;

						case "gif":

						header("Content-Type: image/gif");
						imagegif($picture,$targetPath.$targetFilex);

						break;

					}

				}

				imagedestroy($picture);
				imagedestroy($image);

				# /mini
			}

			echo $fileName;

		}

	}

}



?>

